#ifndef QEMU_THUNK_H
#define QEMU_THUNK_H

#ifdef __i386__
#define MS_STRUCT __attribute__((ms_struct))
#else
#define MS_STRUCT
#endif

typedef void(*qemu_call_t)(void*);
typedef void* qemu_host_var_t;

#define QEMU_STRING(name) #name
#define QEMU_HOST(name) qemu_host_##name
#define QEMU_HOST_REGISTER_METHOD(name) . name = &QEMU_HOST(name)
#define QEMU_GUEST_REGISTER_METHOD(name) . name = &QEMU_GUEST(name)
#define QEMU_HOST_REGISTER_VARIABLE(name) . name = & name
#define QEMU_GUEST(name) qemu_guest_##name
#ifndef NULL
#define NULL ((void*)0)
#endif

extern void* qemu_host_dlopen(const char* libname);

extern void* qemu_host_dlsym(void* handle, const char* name);

extern void qemu_host_dlclose(void* handle);

typedef void (*qemu_call_t)(void*);

typedef struct
{
    qemu_call_t NtAcceptConnectPort;
    qemu_call_t NtAccessCheck;
    qemu_call_t NtAccessCheckAndAuditAlarm;
    qemu_call_t NtAddAtom;
    qemu_call_t NtAdjustGroupsToken;
    qemu_call_t NtAdjustPrivilegesToken;
    qemu_call_t NtAlertResumeThread;
    qemu_call_t NtAlertThread;
    qemu_call_t NtAlertThreadByThreadId;
    qemu_call_t NtAllocateLocallyUniqueId;
    qemu_call_t NtAllocateUuids;
    qemu_call_t NtAllocateVirtualMemory;
    qemu_call_t NtAllocateVirtualMemoryEx;
    qemu_call_t NtAreMappedFilesTheSame;
    qemu_call_t NtAssignProcessToJobObject;
    qemu_call_t NtCancelIoFile;
    qemu_call_t NtCancelIoFileEx;
    qemu_call_t NtCancelSynchronousIoFile;
    qemu_call_t NtCancelTimer;
    qemu_call_t NtClearEvent;
    qemu_call_t NtClose;
    qemu_call_t NtCommitTransaction;
    qemu_call_t NtCompareObjects;
    qemu_call_t NtCompleteConnectPort;
    qemu_call_t NtConnectPort;
    qemu_call_t NtCreateDebugObject;
    qemu_call_t NtCreateDirectoryObject;
    qemu_call_t NtCreateEvent;
    qemu_call_t NtCreateFile;
    qemu_call_t NtCreateIoCompletion;
    qemu_call_t NtCreateJobObject;
    qemu_call_t NtCreateKey;
    qemu_call_t NtCreateKeyTransacted;
    qemu_call_t NtCreateKeyedEvent;
    qemu_call_t NtCreateLowBoxToken;
    qemu_call_t NtCreateMailslotFile;
    qemu_call_t NtCreateMutant;
    qemu_call_t NtCreateNamedPipeFile;
    qemu_call_t NtCreatePagingFile;
    qemu_call_t NtCreatePort;
    qemu_call_t NtCreateSection;
    qemu_call_t NtCreateSemaphore;
    qemu_call_t NtCreateSymbolicLinkObject;
    qemu_call_t NtCreateThreadEx;
    qemu_call_t NtCreateTimer;
    qemu_call_t NtCreateTransaction;
    qemu_call_t NtCreateUserProcess;
    qemu_call_t NtCurrentTeb;
    qemu_call_t NtDebugActiveProcess;
    qemu_call_t NtDebugContinue;
    qemu_call_t NtDelayExecution;
    qemu_call_t NtDeleteAtom;
    qemu_call_t NtDeleteFile;
    qemu_call_t NtDeleteKey;
    qemu_call_t NtDeleteValueKey;
    qemu_call_t NtDeviceIoControlFile;
    qemu_call_t NtDisplayString;
    qemu_call_t NtDuplicateObject;
    qemu_call_t NtDuplicateToken;
    qemu_call_t NtEnumerateKey;
    qemu_call_t NtEnumerateValueKey;
    qemu_call_t NtFilterToken;
    qemu_call_t NtFindAtom;
    qemu_call_t NtFlushBuffersFile;
    qemu_call_t NtFlushInstructionCache;
    qemu_call_t NtFlushKey;
    qemu_call_t NtFlushProcessWriteBuffers;
    qemu_call_t NtFlushVirtualMemory;
    qemu_call_t NtFreeVirtualMemory;
    qemu_call_t NtFsControlFile;
    qemu_call_t NtGetCurrentProcessorNumber;
    qemu_call_t NtGetNextThread;
    qemu_call_t NtGetNlsSectionPtr;
    qemu_call_t NtGetWriteWatch;
    qemu_call_t NtImpersonateAnonymousToken;
    qemu_call_t NtInitializeNlsFiles;
    qemu_call_t NtInitiatePowerAction;
    qemu_call_t NtIsProcessInJob;
    qemu_call_t NtListenPort;
    qemu_call_t NtLoadDriver;
    qemu_call_t NtLoadKey;
    qemu_call_t NtLoadKey2;
    qemu_call_t NtLoadKeyEx;
    qemu_call_t NtLockFile;
    qemu_call_t NtLockVirtualMemory;
    qemu_call_t NtMakeTemporaryObject;
    qemu_call_t NtMapViewOfSection;
    qemu_call_t NtMapViewOfSectionEx;
    qemu_call_t NtNotifyChangeDirectoryFile;
    qemu_call_t NtNotifyChangeKey;
    qemu_call_t NtNotifyChangeMultipleKeys;
    qemu_call_t NtOpenDirectoryObject;
    qemu_call_t NtOpenEvent;
    qemu_call_t NtOpenFile;
    qemu_call_t NtOpenIoCompletion;
    qemu_call_t NtOpenJobObject;
    qemu_call_t NtOpenKey;
    qemu_call_t NtOpenKeyEx;
    qemu_call_t NtOpenKeyTransacted;
    qemu_call_t NtOpenKeyTransactedEx;
    qemu_call_t NtOpenKeyedEvent;
    qemu_call_t NtOpenMutant;
    qemu_call_t NtOpenProcess;
    qemu_call_t NtOpenProcessToken;
    qemu_call_t NtOpenProcessTokenEx;
    qemu_call_t NtOpenSection;
    qemu_call_t NtOpenSemaphore;
    qemu_call_t NtOpenSymbolicLinkObject;
    qemu_call_t NtOpenThread;
    qemu_call_t NtOpenThreadToken;
    qemu_call_t NtOpenThreadTokenEx;
    qemu_call_t NtOpenTimer;
    qemu_call_t NtPowerInformation;
    qemu_call_t NtPrivilegeCheck;
    qemu_call_t NtProtectVirtualMemory;
    qemu_call_t NtPulseEvent;
    qemu_call_t NtQueryAttributesFile;
    qemu_call_t NtQueryDefaultLocale;
    qemu_call_t NtQueryDefaultUILanguage;
    qemu_call_t NtQueryDirectoryFile;
    qemu_call_t NtQueryDirectoryObject;
    qemu_call_t NtQueryEaFile;
    qemu_call_t NtQueryEvent;
    qemu_call_t NtQueryFullAttributesFile;
    qemu_call_t NtQueryInformationAtom;
    qemu_call_t NtQueryInformationFile;
    qemu_call_t NtQueryInformationJobObject;
    qemu_call_t NtQueryInformationProcess;
    qemu_call_t NtQueryInformationThread;
    qemu_call_t NtQueryInformationToken;
    qemu_call_t NtQueryInstallUILanguage;
    qemu_call_t NtQueryIoCompletion;
    qemu_call_t NtQueryKey;
    qemu_call_t NtQueryLicenseValue;
    qemu_call_t NtQueryMultipleValueKey;
    qemu_call_t NtQueryMutant;
    qemu_call_t NtQueryObject;
    qemu_call_t NtQueryPerformanceCounter;
    qemu_call_t NtQuerySection;
    qemu_call_t NtQuerySecurityObject;
    qemu_call_t NtQuerySemaphore;
    qemu_call_t NtQuerySymbolicLinkObject;
    qemu_call_t NtQuerySystemEnvironmentValue;
    qemu_call_t NtQuerySystemEnvironmentValueEx;
    qemu_call_t NtQuerySystemInformation;
    qemu_call_t NtQuerySystemInformationEx;
    qemu_call_t NtQuerySystemTime;
    qemu_call_t NtQueryTimer;
    qemu_call_t NtQueryTimerResolution;
    qemu_call_t NtQueryValueKey;
    qemu_call_t NtQueryVirtualMemory;
    qemu_call_t NtQueryVolumeInformationFile;
    qemu_call_t NtQueueApcThread;
    qemu_call_t NtRaiseHardError;
    qemu_call_t NtReadFile;
    qemu_call_t NtReadFileScatter;
    qemu_call_t NtReadVirtualMemory;
    qemu_call_t NtRegisterThreadTerminatePort;
    qemu_call_t NtReleaseKeyedEvent;
    qemu_call_t NtReleaseMutant;
    qemu_call_t NtReleaseSemaphore;
    qemu_call_t NtRemoveIoCompletion;
    qemu_call_t NtRemoveIoCompletionEx;
    qemu_call_t NtRemoveProcessDebug;
    qemu_call_t NtRenameKey;
    qemu_call_t NtReplaceKey;
    qemu_call_t NtReplyWaitReceivePort;
    qemu_call_t NtRequestWaitReplyPort;
    qemu_call_t NtResetEvent;
    qemu_call_t NtResetWriteWatch;
    qemu_call_t NtRestoreKey;
    qemu_call_t NtResumeProcess;
    qemu_call_t NtResumeThread;
    qemu_call_t NtRollbackTransaction;
    qemu_call_t NtSaveKey;
    qemu_call_t NtSecureConnectPort;
    qemu_call_t NtSetDebugFilterState;
    qemu_call_t NtSetDefaultLocale;
    qemu_call_t NtSetDefaultUILanguage;
    qemu_call_t NtSetEaFile;
    qemu_call_t NtSetEvent;
    qemu_call_t NtSetInformationDebugObject;
    qemu_call_t NtSetInformationFile;
    qemu_call_t NtSetInformationJobObject;
    qemu_call_t NtSetInformationKey;
    qemu_call_t NtSetInformationObject;
    qemu_call_t NtSetInformationProcess;
    qemu_call_t NtSetInformationThread;
    qemu_call_t NtSetInformationToken;
    qemu_call_t NtSetInformationVirtualMemory;
    qemu_call_t NtSetIntervalProfile;
    qemu_call_t NtSetIoCompletion;
    qemu_call_t NtSetSecurityObject;
    qemu_call_t NtSetSystemInformation;
    qemu_call_t NtSetSystemTime;
    qemu_call_t NtSetTimer;
    qemu_call_t NtSetTimerResolution;
    qemu_call_t NtSetValueKey;
    qemu_call_t NtSetVolumeInformationFile;
    qemu_call_t NtShutdownSystem;
    qemu_call_t NtSignalAndWaitForSingleObject;
    qemu_call_t NtSuspendProcess;
    qemu_call_t NtSuspendThread;
    qemu_call_t NtSystemDebugControl;
    qemu_call_t NtTerminateJobObject;
    qemu_call_t NtTestAlert;
    qemu_call_t NtTraceControl;
    qemu_call_t NtUnloadDriver;
    qemu_call_t NtUnloadKey;
    qemu_call_t NtUnlockFile;
    qemu_call_t NtUnlockVirtualMemory;
    qemu_call_t NtUnmapViewOfSection;
    qemu_call_t NtUnmapViewOfSectionEx;
    qemu_call_t NtWaitForAlertByThreadId;
    qemu_call_t NtWaitForDebugEvent;
    qemu_call_t NtWaitForKeyedEvent;
    qemu_call_t NtWaitForMultipleObjects;
    qemu_call_t NtWaitForSingleObject;
    qemu_call_t NtWow64AllocateVirtualMemory64;
    qemu_call_t NtWow64GetNativeSystemInformation;
    qemu_call_t NtWow64ReadVirtualMemory64;
    qemu_call_t NtWow64WriteVirtualMemory64;
    qemu_call_t NtWriteFile;
    qemu_call_t NtWriteFileGather;
    qemu_call_t NtWriteVirtualMemory;
    qemu_call_t NtYieldExecution;
    qemu_call_t __qemu_host_dispatcher;
    qemu_call_t __qemu_host_init;
    qemu_call_t __qemu_switch_host_state;
    qemu_call_t __wine_dbg_header;
    qemu_call_t __wine_dbg_output;
    qemu_call_t __wine_dbg_strdup;
    qemu_call_t __wine_unix_call;
    qemu_call_t get_thread_context;
    qemu_call_t init_thread_stack;
    qemu_call_t process_close_server_socket;
    qemu_call_t send_debug_event;
    qemu_call_t server_init_thread;
    qemu_call_t server_pipe;
    qemu_call_t server_select;
    qemu_call_t set_thread_context;
    qemu_call_t virtual_free_teb;
    qemu_call_t virtual_handle_fault;
    qemu_call_t virtual_is_valid_code_address;
    qemu_call_t virtual_setup_exception;
    qemu_call_t virtual_uninterrupted_read_memory;
    qemu_call_t virtual_uninterrupted_write_memory;
    qemu_call_t wait_suspend;
    qemu_call_t wine_nt_to_unix_file_name;
    qemu_call_t wine_server_call;
    qemu_call_t wine_server_send_fd;
    qemu_call_t wine_unix_to_nt_file_name;

    qemu_host_var_t cpu_info;
    qemu_host_var_t main_image_info;
    qemu_host_var_t nb_threads;
    qemu_host_var_t process_exiting;
    qemu_host_var_t server_block_set;
    qemu_host_var_t server_start_time;
    qemu_host_var_t wow_peb;
} qemu_host_vars_t;

typedef struct
{
    qemu_call_t NtGdiAbortDoc;
    qemu_call_t NtGdiAbortPath;
    qemu_call_t NtGdiAddFontMemResourceEx;
    qemu_call_t NtGdiAddFontResourceW;
    qemu_call_t NtGdiAlphaBlend;
    qemu_call_t NtGdiAngleArc;
    qemu_call_t NtGdiArcInternal;
    qemu_call_t NtGdiBeginPath;
    qemu_call_t NtGdiBitBlt;
    qemu_call_t NtGdiCloseFigure;
    qemu_call_t NtGdiCombineRgn;
    qemu_call_t NtGdiComputeXformCoefficients;
    qemu_call_t NtGdiCreateBitmap;
    qemu_call_t NtGdiCreateClientObj;
    qemu_call_t NtGdiCreateCompatibleBitmap;
    qemu_call_t NtGdiCreateCompatibleDC;
    qemu_call_t NtGdiCreateDIBBrush;
    qemu_call_t NtGdiCreateDIBSection;
    qemu_call_t NtGdiCreateDIBitmapInternal;
    qemu_call_t NtGdiCreateEllipticRgn;
    qemu_call_t NtGdiCreateHalftonePalette;
    qemu_call_t NtGdiCreateHatchBrushInternal;
    qemu_call_t NtGdiCreateMetafileDC;
    qemu_call_t NtGdiCreatePaletteInternal;
    qemu_call_t NtGdiCreatePatternBrushInternal;
    qemu_call_t NtGdiCreatePen;
    qemu_call_t NtGdiCreateRectRgn;
    qemu_call_t NtGdiCreateRoundRectRgn;
    qemu_call_t NtGdiCreateSolidBrush;
    qemu_call_t NtGdiDdDDICheckVidPnExclusiveOwnership;
    qemu_call_t NtGdiDdDDICloseAdapter;
    qemu_call_t NtGdiDdDDICreateDCFromMemory;
    qemu_call_t NtGdiDdDDICreateDevice;
    qemu_call_t NtGdiDdDDIDestroyDCFromMemory;
    qemu_call_t NtGdiDdDDIDestroyDevice;
    qemu_call_t NtGdiDdDDIEscape;
    qemu_call_t NtGdiDdDDIOpenAdapterFromDeviceName;
    qemu_call_t NtGdiDdDDIOpenAdapterFromHdc;
    qemu_call_t NtGdiDdDDIOpenAdapterFromLuid;
    qemu_call_t NtGdiDdDDIQueryStatistics;
    qemu_call_t NtGdiDdDDIQueryVideoMemoryInfo;
    qemu_call_t NtGdiDdDDISetQueuedLimit;
    qemu_call_t NtGdiDdDDISetVidPnSourceOwner;
    qemu_call_t NtGdiDeleteClientObj;
    qemu_call_t NtGdiDeleteObjectApp;
    qemu_call_t NtGdiDescribePixelFormat;
    qemu_call_t NtGdiDoPalette;
    qemu_call_t NtGdiDrawStream;
    qemu_call_t NtGdiEllipse;
    qemu_call_t NtGdiEndDoc;
    qemu_call_t NtGdiEndPage;
    qemu_call_t NtGdiEndPath;
    qemu_call_t NtGdiEnumFonts;
    qemu_call_t NtGdiEqualRgn;
    qemu_call_t NtGdiExcludeClipRect;
    qemu_call_t NtGdiExtCreatePen;
    qemu_call_t NtGdiExtCreateRegion;
    qemu_call_t NtGdiExtEscape;
    qemu_call_t NtGdiExtFloodFill;
    qemu_call_t NtGdiExtGetObjectW;
    qemu_call_t NtGdiExtSelectClipRgn;
    qemu_call_t NtGdiExtTextOutW;
    qemu_call_t NtGdiFillPath;
    qemu_call_t NtGdiFillRgn;
    qemu_call_t NtGdiFlattenPath;
    qemu_call_t NtGdiFlush;
    qemu_call_t NtGdiFontIsLinked;
    qemu_call_t NtGdiFrameRgn;
    qemu_call_t NtGdiGetAndSetDCDword;
    qemu_call_t NtGdiGetAppClipBox;
    qemu_call_t NtGdiGetBitmapBits;
    qemu_call_t NtGdiGetBitmapDimension;
    qemu_call_t NtGdiGetBoundsRect;
    qemu_call_t NtGdiGetCharABCWidthsW;
    qemu_call_t NtGdiGetCharWidthInfo;
    qemu_call_t NtGdiGetCharWidthW;
    qemu_call_t NtGdiGetColorAdjustment;
    qemu_call_t NtGdiGetDCDword;
    qemu_call_t NtGdiGetDCObject;
    qemu_call_t NtGdiGetDCPoint;
    qemu_call_t NtGdiGetDIBitsInternal;
    qemu_call_t NtGdiGetDeviceCaps;
    qemu_call_t NtGdiGetDeviceGammaRamp;
    qemu_call_t NtGdiGetFontData;
    qemu_call_t NtGdiGetFontFileData;
    qemu_call_t NtGdiGetFontFileInfo;
    qemu_call_t NtGdiGetFontUnicodeRanges;
    qemu_call_t NtGdiGetGlyphIndicesW;
    qemu_call_t NtGdiGetGlyphOutline;
    qemu_call_t NtGdiGetKerningPairs;
    qemu_call_t NtGdiGetNearestColor;
    qemu_call_t NtGdiGetNearestPaletteIndex;
    qemu_call_t NtGdiGetOutlineTextMetricsInternalW;
    qemu_call_t NtGdiGetPath;
    qemu_call_t NtGdiGetPixel;
    qemu_call_t NtGdiGetRandomRgn;
    qemu_call_t NtGdiGetRasterizerCaps;
    qemu_call_t NtGdiGetRealizationInfo;
    qemu_call_t NtGdiGetRegionData;
    qemu_call_t NtGdiGetRgnBox;
    qemu_call_t NtGdiGetSpoolMessage;
    qemu_call_t NtGdiGetSystemPaletteUse;
    qemu_call_t NtGdiGetTextCharsetInfo;
    qemu_call_t NtGdiGetTextExtentExW;
    qemu_call_t NtGdiGetTextFaceW;
    qemu_call_t NtGdiGetTextMetricsW;
    qemu_call_t NtGdiGetTransform;
    qemu_call_t NtGdiGradientFill;
    qemu_call_t NtGdiHfontCreate;
    qemu_call_t NtGdiIcmBrushInfo;
    qemu_call_t NtGdiInitSpool;
    qemu_call_t NtGdiIntersectClipRect;
    qemu_call_t NtGdiInvertRgn;
    qemu_call_t NtGdiLineTo;
    qemu_call_t NtGdiMaskBlt;
    qemu_call_t NtGdiModifyWorldTransform;
    qemu_call_t NtGdiMoveTo;
    qemu_call_t NtGdiOffsetClipRgn;
    qemu_call_t NtGdiOffsetRgn;
    qemu_call_t NtGdiOpenDCW;
    qemu_call_t NtGdiPatBlt;
    qemu_call_t NtGdiPathToRegion;
    qemu_call_t NtGdiPlgBlt;
    qemu_call_t NtGdiPolyDraw;
    qemu_call_t NtGdiPolyPolyDraw;
    qemu_call_t NtGdiPtInRegion;
    qemu_call_t NtGdiPtVisible;
    qemu_call_t NtGdiRectInRegion;
    qemu_call_t NtGdiRectVisible;
    qemu_call_t NtGdiRectangle;
    qemu_call_t NtGdiRemoveFontMemResourceEx;
    qemu_call_t NtGdiRemoveFontResourceW;
    qemu_call_t NtGdiResetDC;
    qemu_call_t NtGdiResizePalette;
    qemu_call_t NtGdiRestoreDC;
    qemu_call_t NtGdiRoundRect;
    qemu_call_t NtGdiSaveDC;
    qemu_call_t NtGdiScaleViewportExtEx;
    qemu_call_t NtGdiScaleWindowExtEx;
    qemu_call_t NtGdiSelectBitmap;
    qemu_call_t NtGdiSelectBrush;
    qemu_call_t NtGdiSelectClipPath;
    qemu_call_t NtGdiSelectFont;
    qemu_call_t NtGdiSelectPen;
    qemu_call_t NtGdiSetBitmapBits;
    qemu_call_t NtGdiSetBitmapDimension;
    qemu_call_t NtGdiSetBoundsRect;
    qemu_call_t NtGdiSetBrushOrg;
    qemu_call_t NtGdiSetColorAdjustment;
    qemu_call_t NtGdiSetDIBitsToDeviceInternal;
    qemu_call_t NtGdiSetDeviceGammaRamp;
    qemu_call_t NtGdiSetLayout;
    qemu_call_t NtGdiSetMagicColors;
    qemu_call_t NtGdiSetMetaRgn;
    qemu_call_t NtGdiSetPixel;
    qemu_call_t NtGdiSetPixelFormat;
    qemu_call_t NtGdiSetRectRgn;
    qemu_call_t NtGdiSetSystemPaletteUse;
    qemu_call_t NtGdiSetTextJustification;
    qemu_call_t NtGdiSetVirtualResolution;
    qemu_call_t NtGdiStartDoc;
    qemu_call_t NtGdiStartPage;
    qemu_call_t NtGdiStretchBlt;
    qemu_call_t NtGdiStretchDIBitsInternal;
    qemu_call_t NtGdiStrokeAndFillPath;
    qemu_call_t NtGdiStrokePath;
    qemu_call_t NtGdiSwapBuffers;
    qemu_call_t NtGdiTransformPoints;
    qemu_call_t NtGdiTransparentBlt;
    qemu_call_t NtGdiUnrealizeObject;
    qemu_call_t NtGdiUpdateColors;
    qemu_call_t NtGdiWidenPath;
    qemu_call_t NtUserActivateKeyboardLayout;
    qemu_call_t NtUserAddClipboardFormatListener;
    qemu_call_t NtUserAssociateInputContext;
    qemu_call_t NtUserAttachThreadInput;
    qemu_call_t NtUserBeginPaint;
    qemu_call_t NtUserBuildHimcList;
    qemu_call_t NtUserBuildHwndList;
    qemu_call_t NtUserCallHwnd;
    qemu_call_t NtUserCallHwndParam;
    qemu_call_t NtUserCallMsgFilter;
    qemu_call_t NtUserCallNextHookEx;
    qemu_call_t NtUserCallNoParam;
    qemu_call_t NtUserCallOneParam;
    qemu_call_t NtUserCallTwoParam;
    qemu_call_t NtUserChangeClipboardChain;
    qemu_call_t NtUserChangeDisplaySettings;
    qemu_call_t NtUserCheckMenuItem;
    qemu_call_t NtUserChildWindowFromPointEx;
    qemu_call_t NtUserClipCursor;
    qemu_call_t NtUserCloseClipboard;
    qemu_call_t NtUserCloseDesktop;
    qemu_call_t NtUserCloseWindowStation;
    qemu_call_t NtUserCopyAcceleratorTable;
    qemu_call_t NtUserCountClipboardFormats;
    qemu_call_t NtUserCreateAcceleratorTable;
    qemu_call_t NtUserCreateCaret;
    qemu_call_t NtUserCreateDesktopEx;
    qemu_call_t NtUserCreateInputContext;
    qemu_call_t NtUserCreateWindowEx;
    qemu_call_t NtUserCreateWindowStation;
    qemu_call_t NtUserDeferWindowPosAndBand;
    qemu_call_t NtUserDeleteMenu;
    qemu_call_t NtUserDestroyAcceleratorTable;
    qemu_call_t NtUserDestroyCursor;
    qemu_call_t NtUserDestroyInputContext;
    qemu_call_t NtUserDestroyMenu;
    qemu_call_t NtUserDestroyWindow;
    qemu_call_t NtUserDisableThreadIme;
    qemu_call_t NtUserDispatchMessage;
    qemu_call_t NtUserDisplayConfigGetDeviceInfo;
    qemu_call_t NtUserDragDetect;
    qemu_call_t NtUserDragObject;
    qemu_call_t NtUserDrawCaptionTemp;
    qemu_call_t NtUserDrawIconEx;
    qemu_call_t NtUserDrawMenuBarTemp;
    qemu_call_t NtUserEmptyClipboard;
    qemu_call_t NtUserEnableMenuItem;
    qemu_call_t NtUserEnableMouseInPointer;
    qemu_call_t NtUserEnableScrollBar;
    qemu_call_t NtUserEndDeferWindowPosEx;
    qemu_call_t NtUserEndMenu;
    qemu_call_t NtUserEndPaint;
    qemu_call_t NtUserEnumDisplayDevices;
    qemu_call_t NtUserEnumDisplayMonitors;
    qemu_call_t NtUserEnumDisplaySettings;
    qemu_call_t NtUserExcludeUpdateRgn;
    qemu_call_t NtUserFindExistingCursorIcon;
    qemu_call_t NtUserFindWindowEx;
    qemu_call_t NtUserFlashWindowEx;
    qemu_call_t NtUserGetAncestor;
    qemu_call_t NtUserGetAsyncKeyState;
    qemu_call_t NtUserGetAtomName;
    qemu_call_t NtUserGetCaretBlinkTime;
    qemu_call_t NtUserGetCaretPos;
    qemu_call_t NtUserGetClassInfoEx;
    qemu_call_t NtUserGetClassName;
    qemu_call_t NtUserGetClipboardData;
    qemu_call_t NtUserGetClipboardFormatName;
    qemu_call_t NtUserGetClipboardOwner;
    qemu_call_t NtUserGetClipboardSequenceNumber;
    qemu_call_t NtUserGetClipboardViewer;
    qemu_call_t NtUserGetCursor;
    qemu_call_t NtUserGetCursorFrameInfo;
    qemu_call_t NtUserGetCursorInfo;
    qemu_call_t NtUserGetDC;
    qemu_call_t NtUserGetDCEx;
    qemu_call_t NtUserGetDisplayConfigBufferSizes;
    qemu_call_t NtUserGetDoubleClickTime;
    qemu_call_t NtUserGetDpiForMonitor;
    qemu_call_t NtUserGetForegroundWindow;
    qemu_call_t NtUserGetGUIThreadInfo;
    qemu_call_t NtUserGetIconInfo;
    qemu_call_t NtUserGetIconSize;
    qemu_call_t NtUserGetInternalWindowPos;
    qemu_call_t NtUserGetKeyNameText;
    qemu_call_t NtUserGetKeyState;
    qemu_call_t NtUserGetKeyboardLayout;
    qemu_call_t NtUserGetKeyboardLayoutList;
    qemu_call_t NtUserGetKeyboardLayoutName;
    qemu_call_t NtUserGetKeyboardState;
    qemu_call_t NtUserGetLayeredWindowAttributes;
    qemu_call_t NtUserGetMenuBarInfo;
    qemu_call_t NtUserGetMenuItemRect;
    qemu_call_t NtUserGetMessage;
    qemu_call_t NtUserGetMouseMovePointsEx;
    qemu_call_t NtUserGetObjectInformation;
    qemu_call_t NtUserGetOpenClipboardWindow;
    qemu_call_t NtUserGetPointerInfoList;
    qemu_call_t NtUserGetPriorityClipboardFormat;
    qemu_call_t NtUserGetProcessDpiAwarenessContext;
    qemu_call_t NtUserGetProcessWindowStation;
    qemu_call_t NtUserGetProp;
    qemu_call_t NtUserGetQueueStatus;
    qemu_call_t NtUserGetRawInputBuffer;
    qemu_call_t NtUserGetRawInputData;
    qemu_call_t NtUserGetRawInputDeviceInfo;
    qemu_call_t NtUserGetRawInputDeviceList;
    qemu_call_t NtUserGetRegisteredRawInputDevices;
    qemu_call_t NtUserGetScrollBarInfo;
    qemu_call_t NtUserGetSystemDpiForProcess;
    qemu_call_t NtUserGetSystemMenu;
    qemu_call_t NtUserGetThreadDesktop;
    qemu_call_t NtUserGetTitleBarInfo;
    qemu_call_t NtUserGetUpdateRect;
    qemu_call_t NtUserGetUpdateRgn;
    qemu_call_t NtUserGetUpdatedClipboardFormats;
    qemu_call_t NtUserGetWindowDC;
    qemu_call_t NtUserGetWindowPlacement;
    qemu_call_t NtUserGetWindowRgnEx;
    qemu_call_t NtUserHideCaret;
    qemu_call_t NtUserHiliteMenuItem;
    qemu_call_t NtUserInitializeClientPfnArrays;
    qemu_call_t NtUserInternalGetWindowIcon;
    qemu_call_t NtUserInternalGetWindowText;
    qemu_call_t NtUserInvalidateRect;
    qemu_call_t NtUserInvalidateRgn;
    qemu_call_t NtUserIsClipboardFormatAvailable;
    qemu_call_t NtUserIsMouseInPointerEnabled;
    qemu_call_t NtUserKillTimer;
    qemu_call_t NtUserLockWindowUpdate;
    qemu_call_t NtUserLogicalToPerMonitorDPIPhysicalPoint;
    qemu_call_t NtUserMapVirtualKeyEx;
    qemu_call_t NtUserMenuItemFromPoint;
    qemu_call_t NtUserMessageCall;
    qemu_call_t NtUserMoveWindow;
    qemu_call_t NtUserMsgWaitForMultipleObjectsEx;
    qemu_call_t NtUserNotifyIMEStatus;
    qemu_call_t NtUserNotifyWinEvent;
    qemu_call_t NtUserOpenClipboard;
    qemu_call_t NtUserOpenDesktop;
    qemu_call_t NtUserOpenInputDesktop;
    qemu_call_t NtUserOpenWindowStation;
    qemu_call_t NtUserPeekMessage;
    qemu_call_t NtUserPerMonitorDPIPhysicalToLogicalPoint;
    qemu_call_t NtUserPostMessage;
    qemu_call_t NtUserPostThreadMessage;
    qemu_call_t NtUserPrintWindow;
    qemu_call_t NtUserQueryDisplayConfig;
    qemu_call_t NtUserQueryInputContext;
    qemu_call_t NtUserRealChildWindowFromPoint;
    qemu_call_t NtUserRedrawWindow;
    qemu_call_t NtUserRegisterClassExWOW;
    qemu_call_t NtUserRegisterHotKey;
    qemu_call_t NtUserRegisterRawInputDevices;
    qemu_call_t NtUserReleaseDC;
    qemu_call_t NtUserRemoveClipboardFormatListener;
    qemu_call_t NtUserRemoveMenu;
    qemu_call_t NtUserRemoveProp;
    qemu_call_t NtUserScrollDC;
    qemu_call_t NtUserScrollWindowEx;
    qemu_call_t NtUserSelectPalette;
    qemu_call_t NtUserSendInput;
    qemu_call_t NtUserSetActiveWindow;
    qemu_call_t NtUserSetCapture;
    qemu_call_t NtUserSetClassLong;
    qemu_call_t NtUserSetClassLongPtr;
    qemu_call_t NtUserSetClassWord;
    qemu_call_t NtUserSetClipboardData;
    qemu_call_t NtUserSetClipboardViewer;
    qemu_call_t NtUserSetCursor;
    qemu_call_t NtUserSetCursorIconData;
    qemu_call_t NtUserSetCursorPos;
    qemu_call_t NtUserSetFocus;
    qemu_call_t NtUserSetInternalWindowPos;
    qemu_call_t NtUserSetKeyboardState;
    qemu_call_t NtUserSetLayeredWindowAttributes;
    qemu_call_t NtUserSetMenu;
    qemu_call_t NtUserSetMenuContextHelpId;
    qemu_call_t NtUserSetMenuDefaultItem;
    qemu_call_t NtUserSetObjectInformation;
    qemu_call_t NtUserSetParent;
    qemu_call_t NtUserSetProcessDpiAwarenessContext;
    qemu_call_t NtUserSetProcessWindowStation;
    qemu_call_t NtUserSetProp;
    qemu_call_t NtUserSetScrollInfo;
    qemu_call_t NtUserSetShellWindowEx;
    qemu_call_t NtUserSetSysColors;
    qemu_call_t NtUserSetSystemMenu;
    qemu_call_t NtUserSetSystemTimer;
    qemu_call_t NtUserSetThreadDesktop;
    qemu_call_t NtUserSetTimer;
    qemu_call_t NtUserSetWinEventHook;
    qemu_call_t NtUserSetWindowLong;
    qemu_call_t NtUserSetWindowLongPtr;
    qemu_call_t NtUserSetWindowPlacement;
    qemu_call_t NtUserSetWindowPos;
    qemu_call_t NtUserSetWindowRgn;
    qemu_call_t NtUserSetWindowWord;
    qemu_call_t NtUserSetWindowsHookEx;
    qemu_call_t NtUserShowCaret;
    qemu_call_t NtUserShowCursor;
    qemu_call_t NtUserShowScrollBar;
    qemu_call_t NtUserShowWindow;
    qemu_call_t NtUserShowWindowAsync;
    qemu_call_t NtUserSystemParametersInfo;
    qemu_call_t NtUserSystemParametersInfoForDpi;
    qemu_call_t NtUserThunkedMenuInfo;
    qemu_call_t NtUserThunkedMenuItemInfo;
    qemu_call_t NtUserToUnicodeEx;
    qemu_call_t NtUserTrackMouseEvent;
    qemu_call_t NtUserTrackPopupMenuEx;
    qemu_call_t NtUserTranslateAccelerator;
    qemu_call_t NtUserTranslateMessage;
    qemu_call_t NtUserUnhookWinEvent;
    qemu_call_t NtUserUnhookWindowsHookEx;
    qemu_call_t NtUserUnregisterClass;
    qemu_call_t NtUserUnregisterHotKey;
    qemu_call_t NtUserUpdateInputContext;
    qemu_call_t NtUserUpdateLayeredWindow;
    qemu_call_t NtUserValidateRect;
    qemu_call_t NtUserVkKeyScanEx;
    qemu_call_t NtUserWaitForInputIdle;
    qemu_call_t NtUserWaitMessage;
    qemu_call_t NtUserWindowFromDC;
    qemu_call_t NtUserWindowFromPoint;
    qemu_call_t __wine_get_file_outline_text_metric;
    qemu_call_t __wine_get_icm_profile;
    qemu_call_t __wine_send_input;
} qemu_host_win32u_vars_t;

typedef struct
{
    qemu_call_t KeUserModeCallback;
    qemu_call_t create_thread;
    qemu_call_t abort_thread;
    qemu_call_t call_user_apc_dispatcher;
    qemu_call_t signal_notify;
} qemu_guest_vars_t;

#define QEMU_HOST_DISPATCHER_INIT 0x00000000
#define QEMU_HOST_DISPATCHER_CONFIG 0x00000001
#define QEMU_HOST_DISPATCHER_START 0x00000002

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "winternl.h"
#include "winbase.h"
#include "rpc.h"
#include "sspi.h"
#include "ntsecapi.h"
#include "ntsecpkg.h"
#include "ntgdi.h"
#include "ntuser.h"
#include "wine/debug.h"
#include "wine/server_protocol.h"
#include "wine/unixlib.h"

struct MS_STRUCT NtAcceptConnectPort_params
{
    void *$handle;
    PHANDLE $0;
    ULONG $1;
    PLPC_MESSAGE $2;
    BOOLEAN $3;
    PLPC_SECTION_WRITE $4;
    PLPC_SECTION_READ $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAccessCheck_params
{
    void *$handle;
    PSECURITY_DESCRIPTOR $0;
    HANDLE $1;
    ACCESS_MASK $2;
    PGENERIC_MAPPING $3;
    PPRIVILEGE_SET $4;
    PULONG $5;
    PULONG $6;
    NTSTATUS *$7;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAccessCheckAndAuditAlarm_params
{
    void *$handle;
    PUNICODE_STRING $0;
    HANDLE $1;
    PUNICODE_STRING $2;
    PUNICODE_STRING $3;
    PSECURITY_DESCRIPTOR $4;
    ACCESS_MASK $5;
    PGENERIC_MAPPING $6;
    BOOLEAN $7;
    PACCESS_MASK $8;
    PBOOLEAN $9;
    PBOOLEAN $10;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAddAtom_params
{
    void *$handle;
    const WCHAR *$0;
    ULONG $1;
    RTL_ATOM *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAdjustGroupsToken_params
{
    void *$handle;
    HANDLE $0;
    BOOLEAN $1;
    PTOKEN_GROUPS $2;
    ULONG $3;
    PTOKEN_GROUPS $4;
    PULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAdjustPrivilegesToken_params
{
    void *$handle;
    HANDLE $0;
    BOOLEAN $1;
    PTOKEN_PRIVILEGES $2;
    DWORD $3;
    PTOKEN_PRIVILEGES $4;
    PDWORD $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAlertResumeThread_params
{
    void *$handle;
    HANDLE $0;
    PULONG $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAlertThread_params
{
    void *$handle;
    HANDLE ThreadHandle;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAlertThreadByThreadId_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAllocateLocallyUniqueId_params
{
    void *$handle;
    PLUID lpLuid;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAllocateUuids_params
{
    void *$handle;
    PULARGE_INTEGER $0;
    PULONG $1;
    PULONG $2;
    PUCHAR $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAllocateVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    PVOID *$1;
    ULONG_PTR $2;
    SIZE_T *$3;
    ULONG $4;
    ULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAllocateVirtualMemoryEx_params
{
    void *$handle;
    HANDLE $0;
    PVOID *$1;
    SIZE_T *$2;
    ULONG $3;
    ULONG $4;
    MEM_EXTENDED_PARAMETER *$5;
    ULONG $6;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAreMappedFilesTheSame_params
{
    void *$handle;
    PVOID $0;
    PVOID $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtAssignProcessToJobObject_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCancelIoFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCancelIoFileEx_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PIO_STATUS_BLOCK $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCancelSynchronousIoFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PIO_STATUS_BLOCK $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCancelTimer_params
{
    void *$handle;
    HANDLE $0;
    BOOLEAN *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtClearEvent_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtClose_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCommitTransaction_params
{
    void *$handle;
    HANDLE $0;
    BOOLEAN $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCompareObjects_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCompleteConnectPort_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtConnectPort_params
{
    void *$handle;
    PHANDLE $0;
    PUNICODE_STRING $1;
    PSECURITY_QUALITY_OF_SERVICE $2;
    PLPC_SECTION_WRITE $3;
    PLPC_SECTION_READ $4;
    PULONG $5;
    PVOID $6;
    PULONG $7;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateDebugObject_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    OBJECT_ATTRIBUTES *$2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateDirectoryObject_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateEvent_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    EVENT_TYPE $3;
    BOOLEAN $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateFile_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    PIO_STATUS_BLOCK $3;
    PLARGE_INTEGER $4;
    ULONG $5;
    ULONG $6;
    ULONG $7;
    ULONG $8;
    PVOID $9;
    ULONG $10;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateIoCompletion_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateJobObject_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateKey_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    ULONG $3;
    const UNICODE_STRING *$4;
    ULONG $5;
    PULONG $6;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateKeyTransacted_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    ULONG $3;
    const UNICODE_STRING *$4;
    ULONG $5;
    HANDLE $6;
    ULONG *$7;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateKeyedEvent_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateLowBoxToken_params
{
    void *$handle;
    HANDLE *$0;
    HANDLE $1;
    ACCESS_MASK $2;
    OBJECT_ATTRIBUTES *$3;
    SID *$4;
    ULONG $5;
    SID_AND_ATTRIBUTES *$6;
    ULONG $7;
    HANDLE *$8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateMailslotFile_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    PIO_STATUS_BLOCK $3;
    ULONG $4;
    ULONG $5;
    ULONG $6;
    PLARGE_INTEGER $7;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateMutant_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    BOOLEAN $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateNamedPipeFile_params
{
    void *$handle;
    PHANDLE $0;
    ULONG $1;
    POBJECT_ATTRIBUTES $2;
    PIO_STATUS_BLOCK $3;
    ULONG $4;
    ULONG $5;
    ULONG $6;
    ULONG $7;
    ULONG $8;
    ULONG $9;
    ULONG $10;
    ULONG $11;
    ULONG $12;
    PLARGE_INTEGER $13;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreatePagingFile_params
{
    void *$handle;
    PUNICODE_STRING $0;
    PLARGE_INTEGER $1;
    PLARGE_INTEGER $2;
    PLARGE_INTEGER $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreatePort_params
{
    void *$handle;
    PHANDLE $0;
    POBJECT_ATTRIBUTES $1;
    ULONG $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateSection_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    const LARGE_INTEGER *$3;
    ULONG $4;
    ULONG $5;
    HANDLE $6;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateSemaphore_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    LONG $3;
    LONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateSymbolicLinkObject_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    PUNICODE_STRING $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateThreadEx_params
{
    void *$handle;
    HANDLE *handle; 
    ACCESS_MASK access; 
    OBJECT_ATTRIBUTES *attr; 
    HANDLE process; 
    PRTL_THREAD_START_ROUTINE start; 
    void *param; 
    ULONG flags; 
    ULONG_PTR zero_bits; 
    SIZE_T stack_commit; 
    SIZE_T stack_reserve; 
    PS_ATTRIBUTE_LIST *attr_list;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateTimer_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    TIMER_TYPE $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateTransaction_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    LPGUID $3;
    HANDLE $4;
    ULONG $5;
    ULONG $6;
    ULONG $7;
    PLARGE_INTEGER $8;
    PUNICODE_STRING $9;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCreateUserProcess_params
{
    void *$handle;
    HANDLE *$0;
    HANDLE *$1;
    ACCESS_MASK $2;
    ACCESS_MASK $3;
    OBJECT_ATTRIBUTES *$4;
    OBJECT_ATTRIBUTES *$5;
    ULONG $6;
    ULONG $7;
    RTL_USER_PROCESS_PARAMETERS *$8;
    PS_CREATE_INFO *$9;
    PS_ATTRIBUTE_LIST *$10;
    NTSTATUS $ret;
};

struct MS_STRUCT NtCurrentTeb_params
{
    void *$handle;
    TEB *$ret;
};

struct MS_STRUCT NtDebugActiveProcess_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDebugContinue_params
{
    void *$handle;
    HANDLE $0;
    CLIENT_ID *$1;
    NTSTATUS $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDelayExecution_params
{
    void *$handle;
    BOOLEAN $0;
    const LARGE_INTEGER *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDeleteAtom_params
{
    void *$handle;
    RTL_ATOM $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDeleteFile_params
{
    void *$handle;
    POBJECT_ATTRIBUTES $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDeleteKey_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDeleteValueKey_params
{
    void *$handle;
    HANDLE $0;
    const UNICODE_STRING *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDeviceIoControlFile_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    ULONG $5;
    PVOID $6;
    ULONG $7;
    PVOID $8;
    ULONG $9;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDisplayString_params
{
    void *$handle;
    PUNICODE_STRING $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDuplicateObject_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    HANDLE $2;
    PHANDLE $3;
    ACCESS_MASK $4;
    ULONG $5;
    ULONG $6;
    NTSTATUS $ret;
};

struct MS_STRUCT NtDuplicateToken_params
{
    void *$handle;
    HANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    BOOLEAN $3;
    TOKEN_TYPE $4;
    PHANDLE $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtEnumerateKey_params
{
    void *$handle;
    HANDLE $0;
    ULONG $1;
    KEY_INFORMATION_CLASS $2;
    void *$3;
    DWORD $4;
    DWORD *$5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtEnumerateValueKey_params
{
    void *$handle;
    HANDLE $0;
    ULONG $1;
    KEY_VALUE_INFORMATION_CLASS $2;
    PVOID $3;
    ULONG $4;
    PULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFilterToken_params
{
    void *$handle;
    HANDLE $0;
    ULONG $1;
    TOKEN_GROUPS *$2;
    TOKEN_PRIVILEGES *$3;
    TOKEN_GROUPS *$4;
    HANDLE *$5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFindAtom_params
{
    void *$handle;
    const WCHAR *$0;
    ULONG $1;
    RTL_ATOM *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFlushBuffersFile_params
{
    void *$handle;
    HANDLE $0;
    IO_STATUS_BLOCK *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFlushInstructionCache_params
{
    void *$handle;
    HANDLE $0;
    LPCVOID $1;
    SIZE_T $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFlushKey_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFlushProcessWriteBuffers_params
{
    void *$handle;
};

struct MS_STRUCT NtFlushVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    LPCVOID *$1;
    SIZE_T *$2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFreeVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    PVOID *$1;
    SIZE_T *$2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtFsControlFile_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    ULONG $5;
    PVOID $6;
    ULONG $7;
    PVOID $8;
    ULONG $9;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGetCurrentProcessorNumber_params
{
    void *$handle;
    ULONG $ret;
};

struct MS_STRUCT NtGetNextThread_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    ACCESS_MASK $2;
    ULONG $3;
    ULONG $4;
    HANDLE *$5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGetNlsSectionPtr_params
{
    void *$handle;
    ULONG $0;
    ULONG $1;
    void *$2;
    void **$3;
    SIZE_T *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGetWriteWatch_params
{
    void *$handle;
    HANDLE $0;
    ULONG $1;
    PVOID $2;
    SIZE_T $3;
    PVOID *$4;
    ULONG_PTR *$5;
    ULONG *$6;
    NTSTATUS $ret;
};

struct MS_STRUCT NtImpersonateAnonymousToken_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtInitializeNlsFiles_params
{
    void *$handle;
    void **$0;
    LCID *$1;
    LARGE_INTEGER *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtInitiatePowerAction_params
{
    void *$handle;
    POWER_ACTION $0;
    SYSTEM_POWER_STATE $1;
    ULONG $2;
    BOOLEAN $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtIsProcessInJob_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtListenPort_params
{
    void *$handle;
    HANDLE $0;
    PLPC_MESSAGE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtLoadDriver_params
{
    void *$handle;
    const UNICODE_STRING *$0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtLoadKey_params
{
    void *$handle;
    const OBJECT_ATTRIBUTES *$0;
    OBJECT_ATTRIBUTES *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtLoadKey2_params
{
    void *$handle;
    const OBJECT_ATTRIBUTES *$0;
    OBJECT_ATTRIBUTES *$1;
    ULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtLoadKeyEx_params
{
    void *$handle;
    const OBJECT_ATTRIBUTES *$0;
    OBJECT_ATTRIBUTES *$1;
    ULONG $2;
    HANDLE $3;
    HANDLE $4;
    ACCESS_MASK $5;
    HANDLE *$6;
    IO_STATUS_BLOCK *$7;
    NTSTATUS $ret;
};

struct MS_STRUCT NtLockFile_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    void *$3;
    PIO_STATUS_BLOCK $4;
    PLARGE_INTEGER $5;
    PLARGE_INTEGER $6;
    ULONG *$7;
    BOOLEAN $8;
    BOOLEAN $9;
    NTSTATUS $ret;
};

struct MS_STRUCT NtLockVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    PVOID *$1;
    SIZE_T *$2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtMakeTemporaryObject_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtMapViewOfSection_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PVOID *$2;
    ULONG_PTR $3;
    SIZE_T $4;
    const LARGE_INTEGER *$5;
    SIZE_T *$6;
    SECTION_INHERIT $7;
    ULONG $8;
    ULONG $9;
    NTSTATUS $ret;
};

struct MS_STRUCT NtMapViewOfSectionEx_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PVOID *$2;
    const LARGE_INTEGER *$3;
    SIZE_T *$4;
    ULONG $5;
    ULONG $6;
    MEM_EXTENDED_PARAMETER *$7;
    ULONG $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtNotifyChangeDirectoryFile_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    PVOID $5;
    ULONG $6;
    ULONG $7;
    BOOLEAN $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtNotifyChangeKey_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    ULONG $5;
    BOOLEAN $6;
    PVOID $7;
    ULONG $8;
    BOOLEAN $9;
    NTSTATUS $ret;
};

struct MS_STRUCT NtNotifyChangeMultipleKeys_params
{
    void *$handle;
    HANDLE $0;
    ULONG $1;
    OBJECT_ATTRIBUTES *$2;
    HANDLE $3;
    PIO_APC_ROUTINE $4;
    PVOID $5;
    PIO_STATUS_BLOCK $6;
    ULONG $7;
    BOOLEAN $8;
    PVOID $9;
    ULONG $10;
    BOOLEAN $11;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenDirectoryObject_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenEvent_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenFile_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    POBJECT_ATTRIBUTES $2;
    PIO_STATUS_BLOCK $3;
    ULONG $4;
    ULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenIoCompletion_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenJobObject_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenKey_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenKeyEx_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenKeyTransacted_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    HANDLE $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenKeyTransactedEx_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    ULONG $3;
    HANDLE $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenKeyedEvent_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenMutant_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenProcess_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    const CLIENT_ID *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenProcessToken_params
{
    void *$handle;
    HANDLE $0;
    DWORD $1;
    HANDLE *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenProcessTokenEx_params
{
    void *$handle;
    HANDLE $0;
    DWORD $1;
    DWORD $2;
    HANDLE *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenSection_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenSemaphore_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenSymbolicLinkObject_params
{
    void *$handle;
    PHANDLE $0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenThread_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    const CLIENT_ID *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenThreadToken_params
{
    void *$handle;
    HANDLE $0;
    DWORD $1;
    BOOLEAN $2;
    HANDLE *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenThreadTokenEx_params
{
    void *$handle;
    HANDLE $0;
    DWORD $1;
    BOOLEAN $2;
    DWORD $3;
    HANDLE *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtOpenTimer_params
{
    void *$handle;
    HANDLE *$0;
    ACCESS_MASK $1;
    const OBJECT_ATTRIBUTES *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtPowerInformation_params
{
    void *$handle;
    POWER_INFORMATION_LEVEL $0;
    PVOID $1;
    ULONG $2;
    PVOID $3;
    ULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtPrivilegeCheck_params
{
    void *$handle;
    HANDLE $0;
    PPRIVILEGE_SET $1;
    PBOOLEAN $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtProtectVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    PVOID *$1;
    SIZE_T *$2;
    ULONG $3;
    ULONG *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtPulseEvent_params
{
    void *$handle;
    HANDLE $0;
    LONG *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryAttributesFile_params
{
    void *$handle;
    const OBJECT_ATTRIBUTES *$0;
    FILE_BASIC_INFORMATION *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryDefaultLocale_params
{
    void *$handle;
    BOOLEAN $0;
    LCID *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryDefaultUILanguage_params
{
    void *$handle;
    LANGID *$0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryDirectoryFile_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    PVOID $5;
    ULONG $6;
    FILE_INFORMATION_CLASS $7;
    BOOLEAN $8;
    PUNICODE_STRING $9;
    BOOLEAN $10;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryDirectoryObject_params
{
    void *$handle;
    HANDLE $0;
    PDIRECTORY_BASIC_INFORMATION $1;
    ULONG $2;
    BOOLEAN $3;
    BOOLEAN $4;
    PULONG $5;
    PULONG $6;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryEaFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PVOID $2;
    ULONG $3;
    BOOLEAN $4;
    PVOID $5;
    ULONG $6;
    PULONG $7;
    BOOLEAN $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryEvent_params
{
    void *$handle;
    HANDLE $0;
    EVENT_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryFullAttributesFile_params
{
    void *$handle;
    const OBJECT_ATTRIBUTES *$0;
    FILE_NETWORK_OPEN_INFORMATION *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryInformationAtom_params
{
    void *$handle;
    RTL_ATOM $0;
    ATOM_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    ULONG *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryInformationFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PVOID $2;
    ULONG $3;
    FILE_INFORMATION_CLASS $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryInformationJobObject_params
{
    void *$handle;
    HANDLE $0;
    JOBOBJECTINFOCLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryInformationProcess_params
{
    void *$handle;
    HANDLE $0;
    PROCESSINFOCLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryInformationThread_params
{
    void *$handle;
    HANDLE $0;
    THREADINFOCLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryInformationToken_params
{
    void *$handle;
    HANDLE $0;
    TOKEN_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryInstallUILanguage_params
{
    void *$handle;
    LANGID *$0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryIoCompletion_params
{
    void *$handle;
    HANDLE $0;
    IO_COMPLETION_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryKey_params
{
    void *$handle;
    HANDLE $0;
    KEY_INFORMATION_CLASS $1;
    void *$2;
    DWORD $3;
    DWORD *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryLicenseValue_params
{
    void *$handle;
    const UNICODE_STRING *$0;
    ULONG *$1;
    PVOID $2;
    ULONG $3;
    ULONG *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryMultipleValueKey_params
{
    void *$handle;
    HANDLE $0;
    PKEY_MULTIPLE_VALUE_INFORMATION $1;
    ULONG $2;
    PVOID $3;
    ULONG $4;
    PULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryMutant_params
{
    void *$handle;
    HANDLE $0;
    MUTANT_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryObject_params
{
    void *$handle;
    HANDLE $0;
    OBJECT_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryPerformanceCounter_params
{
    void *$handle;
    PLARGE_INTEGER $0;
    PLARGE_INTEGER $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySection_params
{
    void *$handle;
    HANDLE $0;
    SECTION_INFORMATION_CLASS $1;
    PVOID $2;
    SIZE_T $3;
    SIZE_T *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySecurityObject_params
{
    void *$handle;
    HANDLE $0;
    SECURITY_INFORMATION $1;
    PSECURITY_DESCRIPTOR $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySemaphore_params
{
    void *$handle;
    HANDLE $0;
    SEMAPHORE_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySymbolicLinkObject_params
{
    void *$handle;
    HANDLE $0;
    PUNICODE_STRING $1;
    PULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySystemEnvironmentValue_params
{
    void *$handle;
    PUNICODE_STRING $0;
    PWCHAR $1;
    ULONG $2;
    PULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySystemEnvironmentValueEx_params
{
    void *$handle;
    PUNICODE_STRING $0;
    GUID *$1;
    void *$2;
    ULONG *$3;
    ULONG *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySystemInformation_params
{
    void *$handle;
    SYSTEM_INFORMATION_CLASS $0;
    PVOID $1;
    ULONG $2;
    PULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySystemInformationEx_params
{
    void *$handle;
    SYSTEM_INFORMATION_CLASS $0;
    void *$1;
    ULONG $2;
    void *$3;
    ULONG $4;
    ULONG *$5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQuerySystemTime_params
{
    void *$handle;
    PLARGE_INTEGER $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryTimer_params
{
    void *$handle;
    HANDLE $0;
    TIMER_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryTimerResolution_params
{
    void *$handle;
    PULONG $0;
    PULONG $1;
    PULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryValueKey_params
{
    void *$handle;
    HANDLE $0;
    const UNICODE_STRING *$1;
    KEY_VALUE_INFORMATION_CLASS $2;
    void *$3;
    DWORD $4;
    DWORD *$5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    LPCVOID $1;
    MEMORY_INFORMATION_CLASS $2;
    PVOID $3;
    SIZE_T $4;
    SIZE_T *$5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueryVolumeInformationFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PVOID $2;
    ULONG $3;
    FS_INFORMATION_CLASS $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtQueueApcThread_params
{
    void *$handle;
    HANDLE $0;
    PNTAPCFUNC $1;
    ULONG_PTR $2;
    ULONG_PTR $3;
    ULONG_PTR $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRaiseHardError_params
{
    void *$handle;
    NTSTATUS $0;
    ULONG $1;
    PUNICODE_STRING $2;
    PVOID *$3;
    HARDERROR_RESPONSE_OPTION $4;
    PHARDERROR_RESPONSE $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReadFile_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    PVOID $5;
    ULONG $6;
    PLARGE_INTEGER $7;
    PULONG $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReadFileScatter_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    FILE_SEGMENT_ELEMENT *$5;
    ULONG $6;
    PLARGE_INTEGER $7;
    PULONG $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReadVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    const void *$1;
    void *$2;
    SIZE_T $3;
    SIZE_T *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRegisterThreadTerminatePort_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReleaseKeyedEvent_params
{
    void *$handle;
    HANDLE $0;
    const void *$1;
    BOOLEAN $2;
    const LARGE_INTEGER *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReleaseMutant_params
{
    void *$handle;
    HANDLE $0;
    PLONG $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReleaseSemaphore_params
{
    void *$handle;
    HANDLE $0;
    ULONG $1;
    PULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRemoveIoCompletion_params
{
    void *$handle;
    HANDLE $0;
    PULONG_PTR $1;
    PULONG_PTR $2;
    PIO_STATUS_BLOCK $3;
    PLARGE_INTEGER $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRemoveIoCompletionEx_params
{
    void *$handle;
    HANDLE $0;
    FILE_IO_COMPLETION_INFORMATION *$1;
    ULONG $2;
    ULONG *$3;
    LARGE_INTEGER *$4;
    BOOLEAN $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRemoveProcessDebug_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRenameKey_params
{
    void *$handle;
    HANDLE $0;
    UNICODE_STRING *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReplaceKey_params
{
    void *$handle;
    POBJECT_ATTRIBUTES $0;
    HANDLE $1;
    POBJECT_ATTRIBUTES $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtReplyWaitReceivePort_params
{
    void *$handle;
    HANDLE $0;
    PULONG $1;
    PLPC_MESSAGE $2;
    PLPC_MESSAGE $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRequestWaitReplyPort_params
{
    void *$handle;
    HANDLE $0;
    PLPC_MESSAGE $1;
    PLPC_MESSAGE $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtResetEvent_params
{
    void *$handle;
    HANDLE $0;
    LONG *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtResetWriteWatch_params
{
    void *$handle;
    HANDLE $0;
    PVOID $1;
    SIZE_T $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRestoreKey_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    ULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtResumeProcess_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtResumeThread_params
{
    void *$handle;
    HANDLE $0;
    PULONG $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtRollbackTransaction_params
{
    void *$handle;
    HANDLE $0;
    BOOLEAN $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSaveKey_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSecureConnectPort_params
{
    void *$handle;
    PHANDLE $0;
    PUNICODE_STRING $1;
    PSECURITY_QUALITY_OF_SERVICE $2;
    PLPC_SECTION_WRITE $3;
    PSID $4;
    PLPC_SECTION_READ $5;
    PULONG $6;
    PVOID $7;
    PULONG $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetDebugFilterState_params
{
    void *$handle;
    ULONG $0;
    ULONG $1;
    BOOLEAN $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetDefaultLocale_params
{
    void *$handle;
    BOOLEAN $0;
    LCID $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetDefaultUILanguage_params
{
    void *$handle;
    LANGID $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetEaFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PVOID $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetEvent_params
{
    void *$handle;
    HANDLE $0;
    LONG *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationDebugObject_params
{
    void *$handle;
    HANDLE $0;
    DEBUGOBJECTINFOCLASS $1;
    PVOID $2;
    ULONG $3;
    ULONG *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PVOID $2;
    ULONG $3;
    FILE_INFORMATION_CLASS $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationJobObject_params
{
    void *$handle;
    HANDLE $0;
    JOBOBJECTINFOCLASS $1;
    PVOID $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationKey_params
{
    void *$handle;
    HANDLE $0;
    const int $1;
    PVOID $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationObject_params
{
    void *$handle;
    HANDLE $0;
    OBJECT_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationProcess_params
{
    void *$handle;
    HANDLE $0;
    PROCESSINFOCLASS $1;
    PVOID $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationThread_params
{
    void *$handle;
    HANDLE $0;
    THREADINFOCLASS $1;
    LPCVOID $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationToken_params
{
    void *$handle;
    HANDLE $0;
    TOKEN_INFORMATION_CLASS $1;
    PVOID $2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetInformationVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    VIRTUAL_MEMORY_INFORMATION_CLASS $1;
    ULONG_PTR $2;
    PMEMORY_RANGE_ENTRY $3;
    PVOID $4;
    ULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetIntervalProfile_params
{
    void *$handle;
    ULONG $0;
    KPROFILE_SOURCE $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetIoCompletion_params
{
    void *$handle;
    HANDLE $0;
    ULONG_PTR $1;
    ULONG_PTR $2;
    NTSTATUS $3;
    SIZE_T $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetSecurityObject_params
{
    void *$handle;
    HANDLE $0;
    SECURITY_INFORMATION $1;
    PSECURITY_DESCRIPTOR $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetSystemInformation_params
{
    void *$handle;
    SYSTEM_INFORMATION_CLASS $0;
    PVOID $1;
    ULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetSystemTime_params
{
    void *$handle;
    const LARGE_INTEGER *$0;
    LARGE_INTEGER *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetTimer_params
{
    void *$handle;
    HANDLE $0;
    const LARGE_INTEGER *$1;
    PTIMER_APC_ROUTINE $2;
    PVOID $3;
    BOOLEAN $4;
    ULONG $5;
    BOOLEAN *$6;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetTimerResolution_params
{
    void *$handle;
    ULONG $0;
    BOOLEAN $1;
    PULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetValueKey_params
{
    void *$handle;
    HANDLE $0;
    const UNICODE_STRING *$1;
    ULONG $2;
    ULONG $3;
    const void *$4;
    ULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSetVolumeInformationFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PVOID $2;
    ULONG $3;
    FS_INFORMATION_CLASS $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtShutdownSystem_params
{
    void *$handle;
    SHUTDOWN_ACTION $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSignalAndWaitForSingleObject_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    BOOLEAN $2;
    const LARGE_INTEGER *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSuspendProcess_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSuspendThread_params
{
    void *$handle;
    HANDLE $0;
    PULONG $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtSystemDebugControl_params
{
    void *$handle;
    SYSDBG_COMMAND $0;
    PVOID $1;
    ULONG $2;
    PVOID $3;
    ULONG $4;
    PULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtTerminateJobObject_params
{
    void *$handle;
    HANDLE $0;
    NTSTATUS $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtTestAlert_params
{
    void *$handle;
    NTSTATUS $ret;
};

struct MS_STRUCT NtTraceControl_params
{
    void *$handle;
    ULONG $0;
    void *$1;
    ULONG $2;
    void *$3;
    ULONG $4;
    ULONG *$5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUnloadDriver_params
{
    void *$handle;
    const UNICODE_STRING *$0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUnloadKey_params
{
    void *$handle;
    POBJECT_ATTRIBUTES $0;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUnlockFile_params
{
    void *$handle;
    HANDLE $0;
    PIO_STATUS_BLOCK $1;
    PLARGE_INTEGER $2;
    PLARGE_INTEGER $3;
    PULONG $4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUnlockVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    PVOID *$1;
    SIZE_T *$2;
    ULONG $3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUnmapViewOfSection_params
{
    void *$handle;
    HANDLE $0;
    PVOID $1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUnmapViewOfSectionEx_params
{
    void *$handle;
    HANDLE $0;
    PVOID $1;
    ULONG $2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWaitForAlertByThreadId_params
{
    void *$handle;
    const void *$0;
    const LARGE_INTEGER *$1;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWaitForDebugEvent_params
{
    void *$handle;
    HANDLE $0;
    BOOLEAN $1;
    LARGE_INTEGER *$2;
    DBGUI_WAIT_STATE_CHANGE *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWaitForKeyedEvent_params
{
    void *$handle;
    HANDLE $0;
    const void *$1;
    BOOLEAN $2;
    const LARGE_INTEGER *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWaitForMultipleObjects_params
{
    void *$handle;
    ULONG $0;
    const HANDLE *$1;
    BOOLEAN $2;
    BOOLEAN $3;
    const LARGE_INTEGER *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWaitForSingleObject_params
{
    void *$handle;
    HANDLE $0;
    BOOLEAN $1;
    const LARGE_INTEGER *$2;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWow64AllocateVirtualMemory64_params
{
    void *$handle;
    HANDLE $0;
    ULONG64 *$1;
    ULONG64 $2;
    ULONG64 *$3;
    ULONG $4;
    ULONG $5;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWow64GetNativeSystemInformation_params
{
    void *$handle;
    SYSTEM_INFORMATION_CLASS $0;
    void *$1;
    ULONG $2;
    ULONG *$3;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWow64ReadVirtualMemory64_params
{
    void *$handle;
    HANDLE $0;
    ULONG64 $1;
    void *$2;
    ULONG64 $3;
    ULONG64 *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWow64WriteVirtualMemory64_params
{
    void *$handle;
    HANDLE $0;
    ULONG64 $1;
    const void *$2;
    ULONG64 $3;
    ULONG64 *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWriteFile_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    const void *$5;
    ULONG $6;
    PLARGE_INTEGER $7;
    PULONG $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWriteFileGather_params
{
    void *$handle;
    HANDLE $0;
    HANDLE $1;
    PIO_APC_ROUTINE $2;
    PVOID $3;
    PIO_STATUS_BLOCK $4;
    FILE_SEGMENT_ELEMENT *$5;
    ULONG $6;
    PLARGE_INTEGER $7;
    PULONG $8;
    NTSTATUS $ret;
};

struct MS_STRUCT NtWriteVirtualMemory_params
{
    void *$handle;
    HANDLE $0;
    void *$1;
    const void *$2;
    SIZE_T $3;
    SIZE_T *$4;
    NTSTATUS $ret;
};

struct MS_STRUCT NtYieldExecution_params
{
    void *$handle;
    NTSTATUS $ret;
};

struct MS_STRUCT __qemu_host_dispatcher_params
{
    void *$handle;
    int state;
    void *data;
    NTSTATUS $ret;
};

struct MS_STRUCT __qemu_host_init_params
{
    void *$handle;
    void *sp;
};

struct MS_STRUCT __qemu_switch_host_state_params
{
    void *$handle;
    void **user_data;
};

struct MS_STRUCT __wine_dbg_header_params
{
    void *$handle;
    enum __wine_debug_class cls;
    struct __wine_debug_channel *channel;
    const char *function;
    int $ret;
};

struct MS_STRUCT __wine_dbg_output_params
{
    void *$handle;
    const char *str;
    int $ret;
};

struct MS_STRUCT __wine_dbg_strdup_params
{
    void *$handle;
    const char *str;
    const char *$ret;
};

struct MS_STRUCT __wine_unix_call_params
{
    void *$handle;
    void *args;
    unixlib_entry_t func;
    NTSTATUS $ret;
};

struct MS_STRUCT get_thread_context_params
{
    void *$handle;
    HANDLE handle; 
    void *context; 
    BOOL *self; 
    USHORT machine;
    NTSTATUS $ret;
};

struct MS_STRUCT init_thread_stack_params
{
    void *$handle;
    TEB *teb;
    ULONG_PTR limit;
    SIZE_T reserve_size;
    SIZE_T commit_size;
    NTSTATUS $ret;
};

struct MS_STRUCT process_close_server_socket_params
{
    void *$handle;
};

struct MS_STRUCT send_debug_event_params
{
    void *$handle;
    EXCEPTION_RECORD *rec; 
    CONTEXT *context; 
    BOOL first_chance;
    NTSTATUS $ret;
};

struct MS_STRUCT server_init_thread_params
{
    void *$handle;
    void *entry_point;
    BOOL *suspend;
};

struct MS_STRUCT server_pipe_params
{
    void *$handle;
    int *fd;
    int $ret;
};

struct MS_STRUCT server_select_params
{
    void *$handle;
    const select_op_t *select_op;
    data_size_t size;
    UINT flags;
    timeout_t abs_timeout;
    context_t *context;
    user_apc_t *user_apc;
    unsigned int $ret;
};

struct MS_STRUCT set_thread_context_params
{
    void *$handle;
    HANDLE handle; 
    const void *context; 
    BOOL *self; 
    USHORT machine;
    NTSTATUS $ret;
};

struct MS_STRUCT virtual_free_teb_params
{
    void *$handle;
    TEB* teb;
};

struct MS_STRUCT virtual_handle_fault_params
{
    void *$handle;
    void *addr;
    DWORD err;
    void *stack;
    NTSTATUS $ret;
};

struct MS_STRUCT virtual_is_valid_code_address_params
{
    void *$handle;
    const void *addr;
    SIZE_T size;
    BOOL $ret;
};

struct MS_STRUCT virtual_setup_exception_params
{
    void *$handle;
    void *stack_ptr;
    size_t size;
    EXCEPTION_RECORD *rec;
    void *$ret;
};

struct MS_STRUCT virtual_uninterrupted_read_memory_params
{
    void *$handle;
    const void *addr;
    void *buffer;
    SIZE_T size;
    SIZE_T $ret;
};

struct MS_STRUCT virtual_uninterrupted_write_memory_params
{
    void *$handle;
    void *addr;
    const void *buffer;
    SIZE_T size;
    NTSTATUS $ret;
};

struct MS_STRUCT wait_suspend_params
{
    void *$handle;
    CONTEXT *context;  
};

struct MS_STRUCT wine_nt_to_unix_file_name_params
{
    void *$handle;
    const OBJECT_ATTRIBUTES *attr;
    char *nameA;
    ULONG *size;
    UINT disposition;
    NTSTATUS $ret;
};

struct MS_STRUCT wine_server_call_params
{
    void *$handle;
    void *req_ptr;
    unsigned int $ret;
};

struct MS_STRUCT wine_server_send_fd_params
{
    void *$handle;
    int fd;
};

struct MS_STRUCT wine_unix_to_nt_file_name_params
{
    void *$handle;
    const char *name;
    WCHAR *buffer;
    ULONG *size;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiAbortDoc_params
{
    void *$handle;
    HDC hdc;
    INT $ret;
};

struct MS_STRUCT NtGdiAbortPath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiAddFontMemResourceEx_params
{
    void *$handle;
    void *ptr;
    DWORD size;
    void *dv;
    ULONG dv_size;
    DWORD *count;
    HANDLE $ret;
};

struct MS_STRUCT NtGdiAddFontResourceW_params
{
    void *$handle;
    const WCHAR *str;
    ULONG size;
    ULONG files;
    DWORD flags;
    DWORD tid;
    void *dv;
    INT $ret;
};

struct MS_STRUCT NtGdiAlphaBlend_params
{
    void *$handle;
    HDC hdc_dst;
    int x_dst;
    int y_dst;
    int width_dst;
    int height_dst;
    HDC hdc_src;
    int x_src;
    int y_src;
    int width_src;
    int height_src;
    DWORD blend_function;
    HANDLE xform;
    BOOL $ret;
};

struct MS_STRUCT NtGdiAngleArc_params
{
    void *$handle;
    HDC hdc; 
    INT x; 
    INT y;
    DWORD dwRadius;
    DWORD start_angle;
    DWORD sweep_angle;
    BOOL $ret;
};

struct MS_STRUCT NtGdiArcInternal_params
{
    void *$handle;
    UINT type;
    HDC hdc;
    INT left;
    INT top;
    INT right;
    INT bottom;
    INT xstart;
    INT ystart;
    INT xend;
    INT yend;
    BOOL $ret;
};

struct MS_STRUCT NtGdiBeginPath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiBitBlt_params
{
    void *$handle;
    HDC hdc_dst;
    INT x_dst;
    INT y_dst;
    INT width;
    INT height;
    HDC hdc_src;
    INT x_src;
    INT y_src;
    DWORD rop;
    DWORD bk_color;
    FLONG fl;
    BOOL $ret;
};

struct MS_STRUCT NtGdiCloseFigure_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiCombineRgn_params
{
    void *$handle;
    HRGN dest;
    HRGN src1;
    HRGN src2;
    INT mode;
    INT $ret;
};

struct MS_STRUCT NtGdiComputeXformCoefficients_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiCreateBitmap_params
{
    void *$handle;
    INT width;
    INT height;
    UINT planes;
    UINT bpp;
    const void *bits;
    HBITMAP $ret;
};

struct MS_STRUCT NtGdiCreateClientObj_params
{
    void *$handle;
    ULONG type;
    HANDLE $ret;
};

struct MS_STRUCT NtGdiCreateCompatibleBitmap_params
{
    void *$handle;
    HDC hdc;
    INT width;
    INT height;
    HBITMAP $ret;
};

struct MS_STRUCT NtGdiCreateCompatibleDC_params
{
    void *$handle;
    HDC hdc;
    HDC $ret;
};

struct MS_STRUCT NtGdiCreateDIBBrush_params
{
    void *$handle;
    const void *data;
    UINT coloruse;
    UINT size;
    BOOL is_8x8;
    BOOL pen;
    const void *client;
    HBRUSH $ret;
};

struct MS_STRUCT NtGdiCreateDIBSection_params
{
    void *$handle;
    HDC hdc;
    HANDLE section;
    DWORD offset;
    const BITMAPINFO *bmi;
    UINT usage;
    UINT header_size;
    ULONG flags;
    ULONG_PTR color_space;
    void **bits;
    HBITMAP $ret;
};

struct MS_STRUCT NtGdiCreateDIBitmapInternal_params
{
    void *$handle;
    HDC hdc;
    INT width;
    INT height;
    DWORD init;
    const void *bits;
    const BITMAPINFO *data;
    UINT coloruse;
    UINT max_info;
    UINT max_bits;
    ULONG flags;
    HANDLE xform;
    HBITMAP $ret;
};

struct MS_STRUCT NtGdiCreateEllipticRgn_params
{
    void *$handle;
    INT left;
    INT top;
    INT right;
    INT bottom;
    HRGN $ret;
};

struct MS_STRUCT NtGdiCreateHalftonePalette_params
{
    void *$handle;
    HDC hdc;
    HPALETTE $ret;
};

struct MS_STRUCT NtGdiCreateHatchBrushInternal_params
{
    void *$handle;
    INT style;
    COLORREF color;
    BOOL pen;
    HBRUSH $ret;
};

struct MS_STRUCT NtGdiCreateMetafileDC_params
{
    void *$handle;
    HDC hdc;
    HDC $ret;
};

struct MS_STRUCT NtGdiCreatePaletteInternal_params
{
    void *$handle;
    const LOGPALETTE *palette;
    UINT count;
    HPALETTE $ret;
};

struct MS_STRUCT NtGdiCreatePatternBrushInternal_params
{
    void *$handle;
    HBITMAP hbitmap;
    BOOL pen;
    BOOL is_8x8;
    HBRUSH $ret;
};

struct MS_STRUCT NtGdiCreatePen_params
{
    void *$handle;
    INT style;
    INT width;
    COLORREF color;
    HBRUSH brush;
    HPEN $ret;
};

struct MS_STRUCT NtGdiCreateRectRgn_params
{
    void *$handle;
    INT left;
    INT top;
    INT right;
    INT bottom;
    HRGN $ret;
};

struct MS_STRUCT NtGdiCreateRoundRectRgn_params
{
    void *$handle;
    INT left;
    INT top;
    INT right;
    INT bottom;
    INT ellipse_width;
    INT ellipse_height;
    HRGN $ret;
};

struct MS_STRUCT NtGdiCreateSolidBrush_params
{
    void *$handle;
    COLORREF color;
    HBRUSH brush;
    HBRUSH $ret;
};

struct MS_STRUCT NtGdiDdDDICheckVidPnExclusiveOwnership_params
{
    void *$handle;
    const D3DKMT_CHECKVIDPNEXCLUSIVEOWNERSHIP *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDICloseAdapter_params
{
    void *$handle;
    const D3DKMT_CLOSEADAPTER *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDICreateDCFromMemory_params
{
    void *$handle;
    D3DKMT_CREATEDCFROMMEMORY *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDICreateDevice_params
{
    void *$handle;
    D3DKMT_CREATEDEVICE *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIDestroyDCFromMemory_params
{
    void *$handle;
    const D3DKMT_DESTROYDCFROMMEMORY *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIDestroyDevice_params
{
    void *$handle;
    const D3DKMT_DESTROYDEVICE *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIEscape_params
{
    void *$handle;
    const D3DKMT_ESCAPE *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIOpenAdapterFromDeviceName_params
{
    void *$handle;
    D3DKMT_OPENADAPTERFROMDEVICENAME *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIOpenAdapterFromHdc_params
{
    void *$handle;
    D3DKMT_OPENADAPTERFROMHDC *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIOpenAdapterFromLuid_params
{
    void *$handle;
    D3DKMT_OPENADAPTERFROMLUID *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIQueryStatistics_params
{
    void *$handle;
    D3DKMT_QUERYSTATISTICS *stats;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDIQueryVideoMemoryInfo_params
{
    void *$handle;
    D3DKMT_QUERYVIDEOMEMORYINFO *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDISetQueuedLimit_params
{
    void *$handle;
    D3DKMT_SETQUEUEDLIMIT *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDdDDISetVidPnSourceOwner_params
{
    void *$handle;
    const D3DKMT_SETVIDPNSOURCEOWNER *desc;
    NTSTATUS $ret;
};

struct MS_STRUCT NtGdiDeleteClientObj_params
{
    void *$handle;
    HGDIOBJ obj;
    BOOL $ret;
};

struct MS_STRUCT NtGdiDeleteObjectApp_params
{
    void *$handle;
    HGDIOBJ obj;
    BOOL $ret;
};

struct MS_STRUCT NtGdiDescribePixelFormat_params
{
    void *$handle;
    HDC hdc;
    INT format;
    UINT size;
    PIXELFORMATDESCRIPTOR *descr;
    INT $ret;
};

struct MS_STRUCT NtGdiDoPalette_params
{
    void *$handle;
    HGDIOBJ handle;
    WORD start;
    WORD count;
    void *entries;
    DWORD func;
    BOOL inbound;
    LONG $ret;
};

struct MS_STRUCT NtGdiDrawStream_params
{
    void *$handle;
    HDC hdc;
    ULONG in;
    void *pvin;
    BOOL $ret;
};

struct MS_STRUCT NtGdiEllipse_params
{
    void *$handle;
    HDC hdc;
    INT left;
    INT top;
    INT right;
    INT bottom;
    BOOL $ret;
};

struct MS_STRUCT NtGdiEndDoc_params
{
    void *$handle;
    HDC hdc;
    INT $ret;
};

struct MS_STRUCT NtGdiEndPage_params
{
    void *$handle;
    HDC hdc;
    INT $ret;
};

struct MS_STRUCT NtGdiEndPath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiEnumFonts_params
{
    void *$handle;
    HDC hdc;
    ULONG type;
    ULONG win32_compat;
    ULONG face_name_len;
    const WCHAR *face_name;
    ULONG charset;
    ULONG *count;
    void *buf;
    BOOL $ret;
};

struct MS_STRUCT NtGdiEqualRgn_params
{
    void *$handle;
    HRGN hrgn1;
    HRGN hrgn2;
    BOOL $ret;
};

struct MS_STRUCT NtGdiExcludeClipRect_params
{
    void *$handle;
    HDC hdc;
    INT left;
    INT top;
    INT right;
    INT bottom;
    INT $ret;
};

struct MS_STRUCT NtGdiExtCreatePen_params
{
    void *$handle;
    DWORD style;
    DWORD width;
    ULONG brush_style;
    ULONG color;
    ULONG_PTR client_hatch;
    ULONG_PTR hatch;
    DWORD style_count;
    const DWORD *style_bits;
    ULONG dib_size;
    BOOL old_style;
    HBRUSH brush;
    HPEN $ret;
};

struct MS_STRUCT NtGdiExtCreateRegion_params
{
    void *$handle;
    const XFORM *xform;
    DWORD count;
    const RGNDATA *data;
    HRGN $ret;
};

struct MS_STRUCT NtGdiExtEscape_params
{
    void *$handle;
    HDC hdc;
    WCHAR *driver;
    INT driver_id;
    INT escape;
    INT input_size;
    const char *input;
    INT output_size;
    char *output;
    INT $ret;
};

struct MS_STRUCT NtGdiExtFloodFill_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    COLORREF color;
    UINT type;
    BOOL $ret;
};

struct MS_STRUCT NtGdiExtGetObjectW_params
{
    void *$handle;
    HGDIOBJ handle;
    INT count;
    void *buffer;
    INT $ret;
};

struct MS_STRUCT NtGdiExtSelectClipRgn_params
{
    void *$handle;
    HDC hdc;
    HRGN region;
    INT mode;
    INT $ret;
};

struct MS_STRUCT NtGdiExtTextOutW_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    UINT flags;
    const RECT *rect;
    const WCHAR *str;
    UINT count;
    const INT *dx;
    DWORD cp;
    BOOL $ret;
};

struct MS_STRUCT NtGdiFillPath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiFillRgn_params
{
    void *$handle;
    HDC hdc;
    HRGN hrgn;
    HBRUSH hbrush;
    BOOL $ret;
};

struct MS_STRUCT NtGdiFlattenPath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiFlush_params
{
    void *$handle;
    BOOL $ret;
};

struct MS_STRUCT NtGdiFontIsLinked_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiFrameRgn_params
{
    void *$handle;
    HDC hdc;
    HRGN hrgn;
    HBRUSH brush;
    INT width;
    INT height;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetAndSetDCDword_params
{
    void *$handle;
    HDC hdc;
    UINT method;
    DWORD value;
    DWORD *result;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetAppClipBox_params
{
    void *$handle;
    HDC hdc;
    RECT *rect;
    INT $ret;
};

struct MS_STRUCT NtGdiGetBitmapBits_params
{
    void *$handle;
    HBITMAP bitmap;
    LONG count;
    void *bits;
    LONG $ret;
};

struct MS_STRUCT NtGdiGetBitmapDimension_params
{
    void *$handle;
    HBITMAP bitmap;
    SIZE *size;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetBoundsRect_params
{
    void *$handle;
    HDC hdc;
    RECT *rect;
    UINT flags;
    UINT $ret;
};

struct MS_STRUCT NtGdiGetCharABCWidthsW_params
{
    void *$handle;
    HDC hdc;
    UINT first;
    UINT last;
    WCHAR *chars;
    ULONG flags;
    void *buffer;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetCharWidthInfo_params
{
    void *$handle;
    HDC hdc;
    struct char_width_info *info;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetCharWidthW_params
{
    void *$handle;
    HDC hdc;
    UINT first_char;
    UINT last_char;
    WCHAR *chars;
    ULONG flags;
    void *buffer;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetColorAdjustment_params
{
    void *$handle;
    HDC hdc;
    COLORADJUSTMENT *ca;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetDCDword_params
{
    void *$handle;
    HDC hdc;
    UINT method;
    DWORD *result;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetDCObject_params
{
    void *$handle;
    HDC hdc;
    UINT type;
    HANDLE $ret;
};

struct MS_STRUCT NtGdiGetDCPoint_params
{
    void *$handle;
    HDC hdc;
    UINT method;
    POINT *result;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetDIBitsInternal_params
{
    void *$handle;
    HDC hdc;
    HBITMAP hbitmap;
    UINT startscan;
    UINT lines;
    void *bits;
    BITMAPINFO *info;
    UINT coloruse;
    UINT max_bits;
    UINT max_info;
    INT $ret;
};

struct MS_STRUCT NtGdiGetDeviceCaps_params
{
    void *$handle;
    HDC hdc;
    INT cap;
    INT $ret;
};

struct MS_STRUCT NtGdiGetDeviceGammaRamp_params
{
    void *$handle;
    HDC hdc;
    void *ptr;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetFontData_params
{
    void *$handle;
    HDC hdc;
    DWORD table;
    DWORD offset;
    void *buffer;
    DWORD length;
    DWORD $ret;
};

struct MS_STRUCT NtGdiGetFontFileData_params
{
    void *$handle;
    DWORD instance_id;
    DWORD file_index;
    UINT64 *offset;
    void *buff;
    DWORD buff_size;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetFontFileInfo_params
{
    void *$handle;
    DWORD instance_id;
    DWORD file_index;
    struct font_fileinfo *info;
    SIZE_T size;
    SIZE_T *needed;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetFontUnicodeRanges_params
{
    void *$handle;
    HDC hdc;
    GLYPHSET *lpgs;
    DWORD $ret;
};

struct MS_STRUCT NtGdiGetGlyphIndicesW_params
{
    void *$handle;
    HDC hdc;
    const WCHAR *str;
    INT count;
    WORD *indices;
    DWORD flags;
    DWORD $ret;
};

struct MS_STRUCT NtGdiGetGlyphOutline_params
{
    void *$handle;
    HDC hdc;
    UINT ch;
    UINT format;
    GLYPHMETRICS *metrics;
    DWORD size;
    void *buffer;
    const MAT2 *mat2;
    BOOL ignore_rotation;
    DWORD $ret;
};

struct MS_STRUCT NtGdiGetKerningPairs_params
{
    void *$handle;
    HDC hdc;
    DWORD count;
    KERNINGPAIR *kern_pair;
    DWORD $ret;
};

struct MS_STRUCT NtGdiGetNearestColor_params
{
    void *$handle;
    HDC hdc;
    COLORREF color;
    COLORREF $ret;
};

struct MS_STRUCT NtGdiGetNearestPaletteIndex_params
{
    void *$handle;
    HPALETTE hpalette;
    COLORREF color;
    UINT $ret;
};

struct MS_STRUCT NtGdiGetOutlineTextMetricsInternalW_params
{
    void *$handle;
    HDC hdc;
    UINT cbData;
    OUTLINETEXTMETRICW *otm;
    ULONG opts;
    UINT $ret;
};

struct MS_STRUCT NtGdiGetPath_params
{
    void *$handle;
    HDC hdc;
    POINT *points;
    BYTE *types;
    INT size;
    INT $ret;
};

struct MS_STRUCT NtGdiGetPixel_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    COLORREF $ret;
};

struct MS_STRUCT NtGdiGetRandomRgn_params
{
    void *$handle;
    HDC hdc;
    HRGN region;
    INT code;
    INT $ret;
};

struct MS_STRUCT NtGdiGetRasterizerCaps_params
{
    void *$handle;
    RASTERIZER_STATUS *status;
    UINT size;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetRealizationInfo_params
{
    void *$handle;
    HDC hdc;
    struct font_realization_info *info;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetRegionData_params
{
    void *$handle;
    HRGN hrgn;
    DWORD count;
    RGNDATA *data;
    DWORD $ret;
};

struct MS_STRUCT NtGdiGetRgnBox_params
{
    void *$handle;
    HRGN hrgn;
    RECT *rect;
    INT $ret;
};

struct MS_STRUCT NtGdiGetSpoolMessage_params
{
    void *$handle;
    void *ptr1;
    DWORD data2;
    void *ptr3;
    DWORD data4;
    DWORD $ret;
};

struct MS_STRUCT NtGdiGetSystemPaletteUse_params
{
    void *$handle;
    HDC hdc;
    UINT $ret;
};

struct MS_STRUCT NtGdiGetTextCharsetInfo_params
{
    void *$handle;
    HDC hdc;
    FONTSIGNATURE *fs;
    DWORD flags;
    UINT $ret;
};

struct MS_STRUCT NtGdiGetTextExtentExW_params
{
    void *$handle;
    HDC hdc;
    const WCHAR *str;
    INT count;
    INT max_ext;
    INT *nfit;
    INT *dxs;
    SIZE *size;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetTextFaceW_params
{
    void *$handle;
    HDC hdc;
    INT count;
    WCHAR *name;
    BOOL alias_name;
    INT $ret;
};

struct MS_STRUCT NtGdiGetTextMetricsW_params
{
    void *$handle;
    HDC hdc;
    TEXTMETRICW *metrics;
    ULONG flags;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGetTransform_params
{
    void *$handle;
    HDC hdc;
    DWORD which;
    XFORM *xform;
    BOOL $ret;
};

struct MS_STRUCT NtGdiGradientFill_params
{
    void *$handle;
    HDC hdc;
    TRIVERTEX *vert_array;
    ULONG nvert;
    void *grad_array;
    ULONG ngrad;
    ULONG mode;
    BOOL $ret;
};

struct MS_STRUCT NtGdiHfontCreate_params
{
    void *$handle;
    const void *logfont;
    ULONG unk2;
    ULONG unk3;
    ULONG unk4;
    void *data;
    HFONT $ret;
};

struct MS_STRUCT NtGdiIcmBrushInfo_params
{
    void *$handle;
    HDC hdc; 
    HBRUSH handle; 
    BITMAPINFO *info; 
    void *bits;
    ULONG *bits_size; 
    UINT *usage; 
    BOOL *unk; 
    UINT mode;
    BOOL $ret;
};

struct MS_STRUCT NtGdiInitSpool_params
{
    void *$handle;
    DWORD $ret;
};

struct MS_STRUCT NtGdiIntersectClipRect_params
{
    void *$handle;
    HDC hdc;
    INT left;
    INT top;
    INT right;
    INT bottom;
    INT $ret;
};

struct MS_STRUCT NtGdiInvertRgn_params
{
    void *$handle;
    HDC hdc;
    HRGN hrgn;
    BOOL $ret;
};

struct MS_STRUCT NtGdiLineTo_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    BOOL $ret;
};

struct MS_STRUCT NtGdiMaskBlt_params
{
    void *$handle;
    HDC hdc;
    INT x_dst;
    INT y_dst;
    INT width_dst;
    INT height_dst;
    HDC hdc_src;
    INT x_src;
    INT y_src;
    HBITMAP mask;
    INT x_mask;
    INT y_mask;
    DWORD rop;
    DWORD bk_color;
    BOOL $ret;
};

struct MS_STRUCT NtGdiModifyWorldTransform_params
{
    void *$handle;
    HDC hdc;
    const XFORM *xform;
    DWORD mode;
    BOOL $ret;
};

struct MS_STRUCT NtGdiMoveTo_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    POINT *pt;
    BOOL $ret;
};

struct MS_STRUCT NtGdiOffsetClipRgn_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    INT $ret;
};

struct MS_STRUCT NtGdiOffsetRgn_params
{
    void *$handle;
    HRGN hrgn;
    INT x;
    INT y;
    INT $ret;
};

struct MS_STRUCT NtGdiOpenDCW_params
{
    void *$handle;
    UNICODE_STRING *device;
    const DEVMODEW *devmode;
    UNICODE_STRING *output;
    ULONG type;
    BOOL is_display;
    HANDLE hspool;
    DRIVER_INFO_2W *driver_info;
    void *pdev;
    HDC $ret;
};

struct MS_STRUCT NtGdiPatBlt_params
{
    void *$handle;
    HDC hdc;
    INT left;
    INT top;
    INT width;
    INT height;
    DWORD rop;
    BOOL $ret;
};

struct MS_STRUCT NtGdiPathToRegion_params
{
    void *$handle;
    HDC hdc;
    HRGN $ret;
};

struct MS_STRUCT NtGdiPlgBlt_params
{
    void *$handle;
    HDC hdc;
    const POINT *point;
    HDC hdc_src;
    INT x_src;
    INT y_src;
    INT width;
    INT height;
    HBITMAP mask;
    INT x_mask;
    INT y_mask;
    DWORD bk_color;
    BOOL $ret;
};

struct MS_STRUCT NtGdiPolyDraw_params
{
    void *$handle;
    HDC hdc;
    const POINT *points;
    const BYTE *types;
    DWORD count;
    BOOL $ret;
};

struct MS_STRUCT NtGdiPolyPolyDraw_params
{
    void *$handle;
    HDC hdc;
    const POINT *points;
    const ULONG *counts;
    DWORD count;
    UINT function;
    ULONG $ret;
};

struct MS_STRUCT NtGdiPtInRegion_params
{
    void *$handle;
    HRGN hrgn;
    INT x;
    INT y;
    BOOL $ret;
};

struct MS_STRUCT NtGdiPtVisible_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    BOOL $ret;
};

struct MS_STRUCT NtGdiRectInRegion_params
{
    void *$handle;
    HRGN hrgn;
    const RECT *rect;
    BOOL $ret;
};

struct MS_STRUCT NtGdiRectVisible_params
{
    void *$handle;
    HDC hdc;
    const RECT *rect;
    BOOL $ret;
};

struct MS_STRUCT NtGdiRectangle_params
{
    void *$handle;
    HDC hdc;
    INT left;
    INT top;
    INT right;
    INT bottom;
    BOOL $ret;
};

struct MS_STRUCT NtGdiRemoveFontMemResourceEx_params
{
    void *$handle;
    HANDLE handle;
    BOOL $ret;
};

struct MS_STRUCT NtGdiRemoveFontResourceW_params
{
    void *$handle;
    const WCHAR *str;
    ULONG size;
    ULONG files;
    DWORD flags;
    DWORD tid;
    void *dv;
    BOOL $ret;
};

struct MS_STRUCT NtGdiResetDC_params
{
    void *$handle;
    HDC hdc;
    const DEVMODEW *devmode;
    BOOL *banding;
    DRIVER_INFO_2W *driver_info;
    void *dev;
    BOOL $ret;
};

struct MS_STRUCT NtGdiResizePalette_params
{
    void *$handle;
    HPALETTE palette;
    UINT count;
    BOOL $ret;
};

struct MS_STRUCT NtGdiRestoreDC_params
{
    void *$handle;
    HDC hdc;
    INT level;
    BOOL $ret;
};

struct MS_STRUCT NtGdiRoundRect_params
{
    void *$handle;
    HDC hdc;
    INT left;
    INT top;
    INT right;
    INT bottom;
    INT ell_width;
    INT ell_height;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSaveDC_params
{
    void *$handle;
    HDC hdc;
    INT $ret;
};

struct MS_STRUCT NtGdiScaleViewportExtEx_params
{
    void *$handle;
    HDC hdc;
    INT x_num;
    INT x_denom;
    INT y_num;
    INT y_denom;
    SIZE *size;
    BOOL $ret;
};

struct MS_STRUCT NtGdiScaleWindowExtEx_params
{
    void *$handle;
    HDC hdc;
    INT x_num;
    INT x_denom;
    INT y_num;
    INT y_denom;
    SIZE *size;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSelectBitmap_params
{
    void *$handle;
    HDC hdc;
    HGDIOBJ handle;
    HGDIOBJ $ret;
};

struct MS_STRUCT NtGdiSelectBrush_params
{
    void *$handle;
    HDC hdc;
    HGDIOBJ handle;
    HGDIOBJ $ret;
};

struct MS_STRUCT NtGdiSelectClipPath_params
{
    void *$handle;
    HDC hdc;
    INT mode;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSelectFont_params
{
    void *$handle;
    HDC hdc;
    HGDIOBJ handle;
    HGDIOBJ $ret;
};

struct MS_STRUCT NtGdiSelectPen_params
{
    void *$handle;
    HDC hdc;
    HGDIOBJ handle;
    HGDIOBJ $ret;
};

struct MS_STRUCT NtGdiSetBitmapBits_params
{
    void *$handle;
    HBITMAP hbitmap;
    LONG count;
    const void *bits;
    LONG $ret;
};

struct MS_STRUCT NtGdiSetBitmapDimension_params
{
    void *$handle;
    HBITMAP hbitmap;
    INT x;
    INT y;
    SIZE *prev_size;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetBoundsRect_params
{
    void *$handle;
    HDC hdc;
    const RECT *rect;
    UINT flags;
    UINT $ret;
};

struct MS_STRUCT NtGdiSetBrushOrg_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    POINT *prev_org;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetColorAdjustment_params
{
    void *$handle;
    HDC hdc;
    const COLORADJUSTMENT *ca;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetDIBitsToDeviceInternal_params
{
    void *$handle;
    HDC hdc;
    INT x_dst;
    INT y_dst;
    DWORD cx;
    DWORD cy;
    INT x_src;
    INT y_src;
    UINT startscan;
    UINT lines;
    const void *bits;
    const BITMAPINFO *bmi;
    UINT coloruse;
    UINT max_bits;
    UINT max_info;
    BOOL xform_coords;
    HANDLE xform;
    INT $ret;
};

struct MS_STRUCT NtGdiSetDeviceGammaRamp_params
{
    void *$handle;
    HDC hdc;
    void *ptr;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetLayout_params
{
    void *$handle;
    HDC hdc;
    LONG wox;
    DWORD layout;
    DWORD $ret;
};

struct MS_STRUCT NtGdiSetMagicColors_params
{
    void *$handle;
    HDC hdc;
    DWORD magic;
    ULONG index;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetMetaRgn_params
{
    void *$handle;
    HDC hdc;
    INT $ret;
};

struct MS_STRUCT NtGdiSetPixel_params
{
    void *$handle;
    HDC hdc;
    INT x;
    INT y;
    COLORREF color;
    COLORREF $ret;
};

struct MS_STRUCT NtGdiSetPixelFormat_params
{
    void *$handle;
    HDC hdc;
    INT format;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetRectRgn_params
{
    void *$handle;
    HRGN hrgn;
    INT left;
    INT top;
    INT right;
    INT bottom;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetSystemPaletteUse_params
{
    void *$handle;
    HDC hdc;
    UINT use;
    UINT $ret;
};

struct MS_STRUCT NtGdiSetTextJustification_params
{
    void *$handle;
    HDC hdc;
    INT extra;
    INT breaks;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSetVirtualResolution_params
{
    void *$handle;
    HDC hdc;
    DWORD horz_res;
    DWORD vert_res;
    DWORD horz_size;
    DWORD vert_size;
    BOOL $ret;
};

struct MS_STRUCT NtGdiStartDoc_params
{
    void *$handle;
    HDC hdc;
    const DOCINFOW *doc;
    BOOL *banding;
    INT job;
    INT $ret;
};

struct MS_STRUCT NtGdiStartPage_params
{
    void *$handle;
    HDC hdc;
    INT $ret;
};

struct MS_STRUCT NtGdiStretchBlt_params
{
    void *$handle;
    HDC hdc;
    INT x_dst;
    INT y_dst;
    INT width_dst;
    INT height_dst;
    HDC hdc_src;
    INT x_src;
    INT y_src;
    INT width_src;
    INT height_src;
    DWORD rop;
    COLORREF bk_color;
    BOOL $ret;
};

struct MS_STRUCT NtGdiStretchDIBitsInternal_params
{
    void *$handle;
    HDC hdc;
    INT x_dst;
    INT y_dst;
    INT width_dst;
    INT height_dst;
    INT x_src;
    INT y_src;
    INT width_src;
    INT height_src;
    const void *bits;
    const BITMAPINFO *bmi;
    UINT coloruse;
    DWORD rop;
    UINT max_info;
    UINT max_bits;
    HANDLE xform;
    INT $ret;
};

struct MS_STRUCT NtGdiStrokeAndFillPath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiStrokePath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiSwapBuffers_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiTransformPoints_params
{
    void *$handle;
    HDC hdc;
    const POINT *points_in;
    POINT *points_out;
    INT count;
    UINT mode;
    BOOL $ret;
};

struct MS_STRUCT NtGdiTransparentBlt_params
{
    void *$handle;
    HDC hdc;
    int x_dst;
    int y_dst;
    int width_dst;
    int height_dst;
    HDC hdc_src;
    int x_src;
    int y_src;
    int width_src;
    int height_src;
    UINT color;
    BOOL $ret;
};

struct MS_STRUCT NtGdiUnrealizeObject_params
{
    void *$handle;
    HGDIOBJ obj;
    BOOL $ret;
};

struct MS_STRUCT NtGdiUpdateColors_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtGdiWidenPath_params
{
    void *$handle;
    HDC hdc;
    BOOL $ret;
};

struct MS_STRUCT NtUserActivateKeyboardLayout_params
{
    void *$handle;
    HKL layout;
    UINT flags;
    HKL $ret;
};

struct MS_STRUCT NtUserAddClipboardFormatListener_params
{
    void *$handle;
    HWND hwnd;
    BOOL $ret;
};

struct MS_STRUCT NtUserAssociateInputContext_params
{
    void *$handle;
    HWND hwnd;
    HIMC ctx;
    ULONG flags;
    UINT $ret;
};

struct MS_STRUCT NtUserAttachThreadInput_params
{
    void *$handle;
    DWORD from;
    DWORD to;
    BOOL attach;
    BOOL $ret;
};

struct MS_STRUCT NtUserBeginPaint_params
{
    void *$handle;
    HWND hwnd;
    PAINTSTRUCT *ps;
    HDC $ret;
};

struct MS_STRUCT NtUserBuildHimcList_params
{
    void *$handle;
    UINT thread_id;
    UINT count;
    HIMC *buffer;
    UINT *size;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUserBuildHwndList_params
{
    void *$handle;
    HDESK desktop;
    ULONG unk2;
    ULONG unk3;
    ULONG unk4;
    ULONG thread_id;
    ULONG count;
    HWND *buffer;
    ULONG *size;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUserCallHwnd_params
{
    void *$handle;
    HWND hwnd;
    DWORD code;
    ULONG_PTR $ret;
};

struct MS_STRUCT NtUserCallHwndParam_params
{
    void *$handle;
    HWND hwnd;
    DWORD_PTR param;
    DWORD code;
    ULONG_PTR $ret;
};

struct MS_STRUCT NtUserCallMsgFilter_params
{
    void *$handle;
    MSG *msg;
    INT code;
    BOOL $ret;
};

struct MS_STRUCT NtUserCallNextHookEx_params
{
    void *$handle;
    HHOOK hhook;
    INT code;
    WPARAM wparam;
    LPARAM lparam;
    LRESULT $ret;
};

struct MS_STRUCT NtUserCallNoParam_params
{
    void *$handle;
    ULONG code;
    ULONG_PTR $ret;
};

struct MS_STRUCT NtUserCallOneParam_params
{
    void *$handle;
    ULONG_PTR arg;
    ULONG code;
    ULONG_PTR $ret;
};

struct MS_STRUCT NtUserCallTwoParam_params
{
    void *$handle;
    ULONG_PTR arg1;
    ULONG_PTR arg2;
    ULONG code;
    ULONG_PTR $ret;
};

struct MS_STRUCT NtUserChangeClipboardChain_params
{
    void *$handle;
    HWND hwnd;
    HWND next;
    BOOL $ret;
};

struct MS_STRUCT NtUserChangeDisplaySettings_params
{
    void *$handle;
    UNICODE_STRING *devname;
    DEVMODEW *devmode;
    HWND hwnd;
    DWORD flags;
    void *lparam;
    LONG $ret;
};

struct MS_STRUCT NtUserCheckMenuItem_params
{
    void *$handle;
    HMENU handle;
    UINT id;
    UINT flags;
    DWORD $ret;
};

struct MS_STRUCT NtUserChildWindowFromPointEx_params
{
    void *$handle;
    HWND parent;
    LONG x;
    LONG y;
    UINT flags;
    HWND $ret;
};

struct MS_STRUCT NtUserClipCursor_params
{
    void *$handle;
    const RECT *rect;
    BOOL $ret;
};

struct MS_STRUCT NtUserCloseClipboard_params
{
    void *$handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserCloseDesktop_params
{
    void *$handle;
    HDESK handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserCloseWindowStation_params
{
    void *$handle;
    HWINSTA handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserCopyAcceleratorTable_params
{
    void *$handle;
    HACCEL src;
    ACCEL *dst;
    INT count;
    INT $ret;
};

struct MS_STRUCT NtUserCountClipboardFormats_params
{
    void *$handle;
    INT $ret;
};

struct MS_STRUCT NtUserCreateAcceleratorTable_params
{
    void *$handle;
    ACCEL *table;
    INT count;
    HACCEL $ret;
};

struct MS_STRUCT NtUserCreateCaret_params
{
    void *$handle;
    HWND hwnd;
    HBITMAP bitmap;
    int width;
    int height;
    BOOL $ret;
};

struct MS_STRUCT NtUserCreateDesktopEx_params
{
    void *$handle;
    OBJECT_ATTRIBUTES *attr;
    UNICODE_STRING *device;
    DEVMODEW *devmode;
    DWORD flags;
    ACCESS_MASK access;
    ULONG heap_size;
    HDESK $ret;
};

struct MS_STRUCT NtUserCreateInputContext_params
{
    void *$handle;
    UINT_PTR client_ptr;
    HIMC $ret;
};

struct MS_STRUCT NtUserCreateWindowEx_params
{
    void *$handle;
    DWORD ex_style;
    UNICODE_STRING *class_name;
    UNICODE_STRING *version;
    UNICODE_STRING *window_name;
    DWORD style;
    INT x;
    INT y;
    INT cx;
    INT cy;
    HWND parent;
    HMENU menu;
    HINSTANCE instance;
    void *params;
    DWORD flags;
    HINSTANCE client_instance;
    DWORD unk;
    BOOL ansi;
    HWND $ret;
};

struct MS_STRUCT NtUserCreateWindowStation_params
{
    void *$handle;
    OBJECT_ATTRIBUTES *attr;
    ACCESS_MASK mask;
    ULONG arg3;
    ULONG arg4;
    ULONG arg5;
    ULONG arg6;
    ULONG arg7;
    HWINSTA $ret;
};

struct MS_STRUCT NtUserDeferWindowPosAndBand_params
{
    void *$handle;
    HDWP hdwp;
    HWND hwnd;
    HWND after;
    INT x;
    INT y;
    INT cx;
    INT cy;
    UINT flags;
    UINT unk1;
    UINT unk2;
    HDWP $ret;
};

struct MS_STRUCT NtUserDeleteMenu_params
{
    void *$handle;
    HMENU menu;
    UINT id;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserDestroyAcceleratorTable_params
{
    void *$handle;
    HACCEL handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserDestroyCursor_params
{
    void *$handle;
    HCURSOR cursor;
    ULONG arg;
    BOOL $ret;
};

struct MS_STRUCT NtUserDestroyInputContext_params
{
    void *$handle;
    HIMC handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserDestroyMenu_params
{
    void *$handle;
    HMENU menu;
    BOOL $ret;
};

struct MS_STRUCT NtUserDestroyWindow_params
{
    void *$handle;
    HWND hwnd;
    BOOL $ret;
};

struct MS_STRUCT NtUserDisableThreadIme_params
{
    void *$handle;
    DWORD thread_id;
    BOOL $ret;
};

struct MS_STRUCT NtUserDispatchMessage_params
{
    void *$handle;
    const MSG *msg;
    LRESULT $ret;
};

struct MS_STRUCT NtUserDisplayConfigGetDeviceInfo_params
{
    void *$handle;
    DISPLAYCONFIG_DEVICE_INFO_HEADER *packet;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUserDragDetect_params
{
    void *$handle;
    HWND hwnd;
    int x;
    int y;
    BOOL $ret;
};

struct MS_STRUCT NtUserDragObject_params
{
    void *$handle;
    HWND parent;
    HWND hwnd;
    UINT fmt;
    ULONG_PTR data;
    HCURSOR cursor;
    DWORD $ret;
};

struct MS_STRUCT NtUserDrawCaptionTemp_params
{
    void *$handle;
    HWND hwnd;
    HDC hdc;
    const RECT *rect;
    HFONT font;
    HICON icon;
    const WCHAR *str;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserDrawIconEx_params
{
    void *$handle;
    HDC hdc;
    INT x0;
    INT y0;
    HICON icon;
    INT width;
    INT height;
    UINT istep;
    HBRUSH hbr;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserDrawMenuBarTemp_params
{
    void *$handle;
    HWND hwnd;
    HDC hdc;
    RECT *rect;
    HMENU handle;
    HFONT font;
    DWORD $ret;
};

struct MS_STRUCT NtUserEmptyClipboard_params
{
    void *$handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserEnableMenuItem_params
{
    void *$handle;
    HMENU handle;
    UINT id;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserEnableMouseInPointer_params
{
    void *$handle;
    BOOL $0;
    BOOL $ret;
};

struct MS_STRUCT NtUserEnableScrollBar_params
{
    void *$handle;
    HWND hwnd;
    UINT bar;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserEndDeferWindowPosEx_params
{
    void *$handle;
    HDWP hdwp;
    BOOL async;
    BOOL $ret;
};

struct MS_STRUCT NtUserEndMenu_params
{
    void *$handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserEndPaint_params
{
    void *$handle;
    HWND hwnd;
    const PAINTSTRUCT *ps;
    BOOL $ret;
};

struct MS_STRUCT NtUserEnumDisplayDevices_params
{
    void *$handle;
    UNICODE_STRING *device;
    DWORD index;
    DISPLAY_DEVICEW *info;
    DWORD flags;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUserEnumDisplayMonitors_params
{
    void *$handle;
    HDC hdc;
    RECT *rect;
    MONITORENUMPROC proc;
    LPARAM lp;
    BOOL $ret;
};

struct MS_STRUCT NtUserEnumDisplaySettings_params
{
    void *$handle;
    UNICODE_STRING *device;
    DWORD mode;
    DEVMODEW *dev_mode;
    DWORD flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserExcludeUpdateRgn_params
{
    void *$handle;
    HDC hdc;
    HWND hwnd;
    INT $ret;
};

struct MS_STRUCT NtUserFindExistingCursorIcon_params
{
    void *$handle;
    UNICODE_STRING *module;
    UNICODE_STRING *res_name;
    void *desc;
    HICON $ret;
};

struct MS_STRUCT NtUserFindWindowEx_params
{
    void *$handle;
    HWND parent;
    HWND child;
    UNICODE_STRING *class;
    UNICODE_STRING *title;
    ULONG unk;
    HWND $ret;
};

struct MS_STRUCT NtUserFlashWindowEx_params
{
    void *$handle;
    FLASHWINFO *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetAncestor_params
{
    void *$handle;
    HWND hwnd;
    UINT type;
    HWND $ret;
};

struct MS_STRUCT NtUserGetAsyncKeyState_params
{
    void *$handle;
    INT key;
    SHORT $ret;
};

struct MS_STRUCT NtUserGetAtomName_params
{
    void *$handle;
    ATOM atom;
    UNICODE_STRING *name;
    ULONG $ret;
};

struct MS_STRUCT NtUserGetCaretBlinkTime_params
{
    void *$handle;
    UINT $ret;
};

struct MS_STRUCT NtUserGetCaretPos_params
{
    void *$handle;
    POINT *point;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetClassInfoEx_params
{
    void *$handle;
    HINSTANCE instance;
    UNICODE_STRING *name;
    WNDCLASSEXW *wc;
    struct client_menu_name *menu_name;
    BOOL ansi;
    ATOM $ret;
};

struct MS_STRUCT NtUserGetClassName_params
{
    void *$handle;
    HWND hwnd;
    BOOL real;
    UNICODE_STRING *name;
    INT $ret;
};

struct MS_STRUCT NtUserGetClipboardData_params
{
    void *$handle;
    UINT format;
    struct get_clipboard_params *params;
    HANDLE $ret;
};

struct MS_STRUCT NtUserGetClipboardFormatName_params
{
    void *$handle;
    UINT format;
    WCHAR *buffer;
    INT maxlen;
    INT $ret;
};

struct MS_STRUCT NtUserGetClipboardOwner_params
{
    void *$handle;
    HWND $ret;
};

struct MS_STRUCT NtUserGetClipboardSequenceNumber_params
{
    void *$handle;
    DWORD $ret;
};

struct MS_STRUCT NtUserGetClipboardViewer_params
{
    void *$handle;
    HWND $ret;
};

struct MS_STRUCT NtUserGetCursor_params
{
    void *$handle;
    HCURSOR $ret;
};

struct MS_STRUCT NtUserGetCursorFrameInfo_params
{
    void *$handle;
    HCURSOR hCursor;
    DWORD istep;
    DWORD *rate_jiffies;
    DWORD *num_steps;
    HCURSOR $ret;
};

struct MS_STRUCT NtUserGetCursorInfo_params
{
    void *$handle;
    CURSORINFO *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetDC_params
{
    void *$handle;
    HWND hwnd;
    HDC $ret;
};

struct MS_STRUCT NtUserGetDCEx_params
{
    void *$handle;
    HWND hwnd;
    HRGN clip_rgn;
    DWORD flags;
    HDC $ret;
};

struct MS_STRUCT NtUserGetDisplayConfigBufferSizes_params
{
    void *$handle;
    UINT32 flags;
    UINT32 *num_path_info;
    UINT32 *num_mode_info;
    LONG $ret;
};

struct MS_STRUCT NtUserGetDoubleClickTime_params
{
    void *$handle;
    UINT $ret;
};

struct MS_STRUCT NtUserGetDpiForMonitor_params
{
    void *$handle;
    HMONITOR monitor;
    UINT type;
    UINT *x;
    UINT *y;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetForegroundWindow_params
{
    void *$handle;
    HWND $ret;
};

struct MS_STRUCT NtUserGetGUIThreadInfo_params
{
    void *$handle;
    DWORD id;
    GUITHREADINFO *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetIconInfo_params
{
    void *$handle;
    HICON icon;
    ICONINFO *info;
    UNICODE_STRING *module;
    UNICODE_STRING *res_name;
    DWORD *bpp;
    LONG unk;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetIconSize_params
{
    void *$handle;
    HICON handle;
    UINT step;
    LONG *width;
    LONG *height;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetInternalWindowPos_params
{
    void *$handle;
    HWND hwnd;
    RECT *rect;
    POINT *pt;
    UINT $ret;
};

struct MS_STRUCT NtUserGetKeyNameText_params
{
    void *$handle;
    LONG lparam;
    WCHAR *buffer;
    INT size;
    INT $ret;
};

struct MS_STRUCT NtUserGetKeyState_params
{
    void *$handle;
    INT vkey;
    SHORT $ret;
};

struct MS_STRUCT NtUserGetKeyboardLayout_params
{
    void *$handle;
    DWORD thread_id;
    HKL $ret;
};

struct MS_STRUCT NtUserGetKeyboardLayoutList_params
{
    void *$handle;
    INT size;
    HKL *layouts;
    UINT $ret;
};

struct MS_STRUCT NtUserGetKeyboardLayoutName_params
{
    void *$handle;
    WCHAR *name;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetKeyboardState_params
{
    void *$handle;
    BYTE *state;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetLayeredWindowAttributes_params
{
    void *$handle;
    HWND hwnd;
    COLORREF *key;
    BYTE *alpha;
    DWORD *flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetMenuBarInfo_params
{
    void *$handle;
    HWND hwnd;
    LONG id;
    LONG item;
    MENUBARINFO *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetMenuItemRect_params
{
    void *$handle;
    HWND hwnd;
    HMENU menu;
    UINT item;
    RECT *rect;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetMessage_params
{
    void *$handle;
    MSG *msg;
    HWND hwnd;
    UINT first;
    UINT last;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetMouseMovePointsEx_params
{
    void *$handle;
    UINT size;
    MOUSEMOVEPOINT *ptin;
    MOUSEMOVEPOINT *ptout;
    int count;
    DWORD resolution;
    int $ret;
};

struct MS_STRUCT NtUserGetObjectInformation_params
{
    void *$handle;
    HANDLE handle;
    INT index;
    void *info;
    DWORD len;
    DWORD *needed;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetOpenClipboardWindow_params
{
    void *$handle;
    HWND $ret;
};

struct MS_STRUCT NtUserGetPointerInfoList_params
{
    void *$handle;
    UINT32 id;
    POINTER_INPUT_TYPE type;
    UINT_PTR $2;
    UINT_PTR $3;
    SIZE_T size;
    UINT32 *entry_count;
    UINT32 *pointer_count;
    void *pointer_info;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetPriorityClipboardFormat_params
{
    void *$handle;
    UINT *list;
    INT count;
    INT $ret;
};

struct MS_STRUCT NtUserGetProcessDpiAwarenessContext_params
{
    void *$handle;
    HANDLE process;
    ULONG $ret;
};

struct MS_STRUCT NtUserGetProcessWindowStation_params
{
    void *$handle;
    HWINSTA $ret;
};

struct MS_STRUCT NtUserGetProp_params
{
    void *$handle;
    HWND hwnd;
    const WCHAR *str;
    HANDLE $ret;
};

struct MS_STRUCT NtUserGetQueueStatus_params
{
    void *$handle;
    UINT flags;
    DWORD $ret;
};

struct MS_STRUCT NtUserGetRawInputBuffer_params
{
    void *$handle;
    RAWINPUT *data;
    UINT *data_size;
    UINT header_size;
    UINT $ret;
};

struct MS_STRUCT NtUserGetRawInputData_params
{
    void *$handle;
    HRAWINPUT rawinput;
    UINT command;
    void *data;
    UINT *data_size;
    UINT header_size;
    UINT $ret;
};

struct MS_STRUCT NtUserGetRawInputDeviceInfo_params
{
    void *$handle;
    HANDLE handle;
    UINT command;
    void *data;
    UINT *data_size;
    UINT $ret;
};

struct MS_STRUCT NtUserGetRawInputDeviceList_params
{
    void *$handle;
    RAWINPUTDEVICELIST *devices;
    UINT *device_count;
    UINT size;
    UINT $ret;
};

struct MS_STRUCT NtUserGetRegisteredRawInputDevices_params
{
    void *$handle;
    RAWINPUTDEVICE *devices;
    UINT *device_count;
    UINT size;
    UINT $ret;
};

struct MS_STRUCT NtUserGetScrollBarInfo_params
{
    void *$handle;
    HWND hwnd;
    LONG id;
    SCROLLBARINFO *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetSystemDpiForProcess_params
{
    void *$handle;
    HANDLE process;
    ULONG $ret;
};

struct MS_STRUCT NtUserGetSystemMenu_params
{
    void *$handle;
    HWND hwnd;
    BOOL revert;
    HMENU $ret;
};

struct MS_STRUCT NtUserGetThreadDesktop_params
{
    void *$handle;
    DWORD thread;
    HDESK $ret;
};

struct MS_STRUCT NtUserGetTitleBarInfo_params
{
    void *$handle;
    HWND hwnd;
    TITLEBARINFO *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetUpdateRect_params
{
    void *$handle;
    HWND hwnd;
    RECT *rect;
    BOOL erase;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetUpdateRgn_params
{
    void *$handle;
    HWND hwnd;
    HRGN hrgn;
    BOOL erase;
    INT $ret;
};

struct MS_STRUCT NtUserGetUpdatedClipboardFormats_params
{
    void *$handle;
    UINT *formats;
    UINT size;
    UINT *out_size;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetWindowDC_params
{
    void *$handle;
    HWND hwnd;
    HDC $ret;
};

struct MS_STRUCT NtUserGetWindowPlacement_params
{
    void *$handle;
    HWND hwnd;
    WINDOWPLACEMENT *placement;
    BOOL $ret;
};

struct MS_STRUCT NtUserGetWindowRgnEx_params
{
    void *$handle;
    HWND hwnd;
    HRGN hrgn;
    UINT unk;
    int $ret;
};

struct MS_STRUCT NtUserHideCaret_params
{
    void *$handle;
    HWND hwnd;
    BOOL $ret;
};

struct MS_STRUCT NtUserHiliteMenuItem_params
{
    void *$handle;
    HWND hwnd;
    HMENU handle;
    UINT item;
    UINT hilite;
    BOOL $ret;
};

struct MS_STRUCT NtUserInitializeClientPfnArrays_params
{
    void *$handle;
    const struct user_client_procs *client_procsA;
    const struct user_client_procs *client_procsW;
    const void *client_workers;
    HINSTANCE user_module;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUserInternalGetWindowIcon_params
{
    void *$handle;
    HWND hwnd;
    UINT type;
    HICON $ret;
};

struct MS_STRUCT NtUserInternalGetWindowText_params
{
    void *$handle;
    HWND hwnd;
    WCHAR *text;
    INT count;
    INT $ret;
};

struct MS_STRUCT NtUserInvalidateRect_params
{
    void *$handle;
    HWND hwnd;
    const RECT *rect;
    BOOL erase;
    BOOL $ret;
};

struct MS_STRUCT NtUserInvalidateRgn_params
{
    void *$handle;
    HWND hwnd;
    HRGN hrgn;
    BOOL erase;
    BOOL $ret;
};

struct MS_STRUCT NtUserIsClipboardFormatAvailable_params
{
    void *$handle;
    UINT format;
    BOOL $ret;
};

struct MS_STRUCT NtUserIsMouseInPointerEnabled_params
{
    void *$handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserKillTimer_params
{
    void *$handle;
    HWND hwnd;
    UINT_PTR id;
    BOOL $ret;
};

struct MS_STRUCT NtUserLockWindowUpdate_params
{
    void *$handle;
    HWND hwnd;
    BOOL $ret;
};

struct MS_STRUCT NtUserLogicalToPerMonitorDPIPhysicalPoint_params
{
    void *$handle;
    HWND hwnd;
    POINT *pt;
    BOOL $ret;
};

struct MS_STRUCT NtUserMapVirtualKeyEx_params
{
    void *$handle;
    UINT code;
    UINT type;
    HKL layout;
    UINT $ret;
};

struct MS_STRUCT NtUserMenuItemFromPoint_params
{
    void *$handle;
    HWND hwnd;
    HMENU handle;
    int x;
    int y;
    INT $ret;
};

struct MS_STRUCT NtUserMessageCall_params
{
    void *$handle;
    HWND hwnd;
    UINT msg;
    WPARAM wparam;
    LPARAM lparam;
    void *result_info;
    DWORD type;
    BOOL ansi;
    LRESULT $ret;
};

struct MS_STRUCT NtUserMoveWindow_params
{
    void *$handle;
    HWND hwnd;
    INT x;
    INT y;
    INT cx;
    INT cy;
    BOOL repaint;
    BOOL $ret;
};

struct MS_STRUCT NtUserMsgWaitForMultipleObjectsEx_params
{
    void *$handle;
    DWORD count;
    const HANDLE *handles;
    DWORD timeout;
    DWORD mask;
    DWORD flags;
    DWORD $ret;
};

struct MS_STRUCT NtUserNotifyIMEStatus_params
{
    void *$handle;
    HWND hwnd; 
    UINT status;
};

struct MS_STRUCT NtUserNotifyWinEvent_params
{
    void *$handle;
    DWORD event;
    HWND hwnd;
    LONG object_id;
    LONG child_id;
};

struct MS_STRUCT NtUserOpenClipboard_params
{
    void *$handle;
    HWND hwnd;
    ULONG unk;
    BOOL $ret;
};

struct MS_STRUCT NtUserOpenDesktop_params
{
    void *$handle;
    OBJECT_ATTRIBUTES *attr;
    DWORD flags;
    ACCESS_MASK access;
    HDESK $ret;
};

struct MS_STRUCT NtUserOpenInputDesktop_params
{
    void *$handle;
    DWORD flags;
    BOOL inherit;
    ACCESS_MASK access;
    HDESK $ret;
};

struct MS_STRUCT NtUserOpenWindowStation_params
{
    void *$handle;
    OBJECT_ATTRIBUTES *attr;
    ACCESS_MASK access;
    HWINSTA $ret;
};

struct MS_STRUCT NtUserPeekMessage_params
{
    void *$handle;
    MSG *msg_out;
    HWND hwnd;
    UINT first;
    UINT last;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserPerMonitorDPIPhysicalToLogicalPoint_params
{
    void *$handle;
    HWND hwnd;
    POINT *pt;
    BOOL $ret;
};

struct MS_STRUCT NtUserPostMessage_params
{
    void *$handle;
    HWND hwnd;
    UINT msg;
    WPARAM wparam;
    LPARAM lparam;
    BOOL $ret;
};

struct MS_STRUCT NtUserPostThreadMessage_params
{
    void *$handle;
    DWORD thread;
    UINT msg;
    WPARAM wparam;
    LPARAM lparam;
    BOOL $ret;
};

struct MS_STRUCT NtUserPrintWindow_params
{
    void *$handle;
    HWND hwnd;
    HDC hdc;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserQueryDisplayConfig_params
{
    void *$handle;
    UINT32 flags; 
    UINT32 *paths_count; 
    DISPLAYCONFIG_PATH_INFO *paths;
    UINT32 *modes_count;
    DISPLAYCONFIG_MODE_INFO *modes;
    DISPLAYCONFIG_TOPOLOGY_ID *topology_id;
    LONG $ret;
};

struct MS_STRUCT NtUserQueryInputContext_params
{
    void *$handle;
    HIMC handle;
    UINT attr;
    UINT_PTR $ret;
};

struct MS_STRUCT NtUserRealChildWindowFromPoint_params
{
    void *$handle;
    HWND parent;
    LONG x;
    LONG y;
    HWND $ret;
};

struct MS_STRUCT NtUserRedrawWindow_params
{
    void *$handle;
    HWND hwnd;
    const RECT *rect;
    HRGN hrgn;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserRegisterClassExWOW_params
{
    void *$handle;
    const WNDCLASSEXW *wc;
    UNICODE_STRING *name;
    UNICODE_STRING *version;
    struct client_menu_name *client_menu_name;
    DWORD fnid;
    DWORD flags;
    DWORD *wow;
    ATOM $ret;
};

struct MS_STRUCT NtUserRegisterHotKey_params
{
    void *$handle;
    HWND hwnd;
    INT id;
    UINT modifiers;
    UINT vk;
    BOOL $ret;
};

struct MS_STRUCT NtUserRegisterRawInputDevices_params
{
    void *$handle;
    const RAWINPUTDEVICE *devices;
    UINT device_count;
    UINT size;
    BOOL $ret;
};

struct MS_STRUCT NtUserReleaseDC_params
{
    void *$handle;
    HWND hwnd;
    HDC hdc;
    INT $ret;
};

struct MS_STRUCT NtUserRemoveClipboardFormatListener_params
{
    void *$handle;
    HWND hwnd;
    BOOL $ret;
};

struct MS_STRUCT NtUserRemoveMenu_params
{
    void *$handle;
    HMENU menu;
    UINT id;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserRemoveProp_params
{
    void *$handle;
    HWND hwnd;
    const WCHAR *str;
    HANDLE $ret;
};

struct MS_STRUCT NtUserScrollDC_params
{
    void *$handle;
    HDC hdc;
    INT dx;
    INT dy;
    const RECT *scroll;
    const RECT *clip;
    HRGN ret_update_rgn;
    RECT *update_rect;
    BOOL $ret;
};

struct MS_STRUCT NtUserScrollWindowEx_params
{
    void *$handle;
    HWND hwnd;
    INT dx;
    INT dy;
    const RECT *rect;
    const RECT *clip_rect;
    HRGN update_rgn;
    RECT *update_rect;
    UINT flags;
    INT $ret;
};

struct MS_STRUCT NtUserSelectPalette_params
{
    void *$handle;
    HDC hdc;
    HPALETTE palette;
    WORD force_background;
    HPALETTE $ret;
};

struct MS_STRUCT NtUserSendInput_params
{
    void *$handle;
    UINT count;
    INPUT *inputs;
    int size;
    UINT $ret;
};

struct MS_STRUCT NtUserSetActiveWindow_params
{
    void *$handle;
    HWND hwnd;
    HWND $ret;
};

struct MS_STRUCT NtUserSetCapture_params
{
    void *$handle;
    HWND hwnd;
    HWND $ret;
};

struct MS_STRUCT NtUserSetClassLong_params
{
    void *$handle;
    HWND hwnd;
    INT offset;
    LONG newval;
    BOOL ansi;
    DWORD $ret;
};

struct MS_STRUCT NtUserSetClassLongPtr_params
{
    void *$handle;
    HWND hwnd;
    INT offset;
    LONG_PTR newval;
    BOOL ansi;
    ULONG_PTR $ret;
};

struct MS_STRUCT NtUserSetClassWord_params
{
    void *$handle;
    HWND hwnd;
    INT offset;
    WORD newval;
    WORD $ret;
};

struct MS_STRUCT NtUserSetClipboardData_params
{
    void *$handle;
    UINT format;
    HANDLE handle;
    struct set_clipboard_params *params;
    NTSTATUS $ret;
};

struct MS_STRUCT NtUserSetClipboardViewer_params
{
    void *$handle;
    HWND hwnd;
    HWND $ret;
};

struct MS_STRUCT NtUserSetCursor_params
{
    void *$handle;
    HCURSOR cursor;
    HCURSOR $ret;
};

struct MS_STRUCT NtUserSetCursorIconData_params
{
    void *$handle;
    HCURSOR cursor;
    UNICODE_STRING *module;
    UNICODE_STRING *res_name;
    struct cursoricon_desc *desc;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetCursorPos_params
{
    void *$handle;
    INT x;
    INT y;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetFocus_params
{
    void *$handle;
    HWND hwnd;
    HWND $ret;
};

struct MS_STRUCT NtUserSetInternalWindowPos_params
{
    void *$handle;
    HWND hwnd;
    UINT cmd;
    RECT *rect;
    POINT *pt;
};

struct MS_STRUCT NtUserSetKeyboardState_params
{
    void *$handle;
    BYTE *state;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetLayeredWindowAttributes_params
{
    void *$handle;
    HWND hwnd;
    COLORREF key;
    BYTE alpha;
    DWORD flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetMenu_params
{
    void *$handle;
    HWND hwnd;
    HMENU menu;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetMenuContextHelpId_params
{
    void *$handle;
    HMENU handle;
    DWORD id;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetMenuDefaultItem_params
{
    void *$handle;
    HMENU handle;
    UINT item;
    UINT bypos;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetObjectInformation_params
{
    void *$handle;
    HANDLE handle;
    INT index;
    void *info;
    DWORD len;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetParent_params
{
    void *$handle;
    HWND hwnd;
    HWND parent;
    HWND $ret;
};

struct MS_STRUCT NtUserSetProcessDpiAwarenessContext_params
{
    void *$handle;
    ULONG awareness;
    ULONG unknown;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetProcessWindowStation_params
{
    void *$handle;
    HWINSTA handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetProp_params
{
    void *$handle;
    HWND hwnd;
    const WCHAR *str;
    HANDLE handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetScrollInfo_params
{
    void *$handle;
    HWND hwnd;
    INT bar;
    const SCROLLINFO *info;
    BOOL redraw;
    INT $ret;
};

struct MS_STRUCT NtUserSetShellWindowEx_params
{
    void *$handle;
    HWND shell;
    HWND list_view;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetSysColors_params
{
    void *$handle;
    INT count;
    const INT *colors;
    const COLORREF *values;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetSystemMenu_params
{
    void *$handle;
    HWND hwnd;
    HMENU menu;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetSystemTimer_params
{
    void *$handle;
    HWND hwnd;
    UINT_PTR id;
    UINT timeout;
    UINT_PTR $ret;
};

struct MS_STRUCT NtUserSetThreadDesktop_params
{
    void *$handle;
    HDESK handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetTimer_params
{
    void *$handle;
    HWND hwnd;
    UINT_PTR id;
    UINT timeout;
    TIMERPROC proc;
    ULONG tolerance;
    UINT_PTR $ret;
};

struct MS_STRUCT NtUserSetWinEventHook_params
{
    void *$handle;
    DWORD event_min;
    DWORD event_max;
    HMODULE inst;
    UNICODE_STRING *module;
    WINEVENTPROC proc;
    DWORD pid;
    DWORD tid;
    DWORD flags;
    HWINEVENTHOOK $ret;
};

struct MS_STRUCT NtUserSetWindowLong_params
{
    void *$handle;
    HWND hwnd;
    INT offset;
    LONG newval;
    BOOL ansi;
    LONG $ret;
};

struct MS_STRUCT NtUserSetWindowLongPtr_params
{
    void *$handle;
    HWND hwnd;
    INT offset;
    LONG_PTR newval;
    BOOL ansi;
    LONG_PTR $ret;
};

struct MS_STRUCT NtUserSetWindowPlacement_params
{
    void *$handle;
    HWND hwnd;
    const WINDOWPLACEMENT *wpl;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetWindowPos_params
{
    void *$handle;
    HWND hwnd;
    HWND after;
    INT x;
    INT y;
    INT cx;
    INT cy;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserSetWindowRgn_params
{
    void *$handle;
    HWND hwnd;
    HRGN hrgn;
    BOOL redraw;
    int $ret;
};

struct MS_STRUCT NtUserSetWindowWord_params
{
    void *$handle;
    HWND hwnd;
    INT offset;
    WORD newval;
    WORD $ret;
};

struct MS_STRUCT NtUserSetWindowsHookEx_params
{
    void *$handle;
    HINSTANCE inst;
    UNICODE_STRING *module;
    DWORD tid;
    INT id;
    HOOKPROC proc;
    BOOL ansi;
    HHOOK $ret;
};

struct MS_STRUCT NtUserShowCaret_params
{
    void *$handle;
    HWND hwnd;
    BOOL $ret;
};

struct MS_STRUCT NtUserShowCursor_params
{
    void *$handle;
    BOOL show;
    INT $ret;
};

struct MS_STRUCT NtUserShowScrollBar_params
{
    void *$handle;
    HWND hwnd;
    INT bar;
    BOOL show;
    BOOL $ret;
};

struct MS_STRUCT NtUserShowWindow_params
{
    void *$handle;
    HWND hwnd;
    INT cmd;
    BOOL $ret;
};

struct MS_STRUCT NtUserShowWindowAsync_params
{
    void *$handle;
    HWND hwnd;
    INT cmd;
    BOOL $ret;
};

struct MS_STRUCT NtUserSystemParametersInfo_params
{
    void *$handle;
    UINT action;
    UINT val;
    void *ptr;
    UINT winini;
    BOOL $ret;
};

struct MS_STRUCT NtUserSystemParametersInfoForDpi_params
{
    void *$handle;
    UINT action;
    UINT val;
    PVOID ptr;
    UINT winini;
    UINT dpi;
    BOOL $ret;
};

struct MS_STRUCT NtUserThunkedMenuInfo_params
{
    void *$handle;
    HMENU menu;
    const MENUINFO *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserThunkedMenuItemInfo_params
{
    void *$handle;
    HMENU menu;
    UINT pos;
    UINT flags;
    UINT method;
    MENUITEMINFOW *info;
    UNICODE_STRING *str;
    UINT $ret;
};

struct MS_STRUCT NtUserToUnicodeEx_params
{
    void *$handle;
    UINT virt;
    UINT scan;
    const BYTE *state;
    WCHAR *str;
    int size;
    UINT flags;
    HKL layout;
    INT $ret;
};

struct MS_STRUCT NtUserTrackMouseEvent_params
{
    void *$handle;
    TRACKMOUSEEVENT *info;
    BOOL $ret;
};

struct MS_STRUCT NtUserTrackPopupMenuEx_params
{
    void *$handle;
    HMENU handle;
    UINT flags;
    INT x;
    INT y;
    HWND hwnd;
    TPMPARAMS *params;
    BOOL $ret;
};

struct MS_STRUCT NtUserTranslateAccelerator_params
{
    void *$handle;
    HWND hwnd;
    HACCEL accel;
    MSG *msg;
    INT $ret;
};

struct MS_STRUCT NtUserTranslateMessage_params
{
    void *$handle;
    const MSG *msg;
    UINT flags;
    BOOL $ret;
};

struct MS_STRUCT NtUserUnhookWinEvent_params
{
    void *$handle;
    HWINEVENTHOOK hEventHook;
    BOOL $ret;
};

struct MS_STRUCT NtUserUnhookWindowsHookEx_params
{
    void *$handle;
    HHOOK handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserUnregisterClass_params
{
    void *$handle;
    UNICODE_STRING *name;
    HINSTANCE instance;
    struct client_menu_name *client_menu_name;
    BOOL $ret;
};

struct MS_STRUCT NtUserUnregisterHotKey_params
{
    void *$handle;
    HWND hwnd;
    INT id;
    BOOL $ret;
};

struct MS_STRUCT NtUserUpdateInputContext_params
{
    void *$handle;
    HIMC handle;
    UINT attr;
    UINT_PTR value;
    BOOL $ret;
};

struct MS_STRUCT NtUserUpdateLayeredWindow_params
{
    void *$handle;
    HWND hwnd;
    HDC hdc_dst;
    const POINT *pts_dst;
    const SIZE *size;
    HDC hdc_src;
    const POINT *pts_src;
    COLORREF key;
    const BLENDFUNCTION *blend;
    DWORD flags;
    const RECT *dirty;
    BOOL $ret;
};

struct MS_STRUCT NtUserValidateRect_params
{
    void *$handle;
    HWND hwnd;
    const RECT *rect;
    BOOL $ret;
};

struct MS_STRUCT NtUserVkKeyScanEx_params
{
    void *$handle;
    WCHAR chr;
    HKL layout;
    WORD $ret;
};

struct MS_STRUCT NtUserWaitForInputIdle_params
{
    void *$handle;
    HANDLE process;
    DWORD timeout;
    BOOL wow;
    DWORD $ret;
};

struct MS_STRUCT NtUserWaitMessage_params
{
    void *$handle;
    BOOL $ret;
};

struct MS_STRUCT NtUserWindowFromDC_params
{
    void *$handle;
    HDC hdc;
    HWND $ret;
};

struct MS_STRUCT NtUserWindowFromPoint_params
{
    void *$handle;
    LONG x;
    LONG y;
    HWND $ret;
};

struct MS_STRUCT __wine_get_file_outline_text_metric_params
{
    void *$handle;
    const WCHAR *path; 
    TEXTMETRICW *otm;
    UINT *em_square; 
    WCHAR *face_name;
    BOOL $ret;
};

struct MS_STRUCT __wine_get_icm_profile_params
{
    void *$handle;
    HDC hdc;
    BOOL allow_default;
    DWORD *size;
    WCHAR *filename;
    BOOL $ret;
};

struct MS_STRUCT __wine_send_input_params
{
    void *$handle;
    HWND hwnd; 
    const INPUT *input; 
    const RAWINPUT *rawinput;
    BOOL $ret;
};

#endif // QEMU_THUNK_H