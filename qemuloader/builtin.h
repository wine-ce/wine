#ifndef BUILTIN_H
#define BUILTIN_H

#include <signal.h>
#include "windef.h"

struct k_sigaction 
{
	void *handler;
	int flags;
	void (*restorer)(void);
	int mask[2];
};


int rt_sigprocmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset, size_t sz_sigset);
int rt_sigaction(int sig, const struct k_sigaction *restrict sa, struct k_sigaction *restrict old, size_t sz_sigset);
int munmap_and_exit(void *addr, size_t len, void* error_code);
void __restore(void);
void __restore_rt(void);

#undef assert
#define assert(x)
#define perror(x) ERR(x)
#define pthread_sigmask(how, set, oldset) sigprocmask(how, set, oldset)

#define __NR_CALLCODE 0xca11c0de

extern void* qemu_host_NtCurrentTeb;
extern void* qemu_host___qemu_switch_host_state;

#endif//BUILTIN_H