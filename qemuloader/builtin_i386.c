#ifdef __i386__
#include "wine/asm.h"

#include "builtin.h"
#include "syscall_i386.h"

#define __STRING(x) #x

#define STRING(x) __STRING(x)

#define DEFINE_SYSCALL(name)                     \
__ASM_GLOBAL_FUNC(name,                          \
     "movl %ebx, -4(%esp)                   \n\t"\
     "movl %esi, -8(%esp)                   \n\t"\
     "movl %edi, -12(%esp)                  \n\t"\
     "movl %ebp, -16(%esp)                  \n\t"\
     "movl  4(%esp), %ebx                   \n\t"\
     "movl  8(%esp), %ecx                   \n\t"\
     "movl 12(%esp), %edx                   \n\t"\
     "movl 16(%esp), %esi                   \n\t"\
     "movl 20(%esp), %edi                   \n\t"\
     "movl 24(%esp), %ebp                   \n\t"\
     "movl $" STRING(__NR_ ## name) ", %eax \n\t"\
     "int $0x80                             \n\t"\
     "movl -4(%esp),  %ebx                  \n\t"\
     "movl -8(%esp),  %esi                  \n\t"\
     "movl -12(%esp), %edi                  \n\t"\
     "movl -16(%esp), %ebp                  \n\t"\
     "ret")

__ASM_GLOBAL_FUNC(__restore,
     "popl %eax                                 \n\t"
	 "movl $" STRING(__NR_sigreturn) ", %eax    \n\t"
	 "int $0x80");

__ASM_GLOBAL_FUNC(__restore_rt,
	 "movl $" STRING(__NR_rt_sigreturn) ", %eax \n\t"
	 "int $0x80");

__ASM_GLOBAL_FUNC(munmap_and_exit,
     "movl  4(%esp), %ebx                   \n\t"
     "movl  8(%esp), %ecx                   \n\t"
     "movl  $" STRING(__NR_munmap) ", %eax  \n\t"
     "int $0x80                             \n\t"
     "movl 12(%esp), %ebx                   \n\t"
     "movl $" STRING(__NR_exit) ", %eax \n\t"
     "int $0x80");

__ASM_GLOBAL_FUNC(exit,
     "movl $" STRING(__NR_exit_group) ",%eax\n\t"
     "int $0x80");

__ASM_GLOBAL_FUNC(stat,
     "movl 4(%esp), %ecx                    \n\t"
     "movl 8(%esp), %edx                    \n\t"
     "push %ebx                             \n\t"
     "push %esi                             \n\t"
     "xorl %esi, %esi                       \n\t"
     "movl $-100, %ebx                      \n\t"
     "movl $"STRING(__NR_fstatat64)", %eax  \n\t"
     "int $0x80                             \n\t"
     "ret");

__ASM_GLOBAL_FUNC(clone,
	"push %ebp			\t\n"
	"mov %esp,%ebp		\t\n"
	"push %ebx			\t\n"
	"push %esi			\t\n"
	"push %edi			\t\n"
	"xor %eax,%eax		\t\n"
	"push $0x51			\t\n"
	"mov %gs,%ax		\t\n"
	"push $0xfffff		\t\n"
	"shr $3,%eax		\t\n"
	"push 28(%ebp)		\t\n"
	"push %eax			\t\n"
	"mov $120,%al		\t\n"
	"mov 12(%ebp),%ecx	\t\n"
	"mov 16(%ebp),%ebx	\t\n"
	"and $-16,%ecx		\t\n"
	"sub $16,%ecx		\t\n"
	"mov 20(%ebp),%edi	\t\n"
	"mov %edi,(%ecx)	\t\n"
	"mov 24(%ebp),%edx	\t\n"
	"mov %esp,%esi		\t\n"
	"mov 32(%ebp),%edi	\t\n"
	"mov 8(%ebp),%ebp	\t\n"
	"int $0x80			\t\n"
	"test %eax,%eax		\t\n"
	"jnz 1f				\t\n"
	"mov %ebp,%eax		\t\n"
	"xor %ebp,%ebp		\t\n"
	"call *%eax			\t\n"
	"mov %eax,%ebx		\t\n"
	"xor %eax,%eax		\t\n"
	"inc %eax			\t\n"
	"int $0x80			\t\n"
	"hlt				\t\n"
	"1:	 add $16,%esp	\t\n"
	"pop %edi			\t\n"
	"pop %esi			\t\n"
	"pop %ebx			\t\n"
	"pop %ebp			\t\n"
	"ret");

/***********************************************************************
 *           qemu_host_direct_call
 */
__ASM_GLOBAL_FUNC( qemu_host_direct_call,
                   "movl $"STRING(__NR_CALLCODE)", %eax  \n\t"
                   "xchgl 4(%esp), %ebx                  \n\t"
                   "int $0x80                            \n\t"
                   "movl 4(%esp), %ebx                   \n\t"
                   "ret                                  \n\t" )

DEFINE_SYSCALL(mmap);

DEFINE_SYSCALL(mprotect);

DEFINE_SYSCALL(close);

DEFINE_SYSCALL(sigaltstack);

DEFINE_SYSCALL(arch_prctl);

DEFINE_SYSCALL(rt_sigprocmask);

DEFINE_SYSCALL(rt_sigaction);

DEFINE_SYSCALL(modify_ldt);

DEFINE_SYSCALL(set_thread_area);

#ifdef QEMULOADER_STATIC

__ASM_GLOBAL_FUNC(_start,
    "xorl %ebp, %ebp                        \n\t"
    "leal 4(%esp), %edx                     \n\t"
    "movl (%esp),  %ecx                     \n\t"
    "subl $8,   %esp                        \n\t"
    "andl $-16, %esp                        \n\t"
    "movl %ecx, (%esp)                      \n\t"
    "movl %edx, 4(%esp)                     \n\t"
    "call main                              \n\t"
    "movl %eax, %ebx                        \n\t"
    "movl $" STRING(__NR_exit_group)", %eax \n\t"
    "int $0x80");
    
__attribute__((naked)) struct _TEB* WINAPI NtCurrentTeb(void)
{
    asm volatile("pushl  %%ebx                         \n\t"
                 "leal   -8(%%esp), %%ebx              \n\t"
                 "movl   %0, (%%ebx)                   \n\t"
                 "movl $"STRING(__NR_CALLCODE)", %%eax \n\t"
                 "int $0x80                            \n\t"
                 "movl   -4(%%esp), %%eax              \n\t"
                 "popl   %%ebx                         \n\t"
                 "ret"::"r"(qemu_host_NtCurrentTeb)
                 : "%eax", "%ebx");
}

#endif

#endif