#ifdef __aarch64__
#include "wine/asm.h"

#include "builtin.h"
#include "syscall_aarch64.h"

#define __STRING(x) #x

#define STRING(x) __STRING(x)

#define DEFINE_SYSCALL(name)                    \
__ASM_GLOBAL_FUNC(name,                         \
     "mov x8, #" STRING(__NR_ ## name) " \n\t"  \
     "svc #0                             \n\t"  \
     "ret")

__ASM_GLOBAL_FUNC(__restore,                     
	 "mov x8, #" STRING(__NR_rt_sigreturn) "\n\t"
	 "svc #0");

__ASM_GLOBAL_FUNC(__restore_rt,                  
	 "mov x8, #" STRING(__NR_rt_sigreturn) "\n\t"
	 "svc #0");

__ASM_GLOBAL_FUNC(munmap_and_exit,
     "mov x8, #" STRING(__NR_munmap)"\n\t"
     "svc #0                         \n\t"
     "mov x0, x2                     \n\t"
     "mov x8, #" STRING(__NR_exit)  "\n\t"
     "svc #0");

__ASM_GLOBAL_FUNC(raise,
     "mov x1, x0                     \n\t"
     "mov x8, #" STRING(__NR_gettid)"\n\t"
     "svc #0                         \n\t"
     "mov x8, #" STRING(__NR_tkill) "\n\t"
     "svc #0");

__ASM_GLOBAL_FUNC(exit,
     "mov x8, #" STRING(__NR_exit_group) "\n\t"
     "svc #0");

__ASM_GLOBAL_FUNC(stat,
     "mov x3, #0                        \n\t"
     "mov x2, x1                        \n\t"
     "mov x1, x0                        \n\t"
     "mov x0, #-100                     \n\t"
     "mov x8, #"STRING(__NR_newfstatat)"\n\t"
     "svc #0                            \n\t"
     "ret");

__ASM_GLOBAL_FUNC(clone,
	"and x1,x1,#-16 \n\t"
	"stp x0,x3,[x1,#-16]! \n\t"
	"uxtw x0,w2 \n\t"
	"mov x2,x4 \n\t"
	"mov x3,x5 \n\t"
	"mov x4,x6 \n\t"
	"mov x8,#"STRING(__NR_clone) "\n\t"
	"svc #0 \n\t"
	"cbz x0,1f \n\t"
	"ret \n\t"
    "1:	ldp x1,x0,[sp],#16 \n\t"
	"blr x1 \n\t"
	"mov x8,#"STRING(__NR_exit) "\n\t"
	"svc #0");

/***********************************************************************
 *           qemu_host_direct_call
 */
__ASM_GLOBAL_FUNC( qemu_host_direct_call,
                   "movz x8, #:abs_g0_nc:"STRING(__NR_CALLCODE)"\n\t"
                   "movk x8, #:abs_g1_nc:"STRING(__NR_CALLCODE)"\n\t"
                   "svc #0                                      \n\t"
                   "ret                                         \n\t" );

DEFINE_SYSCALL(mmap);

DEFINE_SYSCALL(mprotect);

DEFINE_SYSCALL(close);

DEFINE_SYSCALL(sigaltstack);

DEFINE_SYSCALL(rt_sigprocmask);

DEFINE_SYSCALL(rt_sigaction);

#ifdef QEMULOADER_STATIC

__ASM_GLOBAL_FUNC(_start,
    "mov x29, #0                        \n\t"
    "mov x30, #0                        \n\t"
    "add x1, sp, #8                     \n\t"
    "mov x0, sp                         \n\t"
    "and sp, x0, #-16                   \n\t"
    "ldr x0, [x0]                       \n\t"
    "bl  main                           \n\t"
    "mov x8, #" STRING(__NR_exit_group)"\n\t"
    "svc #0");

__attribute__((naked)) struct _TEB* WINAPI NtCurrentTeb(void)
{
    asm volatile("sub  x0, sp, #16                              \n\t"
                 "str  %0, [x0]                                 \n\t"
                 "movz x8, #:abs_g0_nc:"STRING(__NR_CALLCODE)"  \n\t"
                 "movk x8, #:abs_g1_nc:"STRING(__NR_CALLCODE)"  \n\t"
                 "svc  #0                                       \n\t"
                 "ldr  x0, [sp, #-8]                            \n\t"
                 "ret"::"r"(qemu_host_NtCurrentTeb) : "x0", "x8");
}

#endif

#endif