/*
 * Unix interface for loader functions
 *
 * Copyright (C) 2020 Alexandre Julliard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdalign.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <dlfcn.h>
#include <sched.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#define NONAMELESSUNION
#define NONAMELESSSTRUCT
#include "winternl.h"
#include "ddk/wdm.h"
#include "windef.h"
#include "winnt.h"
#include "winbase.h"
#include "winnls.h"
#include "winternl.h"
#include "ntgdi.h"
#include "ntuser.h"
#include "unix_private.h"
#include "wine/list.h"
#include "wine/debug.h"
#include "wine/qemuthunk.h"
#include "qemuloader.h"
#include "builtin.h"
#undef HAVE_GETAUXVAL

WINE_DEFAULT_DEBUG_CHANNEL(module);

#ifdef __i386__
WORD native_machine = IMAGE_FILE_MACHINE_I386;
#elif defined(__x86_64__)
WORD native_machine = IMAGE_FILE_MACHINE_AMD64;
#elif defined(__arm__)
WORD native_machine = IMAGE_FILE_MACHINE_ARMNT;
#elif defined(__aarch64__)
WORD native_machine = IMAGE_FILE_MACHINE_ARM64;
#else
#error "Unsupport Architecture"
#endif

void *pDbgUiRemoteBreakin = NULL;
void *pKiRaiseUserExceptionDispatcher = NULL;
void *pKiUserExceptionDispatcher = NULL;
void *pKiUserApcDispatcher = NULL;
void *pKiUserCallbackDispatcher = NULL;
void *pLdrInitializeThunk = NULL;
void *pRtlUserThreadStart = NULL;
void *p__wine_ctrl_routine = NULL;
static void *p__wine_syscall_dispatcher;
extern NTSTATUS __wine_unix_call( void *args, unixlib_entry_t func );
SYSTEM_DLL_INIT_BLOCK *pLdrSystemDllInitBlock = NULL;
PEB* peb = NULL;
struct _KUSER_SHARED_DATA *user_shared_data = (void *)0x7ffe0000;
#define DEALLOC_STACK_SIZE 0x1000

void* qemu_host_NtCurrentTeb = NULL;
void* qemu_host___qemu_switch_host_state = NULL;

static void * const syscalls[] =
{
    NtAcceptConnectPort,
    NtAccessCheck,
    NtAccessCheckAndAuditAlarm,
    NtAddAtom,
    NtAdjustGroupsToken,
    NtAdjustPrivilegesToken,
    NtAlertResumeThread,
    NtAlertThread,
    NtAlertThreadByThreadId,
    NtAllocateLocallyUniqueId,
    NtAllocateUuids,
    NtAllocateVirtualMemory,
    NtAllocateVirtualMemoryEx,
    NtAreMappedFilesTheSame,
    NtAssignProcessToJobObject,
    NtCallbackReturn,
    NtCancelIoFile,
    NtCancelIoFileEx,
    NtCancelSynchronousIoFile,
    NtCancelTimer,
    NtClearEvent,
    NtClose,
    NtCommitTransaction,
    NtCompareObjects,
    NtCompleteConnectPort,
    NtConnectPort,
    NtContinue,
    NtCreateDebugObject,
    NtCreateDirectoryObject,
    NtCreateEvent,
    NtCreateFile,
    NtCreateIoCompletion,
    NtCreateJobObject,
    NtCreateKey,
    NtCreateKeyTransacted,
    NtCreateKeyedEvent,
    NtCreateLowBoxToken,
    NtCreateMailslotFile,
    NtCreateMutant,
    NtCreateNamedPipeFile,
    NtCreatePagingFile,
    NtCreatePort,
    NtCreateSection,
    NtCreateSemaphore,
    NtCreateSymbolicLinkObject,
    NtCreateThread,
    NtCreateThreadEx,
    NtCreateTimer,
    NtCreateTransaction,
    NtCreateUserProcess,
    NtDebugActiveProcess,
    NtDebugContinue,
    NtDelayExecution,
    NtDeleteAtom,
    NtDeleteFile,
    NtDeleteKey,
    NtDeleteValueKey,
    NtDeviceIoControlFile,
    NtDisplayString,
    NtDuplicateObject,
    NtDuplicateToken,
    NtEnumerateKey,
    NtEnumerateValueKey,
    NtFilterToken,
    NtFindAtom,
    NtFlushBuffersFile,
    NtFlushInstructionCache,
    NtFlushKey,
    NtFlushProcessWriteBuffers,
    NtFlushVirtualMemory,
    NtFreeVirtualMemory,
    NtFsControlFile,
    NtGetContextThread,
    NtGetCurrentProcessorNumber,
    NtGetNextThread,
    NtGetNlsSectionPtr,
    NtGetWriteWatch,
    NtImpersonateAnonymousToken,
    NtInitializeNlsFiles,
    NtInitiatePowerAction,
    NtIsProcessInJob,
    NtListenPort,
    NtLoadDriver,
    NtLoadKey,
    NtLoadKey2,
    NtLoadKeyEx,
    NtLockFile,
    NtLockVirtualMemory,
    NtMakeTemporaryObject,
    NtMapViewOfSection,
    NtMapViewOfSectionEx,
    NtNotifyChangeDirectoryFile,
    NtNotifyChangeKey,
    NtNotifyChangeMultipleKeys,
    NtOpenDirectoryObject,
    NtOpenEvent,
    NtOpenFile,
    NtOpenIoCompletion,
    NtOpenJobObject,
    NtOpenKey,
    NtOpenKeyEx,
    NtOpenKeyTransacted,
    NtOpenKeyTransactedEx,
    NtOpenKeyedEvent,
    NtOpenMutant,
    NtOpenProcess,
    NtOpenProcessToken,
    NtOpenProcessTokenEx,
    NtOpenSection,
    NtOpenSemaphore,
    NtOpenSymbolicLinkObject,
    NtOpenThread,
    NtOpenThreadToken,
    NtOpenThreadTokenEx,
    NtOpenTimer,
    NtPowerInformation,
    NtPrivilegeCheck,
    NtProtectVirtualMemory,
    NtPulseEvent,
    NtQueryAttributesFile,
    NtQueryDefaultLocale,
    NtQueryDefaultUILanguage,
    NtQueryDirectoryFile,
    NtQueryDirectoryObject,
    NtQueryEaFile,
    NtQueryEvent,
    NtQueryFullAttributesFile,
    NtQueryInformationAtom,
    NtQueryInformationFile,
    NtQueryInformationJobObject,
    NtQueryInformationProcess,
    NtQueryInformationThread,
    NtQueryInformationToken,
    NtQueryInstallUILanguage,
    NtQueryIoCompletion,
    NtQueryKey,
    NtQueryLicenseValue,
    NtQueryMultipleValueKey,
    NtQueryMutant,
    NtQueryObject,
    NtQueryPerformanceCounter,
    NtQuerySection,
    NtQuerySecurityObject,
    NtQuerySemaphore,
    NtQuerySymbolicLinkObject,
    NtQuerySystemEnvironmentValue,
    NtQuerySystemEnvironmentValueEx,
    NtQuerySystemInformation,
    NtQuerySystemInformationEx,
    NtQuerySystemTime,
    NtQueryTimer,
    NtQueryTimerResolution,
    NtQueryValueKey,
    NtQueryVirtualMemory,
    NtQueryVolumeInformationFile,
    NtQueueApcThread,
    NtRaiseException,
    NtRaiseHardError,
    NtReadFile,
    NtReadFileScatter,
    NtReadVirtualMemory,
    NtRegisterThreadTerminatePort,
    NtReleaseKeyedEvent,
    NtReleaseMutant,
    NtReleaseSemaphore,
    NtRemoveIoCompletion,
    NtRemoveIoCompletionEx,
    NtRemoveProcessDebug,
    NtRenameKey,
    NtReplaceKey,
    NtReplyWaitReceivePort,
    NtRequestWaitReplyPort,
    NtResetEvent,
    NtResetWriteWatch,
    NtRestoreKey,
    NtResumeProcess,
    NtResumeThread,
    NtRollbackTransaction,
    NtSaveKey,
    NtSecureConnectPort,
    NtSetContextThread,
    NtSetDebugFilterState,
    NtSetDefaultLocale,
    NtSetDefaultUILanguage,
    NtSetEaFile,
    NtSetEvent,
    NtSetInformationDebugObject,
    NtSetInformationFile,
    NtSetInformationJobObject,
    NtSetInformationKey,
    NtSetInformationObject,
    NtSetInformationProcess,
    NtSetInformationThread,
    NtSetInformationToken,
    NtSetInformationVirtualMemory,
    NtSetIntervalProfile,
    NtSetIoCompletion,
    NtSetLdtEntries,
    NtSetSecurityObject,
    NtSetSystemInformation,
    NtSetSystemTime,
    NtSetThreadExecutionState,
    NtSetTimer,
    NtSetTimerResolution,
    NtSetValueKey,
    NtSetVolumeInformationFile,
    NtShutdownSystem,
    NtSignalAndWaitForSingleObject,
    NtSuspendProcess,
    NtSuspendThread,
    NtSystemDebugControl,
    NtTerminateJobObject,
    NtTerminateProcess,
    NtTerminateThread,
    NtTestAlert,
    NtTraceControl,
    NtUnloadDriver,
    NtUnloadKey,
    NtUnlockFile,
    NtUnlockVirtualMemory,
    NtUnmapViewOfSection,
    NtUnmapViewOfSectionEx,
    NtWaitForAlertByThreadId,
    NtWaitForDebugEvent,
    NtWaitForKeyedEvent,
    NtWaitForMultipleObjects,
    NtWaitForSingleObject,
#ifndef _WIN64
    NtWow64AllocateVirtualMemory64,
    NtWow64GetNativeSystemInformation,
    NtWow64IsProcessorFeaturePresent,
    NtWow64ReadVirtualMemory64,
    NtWow64WriteVirtualMemory64,
#endif
    NtWriteFile,
    NtWriteFileGather,
    NtWriteVirtualMemory,
    NtYieldExecution,
    wine_nt_to_unix_file_name,
    wine_unix_to_nt_file_name,
};

static BYTE syscall_args[ARRAY_SIZE(syscalls)];

static void * const win32u_syscalls[] =
{
    NtGdiAbortDoc,
    NtGdiAbortPath,
    NtGdiAddFontMemResourceEx,
    NtGdiAddFontResourceW,
    NtGdiAlphaBlend,
    NtGdiAngleArc,
    NtGdiArcInternal,
    NtGdiBeginPath,
    NtGdiBitBlt,
    NtGdiCloseFigure,
    NtGdiCombineRgn,
    NtGdiComputeXformCoefficients,
    NtGdiCreateBitmap,
    NtGdiCreateClientObj,
    NtGdiCreateCompatibleBitmap,
    NtGdiCreateCompatibleDC,
    NtGdiCreateDIBBrush,
    NtGdiCreateDIBSection,
    NtGdiCreateDIBitmapInternal,
    NtGdiCreateEllipticRgn,
    NtGdiCreateHalftonePalette,
    NtGdiCreateHatchBrushInternal,
    NtGdiCreateMetafileDC,
    NtGdiCreatePaletteInternal,
    NtGdiCreatePatternBrushInternal,
    NtGdiCreatePen,
    NtGdiCreateRectRgn,
    NtGdiCreateRoundRectRgn,
    NtGdiCreateSolidBrush,
    NtGdiDdDDICheckVidPnExclusiveOwnership,
    NtGdiDdDDICloseAdapter,
    NtGdiDdDDICreateDCFromMemory,
    NtGdiDdDDICreateDevice,
    NtGdiDdDDIDestroyDCFromMemory,
    NtGdiDdDDIDestroyDevice,
    NtGdiDdDDIEscape,
    NtGdiDdDDIOpenAdapterFromDeviceName,
    NtGdiDdDDIOpenAdapterFromHdc,
    NtGdiDdDDIOpenAdapterFromLuid,
    NtGdiDdDDIQueryStatistics,
    NtGdiDdDDIQueryVideoMemoryInfo,
    NtGdiDdDDISetQueuedLimit,
    NtGdiDdDDISetVidPnSourceOwner,
    NtGdiDeleteClientObj,
    NtGdiDeleteObjectApp,
    NtGdiDescribePixelFormat,
    NtGdiDoPalette,
    NtGdiDrawStream,
    NtGdiEllipse,
    NtGdiEndDoc,
    NtGdiEndPage,
    NtGdiEndPath,
    NtGdiEnumFonts,
    NtGdiEqualRgn,
    NtGdiExcludeClipRect,
    NtGdiExtCreatePen,
    NtGdiExtCreateRegion,
    NtGdiExtEscape,
    NtGdiExtFloodFill,
    NtGdiExtGetObjectW,
    NtGdiExtSelectClipRgn,
    NtGdiExtTextOutW,
    NtGdiFillPath,
    NtGdiFillRgn,
    NtGdiFlattenPath,
    NtGdiFlush,
    NtGdiFontIsLinked,
    NtGdiFrameRgn,
    NtGdiGetAndSetDCDword,
    NtGdiGetAppClipBox,
    NtGdiGetBitmapBits,
    NtGdiGetBitmapDimension,
    NtGdiGetBoundsRect,
    NtGdiGetCharABCWidthsW,
    NtGdiGetCharWidthInfo,
    NtGdiGetCharWidthW,
    NtGdiGetColorAdjustment,
    NtGdiGetDCDword,
    NtGdiGetDCObject,
    NtGdiGetDCPoint,
    NtGdiGetDIBitsInternal,
    NtGdiGetDeviceCaps,
    NtGdiGetDeviceGammaRamp,
    NtGdiGetFontData,
    NtGdiGetFontFileData,
    NtGdiGetFontFileInfo,
    NtGdiGetFontUnicodeRanges,
    NtGdiGetGlyphIndicesW,
    NtGdiGetGlyphOutline,
    NtGdiGetKerningPairs,
    NtGdiGetNearestColor,
    NtGdiGetNearestPaletteIndex,
    NtGdiGetOutlineTextMetricsInternalW,
    NtGdiGetPath,
    NtGdiGetPixel,
    NtGdiGetRandomRgn,
    NtGdiGetRasterizerCaps,
    NtGdiGetRealizationInfo,
    NtGdiGetRegionData,
    NtGdiGetRgnBox,
    NtGdiGetSpoolMessage,
    NtGdiGetSystemPaletteUse,
    NtGdiGetTextCharsetInfo,
    NtGdiGetTextExtentExW,
    NtGdiGetTextFaceW,
    NtGdiGetTextMetricsW,
    NtGdiGetTransform,
    NtGdiGradientFill,
    NtGdiHfontCreate,
    NtGdiIcmBrushInfo,
    NtGdiInitSpool,
    NtGdiIntersectClipRect,
    NtGdiInvertRgn,
    NtGdiLineTo,
    NtGdiMaskBlt,
    NtGdiModifyWorldTransform,
    NtGdiMoveTo,
    NtGdiOffsetClipRgn,
    NtGdiOffsetRgn,
    NtGdiOpenDCW,
    NtGdiPatBlt,
    NtGdiPathToRegion,
    NtGdiPlgBlt,
    NtGdiPolyDraw,
    NtGdiPolyPolyDraw,
    NtGdiPtInRegion,
    NtGdiPtVisible,
    NtGdiRectInRegion,
    NtGdiRectVisible,
    NtGdiRectangle,
    NtGdiRemoveFontMemResourceEx,
    NtGdiRemoveFontResourceW,
    NtGdiResetDC,
    NtGdiResizePalette,
    NtGdiRestoreDC,
    NtGdiRoundRect,
    NtGdiSaveDC,
    NtGdiScaleViewportExtEx,
    NtGdiScaleWindowExtEx,
    NtGdiSelectBitmap,
    NtGdiSelectBrush,
    NtGdiSelectClipPath,
    NtGdiSelectFont,
    NtGdiSelectPen,
    NtGdiSetBitmapBits,
    NtGdiSetBitmapDimension,
    NtGdiSetBoundsRect,
    NtGdiSetBrushOrg,
    NtGdiSetColorAdjustment,
    NtGdiSetDIBitsToDeviceInternal,
    NtGdiSetDeviceGammaRamp,
    NtGdiSetLayout,
    NtGdiSetMagicColors,
    NtGdiSetMetaRgn,
    NtGdiSetPixel,
    NtGdiSetPixelFormat,
    NtGdiSetRectRgn,
    NtGdiSetSystemPaletteUse,
    NtGdiSetTextJustification,
    NtGdiSetVirtualResolution,
    NtGdiStartDoc,
    NtGdiStartPage,
    NtGdiStretchBlt,
    NtGdiStretchDIBitsInternal,
    NtGdiStrokeAndFillPath,
    NtGdiStrokePath,
    NtGdiSwapBuffers,
    NtGdiTransformPoints,
    NtGdiTransparentBlt,
    NtGdiUnrealizeObject,
    NtGdiUpdateColors,
    NtGdiWidenPath,
    NtUserActivateKeyboardLayout,
    NtUserAddClipboardFormatListener,
    NtUserAssociateInputContext,
    NtUserAttachThreadInput,
    NtUserBeginPaint,
    NtUserBuildHimcList,
    NtUserBuildHwndList,
    NtUserCallHwnd,
    NtUserCallHwndParam,
    NtUserCallMsgFilter,
    NtUserCallNextHookEx,
    NtUserCallNoParam,
    NtUserCallOneParam,
    NtUserCallTwoParam,
    NtUserChangeClipboardChain,
    NtUserChangeDisplaySettings,
    NtUserCheckMenuItem,
    NtUserChildWindowFromPointEx,
    NtUserClipCursor,
    NtUserCloseClipboard,
    NtUserCloseDesktop,
    NtUserCloseWindowStation,
    NtUserCopyAcceleratorTable,
    NtUserCountClipboardFormats,
    NtUserCreateAcceleratorTable,
    NtUserCreateCaret,
    NtUserCreateDesktopEx,
    NtUserCreateInputContext,
    NtUserCreateWindowEx,
    NtUserCreateWindowStation,
    NtUserDeferWindowPosAndBand,
    NtUserDeleteMenu,
    NtUserDestroyAcceleratorTable,
    NtUserDestroyCursor,
    NtUserDestroyInputContext,
    NtUserDestroyMenu,
    NtUserDestroyWindow,
    NtUserDisableThreadIme,
    NtUserDispatchMessage,
    NtUserDisplayConfigGetDeviceInfo,
    NtUserDragDetect,
    NtUserDragObject,
    NtUserDrawCaptionTemp,
    NtUserDrawIconEx,
    NtUserDrawMenuBarTemp,
    NtUserEmptyClipboard,
    NtUserEnableMenuItem,
    NtUserEnableMouseInPointer,
    NtUserEnableScrollBar,
    NtUserEndDeferWindowPosEx,
    NtUserEndMenu,
    NtUserEndPaint,
    NtUserEnumDisplayDevices,
    NtUserEnumDisplayMonitors,
    NtUserEnumDisplaySettings,
    NtUserExcludeUpdateRgn,
    NtUserFindExistingCursorIcon,
    NtUserFindWindowEx,
    NtUserFlashWindowEx,
    NtUserGetAncestor,
    NtUserGetAsyncKeyState,
    NtUserGetAtomName,
    NtUserGetCaretBlinkTime,
    NtUserGetCaretPos,
    NtUserGetClassInfoEx,
    NtUserGetClassName,
    NtUserGetClipboardData,
    NtUserGetClipboardFormatName,
    NtUserGetClipboardOwner,
    NtUserGetClipboardSequenceNumber,
    NtUserGetClipboardViewer,
    NtUserGetCursor,
    NtUserGetCursorFrameInfo,
    NtUserGetCursorInfo,
    NtUserGetDC,
    NtUserGetDCEx,
    NtUserGetDisplayConfigBufferSizes,
    NtUserGetDoubleClickTime,
    NtUserGetDpiForMonitor,
    NtUserGetForegroundWindow,
    NtUserGetGUIThreadInfo,
    NtUserGetIconInfo,
    NtUserGetIconSize,
    NtUserGetInternalWindowPos,
    NtUserGetKeyNameText,
    NtUserGetKeyState,
    NtUserGetKeyboardLayout,
    NtUserGetKeyboardLayoutList,
    NtUserGetKeyboardLayoutName,
    NtUserGetKeyboardState,
    NtUserGetLayeredWindowAttributes,
    NtUserGetMenuBarInfo,
    NtUserGetMenuItemRect,
    NtUserGetMessage,
    NtUserGetMouseMovePointsEx,
    NtUserGetObjectInformation,
    NtUserGetOpenClipboardWindow,
    NtUserGetPointerInfoList,
    NtUserGetPriorityClipboardFormat,
    NtUserGetProcessDpiAwarenessContext,
    NtUserGetProcessWindowStation,
    NtUserGetProp,
    NtUserGetQueueStatus,
    NtUserGetRawInputBuffer,
    NtUserGetRawInputData,
    NtUserGetRawInputDeviceInfo,
    NtUserGetRawInputDeviceList,
    NtUserGetRegisteredRawInputDevices,
    NtUserGetScrollBarInfo,
    NtUserGetSystemDpiForProcess,
    NtUserGetSystemMenu,
    NtUserGetThreadDesktop,
    NtUserGetTitleBarInfo,
    NtUserGetUpdateRect,
    NtUserGetUpdateRgn,
    NtUserGetUpdatedClipboardFormats,
    NtUserGetWindowDC,
    NtUserGetWindowPlacement,
    NtUserGetWindowRgnEx,
    NtUserHideCaret,
    NtUserHiliteMenuItem,
    NtUserInitializeClientPfnArrays,
    NtUserInternalGetWindowIcon,
    NtUserInternalGetWindowText,
    NtUserInvalidateRect,
    NtUserInvalidateRgn,
    NtUserIsClipboardFormatAvailable,
    NtUserIsMouseInPointerEnabled,
    NtUserKillTimer,
    NtUserLockWindowUpdate,
    NtUserLogicalToPerMonitorDPIPhysicalPoint,
    NtUserMapVirtualKeyEx,
    NtUserMenuItemFromPoint,
    NtUserMessageCall,
    NtUserMoveWindow,
    NtUserMsgWaitForMultipleObjectsEx,
    NtUserNotifyIMEStatus,
    NtUserNotifyWinEvent,
    NtUserOpenClipboard,
    NtUserOpenDesktop,
    NtUserOpenInputDesktop,
    NtUserOpenWindowStation,
    NtUserPeekMessage,
    NtUserPerMonitorDPIPhysicalToLogicalPoint,
    NtUserPostMessage,
    NtUserPostThreadMessage,
    NtUserPrintWindow,
    NtUserQueryDisplayConfig,
    NtUserQueryInputContext,
    NtUserRealChildWindowFromPoint,
    NtUserRedrawWindow,
    NtUserRegisterClassExWOW,
    NtUserRegisterHotKey,
    NtUserRegisterRawInputDevices,
    NtUserReleaseDC,
    NtUserRemoveClipboardFormatListener,
    NtUserRemoveMenu,
    NtUserRemoveProp,
    NtUserScrollDC,
    NtUserScrollWindowEx,
    NtUserSelectPalette,
    NtUserSendInput,
    NtUserSetActiveWindow,
    NtUserSetCapture,
    NtUserSetClassLong,
    NtUserSetClassLongPtr,
    NtUserSetClassWord,
    NtUserSetClipboardData,
    NtUserSetClipboardViewer,
    NtUserSetCursor,
    NtUserSetCursorIconData,
    NtUserSetCursorPos,
    NtUserSetFocus,
    NtUserSetInternalWindowPos,
    NtUserSetKeyboardState,
    NtUserSetLayeredWindowAttributes,
    NtUserSetMenu,
    NtUserSetMenuContextHelpId,
    NtUserSetMenuDefaultItem,
    NtUserSetObjectInformation,
    NtUserSetParent,
    NtUserSetProcessDpiAwarenessContext,
    NtUserSetProcessWindowStation,
    NtUserSetProp,
    NtUserSetScrollInfo,
    NtUserSetShellWindowEx,
    NtUserSetSysColors,
    NtUserSetSystemMenu,
    NtUserSetSystemTimer,
    NtUserSetThreadDesktop,
    NtUserSetTimer,
    NtUserSetWinEventHook,
    NtUserSetWindowLong,
    NtUserSetWindowLongPtr,
    NtUserSetWindowPlacement,
    NtUserSetWindowPos,
    NtUserSetWindowRgn,
    NtUserSetWindowWord,
    NtUserSetWindowsHookEx,
    NtUserShowCaret,
    NtUserShowCursor,
    NtUserShowScrollBar,
    NtUserShowWindow,
    NtUserShowWindowAsync,
    NtUserSystemParametersInfo,
    NtUserSystemParametersInfoForDpi,
    NtUserThunkedMenuInfo,
    NtUserThunkedMenuItemInfo,
    NtUserToUnicodeEx,
    NtUserTrackMouseEvent,
    NtUserTrackPopupMenuEx,
    NtUserTranslateAccelerator,
    NtUserTranslateMessage,
    NtUserUnhookWinEvent,
    NtUserUnhookWindowsHookEx,
    NtUserUnregisterClass,
    NtUserUnregisterHotKey,
    NtUserUpdateInputContext,
    NtUserUpdateLayeredWindow,
    NtUserValidateRect,
    NtUserVkKeyScanEx,
    NtUserWaitForInputIdle,
    NtUserWaitMessage,
    NtUserWindowFromDC,
    NtUserWindowFromPoint,
    __wine_get_file_outline_text_metric,
    __wine_get_icm_profile,
    __wine_send_input
};

static BYTE win32u_syscall_args[ARRAY_SIZE(win32u_syscalls)];

SYSTEM_SERVICE_TABLE KeServiceDescriptorTable[4] =
{
    { (ULONG_PTR *)syscalls, NULL, ARRAY_SIZE(syscalls), syscall_args },
    { (ULONG_PTR *)win32u_syscalls, NULL, ARRAY_SIZE(win32u_syscalls), win32u_syscall_args },
    { NULL, NULL, 0, NULL },
    { NULL, NULL, 0, NULL },
};

static ULONG_PTR find_ordinal_export( HMODULE module, const IMAGE_EXPORT_DIRECTORY *exports, DWORD ordinal )
{
    const DWORD *functions = (const DWORD *)((BYTE *)module + exports->AddressOfFunctions);

    if (ordinal >= exports->NumberOfFunctions) return 0;
    if (!functions[ordinal]) return 0;
    return (ULONG_PTR)module + functions[ordinal];
}

static ULONG_PTR find_named_export( HMODULE module, const IMAGE_EXPORT_DIRECTORY *exports,
                                    const char *name )
{
    const WORD *ordinals = (const WORD *)((BYTE *)module + exports->AddressOfNameOrdinals);
    const DWORD *names = (const DWORD *)((BYTE *)module + exports->AddressOfNames);
    int min = 0, max = exports->NumberOfNames - 1;

    while (min <= max)
    {
        int res, pos = (min + max) / 2;
        char *ename = (char *)module + names[pos];
        if (!(res = strcmp( ename, name ))) return find_ordinal_export( module, exports, ordinals[pos] );
        if (res > 0) max = pos - 1;
        else min = pos + 1;
    }
    return 0;
}

static inline void *get_rva( void *module, ULONG_PTR addr )
{
    return (BYTE *)module + addr;
}

static const void *get_module_data_dir( HMODULE module, ULONG dir, ULONG *size )
{
    const IMAGE_NT_HEADERS *nt = get_rva( module, ((IMAGE_DOS_HEADER *)module)->e_lfanew );
    const IMAGE_DATA_DIRECTORY *data;

    if (nt->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR64_MAGIC)
        data = &((const IMAGE_NT_HEADERS64 *)nt)->OptionalHeader.DataDirectory[dir];
    else if (nt->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR32_MAGIC)
        data = &((const IMAGE_NT_HEADERS32 *)nt)->OptionalHeader.DataDirectory[dir];
    else
        return NULL;
    if (!data->VirtualAddress || !data->Size) return NULL;
    if (size) *size = data->Size;
    return get_rva( module, data->VirtualAddress );
}

static void load_ntdll_functions( HMODULE module )
{
    void **p__wine_unix_call_dispatcher;
    unixlib_handle_t *p__wine_unixlib_handle;

    const void* ntdll_exports = get_module_data_dir( module, IMAGE_FILE_EXPORT_DIRECTORY, NULL );
    assert( ntdll_exports );

#define GET_FUNC(name) \
    if (!(p##name = (void *)find_named_export( module, ntdll_exports, #name ))) \
        ERR( "%s not found\n", #name )

    GET_FUNC( __wine_unixlib_handle );
    GET_FUNC( LdrSystemDllInitBlock );
    GET_FUNC( DbgUiRemoteBreakin );
    GET_FUNC( KiRaiseUserExceptionDispatcher );
    GET_FUNC( KiUserExceptionDispatcher );
    GET_FUNC( KiUserApcDispatcher );
    GET_FUNC( KiUserCallbackDispatcher );
    GET_FUNC( LdrInitializeThunk );
    GET_FUNC( RtlUserThreadStart );
    GET_FUNC( __wine_ctrl_routine );
    GET_FUNC( __wine_syscall_dispatcher );
    GET_FUNC( __wine_unix_call_dispatcher );
    *p__wine_unix_call_dispatcher = __wine_unix_call_dispatcher;
#undef GET_FUNC
}

static void load_ntdll_wow64_functions( HMODULE module )
{
    const IMAGE_EXPORT_DIRECTORY *exports;

    exports = get_module_data_dir( module, IMAGE_FILE_EXPORT_DIRECTORY, NULL );
    // assert( exports );

    pLdrSystemDllInitBlock->ntdll_handle = (ULONG_PTR)module;

#define GET_FUNC(name) pLdrSystemDllInitBlock->p##name = find_named_export( module, exports, #name )
    GET_FUNC( KiUserApcDispatcher );
    GET_FUNC( KiUserCallbackDispatcher );
    GET_FUNC( KiUserExceptionDispatcher );
    GET_FUNC( LdrInitializeThunk );
    GET_FUNC( LdrSystemDllInitBlock );
    GET_FUNC( RtlUserThreadStart );
    GET_FUNC( RtlpFreezeTimeBias );
    GET_FUNC( RtlpQueryProcessDebugInformationRemote );
#undef GET_FUNC

    p__wine_ctrl_routine = (void *)find_named_export( module, exports, "__wine_ctrl_routine" );

    /* also set the 32-bit LdrSystemDllInitBlock */
    memcpy( (void *)(ULONG_PTR)pLdrSystemDllInitBlock->pLdrSystemDllInitBlock,
            pLdrSystemDllInitBlock, sizeof(*pLdrSystemDllInitBlock) );
}

#ifndef _WIN64
NTSTATUS NtWow64IsProcessorFeaturePresent( UINT feature )
{
    return feature < PROCESSOR_FEATURE_MAX && user_shared_data->ProcessorFeatures[feature];
}
#endif

/***********************************************************************
 *           fpux_to_fpu
 *
 * Build a standard i386 FPU context from an extended one.
 */
void fpux_to_fpu( I386_FLOATING_SAVE_AREA *fpu, const XMM_SAVE_AREA32 *fpux )
{
    unsigned int i, tag, stack_top;

    fpu->ControlWord   = fpux->ControlWord;
    fpu->StatusWord    = fpux->StatusWord;
    fpu->ErrorOffset   = fpux->ErrorOffset;
    fpu->ErrorSelector = fpux->ErrorSelector | (fpux->ErrorOpcode << 16);
    fpu->DataOffset    = fpux->DataOffset;
    fpu->DataSelector  = fpux->DataSelector;
    fpu->Cr0NpxState   = fpux->StatusWord | 0xffff0000;

    stack_top = (fpux->StatusWord >> 11) & 7;
    fpu->TagWord = 0xffff0000;
    for (i = 0; i < 8; i++)
    {
        memcpy( &fpu->RegisterArea[10 * i], &fpux->FloatRegisters[i], 10 );
        if (!(fpux->TagWord & (1 << i))) tag = 3;  /* empty */
        else
        {
            const M128A *reg = &fpux->FloatRegisters[(i - stack_top) & 7];
            if ((reg->High & 0x7fff) == 0x7fff)  /* exponent all ones */
            {
                tag = 2;  /* special */
            }
            else if (!(reg->High & 0x7fff))  /* exponent all zeroes */
            {
                if (reg->Low) tag = 2;  /* special */
                else tag = 1;  /* zero */
            }
            else
            {
                if (reg->Low >> 63) tag = 0;  /* valid */
                else tag = 2;  /* special */
            }
        }
        fpu->TagWord |= tag << (2 * i);
    }
}


/***********************************************************************
 *           fpu_to_fpux
 *
 * Fill extended i386 FPU context from standard one.
 */
void fpu_to_fpux( XMM_SAVE_AREA32 *fpux, const I386_FLOATING_SAVE_AREA *fpu )
{
    unsigned int i;

    fpux->ControlWord   = fpu->ControlWord;
    fpux->StatusWord    = fpu->StatusWord;
    fpux->ErrorOffset   = fpu->ErrorOffset;
    fpux->ErrorSelector = fpu->ErrorSelector;
    fpux->ErrorOpcode   = fpu->ErrorSelector >> 16;
    fpux->DataOffset    = fpu->DataOffset;
    fpux->DataSelector  = fpu->DataSelector;
    fpux->TagWord       = 0;
    for (i = 0; i < 8; i++)
    {
        if (((fpu->TagWord >> (i * 2)) & 3) != 3) fpux->TagWord |= 1 << i;
        memcpy( &fpux->FloatRegisters[i], &fpu->RegisterArea[10 * i], 10 );
    }
}

/***********************************************************************
 *           get_cpu_area
 *
 * cf. RtlWow64GetCurrentCpuArea
 */
void *get_cpu_area( USHORT machine )
{
    WOW64_CPURESERVED *cpu;
    ULONG align;

    if (!wow_peb) return NULL;
#ifdef _WIN64
    cpu = NtCurrentTeb()->TlsSlots[WOW64_TLS_CPURESERVED];
#else
    cpu = ULongToPtr( NtCurrentTeb64()->TlsSlots[WOW64_TLS_CPURESERVED] );
#endif
    if (cpu->Machine != machine) return NULL;
    switch (cpu->Machine)
    {
    case IMAGE_FILE_MACHINE_I386: align = TYPE_ALIGNMENT(I386_CONTEXT); break;
    case IMAGE_FILE_MACHINE_AMD64: align = TYPE_ALIGNMENT(AMD64_CONTEXT); break;
    case IMAGE_FILE_MACHINE_ARMNT: align = TYPE_ALIGNMENT(ARM_CONTEXT); break;
    case IMAGE_FILE_MACHINE_ARM64: align = TYPE_ALIGNMENT(ARM64_NT_CONTEXT); break;
    default: return NULL;
    }
    return (void *)(((ULONG_PTR)(cpu + 1) + align - 1) & ~((ULONG_PTR)align - 1));
}

static void* (*_mmap)(void*, size_t, int, int, int, off_t);
static int   (*_munmap)(void*, size_t);
static int   (*_mprotect)(void*, size_t, int);
static int   (*_madvise)(void*, size_t, int);

enum qemu_host_event_t
{
    QEMU_HOST_UNKNOWN = 0,
    QEMU_HOST_DLOPEN,
    QEMU_HOST_DLSYM,
    QEMU_HOST_DLCLOSE,
    QEMU_HOST_MMAN,
    QEMU_HOST_SIGNAL_NOTIFY,
};

struct qemu_event_context_t
{
    void* $state;
    union { enum qemu_host_event_t event; void* unused; };
};

static void* qemu_host_handle_event;
#ifndef QEMULOADER_STATIC
static int test_ab_config;
#endif

#ifndef QEMULOADER_STATIC
#include <sys/syscall.h>

static inline void qemu_host_direct_call_thunk(void* context)
{
    if (test_ab_config)
    {
        return (*(qemu_call_t*)context)(context);
    }
    qemu_host_direct_call(context);
}
#define qemu_host_direct_call(context) qemu_host_direct_call_thunk(context)

static void *mmap_syscall(void *addr, size_t length, int prot, int flags, int fd, off_t offset)
{
    return (void*)syscall(SYS_mmap, addr, length, prot, flags, fd, offset);
}

static int munmap_syscall(void *addr, size_t length)
{
    return (int)syscall(SYS_munmap, addr, length);
}
#endif

static void initialize(void)
{
#ifndef QEMULOADER_STATIC
    test_ab_config = NULL != getenv("QEMUTESTAB");
    if (test_ab_config)
    {
        _mmap = &mmap_syscall;
        _munmap = &munmap_syscall;
        _mprotect = &mprotect;
        _madvise = &madvise;
        return;
    }
#endif
    {
        qemu_host_direct_call(&qemu_host_handle_event);
        {
            struct { struct qemu_event_context_t $context; void *p_mmap, *p_munmap, *p_mprotect, *p_madvise; } _ = { { (void*)qemu_host_handle_event, { QEMU_HOST_MMAN } } };
            qemu_host_direct_call(&_);
            _mmap = _.p_mmap;
            _munmap = _.p_munmap;
            _mprotect = _.p_mprotect;
            _madvise = _.p_madvise;
        }
    }
}

void* qemu_host_dlsym(void* handle, const char* name)
{
#ifndef QEMULOADER_STATIC
    if (test_ab_config)
    {
        return dlsym(handle, name);
    }
#endif
    struct { struct qemu_event_context_t $context; void* handle; const char* name; void* $ret; } _ = { { (void*)qemu_host_handle_event, { QEMU_HOST_DLSYM }, }, handle, name };
    qemu_host_direct_call(&_);
    return _.$ret;
}

void qemu_host_signal_notify(const void* notify)
{
#ifndef QEMULOADER_STATIC
    if (test_ab_config)
    {
        return;
    }
#endif
    struct { struct qemu_event_context_t $context; const void* notify; } _ = {{ (void*)qemu_host_handle_event, { QEMU_HOST_SIGNAL_NOTIFY }, }, notify };
    qemu_host_direct_call(&_);
}

static inline void __qemu_host_init(void* user_data)
{
    struct __qemu_host_init_params _ = { qemu_host_vars->__qemu_host_init, user_data };
    qemu_host_direct_call(&_);
}

static inline void process_exit_wrapper(int status)
{
    struct process_close_server_socket_params _ = { qemu_host_vars->process_close_server_socket };
    qemu_host_direct_call(&_);
    exit( status );
}

void virtual_free_teb( TEB *teb )
{
    struct virtual_free_teb_params _ = { qemu_host_vars->virtual_free_teb, teb };
    qemu_host_direct_call(&_);
}

static NTSTATUS __qemu_host_dispatcher(int state, void *data)
{
    struct __qemu_host_dispatcher_params _ = {qemu_host_vars->__qemu_host_dispatcher, state, data};
    qemu_host_direct_call(&_);
    return _.$ret;
}


/**********************************************************************
 *           NtQueryInformationProcess  (NTDLL.@)
 */
NTSTATUS NtQueryInformationProcess( HANDLE handle, PROCESSINFOCLASS class, void *info,
                                           ULONG size, ULONG *ret_len )
{
    if (ProcessWineLdtCopy == class)
    {
        if (handle == NtCurrentProcess())
        {
#ifdef __i386__
            len = sizeof(struct ldt_copy *);
            if (size == len) *(struct ldt_copy **)info = &__wine_ldt_copy;
            else return STATUS_INFO_LENGTH_MISMATCH;
#else
            return STATUS_NOT_IMPLEMENTED;
#endif
        }
        else return STATUS_INVALID_PARAMETER;
    }
    else
    {
        struct NtQueryInformationProcess_params _ =
        {
            qemu_host_vars->NtQueryInformationProcess, handle, class, info, size, ret_len
        };
        qemu_host_call(&_);
        return _.$ret;
    }
}

/***********************************************************************
 *              invoke_user_apc
 */
static NTSTATUS invoke_user_apc( CONTEXT *context, const user_apc_t *apc, NTSTATUS status )
{
    return call_user_apc_dispatcher( context, apc->args[0], apc->args[1], apc->args[2],
                                     wine_server_get_ptr( apc->func ), status );
}

NTSTATUS NtContinue( CONTEXT *context, BOOLEAN alertable )
{
    user_apc_t apc;
    NTSTATUS status;
    if (alertable)
    {
        status = server_select( NULL, 0, SELECT_INTERRUPTIBLE | SELECT_ALERTABLE, 0, NULL, &apc );
        if (status == STATUS_USER_APC) return invoke_user_apc( context, &apc, status );
    }
    return signal_set_full_context( context );
}

/*******************************************************************
 *		NtRaiseException (NTDLL.@)
 */
NTSTATUS NtRaiseException( EXCEPTION_RECORD *rec, CONTEXT *context, BOOL first_chance )
{
    NTSTATUS status = send_debug_event( rec, context, first_chance );

    if (status == DBG_CONTINUE || status == DBG_EXCEPTION_HANDLED)
        return NtContinue( context, FALSE );

    if (first_chance) return call_user_exception_dispatcher( rec, context );

    if (rec->ExceptionFlags & EH_STACK_INVALID)
        ERR("Exception frame is not in stack limits => unable to dispatch exception.\n");
    else if (rec->ExceptionCode == STATUS_NONCONTINUABLE_EXCEPTION)
        ERR("Process attempted to continue execution after noncontinuable exception.\n");
    else
        ERR("Unhandled exception code %x flags %x addr %p\n",
                  (int)rec->ExceptionCode, (int)rec->ExceptionFlags, rec->ExceptionAddress );

    NtTerminateProcess( NtCurrentProcess(), rec->ExceptionCode );
    return STATUS_SUCCESS;
}

/***********************************************************************
 *           pthread_exit_wrapper
 */
static void pthread_exit_wrapper( int status )
{
    struct ntdll_thread_data* thread_data = ntdll_get_thread_data();
    close( thread_data->wait_fd[0] );
    close( thread_data->wait_fd[1] );
    close( thread_data->reply_fd );
    close( thread_data->request_fd );
    __qemu_host_dispatcher(QEMU_HOST_DISPATCHER_CONFIG, NULL);
    if (thread_data->exit_buf)
    {
        thread_data->cpu_data[0] = UIntToPtr(status);
        __wine_longjmp((__wine_jmp_buf*)thread_data->exit_buf, 1);
    }
#ifdef QEMULOADER_STATIC
    munmap_and_exit(NULL, 0, UIntToPtr(status));
#else
    pthread_exit( UIntToPtr(status) );
#endif
}


/***********************************************************************
 *           server_init_process_done
 */
void server_init_process_done(void)
{
    void *entry, *teb;
    unsigned int status;
    int suspend;

    /* Install signal handlers; this cannot be done earlier, since we cannot
     * send exceptions to the debugger before the create process event that
     * is sent by init_process_done */
    signal_init_process();

    /* always send the native TEB */
    if (!(teb = NtCurrentTeb64())) teb = NtCurrentTeb();

    /* Signal the parent process to continue */
    SERVER_START_REQ( init_process_done )
    {
        req->teb      = wine_server_client_ptr( teb );
        req->peb      = NtCurrentTeb64() ? NtCurrentTeb64()->Peb : wine_server_client_ptr( peb );
#ifdef __i386__
        req->ldt_copy = wine_server_client_ptr( &__wine_ldt_copy );
#endif
        status = wine_server_call( req );
        suspend = reply->suspend;
        entry = wine_server_get_ptr( reply->entry );
    }
    SERVER_END_REQ;

    assert( !status );
    signal_start_thread( entry, peb, suspend, NtCurrentTeb() );
}

/***********************************************************************
 *           start_thread
 *
 * Startup routine for a newly created thread.
 */
static DECLSPEC_NORETURN void start_thread( void *params )
{
    stack_t ss;
    void* coroutine_stack;
    TEB* teb = (TEB*)params;
    struct ntdll_thread_data *thread_data = (struct ntdll_thread_data *)&teb->GdiTebBatch;
    BOOL suspend;

    coroutine_stack = ((char*)thread_data->kernel_stack) + coroutine_stack_size;
    __qemu_host_init(coroutine_stack);
    mprotect(coroutine_stack, page_size, PROT_NONE);
    __qemu_host_dispatcher(QEMU_HOST_DISPATCHER_CONFIG, teb);
    ss.ss_sp    = get_signal_stack();
    ss.ss_size  = signal_stack_size;
    ss.ss_flags = 0;
    sigaltstack( &ss, NULL );
    server_init_thread( thread_data->start, &suspend );
    signal_start_thread( thread_data->start, thread_data->param, suspend, teb );
}

#ifndef QEMULOADER_STATIC
#include <pthread.h>

/**********************************************************************
 *           NtCurrentTeb   (NTDLL.@)
 */
TEB * NtCurrentTeb(void)
{
    struct NtCurrentTeb_params _ = {qemu_host_NtCurrentTeb};
    qemu_host_direct_call(&_);
    return _.$ret;
}


static void* start_thread_wrapper(void *params)
{
    TEB* teb = (TEB*)params;
    signal_alloc_thread( teb );
    struct ntdll_thread_data *thread_data = (struct ntdll_thread_data *)&teb->GdiTebBatch;
    {
        __wine_jmp_buf current_buf;
        char* sp = ((char*)thread_data->kernel_stack) + kernel_stack_size;
        thread_data->exit_buf = &current_buf;
        if (!__wine_setjmpex(&current_buf, NULL))
        {
            coroutine_create(teb, sp, &start_thread);
        }
    }
    {
        void* exit_code = thread_data->cpu_data[0];
        signal_free_thread(teb);
        virtual_free_teb(teb);
        pthread_exit(exit_code);
    }
    return NULL;
}

static int create_thread(void* params)
{
    int ret = 0;
    pthread_t pthread_id;
    pthread_attr_t pthread_attr;
    pthread_attr_init( &pthread_attr );
    pthread_attr_setstacksize(&pthread_attr, DEALLOC_STACK_SIZE);
    pthread_attr_setguardsize( &pthread_attr, 0 );
    pthread_attr_setscope( &pthread_attr, PTHREAD_SCOPE_SYSTEM ); /* force creating a kernel thread */
    ret = pthread_create( &pthread_id, &pthread_attr, start_thread_wrapper, params );
    pthread_attr_destroy( &pthread_attr );
    return ret;
}

#else

static int start_thread_wrapper(void *stack)
{
    TEB* teb = *(TEB**)stack;
    signal_alloc_thread( teb );
    struct ntdll_thread_data *thread_data = (struct ntdll_thread_data *)&teb->GdiTebBatch;
    {
        __wine_jmp_buf current_buf;
        char* sp = ((char*)thread_data->kernel_stack) + kernel_stack_size;
        thread_data->exit_buf = &current_buf;
        if (!__wine_setjmpex(&current_buf, NULL))
        {
            coroutine_create(teb, sp, &start_thread);
        }
    }
    if (teb == *(TEB**)stack)
    {
        void* exit_code = thread_data->cpu_data[0];
        signal_free_thread(teb);
        virtual_free_teb(teb);
        munmap_and_exit(stack, DEALLOC_STACK_SIZE, exit_code);
    }
    else
    {
        ERR("Dealloc Stack Size %d too small!\n", DEALLOC_STACK_SIZE);
        exit(-1);
    }
    return 0;
}



static int create_thread(void* params)
{
    const int flags = CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND
                            | CLONE_THREAD | CLONE_SYSVSEM | CLONE_SETTLS
                            | CLONE_PARENT_SETTID | CLONE_CHILD_CLEARTID | CLONE_DETACHED;
    int parent_id = 0;
    void** stack = (void**)mmap(NULL, DEALLOC_STACK_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
    char* sp = ((char*)stack) + DEALLOC_STACK_SIZE;
    *stack = params;
    clone(start_thread_wrapper, sp, flags, stack, &parent_id, sp, NULL);
    return 0;
}

#endif

void qemu_host_call(void* context)
{
    qemu_call_t* callee = (qemu_call_t* )context;
    for(;;)
    {
        struct __qemu_switch_host_state_params _ = { qemu_host___qemu_switch_host_state, (void**)&callee };
        qemu_host_direct_call(&_);
        if (!*callee) return;
        (**callee)((void*)callee);
        *callee = (qemu_call_t)NULL;
    }
}

static inline int get_unix_exit_code( NTSTATUS status )
{
    /* prevent a nonzero exit code to end up truncated to zero in unix */
    if (status && !(status & 0xff)) return 1;
    return status;
}

/***********************************************************************
 *              NtCreateThread   (NTDLL.@)
 */
NTSTATUS NtCreateThread( HANDLE *handle, ACCESS_MASK access, OBJECT_ATTRIBUTES *attr,
                                HANDLE process, CLIENT_ID *id, CONTEXT *ctx, INITIAL_TEB *teb,
                                BOOLEAN suspended )
{
    FIXME( "%p %d %p %p %p %p %p %d, stub!\n",
           handle, (int)access, attr, process, id, ctx, teb, suspended );
    return STATUS_NOT_IMPLEMENTED;
}

/***********************************************************************
 *           abort_thread
 */
void abort_thread( int status )
{
    sigprocmask( SIG_BLOCK, &server_block_set, NULL );
    if (InterlockedDecrement( &nb_threads ) <= 0) abort_process( status );
    signal_exit_thread( status, pthread_exit_wrapper, NtCurrentTeb() );
}

/***********************************************************************
 *           abort_process
 */
void abort_process( int status )
{
    exit( get_unix_exit_code( status ));
}


/***********************************************************************
 *           exit_thread
 */
static DECLSPEC_NORETURN void exit_thread( int status )
{
    pthread_sigmask( SIG_BLOCK, &server_block_set, NULL );

    if (InterlockedDecrement( &nb_threads ) <= 0) exit_process( status );

    signal_exit_thread( status, pthread_exit_wrapper, NtCurrentTeb() );
}


/***********************************************************************
 *           exit_process
 */
void exit_process( int status )
{
    pthread_sigmask( SIG_BLOCK, &server_block_set, NULL );
    signal_exit_thread( get_unix_exit_code( status ), process_exit_wrapper, NtCurrentTeb() );
}


/******************************************************************************
 *              NtTerminateThread  (NTDLL.@)
 */
NTSTATUS NtTerminateThread( HANDLE handle, LONG exit_code )
{
    unsigned int ret;
    BOOL self;

    SERVER_START_REQ( terminate_thread )
    {
        req->handle    = wine_server_obj_handle( handle );
        req->exit_code = exit_code;
        ret = wine_server_call( req );
        self = !ret && reply->self;
    }
    SERVER_END_REQ;

    if (self)
    {
        server_select( NULL, 0, SELECT_INTERRUPTIBLE, 0, NULL, NULL );
        exit_thread( exit_code );
    }
    return ret;
}

NTSTATUS NtSetInformationThread( HANDLE handle, THREADINFOCLASS class, const void *data, ULONG length )
{
    if (ThreadWow64Context == class)
    {
        return set_thread_wow64_context( handle, data, length );
    }
    else
    {
        struct NtSetInformationThread_params _ = {qemu_host_vars->NtSetInformationThread, handle, class, data, length};
        qemu_host_call(&_);
        return _.$ret;
    }
}

NTSTATUS NtQueryInformationThread( HANDLE handle, THREADINFOCLASS class, void *data, ULONG length, ULONG *ret_len )
{
    if (ThreadWow64Context == class)
    {
        return get_thread_wow64_context( handle, data, length );
    }
    else
    {
        struct NtQueryInformationThread_params _ = {qemu_host_vars->NtQueryInformationThread, handle, class, data, length, ret_len};
        qemu_host_call(&_);
        return _.$ret;
    }
}

NTSTATUS NtQuerySystemInformation( SYSTEM_INFORMATION_CLASS class,
                                          void *info, ULONG size, ULONG *ret_size )
{
    unsigned int ret = STATUS_SUCCESS;
    ULONG len = 0;

    TRACE( "(0x%08x,%p,0x%08x,%p)\n", class, info, (int)size, ret_size );

    switch (class)
    {
    case SystemNativeBasicInformation:  /* 114 */
        if (!is_win64) return STATUS_INVALID_INFO_CLASS;
        /* fall through */

    case SystemCpuInformation:  /* 1 */
        if (size >= (len = sizeof(cpu_info)))
        {
            if (!info) ret = STATUS_ACCESS_VIOLATION;
            else memcpy(info, &cpu_info, len);
        }
        else ret = STATUS_INFO_LENGTH_MISMATCH;
        break;
    case SystemRecommendedSharedDataAlignment:  /* 58 */
    {
        len = sizeof(DWORD);
        if (size >= len)
        {
            if (!info) ret = STATUS_ACCESS_VIOLATION;
            else
            {
#ifdef __arm__
                *((DWORD *)info) = 32;
#else
                *((DWORD *)info) = 64;
#endif
            }
        }
        else ret = STATUS_INFO_LENGTH_MISMATCH;
        break;
    }

    case SystemEmulationProcessorInformation:  /* 63 */
        if (size >= (len = sizeof(cpu_info)))
        {
            SYSTEM_CPU_INFORMATION cpu = cpu_info;
            if (is_win64)
            {
                if (cpu_info.ProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
                    cpu.ProcessorArchitecture = PROCESSOR_ARCHITECTURE_INTEL;
                else if (cpu_info.ProcessorArchitecture == PROCESSOR_ARCHITECTURE_ARM64)
                    cpu.ProcessorArchitecture = PROCESSOR_ARCHITECTURE_ARM;
            }
            memcpy(info, &cpu, len);
        }
        else ret = STATUS_INFO_LENGTH_MISMATCH;
        break;

    default:
    {
        struct NtQuerySystemInformation_params _ = {qemu_host_vars->NtQuerySystemInformation, class, info, size, ret_size};
        qemu_host_call(&_);
        return _.$ret;
    }
    }

    if (ret_size) *ret_size = len;
    return ret;
}

/******************************************************************************
 *              NtSetThreadExecutionState  (NTDLL.@)
 */
NTSTATUS NtSetThreadExecutionState( EXECUTION_STATE new_state, EXECUTION_STATE *old_state )
{
    static EXECUTION_STATE current = ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED | ES_USER_PRESENT;

    WARN( "(0x%x, %p): stub, harmless.\n", (int)new_state, old_state );
    *old_state = current;
    if (!(current & ES_CONTINUOUS) || (new_state & ES_CONTINUOUS)) current = new_state;
    return STATUS_SUCCESS;
}

/******************************************************************************
 *              NtQuerySystemInformationEx  (NTDLL.@)
 */
NTSTATUS NtQuerySystemInformationEx( SYSTEM_INFORMATION_CLASS class,
                                            void *query, ULONG query_len,
                                            void *info, ULONG size, ULONG *ret_size )
{
    if (SystemSupportedProcessorArchitectures == class)
    {
        static const DWORD supported_machines[] =
#if defined(__i386__)

        {
            IMAGE_FILE_MACHINE_I386  | 0x70000,
            IMAGE_FILE_MACHINE_ARMNT | 0x20000,
            0,
        };
#elif defined (__x86_64__)
        {
            IMAGE_FILE_MACHINE_AMD64 | 0x70000,
            IMAGE_FILE_MACHINE_ARM64 | 0x20000,
            IMAGE_FILE_MACHINE_I386  | 0x20000,
            0,
        };
#elif defined (__arm__)
        {
            IMAGE_FILE_MACHINE_ARMNT | 0x70000,
            IMAGE_FILE_MACHINE_I386  | 0x20000,
            0,
        };
#elif defined (__aarch64__)
        {
            IMAGE_FILE_MACHINE_ARM64 | 0x70000,
            IMAGE_FILE_MACHINE_AMD64 | 0x20000,
            IMAGE_FILE_MACHINE_I386  | 0x20000,
            0,
        };
#else
#error "Unsupport Architecture!"
#endif
        HANDLE process;
        USHORT machine = 0;
        ULONG len = 0;
        unsigned int ret = STATUS_NOT_IMPLEMENTED;
        if (!query || query_len < sizeof(HANDLE)) return STATUS_INVALID_PARAMETER;
        process = *(HANDLE *)query;
        if (process)
        {
            SERVER_START_REQ( get_process_info )
            {
                req->handle = wine_server_obj_handle( process );
                if (!(ret = wine_server_call( req ))) machine = reply->machine;
            }
            SERVER_END_REQ;
            if (ret) return ret;
        }
        len = sizeof(supported_machines);
        if (size < len)
        {
            return STATUS_BUFFER_TOO_SMALL;
        }
        for (UINT i = 0; i < sizeof(supported_machines) / sizeof(*supported_machines); ++i)
        {
            DWORD supported_machine = supported_machines[i];
            if (((WORD)supported_machine) == machine) supported_machine |= 0x80000;
            ((DWORD *)info)[i]  = supported_machine;
        }
        if (ret_size) *ret_size = len;
        return STATUS_SUCCESS;
    }
    else
    {
        struct NtQuerySystemInformationEx_params _ = {qemu_host_vars->NtQuerySystemInformationEx, class, query, query_len, info, size, ret_size};
        qemu_host_call(&_);
        return _.$ret;
    }
}

/******************************************************************************
 *              NtTerminateProcess  (NTDLL.@)
 */
NTSTATUS NtTerminateProcess( HANDLE handle, LONG exit_code )
{
    NTSTATUS ret;
    BOOL self;

    SERVER_START_REQ( terminate_process )
    {
        req->handle    = wine_server_obj_handle( handle );
        req->exit_code = exit_code;
        ret = wine_server_call( req );
        self = reply->self;
    }
    SERVER_END_REQ;
    if (self)
    {
        if (!handle) process_exiting = TRUE;
        else if (process_exiting) exit_process( exit_code );
        else abort_process( exit_code );
    }
    return ret;
}

static void QEMU_GUEST(create_thread)(void* context)
{
    struct MS_STRUCT
    {
        qemu_call_t $handle;
        TEB *teb;
        NTSTATUS result;
    }* _ = (typeof(_))context;
    _->result = create_thread(_->teb);
}

static void QEMU_GUEST(KeUserModeCallback)(void* context)
{
    struct MS_STRUCT
    {
        qemu_call_t $handle;
        ULONG id;
        const void* args;
        ULONG len;
        void **ret_ptr;
        ULONG *ret_len;
        NTSTATUS result;
    }* _ = (typeof(_))context;
    _->result = KeUserModeCallback(_->id, _->args, _->len, _->ret_ptr, _->ret_len);
}

static void QEMU_GUEST(call_user_apc_dispatcher)(void* context)
{
    struct MS_STRUCT
    {
        qemu_call_t $handle;
        CONTEXT *context_ptr;
        ULONG_PTR arg1, arg2, arg3;
        PNTAPCFUNC func;
        NTSTATUS status;
        NTSTATUS result;
    }* _ = (typeof(_))context;
    _->result = call_user_apc_dispatcher(_->context_ptr, _->arg1, _->arg2, _->arg3, _->func, _->status);
}

static void DECLSPEC_NORETURN QEMU_GUEST(abort_thread)(void* context)
{
    struct MS_STRUCT
    {
        qemu_call_t $handle;
        int status;
    } * _ = (typeof(_))context;
    abort_thread(_->status);
}

static void QEMU_GUEST(signal_notify)(void* context)
{
}

static qemu_guest_vars_t qemu_guest_vars =
{
    QEMU_GUEST_REGISTER_METHOD(KeUserModeCallback),
    QEMU_GUEST_REGISTER_METHOD(create_thread),
    QEMU_GUEST_REGISTER_METHOD(abort_thread),
    QEMU_GUEST_REGISTER_METHOD(call_user_apc_dispatcher),
    QEMU_GUEST_REGISTER_METHOD(signal_notify),
};

int main(int argc, char* argv[])
{
    initialize();
    argv[0] = (char*)"wine";
    qemu_host_vars = qemu_host_dlsym(RTLD_DEFAULT, "qemu_host_vars");
    qemu_host_win32u_vars = qemu_host_dlsym(RTLD_DEFAULT, "qemu_host_win32u_vars");
    if (qemu_host_vars && qemu_host_win32u_vars)
    {
        TEB* teb;
        char **envp = argv + argc + 1;
        struct MS_STRUCT
        {
            WORD emulated_machine;
            int kernel_stack_size, argc;
            char** argv, **envp;
            qemu_guest_vars_t* qemu_guest_vars;
            void *p_mmap, *p_munmap, *p_mprotect, *p_madvise;
            const void *host_signal_notify;
            TEB*  teb;
        } data0 = { current_machine, kernel_stack_size, argc, argv, envp, &qemu_guest_vars, _mmap, _munmap, _mprotect, _madvise };
        qemu_host_NtCurrentTeb = qemu_host_vars->NtCurrentTeb;
        qemu_host___qemu_switch_host_state = qemu_host_vars->__qemu_switch_host_state;
        __qemu_host_dispatcher(QEMU_HOST_DISPATCHER_INIT, &data0);
        qemu_host_signal_notify(data0.host_signal_notify);
        teb = data0.teb;
        signal_init_threading();
        signal_alloc_thread( teb );
        {
            struct MS_STRUCT
            {
                SYSTEM_SERVICE_TABLE* KeServiceDescriptorTable;
                void *p__wine_syscall_dispatcher;
                HMODULE ntdll_module, wow64_ntdll_module;
            } data2 = { KeServiceDescriptorTable, __wine_syscall_dispatcher, };
            stack_t ss;
            ss.ss_sp    = get_signal_stack();
            ss.ss_size  = signal_stack_size;
            ss.ss_flags = 0;
            sigaltstack( &ss, NULL );

            __qemu_host_dispatcher(QEMU_HOST_DISPATCHER_START, &data2);
            peb = teb->Peb;
            load_ntdll_functions(data2.ntdll_module);
#ifdef _WIN64
            if (data2.wow64_ntdll_module)
                load_ntdll_wow64_functions(data2.wow64_ntdll_module);
#endif
        }
        {
            struct syscall_info
            {
                void  *dispatcher;
                USHORT limit;
                BYTE  args[1];
            } *info = (struct syscall_info *)p__wine_syscall_dispatcher;
            info->dispatcher = __wine_syscall_dispatcher;
            memcpy( KeServiceDescriptorTable->ArgumentTable, info->args, KeServiceDescriptorTable->ServiceLimit );
        }
        server_init_process_done();
        return 0;
    }
    return -1;
}
