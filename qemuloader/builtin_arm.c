#ifdef __arm__
#include "wine/asm.h"

#include "builtin.h"
#include "syscall_arm.h"

#define __STRING(x) #x

#define STRING(x) __STRING(x)

#define DEFINE_SYSCALL(name)                    \
__ASM_GLOBAL_FUNC(name,                         \
     "mov r7, #" STRING(__NR_ ## name) " \n\t"  \
     "swi #0                             \n\t"  \
     "bx lr")

__ASM_GLOBAL_FUNC(__restore,                     
	 "mov r7, #" STRING(__NR_sigreturn)    "\n\t"
	 "swi #0");

__ASM_GLOBAL_FUNC(__restore_rt,                  
	 "mov r7, #" STRING(__NR_rt_sigreturn) "\n\t"
	 "swi #0");

__ASM_GLOBAL_FUNC(munmap_and_exit,
     "mov r7, #" STRING(__NR_munmap)"\n\t"
     "swi #0                         \n\t"
     "mov r0, r2                     \n\t"
     "mov r7, #" STRING(__NR_exit)  "\n\t"
     "swi #0");

__ASM_GLOBAL_FUNC(raise,
     "mov r1, r0                     \n\t"
     "mov r7, #" STRING(__NR_gettid)"\n\t"
     "swi #0                         \n\t"
     "mov r7, #" STRING(__NR_tkill) "\n\t"
     "swi #0");

__ASM_GLOBAL_FUNC(exit,
     "mov r7, #" STRING(__NR_exit_group) "\n\t"
     "swi #0");

__ASM_GLOBAL_FUNC(stat,
     "mov r3, #0                        \n\t"
     "mov r2, r1                        \n\t"
     "mov r1, r0                        \n\t"
     "mov r0, #-100                     \n\t"
     "mov r7, #"STRING(__NR_fstatat64) "\n\t"
     "swi #0                            \n\t"
     "bx lr");

__ASM_GLOBAL_FUNC(clone,
    "stmfd sp!,{r4,r5,r6,r7}\t\n"
    "mov r7,#"STRING(__NR_clone) "\n\t"
    "mov r6,r3				\t\n"
    "mov r5,r0				\t\n"
    "mov r0,r2				\t\n"
    "and r1,r1,#-16			\t\n"
    "ldr r2,[sp,#16]		\t\n"
    "ldr r3,[sp,#20]		\t\n"
    "ldr r4,[sp,#24]		\t\n"
    "swi #0					\t\n"
    "tst r0,r0				\t\n"
    "beq 1f					\t\n"
    "ldmfd sp!,{r4,r5,r6,r7}\t\n"
    "bx lr					\t\n"
    "1:	mov r0,r6			\t\n"
    "bl 3f					\t\n"
    "2:	mov r7,#"STRING(__NR_exit) "\n\t"
    "swi #0					\t\n"
    "b 2b					\t\n"
    "3:	bx r5				\t\n");

/***********************************************************************
 *           qemu_host_direct_call
 */
__ASM_GLOBAL_FUNC( qemu_host_direct_call,
                   "ldr r7, ="STRING(__NR_CALLCODE)"  \n\t"
                   "swi #0                            \n\t"
                   "bx lr");

DEFINE_SYSCALL(mmap);

DEFINE_SYSCALL(mprotect);

DEFINE_SYSCALL(close);

DEFINE_SYSCALL(sigaltstack);

DEFINE_SYSCALL(rt_sigprocmask);

DEFINE_SYSCALL(rt_sigaction);

#ifdef QEMULOADER_STATIC

__ASM_GLOBAL_FUNC(_start,
    "mov lr, #0                         \n\t"
    "add r1, sp, #4                     \n\t"
    "mov r0, sp                         \n\t"
    "and sp, r0, #-16                   \n\t"
    "ldr r0, [r0]                       \n\t"
    "bl  main                           \n\t"
    "mov r7, #" STRING(__NR_exit_group)"\n\t"
    "swi #0");

__attribute__((naked)) struct _TEB* WINAPI NtCurrentTeb(void)
{
    asm volatile("sub  r0, sp, #8                       \n\t"
                 "str  %0, [r0]                         \n\t"
                 "ldr r7, ="STRING(__NR_CALLCODE)"      \n\t"
                 "swi  #0                               \n\t"
                 "ldr  r0, [sp, #-4]                    \n\t"
                 "bx lr"::"r"(qemu_host_NtCurrentTeb) 
                 : "r0", "r7");
}

#endif

#endif