#ifndef QEMULOADER_H
#define QEMULOADER_H

#include "wine/qemuthunk.h"
#include "builtin.h"
extern qemu_host_vars_t* qemu_host_vars;
extern qemu_host_win32u_vars_t* qemu_host_win32u_vars;

#define cpu_info                        (*(SYSTEM_CPU_INFORMATION*)qemu_host_vars->cpu_info)
#define nb_threads                      (*(int*)qemu_host_vars->nb_threads)
#define main_image_info                 (*(SECTION_IMAGE_INFORMATION*)qemu_host_vars->main_image_info)
#define process_exiting                 (*(BOOL*)qemu_host_vars->process_exiting)
#define server_block_set                (*(sigset_t*)qemu_host_vars->server_block_set)
#define server_start_time               (*(timeout_t*)qemu_host_vars->server_start_time)
#define wow_peb                         (*(WOW_PEB**)qemu_host_vars->wow_peb)
#define xstate_compaction_enabled       (0)

#endif//QEMULOADER_H