#include <fcntl.h>           /* Definition of AT_* constants */
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <stdarg.h>
#include <memory.h>
#include <stdio.h>
#include "builtin.h"
#include "wine/qemuthunk.h"

void *memmove (void *__restrict __dest, const void *__restrict __src,
		     size_t __n)
{
    char *x = (char*)__dest, *y = (char*)__src;
    if (x < y || x >= y + __n)
    {
        for (int i = 0; i < __n; ++i)
            *x++ = *y++;
    }
    else if (x != y)
    {
        x += __n;
        y += __n;
        for (int i = 0; i < __n; ++i)
            *--x = *--y;
    }
    return __dest;
}

void *memcpy (void *__restrict __dest, const void *__restrict __src,
		     size_t __n)
{    
    char *x = (char*)__dest, *y = (char*)__src;
    if (x < y || x >= y + __n)
    {
        for (int i = 0; i < __n; ++i)
            *x++ = *y++;
    }
    else if (x != y)
    {
        x += __n;
        y += __n;
        for (int i = 0; i < __n; ++i)
            *--x = *--y;
    }
    return __dest;
}

void *memset (void *__s, int __c, size_t __n)
{
    char* x = (char*)__s;
    for (int i = 0; i < __n; ++i)
        *x++ = (char)__c;
    return __s;
}

size_t strlen(char const* str)
{
    char const* begin = str;
    for (; *str; ++str);
    return str - begin;
}

int strcmp(char const* dst, char const* src)
{
    for (; *dst && *src && (*dst == *src); ++dst, ++src);
    return *dst - *src;
}

int memcmp(const void *__s1, const void *__s2, size_t __n)
{
    const char* x = (const char*)__s1;
    const char* y = (const char*)__s2;
    for (; __n > 0 && (*x == *y); ++x, ++y, --__n);
    return *x - *y;
}

int sigprocmask(int how, const sigset_t *restrict set, sigset_t *restrict oldset)
{
    return rt_sigprocmask( how, set, oldset, _NSIG / 8);
}

int sigaction(int sig, const struct sigaction *restrict sa, struct sigaction *restrict old)
{
    struct k_sigaction ksa, ksa_old;
    if (sa) 
    {
		ksa.handler = sa->sa_handler;
		ksa.flags = sa->sa_flags;
		ksa.flags |= 0x04000000;
		ksa.restorer = (sa->sa_flags & SA_SIGINFO) ? __restore_rt : __restore;
		memcpy(&ksa.mask, &sa->sa_mask, _NSIG / 8);
	}
    int r = rt_sigaction(sig, sa ? &ksa : 0, old ? &ksa_old : 0, _NSIG / 8 );
    if (old && !r) 
    {
		old->sa_handler = ksa_old.handler;
		old->sa_flags = ksa_old.flags;
		memcpy(&old->sa_mask, &ksa_old.mask, _NSIG/8);
	}
    return r;
}