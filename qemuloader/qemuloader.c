#include <stdarg.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "winternl.h"
#include "winbase.h"
#include "rpc.h"
#include "sspi.h"
#include "ntsecapi.h"
#include "ntsecpkg.h"
#include "ntgdi.h"
#include "ntuser.h"

#include "wine/debug.h"
#include "wine/server.h"
#include "wine/unixlib.h"
#include "unix_private.h"

#include "wine/qemuthunk.h"

qemu_host_vars_t* qemu_host_vars;

qemu_host_win32u_vars_t* qemu_host_win32u_vars;

NTSTATUS WINAPI NtAcceptConnectPort(PHANDLE $0, ULONG $1, PLPC_MESSAGE $2, BOOLEAN $3, PLPC_SECTION_WRITE $4, PLPC_SECTION_READ $5)
{
    struct NtAcceptConnectPort_params _ = {qemu_host_vars->NtAcceptConnectPort, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAccessCheck(PSECURITY_DESCRIPTOR $0, HANDLE $1, ACCESS_MASK $2, PGENERIC_MAPPING $3, PPRIVILEGE_SET $4, PULONG $5, PULONG $6, NTSTATUS *$7)
{
    struct NtAccessCheck_params _ = {qemu_host_vars->NtAccessCheck, $0, $1, $2, $3, $4, $5, $6, $7};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAccessCheckAndAuditAlarm(PUNICODE_STRING $0, HANDLE $1, PUNICODE_STRING $2, PUNICODE_STRING $3, PSECURITY_DESCRIPTOR $4, ACCESS_MASK $5, PGENERIC_MAPPING $6, BOOLEAN $7, PACCESS_MASK $8, PBOOLEAN $9, PBOOLEAN $10)
{
    struct NtAccessCheckAndAuditAlarm_params _ = {qemu_host_vars->NtAccessCheckAndAuditAlarm, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAddAtom(const WCHAR *$0, ULONG $1, RTL_ATOM *$2)
{
    struct NtAddAtom_params _ = {qemu_host_vars->NtAddAtom, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAdjustGroupsToken(HANDLE $0, BOOLEAN $1, PTOKEN_GROUPS $2, ULONG $3, PTOKEN_GROUPS $4, PULONG $5)
{
    struct NtAdjustGroupsToken_params _ = {qemu_host_vars->NtAdjustGroupsToken, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAdjustPrivilegesToken(HANDLE $0, BOOLEAN $1, PTOKEN_PRIVILEGES $2, DWORD $3, PTOKEN_PRIVILEGES $4, PDWORD $5)
{
    struct NtAdjustPrivilegesToken_params _ = {qemu_host_vars->NtAdjustPrivilegesToken, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAlertResumeThread(HANDLE $0, PULONG $1)
{
    struct NtAlertResumeThread_params _ = {qemu_host_vars->NtAlertResumeThread, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAlertThread(HANDLE ThreadHandle)
{
    struct NtAlertThread_params _ = {qemu_host_vars->NtAlertThread, ThreadHandle};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAlertThreadByThreadId(HANDLE $0)
{
    struct NtAlertThreadByThreadId_params _ = {qemu_host_vars->NtAlertThreadByThreadId, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAllocateLocallyUniqueId(PLUID lpLuid)
{
    struct NtAllocateLocallyUniqueId_params _ = {qemu_host_vars->NtAllocateLocallyUniqueId, lpLuid};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAllocateUuids(PULARGE_INTEGER $0, PULONG $1, PULONG $2, PUCHAR $3)
{
    struct NtAllocateUuids_params _ = {qemu_host_vars->NtAllocateUuids, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAllocateVirtualMemory(HANDLE $0, PVOID *$1, ULONG_PTR $2, SIZE_T *$3, ULONG $4, ULONG $5)
{
    struct NtAllocateVirtualMemory_params _ = {qemu_host_vars->NtAllocateVirtualMemory, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAllocateVirtualMemoryEx(HANDLE $0, PVOID *$1, SIZE_T *$2, ULONG $3, ULONG $4, MEM_EXTENDED_PARAMETER *$5, ULONG $6)
{
    struct NtAllocateVirtualMemoryEx_params _ = {qemu_host_vars->NtAllocateVirtualMemoryEx, $0, $1, $2, $3, $4, $5, $6};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAreMappedFilesTheSame(PVOID $0, PVOID $1)
{
    struct NtAreMappedFilesTheSame_params _ = {qemu_host_vars->NtAreMappedFilesTheSame, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtAssignProcessToJobObject(HANDLE $0, HANDLE $1)
{
    struct NtAssignProcessToJobObject_params _ = {qemu_host_vars->NtAssignProcessToJobObject, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCancelIoFile(HANDLE $0, PIO_STATUS_BLOCK $1)
{
    struct NtCancelIoFile_params _ = {qemu_host_vars->NtCancelIoFile, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCancelIoFileEx(HANDLE $0, PIO_STATUS_BLOCK $1, PIO_STATUS_BLOCK $2)
{
    struct NtCancelIoFileEx_params _ = {qemu_host_vars->NtCancelIoFileEx, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCancelSynchronousIoFile(HANDLE $0, PIO_STATUS_BLOCK $1, PIO_STATUS_BLOCK $2)
{
    struct NtCancelSynchronousIoFile_params _ = {qemu_host_vars->NtCancelSynchronousIoFile, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCancelTimer(HANDLE $0, BOOLEAN *$1)
{
    struct NtCancelTimer_params _ = {qemu_host_vars->NtCancelTimer, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtClearEvent(HANDLE $0)
{
    struct NtClearEvent_params _ = {qemu_host_vars->NtClearEvent, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtClose(HANDLE $0)
{
    struct NtClose_params _ = {qemu_host_vars->NtClose, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCommitTransaction(HANDLE $0, BOOLEAN $1)
{
    struct NtCommitTransaction_params _ = {qemu_host_vars->NtCommitTransaction, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCompareObjects(HANDLE $0, HANDLE $1)
{
    struct NtCompareObjects_params _ = {qemu_host_vars->NtCompareObjects, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCompleteConnectPort(HANDLE $0)
{
    struct NtCompleteConnectPort_params _ = {qemu_host_vars->NtCompleteConnectPort, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtConnectPort(PHANDLE $0, PUNICODE_STRING $1, PSECURITY_QUALITY_OF_SERVICE $2, PLPC_SECTION_WRITE $3, PLPC_SECTION_READ $4, PULONG $5, PVOID $6, PULONG $7)
{
    struct NtConnectPort_params _ = {qemu_host_vars->NtConnectPort, $0, $1, $2, $3, $4, $5, $6, $7};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateDebugObject(HANDLE *$0, ACCESS_MASK $1, OBJECT_ATTRIBUTES *$2, ULONG $3)
{
    struct NtCreateDebugObject_params _ = {qemu_host_vars->NtCreateDebugObject, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateDirectoryObject(PHANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2)
{
    struct NtCreateDirectoryObject_params _ = {qemu_host_vars->NtCreateDirectoryObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateEvent(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, EVENT_TYPE $3, BOOLEAN $4)
{
    struct NtCreateEvent_params _ = {qemu_host_vars->NtCreateEvent, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateFile(PHANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2, PIO_STATUS_BLOCK $3, PLARGE_INTEGER $4, ULONG $5, ULONG $6, ULONG $7, ULONG $8, PVOID $9, ULONG $10)
{
    struct NtCreateFile_params _ = {qemu_host_vars->NtCreateFile, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateIoCompletion(PHANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2, ULONG $3)
{
    struct NtCreateIoCompletion_params _ = {qemu_host_vars->NtCreateIoCompletion, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateJobObject(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtCreateJobObject_params _ = {qemu_host_vars->NtCreateJobObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateKey(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, ULONG $3, const UNICODE_STRING *$4, ULONG $5, PULONG $6)
{
    struct NtCreateKey_params _ = {qemu_host_vars->NtCreateKey, $0, $1, $2, $3, $4, $5, $6};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateKeyTransacted(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, ULONG $3, const UNICODE_STRING *$4, ULONG $5, HANDLE $6, ULONG *$7)
{
    struct NtCreateKeyTransacted_params _ = {qemu_host_vars->NtCreateKeyTransacted, $0, $1, $2, $3, $4, $5, $6, $7};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateKeyedEvent(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, ULONG $3)
{
    struct NtCreateKeyedEvent_params _ = {qemu_host_vars->NtCreateKeyedEvent, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateLowBoxToken(HANDLE *$0, HANDLE $1, ACCESS_MASK $2, OBJECT_ATTRIBUTES *$3, SID *$4, ULONG $5, SID_AND_ATTRIBUTES *$6, ULONG $7, HANDLE *$8)
{
    struct NtCreateLowBoxToken_params _ = {qemu_host_vars->NtCreateLowBoxToken, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateMailslotFile(PHANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2, PIO_STATUS_BLOCK $3, ULONG $4, ULONG $5, ULONG $6, PLARGE_INTEGER $7)
{
    struct NtCreateMailslotFile_params _ = {qemu_host_vars->NtCreateMailslotFile, $0, $1, $2, $3, $4, $5, $6, $7};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateMutant(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, BOOLEAN $3)
{
    struct NtCreateMutant_params _ = {qemu_host_vars->NtCreateMutant, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateNamedPipeFile(PHANDLE $0, ULONG $1, POBJECT_ATTRIBUTES $2, PIO_STATUS_BLOCK $3, ULONG $4, ULONG $5, ULONG $6, ULONG $7, ULONG $8, ULONG $9, ULONG $10, ULONG $11, ULONG $12, PLARGE_INTEGER $13)
{
    struct NtCreateNamedPipeFile_params _ = {qemu_host_vars->NtCreateNamedPipeFile, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreatePagingFile(PUNICODE_STRING $0, PLARGE_INTEGER $1, PLARGE_INTEGER $2, PLARGE_INTEGER $3)
{
    struct NtCreatePagingFile_params _ = {qemu_host_vars->NtCreatePagingFile, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreatePort(PHANDLE $0, POBJECT_ATTRIBUTES $1, ULONG $2, ULONG $3, PULONG $4)
{
    struct NtCreatePort_params _ = {qemu_host_vars->NtCreatePort, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateSection(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, const LARGE_INTEGER *$3, ULONG $4, ULONG $5, HANDLE $6)
{
    struct NtCreateSection_params _ = {qemu_host_vars->NtCreateSection, $0, $1, $2, $3, $4, $5, $6};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateSemaphore(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, LONG $3, LONG $4)
{
    struct NtCreateSemaphore_params _ = {qemu_host_vars->NtCreateSemaphore, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateSymbolicLinkObject(PHANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2, PUNICODE_STRING $3)
{
    struct NtCreateSymbolicLinkObject_params _ = {qemu_host_vars->NtCreateSymbolicLinkObject, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateThreadEx(HANDLE *handle, ACCESS_MASK access, OBJECT_ATTRIBUTES *attr, HANDLE process, PRTL_THREAD_START_ROUTINE start, void *param, ULONG flags, ULONG_PTR zero_bits, SIZE_T stack_commit, SIZE_T stack_reserve, PS_ATTRIBUTE_LIST *attr_list)
{
    struct NtCreateThreadEx_params _ = {qemu_host_vars->NtCreateThreadEx, handle, access, attr, process, start, param, flags, zero_bits, stack_commit, stack_reserve, attr_list};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateTimer(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, TIMER_TYPE $3)
{
    struct NtCreateTimer_params _ = {qemu_host_vars->NtCreateTimer, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateTransaction(PHANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2, LPGUID $3, HANDLE $4, ULONG $5, ULONG $6, ULONG $7, PLARGE_INTEGER $8, PUNICODE_STRING $9)
{
    struct NtCreateTransaction_params _ = {qemu_host_vars->NtCreateTransaction, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtCreateUserProcess(HANDLE *$0, HANDLE *$1, ACCESS_MASK $2, ACCESS_MASK $3, OBJECT_ATTRIBUTES *$4, OBJECT_ATTRIBUTES *$5, ULONG $6, ULONG $7, RTL_USER_PROCESS_PARAMETERS *$8, PS_CREATE_INFO *$9, PS_ATTRIBUTE_LIST *$10)
{
    struct NtCreateUserProcess_params _ = {qemu_host_vars->NtCreateUserProcess, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDebugActiveProcess(HANDLE $0, HANDLE $1)
{
    struct NtDebugActiveProcess_params _ = {qemu_host_vars->NtDebugActiveProcess, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDebugContinue(HANDLE $0, CLIENT_ID *$1, NTSTATUS $2)
{
    struct NtDebugContinue_params _ = {qemu_host_vars->NtDebugContinue, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDelayExecution(BOOLEAN $0, const LARGE_INTEGER *$1)
{
    struct NtDelayExecution_params _ = {qemu_host_vars->NtDelayExecution, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDeleteAtom(RTL_ATOM $0)
{
    struct NtDeleteAtom_params _ = {qemu_host_vars->NtDeleteAtom, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDeleteFile(POBJECT_ATTRIBUTES $0)
{
    struct NtDeleteFile_params _ = {qemu_host_vars->NtDeleteFile, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDeleteKey(HANDLE $0)
{
    struct NtDeleteKey_params _ = {qemu_host_vars->NtDeleteKey, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDeleteValueKey(HANDLE $0, const UNICODE_STRING *$1)
{
    struct NtDeleteValueKey_params _ = {qemu_host_vars->NtDeleteValueKey, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDeviceIoControlFile(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, ULONG $5, PVOID $6, ULONG $7, PVOID $8, ULONG $9)
{
    struct NtDeviceIoControlFile_params _ = {qemu_host_vars->NtDeviceIoControlFile, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDisplayString(PUNICODE_STRING $0)
{
    struct NtDisplayString_params _ = {qemu_host_vars->NtDisplayString, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDuplicateObject(HANDLE $0, HANDLE $1, HANDLE $2, PHANDLE $3, ACCESS_MASK $4, ULONG $5, ULONG $6)
{
    struct NtDuplicateObject_params _ = {qemu_host_vars->NtDuplicateObject, $0, $1, $2, $3, $4, $5, $6};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtDuplicateToken(HANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2, BOOLEAN $3, TOKEN_TYPE $4, PHANDLE $5)
{
    struct NtDuplicateToken_params _ = {qemu_host_vars->NtDuplicateToken, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtEnumerateKey(HANDLE $0, ULONG $1, KEY_INFORMATION_CLASS $2, void *$3, DWORD $4, DWORD *$5)
{
    struct NtEnumerateKey_params _ = {qemu_host_vars->NtEnumerateKey, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtEnumerateValueKey(HANDLE $0, ULONG $1, KEY_VALUE_INFORMATION_CLASS $2, PVOID $3, ULONG $4, PULONG $5)
{
    struct NtEnumerateValueKey_params _ = {qemu_host_vars->NtEnumerateValueKey, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtFilterToken(HANDLE $0, ULONG $1, TOKEN_GROUPS *$2, TOKEN_PRIVILEGES *$3, TOKEN_GROUPS *$4, HANDLE *$5)
{
    struct NtFilterToken_params _ = {qemu_host_vars->NtFilterToken, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtFindAtom(const WCHAR *$0, ULONG $1, RTL_ATOM *$2)
{
    struct NtFindAtom_params _ = {qemu_host_vars->NtFindAtom, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtFlushBuffersFile(HANDLE $0, IO_STATUS_BLOCK *$1)
{
    struct NtFlushBuffersFile_params _ = {qemu_host_vars->NtFlushBuffersFile, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtFlushInstructionCache(HANDLE $0, LPCVOID $1, SIZE_T $2)
{
    struct NtFlushInstructionCache_params _ = {qemu_host_vars->NtFlushInstructionCache, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtFlushKey(HANDLE $0)
{
    struct NtFlushKey_params _ = {qemu_host_vars->NtFlushKey, $0};
    qemu_host_call(&_);
    return _.$ret;
}

void WINAPI NtFlushProcessWriteBuffers(void)
{
    struct NtFlushProcessWriteBuffers_params _ = {qemu_host_vars->NtFlushProcessWriteBuffers};
    qemu_host_call(&_);
}

NTSTATUS WINAPI NtFlushVirtualMemory(HANDLE $0, LPCVOID *$1, SIZE_T *$2, ULONG $3)
{
    struct NtFlushVirtualMemory_params _ = {qemu_host_vars->NtFlushVirtualMemory, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtFreeVirtualMemory(HANDLE $0, PVOID *$1, SIZE_T *$2, ULONG $3)
{
    struct NtFreeVirtualMemory_params _ = {qemu_host_vars->NtFreeVirtualMemory, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtFsControlFile(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, ULONG $5, PVOID $6, ULONG $7, PVOID $8, ULONG $9)
{
    struct NtFsControlFile_params _ = {qemu_host_vars->NtFsControlFile, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG WINAPI NtGetCurrentProcessorNumber(void)
{
    struct NtGetCurrentProcessorNumber_params _ = {qemu_host_vars->NtGetCurrentProcessorNumber};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGetNextThread(HANDLE $0, HANDLE $1, ACCESS_MASK $2, ULONG $3, ULONG $4, HANDLE *$5)
{
    struct NtGetNextThread_params _ = {qemu_host_vars->NtGetNextThread, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGetNlsSectionPtr(ULONG $0, ULONG $1, void *$2, void **$3, SIZE_T *$4)
{
    struct NtGetNlsSectionPtr_params _ = {qemu_host_vars->NtGetNlsSectionPtr, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGetWriteWatch(HANDLE $0, ULONG $1, PVOID $2, SIZE_T $3, PVOID *$4, ULONG_PTR *$5, ULONG *$6)
{
    struct NtGetWriteWatch_params _ = {qemu_host_vars->NtGetWriteWatch, $0, $1, $2, $3, $4, $5, $6};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtImpersonateAnonymousToken(HANDLE $0)
{
    struct NtImpersonateAnonymousToken_params _ = {qemu_host_vars->NtImpersonateAnonymousToken, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtInitializeNlsFiles(void **$0, LCID *$1, LARGE_INTEGER *$2)
{
    struct NtInitializeNlsFiles_params _ = {qemu_host_vars->NtInitializeNlsFiles, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtInitiatePowerAction(POWER_ACTION $0, SYSTEM_POWER_STATE $1, ULONG $2, BOOLEAN $3)
{
    struct NtInitiatePowerAction_params _ = {qemu_host_vars->NtInitiatePowerAction, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtIsProcessInJob(HANDLE $0, HANDLE $1)
{
    struct NtIsProcessInJob_params _ = {qemu_host_vars->NtIsProcessInJob, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtListenPort(HANDLE $0, PLPC_MESSAGE $1)
{
    struct NtListenPort_params _ = {qemu_host_vars->NtListenPort, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtLoadDriver(const UNICODE_STRING *$0)
{
    struct NtLoadDriver_params _ = {qemu_host_vars->NtLoadDriver, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtLoadKey(const OBJECT_ATTRIBUTES *$0, OBJECT_ATTRIBUTES *$1)
{
    struct NtLoadKey_params _ = {qemu_host_vars->NtLoadKey, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtLoadKey2(const OBJECT_ATTRIBUTES *$0, OBJECT_ATTRIBUTES *$1, ULONG $2)
{
    struct NtLoadKey2_params _ = {qemu_host_vars->NtLoadKey2, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtLoadKeyEx(const OBJECT_ATTRIBUTES *$0, OBJECT_ATTRIBUTES *$1, ULONG $2, HANDLE $3, HANDLE $4, ACCESS_MASK $5, HANDLE *$6, IO_STATUS_BLOCK *$7)
{
    struct NtLoadKeyEx_params _ = {qemu_host_vars->NtLoadKeyEx, $0, $1, $2, $3, $4, $5, $6, $7};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtLockFile(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, void *$3, PIO_STATUS_BLOCK $4, PLARGE_INTEGER $5, PLARGE_INTEGER $6, ULONG *$7, BOOLEAN $8, BOOLEAN $9)
{
    struct NtLockFile_params _ = {qemu_host_vars->NtLockFile, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtLockVirtualMemory(HANDLE $0, PVOID *$1, SIZE_T *$2, ULONG $3)
{
    struct NtLockVirtualMemory_params _ = {qemu_host_vars->NtLockVirtualMemory, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtMakeTemporaryObject(HANDLE $0)
{
    struct NtMakeTemporaryObject_params _ = {qemu_host_vars->NtMakeTemporaryObject, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtMapViewOfSection(HANDLE $0, HANDLE $1, PVOID *$2, ULONG_PTR $3, SIZE_T $4, const LARGE_INTEGER *$5, SIZE_T *$6, SECTION_INHERIT $7, ULONG $8, ULONG $9)
{
    struct NtMapViewOfSection_params _ = {qemu_host_vars->NtMapViewOfSection, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtMapViewOfSectionEx(HANDLE $0, HANDLE $1, PVOID *$2, const LARGE_INTEGER *$3, SIZE_T *$4, ULONG $5, ULONG $6, MEM_EXTENDED_PARAMETER *$7, ULONG $8)
{
    struct NtMapViewOfSectionEx_params _ = {qemu_host_vars->NtMapViewOfSectionEx, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtNotifyChangeDirectoryFile(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, PVOID $5, ULONG $6, ULONG $7, BOOLEAN $8)
{
    struct NtNotifyChangeDirectoryFile_params _ = {qemu_host_vars->NtNotifyChangeDirectoryFile, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtNotifyChangeKey(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, ULONG $5, BOOLEAN $6, PVOID $7, ULONG $8, BOOLEAN $9)
{
    struct NtNotifyChangeKey_params _ = {qemu_host_vars->NtNotifyChangeKey, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtNotifyChangeMultipleKeys(HANDLE $0, ULONG $1, OBJECT_ATTRIBUTES *$2, HANDLE $3, PIO_APC_ROUTINE $4, PVOID $5, PIO_STATUS_BLOCK $6, ULONG $7, BOOLEAN $8, PVOID $9, ULONG $10, BOOLEAN $11)
{
    struct NtNotifyChangeMultipleKeys_params _ = {qemu_host_vars->NtNotifyChangeMultipleKeys, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenDirectoryObject(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenDirectoryObject_params _ = {qemu_host_vars->NtOpenDirectoryObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenEvent(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenEvent_params _ = {qemu_host_vars->NtOpenEvent, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenFile(PHANDLE $0, ACCESS_MASK $1, POBJECT_ATTRIBUTES $2, PIO_STATUS_BLOCK $3, ULONG $4, ULONG $5)
{
    struct NtOpenFile_params _ = {qemu_host_vars->NtOpenFile, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenIoCompletion(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenIoCompletion_params _ = {qemu_host_vars->NtOpenIoCompletion, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenJobObject(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenJobObject_params _ = {qemu_host_vars->NtOpenJobObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenKey(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenKey_params _ = {qemu_host_vars->NtOpenKey, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenKeyEx(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, ULONG $3)
{
    struct NtOpenKeyEx_params _ = {qemu_host_vars->NtOpenKeyEx, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenKeyTransacted(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, HANDLE $3)
{
    struct NtOpenKeyTransacted_params _ = {qemu_host_vars->NtOpenKeyTransacted, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenKeyTransactedEx(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, ULONG $3, HANDLE $4)
{
    struct NtOpenKeyTransactedEx_params _ = {qemu_host_vars->NtOpenKeyTransactedEx, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenKeyedEvent(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenKeyedEvent_params _ = {qemu_host_vars->NtOpenKeyedEvent, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenMutant(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenMutant_params _ = {qemu_host_vars->NtOpenMutant, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenProcess(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, const CLIENT_ID *$3)
{
    struct NtOpenProcess_params _ = {qemu_host_vars->NtOpenProcess, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenProcessToken(HANDLE $0, DWORD $1, HANDLE *$2)
{
    struct NtOpenProcessToken_params _ = {qemu_host_vars->NtOpenProcessToken, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenProcessTokenEx(HANDLE $0, DWORD $1, DWORD $2, HANDLE *$3)
{
    struct NtOpenProcessTokenEx_params _ = {qemu_host_vars->NtOpenProcessTokenEx, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenSection(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenSection_params _ = {qemu_host_vars->NtOpenSection, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenSemaphore(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenSemaphore_params _ = {qemu_host_vars->NtOpenSemaphore, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenSymbolicLinkObject(PHANDLE $0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenSymbolicLinkObject_params _ = {qemu_host_vars->NtOpenSymbolicLinkObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenThread(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2, const CLIENT_ID *$3)
{
    struct NtOpenThread_params _ = {qemu_host_vars->NtOpenThread, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenThreadToken(HANDLE $0, DWORD $1, BOOLEAN $2, HANDLE *$3)
{
    struct NtOpenThreadToken_params _ = {qemu_host_vars->NtOpenThreadToken, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenThreadTokenEx(HANDLE $0, DWORD $1, BOOLEAN $2, DWORD $3, HANDLE *$4)
{
    struct NtOpenThreadTokenEx_params _ = {qemu_host_vars->NtOpenThreadTokenEx, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtOpenTimer(HANDLE *$0, ACCESS_MASK $1, const OBJECT_ATTRIBUTES *$2)
{
    struct NtOpenTimer_params _ = {qemu_host_vars->NtOpenTimer, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtPowerInformation(POWER_INFORMATION_LEVEL $0, PVOID $1, ULONG $2, PVOID $3, ULONG $4)
{
    struct NtPowerInformation_params _ = {qemu_host_vars->NtPowerInformation, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtPrivilegeCheck(HANDLE $0, PPRIVILEGE_SET $1, PBOOLEAN $2)
{
    struct NtPrivilegeCheck_params _ = {qemu_host_vars->NtPrivilegeCheck, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtProtectVirtualMemory(HANDLE $0, PVOID *$1, SIZE_T *$2, ULONG $3, ULONG *$4)
{
    struct NtProtectVirtualMemory_params _ = {qemu_host_vars->NtProtectVirtualMemory, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtPulseEvent(HANDLE $0, LONG *$1)
{
    struct NtPulseEvent_params _ = {qemu_host_vars->NtPulseEvent, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryAttributesFile(const OBJECT_ATTRIBUTES *$0, FILE_BASIC_INFORMATION *$1)
{
    struct NtQueryAttributesFile_params _ = {qemu_host_vars->NtQueryAttributesFile, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryDefaultLocale(BOOLEAN $0, LCID *$1)
{
    struct NtQueryDefaultLocale_params _ = {qemu_host_vars->NtQueryDefaultLocale, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryDefaultUILanguage(LANGID *$0)
{
    struct NtQueryDefaultUILanguage_params _ = {qemu_host_vars->NtQueryDefaultUILanguage, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryDirectoryFile(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, PVOID $5, ULONG $6, FILE_INFORMATION_CLASS $7, BOOLEAN $8, PUNICODE_STRING $9, BOOLEAN $10)
{
    struct NtQueryDirectoryFile_params _ = {qemu_host_vars->NtQueryDirectoryFile, $0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryDirectoryObject(HANDLE $0, PDIRECTORY_BASIC_INFORMATION $1, ULONG $2, BOOLEAN $3, BOOLEAN $4, PULONG $5, PULONG $6)
{
    struct NtQueryDirectoryObject_params _ = {qemu_host_vars->NtQueryDirectoryObject, $0, $1, $2, $3, $4, $5, $6};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryEaFile(HANDLE $0, PIO_STATUS_BLOCK $1, PVOID $2, ULONG $3, BOOLEAN $4, PVOID $5, ULONG $6, PULONG $7, BOOLEAN $8)
{
    struct NtQueryEaFile_params _ = {qemu_host_vars->NtQueryEaFile, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryEvent(HANDLE $0, EVENT_INFORMATION_CLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQueryEvent_params _ = {qemu_host_vars->NtQueryEvent, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryFullAttributesFile(const OBJECT_ATTRIBUTES *$0, FILE_NETWORK_OPEN_INFORMATION *$1)
{
    struct NtQueryFullAttributesFile_params _ = {qemu_host_vars->NtQueryFullAttributesFile, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryInformationAtom(RTL_ATOM $0, ATOM_INFORMATION_CLASS $1, PVOID $2, ULONG $3, ULONG *$4)
{
    struct NtQueryInformationAtom_params _ = {qemu_host_vars->NtQueryInformationAtom, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryInformationFile(HANDLE $0, PIO_STATUS_BLOCK $1, PVOID $2, ULONG $3, FILE_INFORMATION_CLASS $4)
{
    struct NtQueryInformationFile_params _ = {qemu_host_vars->NtQueryInformationFile, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryInformationJobObject(HANDLE $0, JOBOBJECTINFOCLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQueryInformationJobObject_params _ = {qemu_host_vars->NtQueryInformationJobObject, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryInformationToken(HANDLE $0, TOKEN_INFORMATION_CLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQueryInformationToken_params _ = {qemu_host_vars->NtQueryInformationToken, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryInstallUILanguage(LANGID *$0)
{
    struct NtQueryInstallUILanguage_params _ = {qemu_host_vars->NtQueryInstallUILanguage, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryIoCompletion(HANDLE $0, IO_COMPLETION_INFORMATION_CLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQueryIoCompletion_params _ = {qemu_host_vars->NtQueryIoCompletion, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryKey(HANDLE $0, KEY_INFORMATION_CLASS $1, void *$2, DWORD $3, DWORD *$4)
{
    struct NtQueryKey_params _ = {qemu_host_vars->NtQueryKey, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryLicenseValue(const UNICODE_STRING *$0, ULONG *$1, PVOID $2, ULONG $3, ULONG *$4)
{
    struct NtQueryLicenseValue_params _ = {qemu_host_vars->NtQueryLicenseValue, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryMultipleValueKey(HANDLE $0, PKEY_MULTIPLE_VALUE_INFORMATION $1, ULONG $2, PVOID $3, ULONG $4, PULONG $5)
{
    struct NtQueryMultipleValueKey_params _ = {qemu_host_vars->NtQueryMultipleValueKey, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryMutant(HANDLE $0, MUTANT_INFORMATION_CLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQueryMutant_params _ = {qemu_host_vars->NtQueryMutant, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryObject(HANDLE $0, OBJECT_INFORMATION_CLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQueryObject_params _ = {qemu_host_vars->NtQueryObject, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryPerformanceCounter(PLARGE_INTEGER $0, PLARGE_INTEGER $1)
{
    struct NtQueryPerformanceCounter_params _ = {qemu_host_vars->NtQueryPerformanceCounter, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQuerySection(HANDLE $0, SECTION_INFORMATION_CLASS $1, PVOID $2, SIZE_T $3, SIZE_T *$4)
{
    struct NtQuerySection_params _ = {qemu_host_vars->NtQuerySection, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQuerySecurityObject(HANDLE $0, SECURITY_INFORMATION $1, PSECURITY_DESCRIPTOR $2, ULONG $3, PULONG $4)
{
    struct NtQuerySecurityObject_params _ = {qemu_host_vars->NtQuerySecurityObject, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQuerySemaphore(HANDLE $0, SEMAPHORE_INFORMATION_CLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQuerySemaphore_params _ = {qemu_host_vars->NtQuerySemaphore, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQuerySymbolicLinkObject(HANDLE $0, PUNICODE_STRING $1, PULONG $2)
{
    struct NtQuerySymbolicLinkObject_params _ = {qemu_host_vars->NtQuerySymbolicLinkObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQuerySystemEnvironmentValue(PUNICODE_STRING $0, PWCHAR $1, ULONG $2, PULONG $3)
{
    struct NtQuerySystemEnvironmentValue_params _ = {qemu_host_vars->NtQuerySystemEnvironmentValue, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQuerySystemEnvironmentValueEx(PUNICODE_STRING $0, GUID *$1, void *$2, ULONG *$3, ULONG *$4)
{
    struct NtQuerySystemEnvironmentValueEx_params _ = {qemu_host_vars->NtQuerySystemEnvironmentValueEx, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQuerySystemTime(PLARGE_INTEGER $0)
{
    struct NtQuerySystemTime_params _ = {qemu_host_vars->NtQuerySystemTime, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryTimer(HANDLE $0, TIMER_INFORMATION_CLASS $1, PVOID $2, ULONG $3, PULONG $4)
{
    struct NtQueryTimer_params _ = {qemu_host_vars->NtQueryTimer, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryTimerResolution(PULONG $0, PULONG $1, PULONG $2)
{
    struct NtQueryTimerResolution_params _ = {qemu_host_vars->NtQueryTimerResolution, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryValueKey(HANDLE $0, const UNICODE_STRING *$1, KEY_VALUE_INFORMATION_CLASS $2, void *$3, DWORD $4, DWORD *$5)
{
    struct NtQueryValueKey_params _ = {qemu_host_vars->NtQueryValueKey, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryVirtualMemory(HANDLE $0, LPCVOID $1, MEMORY_INFORMATION_CLASS $2, PVOID $3, SIZE_T $4, SIZE_T *$5)
{
    struct NtQueryVirtualMemory_params _ = {qemu_host_vars->NtQueryVirtualMemory, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueryVolumeInformationFile(HANDLE $0, PIO_STATUS_BLOCK $1, PVOID $2, ULONG $3, FS_INFORMATION_CLASS $4)
{
    struct NtQueryVolumeInformationFile_params _ = {qemu_host_vars->NtQueryVolumeInformationFile, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtQueueApcThread(HANDLE $0, PNTAPCFUNC $1, ULONG_PTR $2, ULONG_PTR $3, ULONG_PTR $4)
{
    struct NtQueueApcThread_params _ = {qemu_host_vars->NtQueueApcThread, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRaiseHardError(NTSTATUS $0, ULONG $1, PUNICODE_STRING $2, PVOID *$3, HARDERROR_RESPONSE_OPTION $4, PHARDERROR_RESPONSE $5)
{
    struct NtRaiseHardError_params _ = {qemu_host_vars->NtRaiseHardError, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReadFile(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, PVOID $5, ULONG $6, PLARGE_INTEGER $7, PULONG $8)
{
    struct NtReadFile_params _ = {qemu_host_vars->NtReadFile, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReadFileScatter(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, FILE_SEGMENT_ELEMENT *$5, ULONG $6, PLARGE_INTEGER $7, PULONG $8)
{
    struct NtReadFileScatter_params _ = {qemu_host_vars->NtReadFileScatter, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReadVirtualMemory(HANDLE $0, const void *$1, void *$2, SIZE_T $3, SIZE_T *$4)
{
    struct NtReadVirtualMemory_params _ = {qemu_host_vars->NtReadVirtualMemory, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRegisterThreadTerminatePort(HANDLE $0)
{
    struct NtRegisterThreadTerminatePort_params _ = {qemu_host_vars->NtRegisterThreadTerminatePort, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReleaseKeyedEvent(HANDLE $0, const void *$1, BOOLEAN $2, const LARGE_INTEGER *$3)
{
    struct NtReleaseKeyedEvent_params _ = {qemu_host_vars->NtReleaseKeyedEvent, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReleaseMutant(HANDLE $0, PLONG $1)
{
    struct NtReleaseMutant_params _ = {qemu_host_vars->NtReleaseMutant, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReleaseSemaphore(HANDLE $0, ULONG $1, PULONG $2)
{
    struct NtReleaseSemaphore_params _ = {qemu_host_vars->NtReleaseSemaphore, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRemoveIoCompletion(HANDLE $0, PULONG_PTR $1, PULONG_PTR $2, PIO_STATUS_BLOCK $3, PLARGE_INTEGER $4)
{
    struct NtRemoveIoCompletion_params _ = {qemu_host_vars->NtRemoveIoCompletion, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRemoveIoCompletionEx(HANDLE $0, FILE_IO_COMPLETION_INFORMATION *$1, ULONG $2, ULONG *$3, LARGE_INTEGER *$4, BOOLEAN $5)
{
    struct NtRemoveIoCompletionEx_params _ = {qemu_host_vars->NtRemoveIoCompletionEx, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRemoveProcessDebug(HANDLE $0, HANDLE $1)
{
    struct NtRemoveProcessDebug_params _ = {qemu_host_vars->NtRemoveProcessDebug, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRenameKey(HANDLE $0, UNICODE_STRING *$1)
{
    struct NtRenameKey_params _ = {qemu_host_vars->NtRenameKey, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReplaceKey(POBJECT_ATTRIBUTES $0, HANDLE $1, POBJECT_ATTRIBUTES $2)
{
    struct NtReplaceKey_params _ = {qemu_host_vars->NtReplaceKey, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtReplyWaitReceivePort(HANDLE $0, PULONG $1, PLPC_MESSAGE $2, PLPC_MESSAGE $3)
{
    struct NtReplyWaitReceivePort_params _ = {qemu_host_vars->NtReplyWaitReceivePort, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRequestWaitReplyPort(HANDLE $0, PLPC_MESSAGE $1, PLPC_MESSAGE $2)
{
    struct NtRequestWaitReplyPort_params _ = {qemu_host_vars->NtRequestWaitReplyPort, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtResetEvent(HANDLE $0, LONG *$1)
{
    struct NtResetEvent_params _ = {qemu_host_vars->NtResetEvent, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtResetWriteWatch(HANDLE $0, PVOID $1, SIZE_T $2)
{
    struct NtResetWriteWatch_params _ = {qemu_host_vars->NtResetWriteWatch, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRestoreKey(HANDLE $0, HANDLE $1, ULONG $2)
{
    struct NtRestoreKey_params _ = {qemu_host_vars->NtRestoreKey, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtResumeProcess(HANDLE $0)
{
    struct NtResumeProcess_params _ = {qemu_host_vars->NtResumeProcess, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtResumeThread(HANDLE $0, PULONG $1)
{
    struct NtResumeThread_params _ = {qemu_host_vars->NtResumeThread, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtRollbackTransaction(HANDLE $0, BOOLEAN $1)
{
    struct NtRollbackTransaction_params _ = {qemu_host_vars->NtRollbackTransaction, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSaveKey(HANDLE $0, HANDLE $1)
{
    struct NtSaveKey_params _ = {qemu_host_vars->NtSaveKey, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSecureConnectPort(PHANDLE $0, PUNICODE_STRING $1, PSECURITY_QUALITY_OF_SERVICE $2, PLPC_SECTION_WRITE $3, PSID $4, PLPC_SECTION_READ $5, PULONG $6, PVOID $7, PULONG $8)
{
    struct NtSecureConnectPort_params _ = {qemu_host_vars->NtSecureConnectPort, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetDebugFilterState(ULONG $0, ULONG $1, BOOLEAN $2)
{
    struct NtSetDebugFilterState_params _ = {qemu_host_vars->NtSetDebugFilterState, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetDefaultLocale(BOOLEAN $0, LCID $1)
{
    struct NtSetDefaultLocale_params _ = {qemu_host_vars->NtSetDefaultLocale, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetDefaultUILanguage(LANGID $0)
{
    struct NtSetDefaultUILanguage_params _ = {qemu_host_vars->NtSetDefaultUILanguage, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetEaFile(HANDLE $0, PIO_STATUS_BLOCK $1, PVOID $2, ULONG $3)
{
    struct NtSetEaFile_params _ = {qemu_host_vars->NtSetEaFile, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetEvent(HANDLE $0, LONG *$1)
{
    struct NtSetEvent_params _ = {qemu_host_vars->NtSetEvent, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationDebugObject(HANDLE $0, DEBUGOBJECTINFOCLASS $1, PVOID $2, ULONG $3, ULONG *$4)
{
    struct NtSetInformationDebugObject_params _ = {qemu_host_vars->NtSetInformationDebugObject, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationFile(HANDLE $0, PIO_STATUS_BLOCK $1, PVOID $2, ULONG $3, FILE_INFORMATION_CLASS $4)
{
    struct NtSetInformationFile_params _ = {qemu_host_vars->NtSetInformationFile, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationJobObject(HANDLE $0, JOBOBJECTINFOCLASS $1, PVOID $2, ULONG $3)
{
    struct NtSetInformationJobObject_params _ = {qemu_host_vars->NtSetInformationJobObject, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationKey(HANDLE $0, const int $1, PVOID $2, ULONG $3)
{
    struct NtSetInformationKey_params _ = {qemu_host_vars->NtSetInformationKey, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationObject(HANDLE $0, OBJECT_INFORMATION_CLASS $1, PVOID $2, ULONG $3)
{
    struct NtSetInformationObject_params _ = {qemu_host_vars->NtSetInformationObject, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationProcess(HANDLE $0, PROCESSINFOCLASS $1, PVOID $2, ULONG $3)
{
    struct NtSetInformationProcess_params _ = {qemu_host_vars->NtSetInformationProcess, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationToken(HANDLE $0, TOKEN_INFORMATION_CLASS $1, PVOID $2, ULONG $3)
{
    struct NtSetInformationToken_params _ = {qemu_host_vars->NtSetInformationToken, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetInformationVirtualMemory(HANDLE $0, VIRTUAL_MEMORY_INFORMATION_CLASS $1, ULONG_PTR $2, PMEMORY_RANGE_ENTRY $3, PVOID $4, ULONG $5)
{
    struct NtSetInformationVirtualMemory_params _ = {qemu_host_vars->NtSetInformationVirtualMemory, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetIntervalProfile(ULONG $0, KPROFILE_SOURCE $1)
{
    struct NtSetIntervalProfile_params _ = {qemu_host_vars->NtSetIntervalProfile, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetIoCompletion(HANDLE $0, ULONG_PTR $1, ULONG_PTR $2, NTSTATUS $3, SIZE_T $4)
{
    struct NtSetIoCompletion_params _ = {qemu_host_vars->NtSetIoCompletion, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetSecurityObject(HANDLE $0, SECURITY_INFORMATION $1, PSECURITY_DESCRIPTOR $2)
{
    struct NtSetSecurityObject_params _ = {qemu_host_vars->NtSetSecurityObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetSystemInformation(SYSTEM_INFORMATION_CLASS $0, PVOID $1, ULONG $2)
{
    struct NtSetSystemInformation_params _ = {qemu_host_vars->NtSetSystemInformation, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetSystemTime(const LARGE_INTEGER *$0, LARGE_INTEGER *$1)
{
    struct NtSetSystemTime_params _ = {qemu_host_vars->NtSetSystemTime, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetTimer(HANDLE $0, const LARGE_INTEGER *$1, PTIMER_APC_ROUTINE $2, PVOID $3, BOOLEAN $4, ULONG $5, BOOLEAN *$6)
{
    struct NtSetTimer_params _ = {qemu_host_vars->NtSetTimer, $0, $1, $2, $3, $4, $5, $6};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetTimerResolution(ULONG $0, BOOLEAN $1, PULONG $2)
{
    struct NtSetTimerResolution_params _ = {qemu_host_vars->NtSetTimerResolution, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetValueKey(HANDLE $0, const UNICODE_STRING *$1, ULONG $2, ULONG $3, const void *$4, ULONG $5)
{
    struct NtSetValueKey_params _ = {qemu_host_vars->NtSetValueKey, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSetVolumeInformationFile(HANDLE $0, PIO_STATUS_BLOCK $1, PVOID $2, ULONG $3, FS_INFORMATION_CLASS $4)
{
    struct NtSetVolumeInformationFile_params _ = {qemu_host_vars->NtSetVolumeInformationFile, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtShutdownSystem(SHUTDOWN_ACTION $0)
{
    struct NtShutdownSystem_params _ = {qemu_host_vars->NtShutdownSystem, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSignalAndWaitForSingleObject(HANDLE $0, HANDLE $1, BOOLEAN $2, const LARGE_INTEGER *$3)
{
    struct NtSignalAndWaitForSingleObject_params _ = {qemu_host_vars->NtSignalAndWaitForSingleObject, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSuspendProcess(HANDLE $0)
{
    struct NtSuspendProcess_params _ = {qemu_host_vars->NtSuspendProcess, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSuspendThread(HANDLE $0, PULONG $1)
{
    struct NtSuspendThread_params _ = {qemu_host_vars->NtSuspendThread, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtSystemDebugControl(SYSDBG_COMMAND $0, PVOID $1, ULONG $2, PVOID $3, ULONG $4, PULONG $5)
{
    struct NtSystemDebugControl_params _ = {qemu_host_vars->NtSystemDebugControl, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtTerminateJobObject(HANDLE $0, NTSTATUS $1)
{
    struct NtTerminateJobObject_params _ = {qemu_host_vars->NtTerminateJobObject, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtTestAlert(void)
{
    struct NtTestAlert_params _ = {qemu_host_vars->NtTestAlert};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtTraceControl(ULONG $0, void *$1, ULONG $2, void *$3, ULONG $4, ULONG *$5)
{
    struct NtTraceControl_params _ = {qemu_host_vars->NtTraceControl, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUnloadDriver(const UNICODE_STRING *$0)
{
    struct NtUnloadDriver_params _ = {qemu_host_vars->NtUnloadDriver, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUnloadKey(POBJECT_ATTRIBUTES $0)
{
    struct NtUnloadKey_params _ = {qemu_host_vars->NtUnloadKey, $0};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUnlockFile(HANDLE $0, PIO_STATUS_BLOCK $1, PLARGE_INTEGER $2, PLARGE_INTEGER $3, PULONG $4)
{
    struct NtUnlockFile_params _ = {qemu_host_vars->NtUnlockFile, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUnlockVirtualMemory(HANDLE $0, PVOID *$1, SIZE_T *$2, ULONG $3)
{
    struct NtUnlockVirtualMemory_params _ = {qemu_host_vars->NtUnlockVirtualMemory, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUnmapViewOfSection(HANDLE $0, PVOID $1)
{
    struct NtUnmapViewOfSection_params _ = {qemu_host_vars->NtUnmapViewOfSection, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUnmapViewOfSectionEx(HANDLE $0, PVOID $1, ULONG $2)
{
    struct NtUnmapViewOfSectionEx_params _ = {qemu_host_vars->NtUnmapViewOfSectionEx, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWaitForAlertByThreadId(const void *$0, const LARGE_INTEGER *$1)
{
    struct NtWaitForAlertByThreadId_params _ = {qemu_host_vars->NtWaitForAlertByThreadId, $0, $1};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWaitForDebugEvent(HANDLE $0, BOOLEAN $1, LARGE_INTEGER *$2, DBGUI_WAIT_STATE_CHANGE *$3)
{
    struct NtWaitForDebugEvent_params _ = {qemu_host_vars->NtWaitForDebugEvent, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWaitForKeyedEvent(HANDLE $0, const void *$1, BOOLEAN $2, const LARGE_INTEGER *$3)
{
    struct NtWaitForKeyedEvent_params _ = {qemu_host_vars->NtWaitForKeyedEvent, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWaitForMultipleObjects(ULONG $0, const HANDLE *$1, BOOLEAN $2, BOOLEAN $3, const LARGE_INTEGER *$4)
{
    struct NtWaitForMultipleObjects_params _ = {qemu_host_vars->NtWaitForMultipleObjects, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWaitForSingleObject(HANDLE $0, BOOLEAN $1, const LARGE_INTEGER *$2)
{
    struct NtWaitForSingleObject_params _ = {qemu_host_vars->NtWaitForSingleObject, $0, $1, $2};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWow64AllocateVirtualMemory64(HANDLE $0, ULONG64 *$1, ULONG64 $2, ULONG64 *$3, ULONG $4, ULONG $5)
{
    struct NtWow64AllocateVirtualMemory64_params _ = {qemu_host_vars->NtWow64AllocateVirtualMemory64, $0, $1, $2, $3, $4, $5};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWow64GetNativeSystemInformation(SYSTEM_INFORMATION_CLASS $0, void *$1, ULONG $2, ULONG *$3)
{
    struct NtWow64GetNativeSystemInformation_params _ = {qemu_host_vars->NtWow64GetNativeSystemInformation, $0, $1, $2, $3};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWow64ReadVirtualMemory64(HANDLE $0, ULONG64 $1, void *$2, ULONG64 $3, ULONG64 *$4)
{
    struct NtWow64ReadVirtualMemory64_params _ = {qemu_host_vars->NtWow64ReadVirtualMemory64, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWow64WriteVirtualMemory64(HANDLE $0, ULONG64 $1, const void *$2, ULONG64 $3, ULONG64 *$4)
{
    struct NtWow64WriteVirtualMemory64_params _ = {qemu_host_vars->NtWow64WriteVirtualMemory64, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWriteFile(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, const void *$5, ULONG $6, PLARGE_INTEGER $7, PULONG $8)
{
    struct NtWriteFile_params _ = {qemu_host_vars->NtWriteFile, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWriteFileGather(HANDLE $0, HANDLE $1, PIO_APC_ROUTINE $2, PVOID $3, PIO_STATUS_BLOCK $4, FILE_SEGMENT_ELEMENT *$5, ULONG $6, PLARGE_INTEGER $7, PULONG $8)
{
    struct NtWriteFileGather_params _ = {qemu_host_vars->NtWriteFileGather, $0, $1, $2, $3, $4, $5, $6, $7, $8};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtWriteVirtualMemory(HANDLE $0, void *$1, const void *$2, SIZE_T $3, SIZE_T *$4)
{
    struct NtWriteVirtualMemory_params _ = {qemu_host_vars->NtWriteVirtualMemory, $0, $1, $2, $3, $4};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtYieldExecution(void)
{
    struct NtYieldExecution_params _ = {qemu_host_vars->NtYieldExecution};
    qemu_host_call(&_);
    return _.$ret;
}

int __wine_dbg_header(enum __wine_debug_class cls, struct __wine_debug_channel *channel, const char *function)
{
    struct __wine_dbg_header_params _ = {qemu_host_vars->__wine_dbg_header, cls, channel, function};
    qemu_host_call(&_);
    return _.$ret;
}

int __wine_dbg_output(const char *str)
{
    struct __wine_dbg_output_params _ = {qemu_host_vars->__wine_dbg_output, str};
    qemu_host_call(&_);
    return _.$ret;
}

const char *__wine_dbg_strdup(const char *str)
{
    struct __wine_dbg_strdup_params _ = {qemu_host_vars->__wine_dbg_strdup, str};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS __wine_unix_call( void *args, unixlib_entry_t func )
{
    struct __wine_unix_call_params _ = {qemu_host_vars->__wine_unix_call, args, func};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS get_thread_context( HANDLE handle, void *context, BOOL *self, USHORT machine)
{
    struct get_thread_context_params _ = {qemu_host_vars->get_thread_context, handle, context, self, machine};
    qemu_host_call(&_);
    return _.$ret;
}

int server_pipe(int *fd)
{
    struct server_pipe_params _ = {qemu_host_vars->server_pipe, fd};
    qemu_host_call(&_);
    return _.$ret;
}

void server_init_thread(void *entry_point, BOOL *suspend)
{
    struct server_init_thread_params _ = {qemu_host_vars->server_init_thread, entry_point, suspend};
    qemu_host_call(&_);
}

NTSTATUS send_debug_event( EXCEPTION_RECORD *rec, CONTEXT *context, BOOL first_chance )
{
    struct send_debug_event_params _ = {qemu_host_vars->send_debug_event, rec, context, first_chance};
    qemu_host_call(&_);
    return _.$ret;
}

unsigned int server_select(const select_op_t *select_op, data_size_t size, UINT flags, timeout_t abs_timeout, context_t *context, user_apc_t *user_apc)
{
    struct server_select_params _ = {qemu_host_vars->server_select, select_op, size, flags, abs_timeout, context, user_apc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS set_thread_context( HANDLE handle, const void *context, BOOL *self, USHORT machine)
{
    struct set_thread_context_params _ = {qemu_host_vars->set_thread_context, handle, context, self, machine};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS virtual_handle_fault(void *addr, DWORD err, void *stack)
{
    struct virtual_handle_fault_params _ = {qemu_host_vars->virtual_handle_fault, addr, err, stack};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL virtual_is_valid_code_address(const void *addr, SIZE_T size)
{
    struct virtual_is_valid_code_address_params _ = {qemu_host_vars->virtual_is_valid_code_address, addr, size};
    qemu_host_call(&_);
    return _.$ret;
}

void *virtual_setup_exception(void *stack_ptr, size_t size, EXCEPTION_RECORD *rec)
{
    struct virtual_setup_exception_params _ = {qemu_host_vars->virtual_setup_exception, stack_ptr, size, rec};
    qemu_host_call(&_);
    return _.$ret;
}

SIZE_T virtual_uninterrupted_read_memory(const void *addr, void *buffer, SIZE_T size)
{
    struct virtual_uninterrupted_read_memory_params _ = {qemu_host_vars->virtual_uninterrupted_read_memory, addr, buffer, size};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS virtual_uninterrupted_write_memory(void *addr, const void *buffer, SIZE_T size)
{
    struct virtual_uninterrupted_write_memory_params _ = {qemu_host_vars->virtual_uninterrupted_write_memory, addr, buffer, size};
    qemu_host_call(&_);
    return _.$ret;
}

void wait_suspend( CONTEXT *context )
{
    struct wait_suspend_params _ = {qemu_host_vars->wait_suspend, context};
    qemu_host_call(&_);
}

NTSTATUS WINAPI wine_nt_to_unix_file_name(const OBJECT_ATTRIBUTES *attr, char *nameA, ULONG *size, UINT disposition)
{
    struct wine_nt_to_unix_file_name_params _ = {qemu_host_vars->wine_nt_to_unix_file_name, attr, nameA, size, disposition};
    qemu_host_call(&_);
    return _.$ret;
}

unsigned int wine_server_call(void *req_ptr)
{
    struct wine_server_call_params _ = {qemu_host_vars->wine_server_call, req_ptr};
    qemu_host_call(&_);
    return _.$ret;
}

void wine_server_send_fd(int fd)
{
    struct wine_server_send_fd_params _ = {qemu_host_vars->wine_server_send_fd, fd};
    qemu_host_call(&_);
}

NTSTATUS WINAPI wine_unix_to_nt_file_name(const char *name, WCHAR *buffer, ULONG *size)
{
    struct wine_unix_to_nt_file_name_params _ = {qemu_host_vars->wine_unix_to_nt_file_name, name, buffer, size};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiAbortDoc(HDC hdc)
{
    struct NtGdiAbortDoc_params _ = {qemu_host_win32u_vars->NtGdiAbortDoc, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiAbortPath(HDC hdc)
{
    struct NtGdiAbortPath_params _ = {qemu_host_win32u_vars->NtGdiAbortPath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

HANDLE WINAPI NtGdiAddFontMemResourceEx(void *ptr, DWORD size, void *dv, ULONG dv_size, DWORD *count)
{
    struct NtGdiAddFontMemResourceEx_params _ = {qemu_host_win32u_vars->NtGdiAddFontMemResourceEx, ptr, size, dv, dv_size, count};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiAddFontResourceW(const WCHAR *str, ULONG size, ULONG files, DWORD flags, DWORD tid, void *dv)
{
    struct NtGdiAddFontResourceW_params _ = {qemu_host_win32u_vars->NtGdiAddFontResourceW, str, size, files, flags, tid, dv};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiAlphaBlend(HDC hdc_dst, int x_dst, int y_dst, int width_dst, int height_dst, HDC hdc_src, int x_src, int y_src, int width_src, int height_src, DWORD blend_function, HANDLE xform)
{
    struct NtGdiAlphaBlend_params _ = {qemu_host_win32u_vars->NtGdiAlphaBlend, hdc_dst, x_dst, y_dst, width_dst, height_dst, hdc_src, x_src, y_src, width_src, height_src, blend_function, xform};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiAngleArc(HDC hdc, INT x, INT y, DWORD radius, DWORD start_angle, DWORD sweep_angle)
{
    struct NtGdiAngleArc_params _ = {qemu_host_win32u_vars->NtGdiAngleArc, hdc, x, y, radius, start_angle, sweep_angle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiArcInternal(UINT type, HDC hdc, INT left, INT top, INT right, INT bottom, INT xstart, INT ystart, INT xend, INT yend)
{
    struct NtGdiArcInternal_params _ = {qemu_host_win32u_vars->NtGdiArcInternal, type, hdc, left, top, right, bottom, xstart, ystart, xend, yend};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiBeginPath(HDC hdc)
{
    struct NtGdiBeginPath_params _ = {qemu_host_win32u_vars->NtGdiBeginPath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiBitBlt(HDC hdc_dst, INT x_dst, INT y_dst, INT width, INT height, HDC hdc_src, INT x_src, INT y_src, DWORD rop, DWORD bk_color, FLONG fl)
{
    struct NtGdiBitBlt_params _ = {qemu_host_win32u_vars->NtGdiBitBlt, hdc_dst, x_dst, y_dst, width, height, hdc_src, x_src, y_src, rop, bk_color, fl};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiCloseFigure(HDC hdc)
{
    struct NtGdiCloseFigure_params _ = {qemu_host_win32u_vars->NtGdiCloseFigure, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiCombineRgn(HRGN dest, HRGN src1, HRGN src2, INT mode)
{
    struct NtGdiCombineRgn_params _ = {qemu_host_win32u_vars->NtGdiCombineRgn, dest, src1, src2, mode};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiComputeXformCoefficients(HDC hdc)
{
    struct NtGdiComputeXformCoefficients_params _ = {qemu_host_win32u_vars->NtGdiComputeXformCoefficients, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

HBITMAP WINAPI NtGdiCreateBitmap(INT width, INT height, UINT planes, UINT bpp, const void *bits)
{
    struct NtGdiCreateBitmap_params _ = {qemu_host_win32u_vars->NtGdiCreateBitmap, width, height, planes, bpp, bits};
    qemu_host_call(&_);
    return _.$ret;
}

HANDLE WINAPI NtGdiCreateClientObj(ULONG type)
{
    struct NtGdiCreateClientObj_params _ = {qemu_host_win32u_vars->NtGdiCreateClientObj, type};
    qemu_host_call(&_);
    return _.$ret;
}

HBITMAP WINAPI NtGdiCreateCompatibleBitmap(HDC hdc, INT width, INT height)
{
    struct NtGdiCreateCompatibleBitmap_params _ = {qemu_host_win32u_vars->NtGdiCreateCompatibleBitmap, hdc, width, height};
    qemu_host_call(&_);
    return _.$ret;
}

HDC WINAPI NtGdiCreateCompatibleDC(HDC hdc)
{
    struct NtGdiCreateCompatibleDC_params _ = {qemu_host_win32u_vars->NtGdiCreateCompatibleDC, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

HBRUSH WINAPI NtGdiCreateDIBBrush(const void *data, UINT coloruse, UINT size, BOOL is_8x8, BOOL pen, const void *client)
{
    struct NtGdiCreateDIBBrush_params _ = {qemu_host_win32u_vars->NtGdiCreateDIBBrush, data, coloruse, size, is_8x8, pen, client};
    qemu_host_call(&_);
    return _.$ret;
}

HBITMAP WINAPI NtGdiCreateDIBSection(HDC hdc, HANDLE section, DWORD offset, const BITMAPINFO *bmi, UINT usage, UINT header_size, ULONG flags, ULONG_PTR color_space, void **bits)
{
    struct NtGdiCreateDIBSection_params _ = {qemu_host_win32u_vars->NtGdiCreateDIBSection, hdc, section, offset, bmi, usage, header_size, flags, color_space, bits};
    qemu_host_call(&_);
    return _.$ret;
}

HBITMAP WINAPI NtGdiCreateDIBitmapInternal(HDC hdc, INT width, INT height, DWORD init, const void *bits, const BITMAPINFO *data, UINT coloruse, UINT max_info, UINT max_bits, ULONG flags, HANDLE xform)
{
    struct NtGdiCreateDIBitmapInternal_params _ = {qemu_host_win32u_vars->NtGdiCreateDIBitmapInternal, hdc, width, height, init, bits, data, coloruse, max_info, max_bits, flags, xform};
    qemu_host_call(&_);
    return _.$ret;
}

HRGN WINAPI NtGdiCreateEllipticRgn(INT left, INT top, INT right, INT bottom)
{
    struct NtGdiCreateEllipticRgn_params _ = {qemu_host_win32u_vars->NtGdiCreateEllipticRgn, left, top, right, bottom};
    qemu_host_call(&_);
    return _.$ret;
}

HPALETTE WINAPI NtGdiCreateHalftonePalette(HDC hdc)
{
    struct NtGdiCreateHalftonePalette_params _ = {qemu_host_win32u_vars->NtGdiCreateHalftonePalette, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

HBRUSH WINAPI NtGdiCreateHatchBrushInternal(INT style, COLORREF color, BOOL pen)
{
    struct NtGdiCreateHatchBrushInternal_params _ = {qemu_host_win32u_vars->NtGdiCreateHatchBrushInternal, style, color, pen};
    qemu_host_call(&_);
    return _.$ret;
}

HDC WINAPI NtGdiCreateMetafileDC(HDC hdc)
{
    struct NtGdiCreateMetafileDC_params _ = {qemu_host_win32u_vars->NtGdiCreateMetafileDC, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

HPALETTE WINAPI NtGdiCreatePaletteInternal(const LOGPALETTE *palette, UINT count)
{
    struct NtGdiCreatePaletteInternal_params _ = {qemu_host_win32u_vars->NtGdiCreatePaletteInternal, palette, count};
    qemu_host_call(&_);
    return _.$ret;
}

HBRUSH WINAPI NtGdiCreatePatternBrushInternal(HBITMAP hbitmap, BOOL pen, BOOL is_8x8)
{
    struct NtGdiCreatePatternBrushInternal_params _ = {qemu_host_win32u_vars->NtGdiCreatePatternBrushInternal, hbitmap, pen, is_8x8};
    qemu_host_call(&_);
    return _.$ret;
}

HPEN WINAPI NtGdiCreatePen(INT style, INT width, COLORREF color, HBRUSH brush)
{
    struct NtGdiCreatePen_params _ = {qemu_host_win32u_vars->NtGdiCreatePen, style, width, color, brush};
    qemu_host_call(&_);
    return _.$ret;
}

HRGN WINAPI NtGdiCreateRectRgn(INT left, INT top, INT right, INT bottom)
{
    struct NtGdiCreateRectRgn_params _ = {qemu_host_win32u_vars->NtGdiCreateRectRgn, left, top, right, bottom};
    qemu_host_call(&_);
    return _.$ret;
}

HRGN WINAPI NtGdiCreateRoundRectRgn(INT left, INT top, INT right, INT bottom, INT ellipse_width, INT ellipse_height)
{
    struct NtGdiCreateRoundRectRgn_params _ = {qemu_host_win32u_vars->NtGdiCreateRoundRectRgn, left, top, right, bottom, ellipse_width, ellipse_height};
    qemu_host_call(&_);
    return _.$ret;
}

HBRUSH WINAPI NtGdiCreateSolidBrush(COLORREF color, HBRUSH brush)
{
    struct NtGdiCreateSolidBrush_params _ = {qemu_host_win32u_vars->NtGdiCreateSolidBrush, color, brush};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDICheckVidPnExclusiveOwnership(const D3DKMT_CHECKVIDPNEXCLUSIVEOWNERSHIP *desc)
{
    struct NtGdiDdDDICheckVidPnExclusiveOwnership_params _ = {qemu_host_win32u_vars->NtGdiDdDDICheckVidPnExclusiveOwnership, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDICloseAdapter(const D3DKMT_CLOSEADAPTER *desc)
{
    struct NtGdiDdDDICloseAdapter_params _ = {qemu_host_win32u_vars->NtGdiDdDDICloseAdapter, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDICreateDCFromMemory(D3DKMT_CREATEDCFROMMEMORY *desc)
{
    struct NtGdiDdDDICreateDCFromMemory_params _ = {qemu_host_win32u_vars->NtGdiDdDDICreateDCFromMemory, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDICreateDevice(D3DKMT_CREATEDEVICE *desc)
{
    struct NtGdiDdDDICreateDevice_params _ = {qemu_host_win32u_vars->NtGdiDdDDICreateDevice, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIDestroyDCFromMemory(const D3DKMT_DESTROYDCFROMMEMORY *desc)
{
    struct NtGdiDdDDIDestroyDCFromMemory_params _ = {qemu_host_win32u_vars->NtGdiDdDDIDestroyDCFromMemory, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIDestroyDevice(const D3DKMT_DESTROYDEVICE *desc)
{
    struct NtGdiDdDDIDestroyDevice_params _ = {qemu_host_win32u_vars->NtGdiDdDDIDestroyDevice, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIEscape(const D3DKMT_ESCAPE *desc)
{
    struct NtGdiDdDDIEscape_params _ = {qemu_host_win32u_vars->NtGdiDdDDIEscape, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIOpenAdapterFromDeviceName(D3DKMT_OPENADAPTERFROMDEVICENAME *desc)
{
    struct NtGdiDdDDIOpenAdapterFromDeviceName_params _ = {qemu_host_win32u_vars->NtGdiDdDDIOpenAdapterFromDeviceName, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIOpenAdapterFromHdc(D3DKMT_OPENADAPTERFROMHDC *desc)
{
    struct NtGdiDdDDIOpenAdapterFromHdc_params _ = {qemu_host_win32u_vars->NtGdiDdDDIOpenAdapterFromHdc, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIOpenAdapterFromLuid(D3DKMT_OPENADAPTERFROMLUID *desc)
{
    struct NtGdiDdDDIOpenAdapterFromLuid_params _ = {qemu_host_win32u_vars->NtGdiDdDDIOpenAdapterFromLuid, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIQueryStatistics(D3DKMT_QUERYSTATISTICS *stats)
{
    struct NtGdiDdDDIQueryStatistics_params _ = {qemu_host_win32u_vars->NtGdiDdDDIQueryStatistics, stats};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDIQueryVideoMemoryInfo(D3DKMT_QUERYVIDEOMEMORYINFO *desc)
{
    struct NtGdiDdDDIQueryVideoMemoryInfo_params _ = {qemu_host_win32u_vars->NtGdiDdDDIQueryVideoMemoryInfo, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDISetQueuedLimit(D3DKMT_SETQUEUEDLIMIT *desc)
{
    struct NtGdiDdDDISetQueuedLimit_params _ = {qemu_host_win32u_vars->NtGdiDdDDISetQueuedLimit, desc};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtGdiDdDDISetVidPnSourceOwner(const D3DKMT_SETVIDPNSOURCEOWNER *desc)
{
    struct NtGdiDdDDISetVidPnSourceOwner_params _ = {qemu_host_win32u_vars->NtGdiDdDDISetVidPnSourceOwner, desc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiDeleteClientObj(HGDIOBJ obj)
{
    struct NtGdiDeleteClientObj_params _ = {qemu_host_win32u_vars->NtGdiDeleteClientObj, obj};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiDeleteObjectApp(HGDIOBJ obj)
{
    struct NtGdiDeleteObjectApp_params _ = {qemu_host_win32u_vars->NtGdiDeleteObjectApp, obj};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiDescribePixelFormat(HDC hdc, INT format, UINT size, PIXELFORMATDESCRIPTOR *descr)
{
    struct NtGdiDescribePixelFormat_params _ = {qemu_host_win32u_vars->NtGdiDescribePixelFormat, hdc, format, size, descr};
    qemu_host_call(&_);
    return _.$ret;
}

LONG WINAPI NtGdiDoPalette(HGDIOBJ handle, WORD start, WORD count, void *entries, DWORD func, BOOL inbound)
{
    struct NtGdiDoPalette_params _ = {qemu_host_win32u_vars->NtGdiDoPalette, handle, start, count, entries, func, inbound};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiDrawStream(HDC hdc, ULONG in, void *pvin)
{
    struct NtGdiDrawStream_params _ = {qemu_host_win32u_vars->NtGdiDrawStream, hdc, in, pvin};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiEllipse(HDC hdc, INT left, INT top, INT right, INT bottom)
{
    struct NtGdiEllipse_params _ = {qemu_host_win32u_vars->NtGdiEllipse, hdc, left, top, right, bottom};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiEndDoc(HDC hdc)
{
    struct NtGdiEndDoc_params _ = {qemu_host_win32u_vars->NtGdiEndDoc, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiEndPage(HDC hdc)
{
    struct NtGdiEndPage_params _ = {qemu_host_win32u_vars->NtGdiEndPage, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiEndPath(HDC hdc)
{
    struct NtGdiEndPath_params _ = {qemu_host_win32u_vars->NtGdiEndPath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiEnumFonts(HDC hdc, ULONG type, ULONG win32_compat, ULONG face_name_len, const WCHAR *face_name, ULONG charset, ULONG *count, void *buf)
{
    struct NtGdiEnumFonts_params _ = {qemu_host_win32u_vars->NtGdiEnumFonts, hdc, type, win32_compat, face_name_len, face_name, charset, count, buf};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiEqualRgn(HRGN hrgn1, HRGN hrgn2)
{
    struct NtGdiEqualRgn_params _ = {qemu_host_win32u_vars->NtGdiEqualRgn, hrgn1, hrgn2};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiExcludeClipRect(HDC hdc, INT left, INT top, INT right, INT bottom)
{
    struct NtGdiExcludeClipRect_params _ = {qemu_host_win32u_vars->NtGdiExcludeClipRect, hdc, left, top, right, bottom};
    qemu_host_call(&_);
    return _.$ret;
}

HPEN WINAPI NtGdiExtCreatePen(DWORD style, DWORD width, ULONG brush_style, ULONG color, ULONG_PTR client_hatch, ULONG_PTR hatch, DWORD style_count, const DWORD *style_bits, ULONG dib_size, BOOL old_style, HBRUSH brush)
{
    struct NtGdiExtCreatePen_params _ = {qemu_host_win32u_vars->NtGdiExtCreatePen, style, width, brush_style, color, client_hatch, hatch, style_count, style_bits, dib_size, old_style, brush};
    qemu_host_call(&_);
    return _.$ret;
}

HRGN WINAPI NtGdiExtCreateRegion(const XFORM *xform, DWORD count, const RGNDATA *data)
{
    struct NtGdiExtCreateRegion_params _ = {qemu_host_win32u_vars->NtGdiExtCreateRegion, xform, count, data};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiExtEscape(HDC hdc, WCHAR *driver, INT driver_id, INT escape, INT input_size, const char *input, INT output_size, char *output)
{
    struct NtGdiExtEscape_params _ = {qemu_host_win32u_vars->NtGdiExtEscape, hdc, driver, driver_id, escape, input_size, input, output_size, output};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiExtFloodFill(HDC hdc, INT x, INT y, COLORREF color, UINT type)
{
    struct NtGdiExtFloodFill_params _ = {qemu_host_win32u_vars->NtGdiExtFloodFill, hdc, x, y, color, type};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiExtGetObjectW(HGDIOBJ handle, INT count, void *buffer)
{
    struct NtGdiExtGetObjectW_params _ = {qemu_host_win32u_vars->NtGdiExtGetObjectW, handle, count, buffer};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiExtSelectClipRgn(HDC hdc, HRGN region, INT mode)
{
    struct NtGdiExtSelectClipRgn_params _ = {qemu_host_win32u_vars->NtGdiExtSelectClipRgn, hdc, region, mode};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiExtTextOutW(HDC hdc, INT x, INT y, UINT flags, const RECT *rect, const WCHAR *str, UINT count, const INT *dx, DWORD cp)
{
    struct NtGdiExtTextOutW_params _ = {qemu_host_win32u_vars->NtGdiExtTextOutW, hdc, x, y, flags, rect, str, count, dx, cp};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiFillPath(HDC hdc)
{
    struct NtGdiFillPath_params _ = {qemu_host_win32u_vars->NtGdiFillPath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiFillRgn(HDC hdc, HRGN hrgn, HBRUSH hbrush)
{
    struct NtGdiFillRgn_params _ = {qemu_host_win32u_vars->NtGdiFillRgn, hdc, hrgn, hbrush};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiFlattenPath(HDC hdc)
{
    struct NtGdiFlattenPath_params _ = {qemu_host_win32u_vars->NtGdiFlattenPath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiFlush(void)
{
    struct NtGdiFlush_params _ = {qemu_host_win32u_vars->NtGdiFlush};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiFontIsLinked(HDC hdc)
{
    struct NtGdiFontIsLinked_params _ = {qemu_host_win32u_vars->NtGdiFontIsLinked, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiFrameRgn(HDC hdc, HRGN hrgn, HBRUSH brush, INT width, INT height)
{
    struct NtGdiFrameRgn_params _ = {qemu_host_win32u_vars->NtGdiFrameRgn, hdc, hrgn, brush, width, height};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetAndSetDCDword(HDC hdc, UINT method, DWORD value, DWORD *result)
{
    struct NtGdiGetAndSetDCDword_params _ = {qemu_host_win32u_vars->NtGdiGetAndSetDCDword, hdc, method, value, result};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiGetAppClipBox(HDC hdc, RECT *rect)
{
    struct NtGdiGetAppClipBox_params _ = {qemu_host_win32u_vars->NtGdiGetAppClipBox, hdc, rect};
    qemu_host_call(&_);
    return _.$ret;
}

LONG WINAPI NtGdiGetBitmapBits(HBITMAP bitmap, LONG count, void *bits)
{
    struct NtGdiGetBitmapBits_params _ = {qemu_host_win32u_vars->NtGdiGetBitmapBits, bitmap, count, bits};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetBitmapDimension(HBITMAP bitmap, SIZE *size)
{
    struct NtGdiGetBitmapDimension_params _ = {qemu_host_win32u_vars->NtGdiGetBitmapDimension, bitmap, size};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtGdiGetBoundsRect(HDC hdc, RECT *rect, UINT flags)
{
    struct NtGdiGetBoundsRect_params _ = {qemu_host_win32u_vars->NtGdiGetBoundsRect, hdc, rect, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetCharABCWidthsW(HDC hdc, UINT first, UINT last, WCHAR *chars, ULONG flags, void *buffer)
{
    struct NtGdiGetCharABCWidthsW_params _ = {qemu_host_win32u_vars->NtGdiGetCharABCWidthsW, hdc, first, last, chars, flags, buffer};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetCharWidthInfo(HDC hdc, struct char_width_info *info)
{
    struct NtGdiGetCharWidthInfo_params _ = {qemu_host_win32u_vars->NtGdiGetCharWidthInfo, hdc, info};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetCharWidthW(HDC hdc, UINT first_char, UINT last_char, WCHAR *chars, ULONG flags, void *buffer)
{
    struct NtGdiGetCharWidthW_params _ = {qemu_host_win32u_vars->NtGdiGetCharWidthW, hdc, first_char, last_char, chars, flags, buffer};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetColorAdjustment(HDC hdc, COLORADJUSTMENT *ca)
{
    struct NtGdiGetColorAdjustment_params _ = {qemu_host_win32u_vars->NtGdiGetColorAdjustment, hdc, ca};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetDCDword(HDC hdc, UINT method, DWORD *result)
{
    struct NtGdiGetDCDword_params _ = {qemu_host_win32u_vars->NtGdiGetDCDword, hdc, method, result};
    qemu_host_call(&_);
    return _.$ret;
}

HANDLE WINAPI NtGdiGetDCObject(HDC hdc, UINT type)
{
    struct NtGdiGetDCObject_params _ = {qemu_host_win32u_vars->NtGdiGetDCObject, hdc, type};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetDCPoint(HDC hdc, UINT method, POINT *result)
{
    struct NtGdiGetDCPoint_params _ = {qemu_host_win32u_vars->NtGdiGetDCPoint, hdc, method, result};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiGetDIBitsInternal(HDC hdc, HBITMAP hbitmap, UINT startscan, UINT lines, void *bits, BITMAPINFO *info, UINT coloruse, UINT max_bits, UINT max_info)
{
    struct NtGdiGetDIBitsInternal_params _ = {qemu_host_win32u_vars->NtGdiGetDIBitsInternal, hdc, hbitmap, startscan, lines, bits, info, coloruse, max_bits, max_info};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiGetDeviceCaps(HDC hdc, INT cap)
{
    struct NtGdiGetDeviceCaps_params _ = {qemu_host_win32u_vars->NtGdiGetDeviceCaps, hdc, cap};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetDeviceGammaRamp(HDC hdc, void *ptr)
{
    struct NtGdiGetDeviceGammaRamp_params _ = {qemu_host_win32u_vars->NtGdiGetDeviceGammaRamp, hdc, ptr};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiGetFontData(HDC hdc, DWORD table, DWORD offset, void *buffer, DWORD length)
{
    struct NtGdiGetFontData_params _ = {qemu_host_win32u_vars->NtGdiGetFontData, hdc, table, offset, buffer, length};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetFontFileData(DWORD instance_id, DWORD file_index, UINT64 *offset, void *buff, DWORD buff_size)
{
    struct NtGdiGetFontFileData_params _ = {qemu_host_win32u_vars->NtGdiGetFontFileData, instance_id, file_index, offset, buff, buff_size};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetFontFileInfo(DWORD instance_id, DWORD file_index, struct font_fileinfo *info, SIZE_T size, SIZE_T *needed)
{
    struct NtGdiGetFontFileInfo_params _ = {qemu_host_win32u_vars->NtGdiGetFontFileInfo, instance_id, file_index, info, size, needed};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiGetFontUnicodeRanges(HDC hdc, GLYPHSET *lpgs)
{
    struct NtGdiGetFontUnicodeRanges_params _ = {qemu_host_win32u_vars->NtGdiGetFontUnicodeRanges, hdc, lpgs};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiGetGlyphIndicesW(HDC hdc, const WCHAR *str, INT count, WORD *indices, DWORD flags)
{
    struct NtGdiGetGlyphIndicesW_params _ = {qemu_host_win32u_vars->NtGdiGetGlyphIndicesW, hdc, str, count, indices, flags};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiGetGlyphOutline(HDC hdc, UINT ch, UINT format, GLYPHMETRICS *metrics, DWORD size, void *buffer, const MAT2 *mat2, BOOL ignore_rotation)
{
    struct NtGdiGetGlyphOutline_params _ = {qemu_host_win32u_vars->NtGdiGetGlyphOutline, hdc, ch, format, metrics, size, buffer, mat2, ignore_rotation};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiGetKerningPairs(HDC hdc, DWORD count, KERNINGPAIR *kern_pair)
{
    struct NtGdiGetKerningPairs_params _ = {qemu_host_win32u_vars->NtGdiGetKerningPairs, hdc, count, kern_pair};
    qemu_host_call(&_);
    return _.$ret;
}

COLORREF WINAPI NtGdiGetNearestColor(HDC hdc, COLORREF color)
{
    struct NtGdiGetNearestColor_params _ = {qemu_host_win32u_vars->NtGdiGetNearestColor, hdc, color};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtGdiGetNearestPaletteIndex(HPALETTE hpalette, COLORREF color)
{
    struct NtGdiGetNearestPaletteIndex_params _ = {qemu_host_win32u_vars->NtGdiGetNearestPaletteIndex, hpalette, color};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtGdiGetOutlineTextMetricsInternalW(HDC hdc, UINT cbData, OUTLINETEXTMETRICW *otm, ULONG opts)
{
    struct NtGdiGetOutlineTextMetricsInternalW_params _ = {qemu_host_win32u_vars->NtGdiGetOutlineTextMetricsInternalW, hdc, cbData, otm, opts};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiGetPath(HDC hdc, POINT *points, BYTE *types, INT size)
{
    struct NtGdiGetPath_params _ = {qemu_host_win32u_vars->NtGdiGetPath, hdc, points, types, size};
    qemu_host_call(&_);
    return _.$ret;
}

COLORREF WINAPI NtGdiGetPixel(HDC hdc, INT x, INT y)
{
    struct NtGdiGetPixel_params _ = {qemu_host_win32u_vars->NtGdiGetPixel, hdc, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiGetRandomRgn(HDC hdc, HRGN region, INT code)
{
    struct NtGdiGetRandomRgn_params _ = {qemu_host_win32u_vars->NtGdiGetRandomRgn, hdc, region, code};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetRasterizerCaps(RASTERIZER_STATUS *status, UINT size)
{
    struct NtGdiGetRasterizerCaps_params _ = {qemu_host_win32u_vars->NtGdiGetRasterizerCaps, status, size};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetRealizationInfo(HDC hdc, struct font_realization_info *info)
{
    struct NtGdiGetRealizationInfo_params _ = {qemu_host_win32u_vars->NtGdiGetRealizationInfo, hdc, info};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiGetRegionData(HRGN hrgn, DWORD count, RGNDATA *data)
{
    struct NtGdiGetRegionData_params _ = {qemu_host_win32u_vars->NtGdiGetRegionData, hrgn, count, data};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiGetRgnBox(HRGN hrgn, RECT *rect)
{
    struct NtGdiGetRgnBox_params _ = {qemu_host_win32u_vars->NtGdiGetRgnBox, hrgn, rect};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiGetSpoolMessage(void *ptr1, DWORD data2, void *ptr3, DWORD data4)
{
    struct NtGdiGetSpoolMessage_params _ = {qemu_host_win32u_vars->NtGdiGetSpoolMessage, ptr1, data2, ptr3, data4};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtGdiGetSystemPaletteUse(HDC hdc)
{
    struct NtGdiGetSystemPaletteUse_params _ = {qemu_host_win32u_vars->NtGdiGetSystemPaletteUse, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtGdiGetTextCharsetInfo(HDC hdc, FONTSIGNATURE *fs, DWORD flags)
{
    struct NtGdiGetTextCharsetInfo_params _ = {qemu_host_win32u_vars->NtGdiGetTextCharsetInfo, hdc, fs, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetTextExtentExW(HDC hdc, const WCHAR *str, INT count, INT max_ext, INT *nfit, INT *dxs, SIZE *size, UINT flags)
{
    struct NtGdiGetTextExtentExW_params _ = {qemu_host_win32u_vars->NtGdiGetTextExtentExW, hdc, str, count, max_ext, nfit, dxs, size, flags};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiGetTextFaceW(HDC hdc, INT count, WCHAR *name, BOOL alias_name)
{
    struct NtGdiGetTextFaceW_params _ = {qemu_host_win32u_vars->NtGdiGetTextFaceW, hdc, count, name, alias_name};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetTextMetricsW(HDC hdc, TEXTMETRICW *metrics, ULONG flags)
{
    struct NtGdiGetTextMetricsW_params _ = {qemu_host_win32u_vars->NtGdiGetTextMetricsW, hdc, metrics, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGetTransform(HDC hdc, DWORD which, XFORM *xform)
{
    struct NtGdiGetTransform_params _ = {qemu_host_win32u_vars->NtGdiGetTransform, hdc, which, xform};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiGradientFill(HDC hdc, TRIVERTEX *vert_array, ULONG nvert, void *grad_array, ULONG ngrad, ULONG mode)
{
    struct NtGdiGradientFill_params _ = {qemu_host_win32u_vars->NtGdiGradientFill, hdc, vert_array, nvert, grad_array, ngrad, mode};
    qemu_host_call(&_);
    return _.$ret;
}

HFONT WINAPI NtGdiHfontCreate(const void *logfont, ULONG unk2, ULONG unk3, ULONG unk4, void *data)
{
    struct NtGdiHfontCreate_params _ = {qemu_host_win32u_vars->NtGdiHfontCreate, logfont, unk2, unk3, unk4, data};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiIcmBrushInfo( HDC hdc, HBRUSH handle, BITMAPINFO *info, void *bits, ULONG *bits_size, UINT *usage, BOOL *unk, UINT mode )
{
    struct NtGdiIcmBrushInfo_params _ = {qemu_host_win32u_vars->NtGdiIcmBrushInfo, hdc, handle, info, bits, bits_size, usage, unk, mode};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiInitSpool(void)
{
    struct NtGdiInitSpool_params _ = {qemu_host_win32u_vars->NtGdiInitSpool};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiIntersectClipRect(HDC hdc, INT left, INT top, INT right, INT bottom)
{
    struct NtGdiIntersectClipRect_params _ = {qemu_host_win32u_vars->NtGdiIntersectClipRect, hdc, left, top, right, bottom};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiInvertRgn(HDC hdc, HRGN hrgn)
{
    struct NtGdiInvertRgn_params _ = {qemu_host_win32u_vars->NtGdiInvertRgn, hdc, hrgn};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiLineTo(HDC hdc, INT x, INT y)
{
    struct NtGdiLineTo_params _ = {qemu_host_win32u_vars->NtGdiLineTo, hdc, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiMaskBlt(HDC hdc, INT x_dst, INT y_dst, INT width_dst, INT height_dst, HDC hdc_src, INT x_src, INT y_src, HBITMAP mask, INT x_mask, INT y_mask, DWORD rop, DWORD bk_color)
{
    struct NtGdiMaskBlt_params _ = {qemu_host_win32u_vars->NtGdiMaskBlt, hdc, x_dst, y_dst, width_dst, height_dst, hdc_src, x_src, y_src, mask, x_mask, y_mask, rop, bk_color};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiModifyWorldTransform(HDC hdc, const XFORM *xform, DWORD mode)
{
    struct NtGdiModifyWorldTransform_params _ = {qemu_host_win32u_vars->NtGdiModifyWorldTransform, hdc, xform, mode};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiMoveTo(HDC hdc, INT x, INT y, POINT *pt)
{
    struct NtGdiMoveTo_params _ = {qemu_host_win32u_vars->NtGdiMoveTo, hdc, x, y, pt};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiOffsetClipRgn(HDC hdc, INT x, INT y)
{
    struct NtGdiOffsetClipRgn_params _ = {qemu_host_win32u_vars->NtGdiOffsetClipRgn, hdc, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiOffsetRgn(HRGN hrgn, INT x, INT y)
{
    struct NtGdiOffsetRgn_params _ = {qemu_host_win32u_vars->NtGdiOffsetRgn, hrgn, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

HDC WINAPI NtGdiOpenDCW(UNICODE_STRING *device, const DEVMODEW *devmode, UNICODE_STRING *output, ULONG type, BOOL is_display, HANDLE hspool, DRIVER_INFO_2W *driver_info, void *pdev)
{
    struct NtGdiOpenDCW_params _ = {qemu_host_win32u_vars->NtGdiOpenDCW, device, devmode, output, type, is_display, hspool, driver_info, pdev};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiPatBlt(HDC hdc, INT left, INT top, INT width, INT height, DWORD rop)
{
    struct NtGdiPatBlt_params _ = {qemu_host_win32u_vars->NtGdiPatBlt, hdc, left, top, width, height, rop};
    qemu_host_call(&_);
    return _.$ret;
}

HRGN WINAPI NtGdiPathToRegion(HDC hdc)
{
    struct NtGdiPathToRegion_params _ = {qemu_host_win32u_vars->NtGdiPathToRegion, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiPlgBlt(HDC hdc, const POINT *point, HDC hdc_src, INT x_src, INT y_src, INT width, INT height, HBITMAP mask, INT x_mask, INT y_mask, DWORD bk_color)
{
    struct NtGdiPlgBlt_params _ = {qemu_host_win32u_vars->NtGdiPlgBlt, hdc, point, hdc_src, x_src, y_src, width, height, mask, x_mask, y_mask, bk_color};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiPolyDraw(HDC hdc, const POINT *points, const BYTE *types, DWORD count)
{
    struct NtGdiPolyDraw_params _ = {qemu_host_win32u_vars->NtGdiPolyDraw, hdc, points, types, count};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG WINAPI NtGdiPolyPolyDraw(HDC hdc, const POINT *points, const ULONG *counts, DWORD count, UINT function)
{
    struct NtGdiPolyPolyDraw_params _ = {qemu_host_win32u_vars->NtGdiPolyPolyDraw, hdc, points, counts, count, function};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiPtInRegion(HRGN hrgn, INT x, INT y)
{
    struct NtGdiPtInRegion_params _ = {qemu_host_win32u_vars->NtGdiPtInRegion, hrgn, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiPtVisible(HDC hdc, INT x, INT y)
{
    struct NtGdiPtVisible_params _ = {qemu_host_win32u_vars->NtGdiPtVisible, hdc, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiRectInRegion(HRGN hrgn, const RECT *rect)
{
    struct NtGdiRectInRegion_params _ = {qemu_host_win32u_vars->NtGdiRectInRegion, hrgn, rect};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiRectVisible(HDC hdc, const RECT *rect)
{
    struct NtGdiRectVisible_params _ = {qemu_host_win32u_vars->NtGdiRectVisible, hdc, rect};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiRectangle(HDC hdc, INT left, INT top, INT right, INT bottom)
{
    struct NtGdiRectangle_params _ = {qemu_host_win32u_vars->NtGdiRectangle, hdc, left, top, right, bottom};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiRemoveFontMemResourceEx(HANDLE handle)
{
    struct NtGdiRemoveFontMemResourceEx_params _ = {qemu_host_win32u_vars->NtGdiRemoveFontMemResourceEx, handle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiRemoveFontResourceW(const WCHAR *str, ULONG size, ULONG files, DWORD flags, DWORD tid, void *dv)
{
    struct NtGdiRemoveFontResourceW_params _ = {qemu_host_win32u_vars->NtGdiRemoveFontResourceW, str, size, files, flags, tid, dv};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiResetDC(HDC hdc, const DEVMODEW *devmode, BOOL *banding, DRIVER_INFO_2W *driver_info, void *dev)
{
    struct NtGdiResetDC_params _ = {qemu_host_win32u_vars->NtGdiResetDC, hdc, devmode, banding, driver_info, dev};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiResizePalette(HPALETTE palette, UINT count)
{
    struct NtGdiResizePalette_params _ = {qemu_host_win32u_vars->NtGdiResizePalette, palette, count};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiRestoreDC(HDC hdc, INT level)
{
    struct NtGdiRestoreDC_params _ = {qemu_host_win32u_vars->NtGdiRestoreDC, hdc, level};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiRoundRect(HDC hdc, INT left, INT top, INT right, INT bottom, INT ell_width, INT ell_height)
{
    struct NtGdiRoundRect_params _ = {qemu_host_win32u_vars->NtGdiRoundRect, hdc, left, top, right, bottom, ell_width, ell_height};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiSaveDC(HDC hdc)
{
    struct NtGdiSaveDC_params _ = {qemu_host_win32u_vars->NtGdiSaveDC, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiScaleViewportExtEx(HDC hdc, INT x_num, INT x_denom, INT y_num, INT y_denom, SIZE *size)
{
    struct NtGdiScaleViewportExtEx_params _ = {qemu_host_win32u_vars->NtGdiScaleViewportExtEx, hdc, x_num, x_denom, y_num, y_denom, size};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiScaleWindowExtEx(HDC hdc, INT x_num, INT x_denom, INT y_num, INT y_denom, SIZE *size)
{
    struct NtGdiScaleWindowExtEx_params _ = {qemu_host_win32u_vars->NtGdiScaleWindowExtEx, hdc, x_num, x_denom, y_num, y_denom, size};
    qemu_host_call(&_);
    return _.$ret;
}

HGDIOBJ WINAPI NtGdiSelectBitmap(HDC hdc, HGDIOBJ handle)
{
    struct NtGdiSelectBitmap_params _ = {qemu_host_win32u_vars->NtGdiSelectBitmap, hdc, handle};
    qemu_host_call(&_);
    return _.$ret;
}

HGDIOBJ WINAPI NtGdiSelectBrush(HDC hdc, HGDIOBJ handle)
{
    struct NtGdiSelectBrush_params _ = {qemu_host_win32u_vars->NtGdiSelectBrush, hdc, handle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSelectClipPath(HDC hdc, INT mode)
{
    struct NtGdiSelectClipPath_params _ = {qemu_host_win32u_vars->NtGdiSelectClipPath, hdc, mode};
    qemu_host_call(&_);
    return _.$ret;
}

HGDIOBJ WINAPI NtGdiSelectFont(HDC hdc, HGDIOBJ handle)
{
    struct NtGdiSelectFont_params _ = {qemu_host_win32u_vars->NtGdiSelectFont, hdc, handle};
    qemu_host_call(&_);
    return _.$ret;
}

HGDIOBJ WINAPI NtGdiSelectPen(HDC hdc, HGDIOBJ handle)
{
    struct NtGdiSelectPen_params _ = {qemu_host_win32u_vars->NtGdiSelectPen, hdc, handle};
    qemu_host_call(&_);
    return _.$ret;
}

LONG WINAPI NtGdiSetBitmapBits(HBITMAP hbitmap, LONG count, const void *bits)
{
    struct NtGdiSetBitmapBits_params _ = {qemu_host_win32u_vars->NtGdiSetBitmapBits, hbitmap, count, bits};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetBitmapDimension(HBITMAP hbitmap, INT x, INT y, SIZE *prev_size)
{
    struct NtGdiSetBitmapDimension_params _ = {qemu_host_win32u_vars->NtGdiSetBitmapDimension, hbitmap, x, y, prev_size};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtGdiSetBoundsRect(HDC hdc, const RECT *rect, UINT flags)
{
    struct NtGdiSetBoundsRect_params _ = {qemu_host_win32u_vars->NtGdiSetBoundsRect, hdc, rect, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetBrushOrg(HDC hdc, INT x, INT y, POINT *prev_org)
{
    struct NtGdiSetBrushOrg_params _ = {qemu_host_win32u_vars->NtGdiSetBrushOrg, hdc, x, y, prev_org};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetColorAdjustment(HDC hdc, const COLORADJUSTMENT *ca)
{
    struct NtGdiSetColorAdjustment_params _ = {qemu_host_win32u_vars->NtGdiSetColorAdjustment, hdc, ca};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiSetDIBitsToDeviceInternal(HDC hdc, INT x_dst, INT y_dst, DWORD cx, DWORD cy, INT x_src, INT y_src, UINT startscan, UINT lines, const void *bits, const BITMAPINFO *bmi, UINT coloruse, UINT max_bits, UINT max_info, BOOL xform_coords, HANDLE xform)
{
    struct NtGdiSetDIBitsToDeviceInternal_params _ = {qemu_host_win32u_vars->NtGdiSetDIBitsToDeviceInternal, hdc, x_dst, y_dst, cx, cy, x_src, y_src, startscan, lines, bits, bmi, coloruse, max_bits, max_info, xform_coords, xform};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetDeviceGammaRamp(HDC hdc, void *ptr)
{
    struct NtGdiSetDeviceGammaRamp_params _ = {qemu_host_win32u_vars->NtGdiSetDeviceGammaRamp, hdc, ptr};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtGdiSetLayout(HDC hdc, LONG wox, DWORD layout)
{
    struct NtGdiSetLayout_params _ = {qemu_host_win32u_vars->NtGdiSetLayout, hdc, wox, layout};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetMagicColors(HDC hdc, DWORD magic, ULONG index)
{
    struct NtGdiSetMagicColors_params _ = {qemu_host_win32u_vars->NtGdiSetMagicColors, hdc, magic, index};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiSetMetaRgn(HDC hdc)
{
    struct NtGdiSetMetaRgn_params _ = {qemu_host_win32u_vars->NtGdiSetMetaRgn, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

COLORREF WINAPI NtGdiSetPixel(HDC hdc, INT x, INT y, COLORREF color)
{
    struct NtGdiSetPixel_params _ = {qemu_host_win32u_vars->NtGdiSetPixel, hdc, x, y, color};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetPixelFormat(HDC hdc, INT format)
{
    struct NtGdiSetPixelFormat_params _ = {qemu_host_win32u_vars->NtGdiSetPixelFormat, hdc, format};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetRectRgn(HRGN hrgn, INT left, INT top, INT right, INT bottom)
{
    struct NtGdiSetRectRgn_params _ = {qemu_host_win32u_vars->NtGdiSetRectRgn, hrgn, left, top, right, bottom};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtGdiSetSystemPaletteUse(HDC hdc, UINT use)
{
    struct NtGdiSetSystemPaletteUse_params _ = {qemu_host_win32u_vars->NtGdiSetSystemPaletteUse, hdc, use};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetTextJustification(HDC hdc, INT extra, INT breaks)
{
    struct NtGdiSetTextJustification_params _ = {qemu_host_win32u_vars->NtGdiSetTextJustification, hdc, extra, breaks};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSetVirtualResolution(HDC hdc, DWORD horz_res, DWORD vert_res, DWORD horz_size, DWORD vert_size)
{
    struct NtGdiSetVirtualResolution_params _ = {qemu_host_win32u_vars->NtGdiSetVirtualResolution, hdc, horz_res, vert_res, horz_size, vert_size};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiStartDoc(HDC hdc, const DOCINFOW *doc, BOOL *banding, INT job)
{
    struct NtGdiStartDoc_params _ = {qemu_host_win32u_vars->NtGdiStartDoc, hdc, doc, banding, job};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiStartPage(HDC hdc)
{
    struct NtGdiStartPage_params _ = {qemu_host_win32u_vars->NtGdiStartPage, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiStretchBlt(HDC hdc, INT x_dst, INT y_dst, INT width_dst, INT height_dst, HDC hdc_src, INT x_src, INT y_src, INT width_src, INT height_src, DWORD rop, COLORREF bk_color)
{
    struct NtGdiStretchBlt_params _ = {qemu_host_win32u_vars->NtGdiStretchBlt, hdc, x_dst, y_dst, width_dst, height_dst, hdc_src, x_src, y_src, width_src, height_src, rop, bk_color};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtGdiStretchDIBitsInternal(HDC hdc, INT x_dst, INT y_dst, INT width_dst, INT height_dst, INT x_src, INT y_src, INT width_src, INT height_src, const void *bits, const BITMAPINFO *bmi, UINT coloruse, DWORD rop, UINT max_info, UINT max_bits, HANDLE xform)
{
    struct NtGdiStretchDIBitsInternal_params _ = {qemu_host_win32u_vars->NtGdiStretchDIBitsInternal, hdc, x_dst, y_dst, width_dst, height_dst, x_src, y_src, width_src, height_src, bits, bmi, coloruse, rop, max_info, max_bits, xform};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiStrokeAndFillPath(HDC hdc)
{
    struct NtGdiStrokeAndFillPath_params _ = {qemu_host_win32u_vars->NtGdiStrokeAndFillPath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiStrokePath(HDC hdc)
{
    struct NtGdiStrokePath_params _ = {qemu_host_win32u_vars->NtGdiStrokePath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiSwapBuffers(HDC hdc)
{
    struct NtGdiSwapBuffers_params _ = {qemu_host_win32u_vars->NtGdiSwapBuffers, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiTransformPoints(HDC hdc, const POINT *points_in, POINT *points_out, INT count, UINT mode)
{
    struct NtGdiTransformPoints_params _ = {qemu_host_win32u_vars->NtGdiTransformPoints, hdc, points_in, points_out, count, mode};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiTransparentBlt(HDC hdc, int x_dst, int y_dst, int width_dst, int height_dst, HDC hdc_src, int x_src, int y_src, int width_src, int height_src, UINT color)
{
    struct NtGdiTransparentBlt_params _ = {qemu_host_win32u_vars->NtGdiTransparentBlt, hdc, x_dst, y_dst, width_dst, height_dst, hdc_src, x_src, y_src, width_src, height_src, color};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiUnrealizeObject(HGDIOBJ obj)
{
    struct NtGdiUnrealizeObject_params _ = {qemu_host_win32u_vars->NtGdiUnrealizeObject, obj};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiUpdateColors(HDC hdc)
{
    struct NtGdiUpdateColors_params _ = {qemu_host_win32u_vars->NtGdiUpdateColors, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtGdiWidenPath(HDC hdc)
{
    struct NtGdiWidenPath_params _ = {qemu_host_win32u_vars->NtGdiWidenPath, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

HKL WINAPI NtUserActivateKeyboardLayout(HKL layout, UINT flags)
{
    struct NtUserActivateKeyboardLayout_params _ = {qemu_host_win32u_vars->NtUserActivateKeyboardLayout, layout, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserAddClipboardFormatListener(HWND hwnd)
{
    struct NtUserAddClipboardFormatListener_params _ = {qemu_host_win32u_vars->NtUserAddClipboardFormatListener, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserAssociateInputContext(HWND hwnd, HIMC ctx, ULONG flags)
{
    struct NtUserAssociateInputContext_params _ = {qemu_host_win32u_vars->NtUserAssociateInputContext, hwnd, ctx, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserAttachThreadInput(DWORD from, DWORD to, BOOL attach)
{
    struct NtUserAttachThreadInput_params _ = {qemu_host_win32u_vars->NtUserAttachThreadInput, from, to, attach};
    qemu_host_call(&_);
    return _.$ret;
}

HDC WINAPI NtUserBeginPaint(HWND hwnd, PAINTSTRUCT *ps)
{
    struct NtUserBeginPaint_params _ = {qemu_host_win32u_vars->NtUserBeginPaint, hwnd, ps};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUserBuildHimcList(UINT thread_id, UINT count, HIMC *buffer, UINT *size)
{
    struct NtUserBuildHimcList_params _ = {qemu_host_win32u_vars->NtUserBuildHimcList, thread_id, count, buffer, size};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUserBuildHwndList(HDESK desktop, ULONG unk2, ULONG unk3, ULONG unk4, ULONG thread_id, ULONG count, HWND *buffer, ULONG *size)
{
    struct NtUserBuildHwndList_params _ = {qemu_host_win32u_vars->NtUserBuildHwndList, desktop, unk2, unk3, unk4, thread_id, count, buffer, size};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG_PTR WINAPI NtUserCallHwnd(HWND hwnd, DWORD code)
{
    struct NtUserCallHwnd_params _ = {qemu_host_win32u_vars->NtUserCallHwnd, hwnd, code};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG_PTR WINAPI NtUserCallHwndParam(HWND hwnd, DWORD_PTR param, DWORD code)
{
    struct NtUserCallHwndParam_params _ = {qemu_host_win32u_vars->NtUserCallHwndParam, hwnd, param, code};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserCallMsgFilter(MSG *msg, INT code)
{
    struct NtUserCallMsgFilter_params _ = {qemu_host_win32u_vars->NtUserCallMsgFilter, msg, code};
    qemu_host_call(&_);
    return _.$ret;
}

LRESULT WINAPI NtUserCallNextHookEx(HHOOK hhook, INT code, WPARAM wparam, LPARAM lparam)
{
    struct NtUserCallNextHookEx_params _ = {qemu_host_win32u_vars->NtUserCallNextHookEx, hhook, code, wparam, lparam};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG_PTR WINAPI NtUserCallNoParam(ULONG code)
{
    struct NtUserCallNoParam_params _ = {qemu_host_win32u_vars->NtUserCallNoParam, code};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG_PTR WINAPI NtUserCallOneParam(ULONG_PTR arg, ULONG code)
{
    struct NtUserCallOneParam_params _ = {qemu_host_win32u_vars->NtUserCallOneParam, arg, code};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG_PTR WINAPI NtUserCallTwoParam(ULONG_PTR arg1, ULONG_PTR arg2, ULONG code)
{
    struct NtUserCallTwoParam_params _ = {qemu_host_win32u_vars->NtUserCallTwoParam, arg1, arg2, code};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserChangeClipboardChain(HWND hwnd, HWND next)
{
    struct NtUserChangeClipboardChain_params _ = {qemu_host_win32u_vars->NtUserChangeClipboardChain, hwnd, next};
    qemu_host_call(&_);
    return _.$ret;
}

LONG WINAPI NtUserChangeDisplaySettings(UNICODE_STRING *devname, DEVMODEW *devmode, HWND hwnd, DWORD flags, void *lparam)
{
    struct NtUserChangeDisplaySettings_params _ = {qemu_host_win32u_vars->NtUserChangeDisplaySettings, devname, devmode, hwnd, flags, lparam};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserCheckMenuItem(HMENU handle, UINT id, UINT flags)
{
    struct NtUserCheckMenuItem_params _ = {qemu_host_win32u_vars->NtUserCheckMenuItem, handle, id, flags};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserChildWindowFromPointEx(HWND parent, LONG x, LONG y, UINT flags)
{
    struct NtUserChildWindowFromPointEx_params _ = {qemu_host_win32u_vars->NtUserChildWindowFromPointEx, parent, x, y, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserClipCursor(const RECT *rect)
{
    struct NtUserClipCursor_params _ = {qemu_host_win32u_vars->NtUserClipCursor, rect};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserCloseClipboard(void)
{
    struct NtUserCloseClipboard_params _ = {qemu_host_win32u_vars->NtUserCloseClipboard};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserCloseDesktop(HDESK handle)
{
    struct NtUserCloseDesktop_params _ = {qemu_host_win32u_vars->NtUserCloseDesktop, handle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserCloseWindowStation(HWINSTA handle)
{
    struct NtUserCloseWindowStation_params _ = {qemu_host_win32u_vars->NtUserCloseWindowStation, handle};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserCopyAcceleratorTable(HACCEL src, ACCEL *dst, INT count)
{
    struct NtUserCopyAcceleratorTable_params _ = {qemu_host_win32u_vars->NtUserCopyAcceleratorTable, src, dst, count};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserCountClipboardFormats(void)
{
    struct NtUserCountClipboardFormats_params _ = {qemu_host_win32u_vars->NtUserCountClipboardFormats};
    qemu_host_call(&_);
    return _.$ret;
}

HACCEL WINAPI NtUserCreateAcceleratorTable(ACCEL *table, INT count)
{
    struct NtUserCreateAcceleratorTable_params _ = {qemu_host_win32u_vars->NtUserCreateAcceleratorTable, table, count};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserCreateCaret(HWND hwnd, HBITMAP bitmap, int width, int height)
{
    struct NtUserCreateCaret_params _ = {qemu_host_win32u_vars->NtUserCreateCaret, hwnd, bitmap, width, height};
    qemu_host_call(&_);
    return _.$ret;
}

HDESK WINAPI NtUserCreateDesktopEx(OBJECT_ATTRIBUTES *attr, UNICODE_STRING *device, DEVMODEW *devmode, DWORD flags, ACCESS_MASK access, ULONG heap_size)
{
    struct NtUserCreateDesktopEx_params _ = {qemu_host_win32u_vars->NtUserCreateDesktopEx, attr, device, devmode, flags, access, heap_size};
    qemu_host_call(&_);
    return _.$ret;
}

HIMC WINAPI NtUserCreateInputContext(UINT_PTR client_ptr)
{
    struct NtUserCreateInputContext_params _ = {qemu_host_win32u_vars->NtUserCreateInputContext, client_ptr};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserCreateWindowEx(DWORD ex_style, UNICODE_STRING *class_name, UNICODE_STRING *version, UNICODE_STRING *window_name, DWORD style, INT x, INT y, INT cx, INT cy, HWND parent, HMENU menu, HINSTANCE instance, void *params, DWORD flags, HINSTANCE client_instance, DWORD unk, BOOL ansi)
{
    struct NtUserCreateWindowEx_params _ = {qemu_host_win32u_vars->NtUserCreateWindowEx, ex_style, class_name, version, window_name, style, x, y, cx, cy, parent, menu, instance, params, flags, client_instance, unk, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

HWINSTA WINAPI NtUserCreateWindowStation(OBJECT_ATTRIBUTES *attr, ACCESS_MASK mask, ULONG arg3, ULONG arg4, ULONG arg5, ULONG arg6, ULONG arg7)
{
    struct NtUserCreateWindowStation_params _ = {qemu_host_win32u_vars->NtUserCreateWindowStation, attr, mask, arg3, arg4, arg5, arg6, arg7};
    qemu_host_call(&_);
    return _.$ret;
}

HDWP WINAPI NtUserDeferWindowPosAndBand(HDWP hdwp, HWND hwnd, HWND after, INT x, INT y, INT cx, INT cy, UINT flags, UINT unk1, UINT unk2)
{
    struct NtUserDeferWindowPosAndBand_params _ = {qemu_host_win32u_vars->NtUserDeferWindowPosAndBand, hdwp, hwnd, after, x, y, cx, cy, flags, unk1, unk2};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDeleteMenu(HMENU menu, UINT id, UINT flags)
{
    struct NtUserDeleteMenu_params _ = {qemu_host_win32u_vars->NtUserDeleteMenu, menu, id, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDestroyAcceleratorTable(HACCEL handle)
{
    struct NtUserDestroyAcceleratorTable_params _ = {qemu_host_win32u_vars->NtUserDestroyAcceleratorTable, handle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDestroyCursor(HCURSOR cursor, ULONG arg)
{
    struct NtUserDestroyCursor_params _ = {qemu_host_win32u_vars->NtUserDestroyCursor, cursor, arg};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDestroyInputContext(HIMC handle)
{
    struct NtUserDestroyInputContext_params _ = {qemu_host_win32u_vars->NtUserDestroyInputContext, handle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDestroyMenu(HMENU menu)
{
    struct NtUserDestroyMenu_params _ = {qemu_host_win32u_vars->NtUserDestroyMenu, menu};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDestroyWindow(HWND hwnd)
{
    struct NtUserDestroyWindow_params _ = {qemu_host_win32u_vars->NtUserDestroyWindow, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDisableThreadIme(DWORD thread_id)
{
    struct NtUserDisableThreadIme_params _ = {qemu_host_win32u_vars->NtUserDisableThreadIme, thread_id};
    qemu_host_call(&_);
    return _.$ret;
}

LRESULT WINAPI NtUserDispatchMessage(const MSG *msg)
{
    struct NtUserDispatchMessage_params _ = {qemu_host_win32u_vars->NtUserDispatchMessage, msg};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUserDisplayConfigGetDeviceInfo(DISPLAYCONFIG_DEVICE_INFO_HEADER *packet)
{
    struct NtUserDisplayConfigGetDeviceInfo_params _ = {qemu_host_win32u_vars->NtUserDisplayConfigGetDeviceInfo, packet};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDragDetect(HWND hwnd, int x, int y)
{
    struct NtUserDragDetect_params _ = {qemu_host_win32u_vars->NtUserDragDetect, hwnd, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserDragObject(HWND parent, HWND hwnd, UINT fmt, ULONG_PTR data, HCURSOR cursor)
{
    struct NtUserDragObject_params _ = {qemu_host_win32u_vars->NtUserDragObject, parent, hwnd, fmt, data, cursor};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDrawCaptionTemp(HWND hwnd, HDC hdc, const RECT *rect, HFONT font, HICON icon, const WCHAR *str, UINT flags)
{
    struct NtUserDrawCaptionTemp_params _ = {qemu_host_win32u_vars->NtUserDrawCaptionTemp, hwnd, hdc, rect, font, icon, str, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserDrawIconEx(HDC hdc, INT x0, INT y0, HICON icon, INT width, INT height, UINT istep, HBRUSH hbr, UINT flags)
{
    struct NtUserDrawIconEx_params _ = {qemu_host_win32u_vars->NtUserDrawIconEx, hdc, x0, y0, icon, width, height, istep, hbr, flags};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserDrawMenuBarTemp(HWND hwnd, HDC hdc, RECT *rect, HMENU handle, HFONT font)
{
    struct NtUserDrawMenuBarTemp_params _ = {qemu_host_win32u_vars->NtUserDrawMenuBarTemp, hwnd, hdc, rect, handle, font};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEmptyClipboard(void)
{
    struct NtUserEmptyClipboard_params _ = {qemu_host_win32u_vars->NtUserEmptyClipboard};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEnableMenuItem(HMENU handle, UINT id, UINT flags)
{
    struct NtUserEnableMenuItem_params _ = {qemu_host_win32u_vars->NtUserEnableMenuItem, handle, id, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEnableMouseInPointer(BOOL $0)
{
    struct NtUserEnableMouseInPointer_params _ = {qemu_host_win32u_vars->NtUserEnableMouseInPointer, $0};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEnableScrollBar(HWND hwnd, UINT bar, UINT flags)
{
    struct NtUserEnableScrollBar_params _ = {qemu_host_win32u_vars->NtUserEnableScrollBar, hwnd, bar, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEndDeferWindowPosEx(HDWP hdwp, BOOL async)
{
    struct NtUserEndDeferWindowPosEx_params _ = {qemu_host_win32u_vars->NtUserEndDeferWindowPosEx, hdwp, async};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEndMenu(void)
{
    struct NtUserEndMenu_params _ = {qemu_host_win32u_vars->NtUserEndMenu};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEndPaint(HWND hwnd, const PAINTSTRUCT *ps)
{
    struct NtUserEndPaint_params _ = {qemu_host_win32u_vars->NtUserEndPaint, hwnd, ps};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUserEnumDisplayDevices(UNICODE_STRING *device, DWORD index, DISPLAY_DEVICEW *info, DWORD flags)
{
    struct NtUserEnumDisplayDevices_params _ = {qemu_host_win32u_vars->NtUserEnumDisplayDevices, device, index, info, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEnumDisplayMonitors(HDC hdc, RECT *rect, MONITORENUMPROC proc, LPARAM lp)
{
    struct NtUserEnumDisplayMonitors_params _ = {qemu_host_win32u_vars->NtUserEnumDisplayMonitors, hdc, rect, proc, lp};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserEnumDisplaySettings(UNICODE_STRING *device, DWORD mode, DEVMODEW *dev_mode, DWORD flags)
{
    struct NtUserEnumDisplaySettings_params _ = {qemu_host_win32u_vars->NtUserEnumDisplaySettings, device, mode, dev_mode, flags};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserExcludeUpdateRgn(HDC hdc, HWND hwnd)
{
    struct NtUserExcludeUpdateRgn_params _ = {qemu_host_win32u_vars->NtUserExcludeUpdateRgn, hdc, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

HICON WINAPI NtUserFindExistingCursorIcon(UNICODE_STRING *module, UNICODE_STRING *res_name, void *desc)
{
    struct NtUserFindExistingCursorIcon_params _ = {qemu_host_win32u_vars->NtUserFindExistingCursorIcon, module, res_name, desc};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserFindWindowEx(HWND parent, HWND child, UNICODE_STRING *class, UNICODE_STRING *title, ULONG unk)
{
    struct NtUserFindWindowEx_params _ = {qemu_host_win32u_vars->NtUserFindWindowEx, parent, child, class, title, unk};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserFlashWindowEx(FLASHWINFO *info)
{
    struct NtUserFlashWindowEx_params _ = {qemu_host_win32u_vars->NtUserFlashWindowEx, info};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserGetAncestor(HWND hwnd, UINT type)
{
    struct NtUserGetAncestor_params _ = {qemu_host_win32u_vars->NtUserGetAncestor, hwnd, type};
    qemu_host_call(&_);
    return _.$ret;
}

SHORT WINAPI NtUserGetAsyncKeyState(INT key)
{
    struct NtUserGetAsyncKeyState_params _ = {qemu_host_win32u_vars->NtUserGetAsyncKeyState, key};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG WINAPI NtUserGetAtomName(ATOM atom, UNICODE_STRING *name)
{
    struct NtUserGetAtomName_params _ = {qemu_host_win32u_vars->NtUserGetAtomName, atom, name};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetCaretBlinkTime(void)
{
    struct NtUserGetCaretBlinkTime_params _ = {qemu_host_win32u_vars->NtUserGetCaretBlinkTime};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetCaretPos(POINT *point)
{
    struct NtUserGetCaretPos_params _ = {qemu_host_win32u_vars->NtUserGetCaretPos, point};
    qemu_host_call(&_);
    return _.$ret;
}

ATOM WINAPI NtUserGetClassInfoEx(HINSTANCE instance, UNICODE_STRING *name, WNDCLASSEXW *wc, struct client_menu_name *menu_name, BOOL ansi)
{
    struct NtUserGetClassInfoEx_params _ = {qemu_host_win32u_vars->NtUserGetClassInfoEx, instance, name, wc, menu_name, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserGetClassName(HWND hwnd, BOOL real, UNICODE_STRING *name)
{
    struct NtUserGetClassName_params _ = {qemu_host_win32u_vars->NtUserGetClassName, hwnd, real, name};
    qemu_host_call(&_);
    return _.$ret;
}

HANDLE WINAPI NtUserGetClipboardData(UINT format, struct get_clipboard_params *params)
{
    struct NtUserGetClipboardData_params _ = {qemu_host_win32u_vars->NtUserGetClipboardData, format, params};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserGetClipboardFormatName(UINT format, WCHAR *buffer, INT maxlen)
{
    struct NtUserGetClipboardFormatName_params _ = {qemu_host_win32u_vars->NtUserGetClipboardFormatName, format, buffer, maxlen};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserGetClipboardOwner(void)
{
    struct NtUserGetClipboardOwner_params _ = {qemu_host_win32u_vars->NtUserGetClipboardOwner};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserGetClipboardSequenceNumber(void)
{
    struct NtUserGetClipboardSequenceNumber_params _ = {qemu_host_win32u_vars->NtUserGetClipboardSequenceNumber};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserGetClipboardViewer(void)
{
    struct NtUserGetClipboardViewer_params _ = {qemu_host_win32u_vars->NtUserGetClipboardViewer};
    qemu_host_call(&_);
    return _.$ret;
}

HCURSOR WINAPI NtUserGetCursor(void)
{
    struct NtUserGetCursor_params _ = {qemu_host_win32u_vars->NtUserGetCursor};
    qemu_host_call(&_);
    return _.$ret;
}

HCURSOR WINAPI NtUserGetCursorFrameInfo(HCURSOR hCursor, DWORD istep, DWORD *rate_jiffies, DWORD *num_steps)
{
    struct NtUserGetCursorFrameInfo_params _ = {qemu_host_win32u_vars->NtUserGetCursorFrameInfo, hCursor, istep, rate_jiffies, num_steps};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetCursorInfo(CURSORINFO *info)
{
    struct NtUserGetCursorInfo_params _ = {qemu_host_win32u_vars->NtUserGetCursorInfo, info};
    qemu_host_call(&_);
    return _.$ret;
}

HDC WINAPI NtUserGetDC(HWND hwnd)
{
    struct NtUserGetDC_params _ = {qemu_host_win32u_vars->NtUserGetDC, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

HDC WINAPI NtUserGetDCEx(HWND hwnd, HRGN clip_rgn, DWORD flags)
{
    struct NtUserGetDCEx_params _ = {qemu_host_win32u_vars->NtUserGetDCEx, hwnd, clip_rgn, flags};
    qemu_host_call(&_);
    return _.$ret;
}

LONG WINAPI NtUserGetDisplayConfigBufferSizes(UINT32 flags, UINT32 *num_path_info, UINT32 *num_mode_info)
{
    struct NtUserGetDisplayConfigBufferSizes_params _ = {qemu_host_win32u_vars->NtUserGetDisplayConfigBufferSizes, flags, num_path_info, num_mode_info};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetDoubleClickTime(void)
{
    struct NtUserGetDoubleClickTime_params _ = {qemu_host_win32u_vars->NtUserGetDoubleClickTime};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetDpiForMonitor(HMONITOR monitor, UINT type, UINT *x, UINT *y)
{
    struct NtUserGetDpiForMonitor_params _ = {qemu_host_win32u_vars->NtUserGetDpiForMonitor, monitor, type, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserGetForegroundWindow(void)
{
    struct NtUserGetForegroundWindow_params _ = {qemu_host_win32u_vars->NtUserGetForegroundWindow};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetGUIThreadInfo(DWORD id, GUITHREADINFO *info)
{
    struct NtUserGetGUIThreadInfo_params _ = {qemu_host_win32u_vars->NtUserGetGUIThreadInfo, id, info};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetIconInfo(HICON icon, ICONINFO *info, UNICODE_STRING *module, UNICODE_STRING *res_name, DWORD *bpp, LONG unk)
{
    struct NtUserGetIconInfo_params _ = {qemu_host_win32u_vars->NtUserGetIconInfo, icon, info, module, res_name, bpp, unk};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetIconSize(HICON handle, UINT step, LONG *width, LONG *height)
{
    struct NtUserGetIconSize_params _ = {qemu_host_win32u_vars->NtUserGetIconSize, handle, step, width, height};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetInternalWindowPos(HWND hwnd, RECT *rect, POINT *pt)
{
    struct NtUserGetInternalWindowPos_params _ = {qemu_host_win32u_vars->NtUserGetInternalWindowPos, hwnd, rect, pt};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserGetKeyNameText(LONG lparam, WCHAR *buffer, INT size)
{
    struct NtUserGetKeyNameText_params _ = {qemu_host_win32u_vars->NtUserGetKeyNameText, lparam, buffer, size};
    qemu_host_call(&_);
    return _.$ret;
}

SHORT WINAPI NtUserGetKeyState(INT vkey)
{
    struct NtUserGetKeyState_params _ = {qemu_host_win32u_vars->NtUserGetKeyState, vkey};
    qemu_host_call(&_);
    return _.$ret;
}

HKL WINAPI NtUserGetKeyboardLayout(DWORD thread_id)
{
    struct NtUserGetKeyboardLayout_params _ = {qemu_host_win32u_vars->NtUserGetKeyboardLayout, thread_id};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetKeyboardLayoutList(INT size, HKL *layouts)
{
    struct NtUserGetKeyboardLayoutList_params _ = {qemu_host_win32u_vars->NtUserGetKeyboardLayoutList, size, layouts};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetKeyboardLayoutName(WCHAR *name)
{
    struct NtUserGetKeyboardLayoutName_params _ = {qemu_host_win32u_vars->NtUserGetKeyboardLayoutName, name};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetKeyboardState(BYTE *state)
{
    struct NtUserGetKeyboardState_params _ = {qemu_host_win32u_vars->NtUserGetKeyboardState, state};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetLayeredWindowAttributes(HWND hwnd, COLORREF *key, BYTE *alpha, DWORD *flags)
{
    struct NtUserGetLayeredWindowAttributes_params _ = {qemu_host_win32u_vars->NtUserGetLayeredWindowAttributes, hwnd, key, alpha, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetMenuBarInfo(HWND hwnd, LONG id, LONG item, MENUBARINFO *info)
{
    struct NtUserGetMenuBarInfo_params _ = {qemu_host_win32u_vars->NtUserGetMenuBarInfo, hwnd, id, item, info};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetMenuItemRect(HWND hwnd, HMENU menu, UINT item, RECT *rect)
{
    struct NtUserGetMenuItemRect_params _ = {qemu_host_win32u_vars->NtUserGetMenuItemRect, hwnd, menu, item, rect};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetMessage(MSG *msg, HWND hwnd, UINT first, UINT last)
{
    struct NtUserGetMessage_params _ = {qemu_host_win32u_vars->NtUserGetMessage, msg, hwnd, first, last};
    qemu_host_call(&_);
    return _.$ret;
}

int WINAPI NtUserGetMouseMovePointsEx(UINT size, MOUSEMOVEPOINT *ptin, MOUSEMOVEPOINT *ptout, int count, DWORD resolution)
{
    struct NtUserGetMouseMovePointsEx_params _ = {qemu_host_win32u_vars->NtUserGetMouseMovePointsEx, size, ptin, ptout, count, resolution};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetObjectInformation(HANDLE handle, INT index, void *info, DWORD len, DWORD *needed)
{
    struct NtUserGetObjectInformation_params _ = {qemu_host_win32u_vars->NtUserGetObjectInformation, handle, index, info, len, needed};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserGetOpenClipboardWindow(void)
{
    struct NtUserGetOpenClipboardWindow_params _ = {qemu_host_win32u_vars->NtUserGetOpenClipboardWindow};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetPointerInfoList(UINT32 id, POINTER_INPUT_TYPE type, UINT_PTR $2, UINT_PTR $3, SIZE_T size, UINT32 *entry_count, UINT32 *pointer_count, void *pointer_info)
{
    struct NtUserGetPointerInfoList_params _ = {qemu_host_win32u_vars->NtUserGetPointerInfoList, id, type, $2, $3, size, entry_count, pointer_count, pointer_info};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserGetPriorityClipboardFormat(UINT *list, INT count)
{
    struct NtUserGetPriorityClipboardFormat_params _ = {qemu_host_win32u_vars->NtUserGetPriorityClipboardFormat, list, count};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG WINAPI NtUserGetProcessDpiAwarenessContext(HANDLE process)
{
    struct NtUserGetProcessDpiAwarenessContext_params _ = {qemu_host_win32u_vars->NtUserGetProcessDpiAwarenessContext, process};
    qemu_host_call(&_);
    return _.$ret;
}

HWINSTA WINAPI NtUserGetProcessWindowStation(void)
{
    struct NtUserGetProcessWindowStation_params _ = {qemu_host_win32u_vars->NtUserGetProcessWindowStation};
    qemu_host_call(&_);
    return _.$ret;
}

HANDLE WINAPI NtUserGetProp(HWND hwnd, const WCHAR *str)
{
    struct NtUserGetProp_params _ = {qemu_host_win32u_vars->NtUserGetProp, hwnd, str};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserGetQueueStatus(UINT flags)
{
    struct NtUserGetQueueStatus_params _ = {qemu_host_win32u_vars->NtUserGetQueueStatus, flags};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetRawInputBuffer(RAWINPUT *data, UINT *data_size, UINT header_size)
{
    struct NtUserGetRawInputBuffer_params _ = {qemu_host_win32u_vars->NtUserGetRawInputBuffer, data, data_size, header_size};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetRawInputData(HRAWINPUT rawinput, UINT command, void *data, UINT *data_size, UINT header_size)
{
    struct NtUserGetRawInputData_params _ = {qemu_host_win32u_vars->NtUserGetRawInputData, rawinput, command, data, data_size, header_size};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetRawInputDeviceInfo(HANDLE handle, UINT command, void *data, UINT *data_size)
{
    struct NtUserGetRawInputDeviceInfo_params _ = {qemu_host_win32u_vars->NtUserGetRawInputDeviceInfo, handle, command, data, data_size};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetRawInputDeviceList(RAWINPUTDEVICELIST *devices, UINT *device_count, UINT size)
{
    struct NtUserGetRawInputDeviceList_params _ = {qemu_host_win32u_vars->NtUserGetRawInputDeviceList, devices, device_count, size};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserGetRegisteredRawInputDevices(RAWINPUTDEVICE *devices, UINT *device_count, UINT size)
{
    struct NtUserGetRegisteredRawInputDevices_params _ = {qemu_host_win32u_vars->NtUserGetRegisteredRawInputDevices, devices, device_count, size};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetScrollBarInfo(HWND hwnd, LONG id, SCROLLBARINFO *info)
{
    struct NtUserGetScrollBarInfo_params _ = {qemu_host_win32u_vars->NtUserGetScrollBarInfo, hwnd, id, info};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG WINAPI NtUserGetSystemDpiForProcess(HANDLE process)
{
    struct NtUserGetSystemDpiForProcess_params _ = {qemu_host_win32u_vars->NtUserGetSystemDpiForProcess, process};
    qemu_host_call(&_);
    return _.$ret;
}

HMENU WINAPI NtUserGetSystemMenu(HWND hwnd, BOOL revert)
{
    struct NtUserGetSystemMenu_params _ = {qemu_host_win32u_vars->NtUserGetSystemMenu, hwnd, revert};
    qemu_host_call(&_);
    return _.$ret;
}

HDESK WINAPI NtUserGetThreadDesktop(DWORD thread)
{
    struct NtUserGetThreadDesktop_params _ = {qemu_host_win32u_vars->NtUserGetThreadDesktop, thread};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetTitleBarInfo(HWND hwnd, TITLEBARINFO *info)
{
    struct NtUserGetTitleBarInfo_params _ = {qemu_host_win32u_vars->NtUserGetTitleBarInfo, hwnd, info};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetUpdateRect(HWND hwnd, RECT *rect, BOOL erase)
{
    struct NtUserGetUpdateRect_params _ = {qemu_host_win32u_vars->NtUserGetUpdateRect, hwnd, rect, erase};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserGetUpdateRgn(HWND hwnd, HRGN hrgn, BOOL erase)
{
    struct NtUserGetUpdateRgn_params _ = {qemu_host_win32u_vars->NtUserGetUpdateRgn, hwnd, hrgn, erase};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetUpdatedClipboardFormats(UINT *formats, UINT size, UINT *out_size)
{
    struct NtUserGetUpdatedClipboardFormats_params _ = {qemu_host_win32u_vars->NtUserGetUpdatedClipboardFormats, formats, size, out_size};
    qemu_host_call(&_);
    return _.$ret;
}

HDC WINAPI NtUserGetWindowDC(HWND hwnd)
{
    struct NtUserGetWindowDC_params _ = {qemu_host_win32u_vars->NtUserGetWindowDC, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserGetWindowPlacement(HWND hwnd, WINDOWPLACEMENT *placement)
{
    struct NtUserGetWindowPlacement_params _ = {qemu_host_win32u_vars->NtUserGetWindowPlacement, hwnd, placement};
    qemu_host_call(&_);
    return _.$ret;
}

int WINAPI NtUserGetWindowRgnEx(HWND hwnd, HRGN hrgn, UINT unk)
{
    struct NtUserGetWindowRgnEx_params _ = {qemu_host_win32u_vars->NtUserGetWindowRgnEx, hwnd, hrgn, unk};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserHideCaret(HWND hwnd)
{
    struct NtUserHideCaret_params _ = {qemu_host_win32u_vars->NtUserHideCaret, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserHiliteMenuItem(HWND hwnd, HMENU handle, UINT item, UINT hilite)
{
    struct NtUserHiliteMenuItem_params _ = {qemu_host_win32u_vars->NtUserHiliteMenuItem, hwnd, handle, item, hilite};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUserInitializeClientPfnArrays(const struct user_client_procs *client_procsA, const struct user_client_procs *client_procsW, const void *client_workers, HINSTANCE user_module)
{
    struct NtUserInitializeClientPfnArrays_params _ = {qemu_host_win32u_vars->NtUserInitializeClientPfnArrays, client_procsA, client_procsW, client_workers, user_module};
    qemu_host_call(&_);
    return _.$ret;
}

HICON WINAPI NtUserInternalGetWindowIcon(HWND hwnd, UINT type)
{
    struct NtUserInternalGetWindowIcon_params _ = {qemu_host_win32u_vars->NtUserInternalGetWindowIcon, hwnd, type};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserInternalGetWindowText(HWND hwnd, WCHAR *text, INT count)
{
    struct NtUserInternalGetWindowText_params _ = {qemu_host_win32u_vars->NtUserInternalGetWindowText, hwnd, text, count};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserInvalidateRect(HWND hwnd, const RECT *rect, BOOL erase)
{
    struct NtUserInvalidateRect_params _ = {qemu_host_win32u_vars->NtUserInvalidateRect, hwnd, rect, erase};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserInvalidateRgn(HWND hwnd, HRGN hrgn, BOOL erase)
{
    struct NtUserInvalidateRgn_params _ = {qemu_host_win32u_vars->NtUserInvalidateRgn, hwnd, hrgn, erase};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserIsClipboardFormatAvailable(UINT format)
{
    struct NtUserIsClipboardFormatAvailable_params _ = {qemu_host_win32u_vars->NtUserIsClipboardFormatAvailable, format};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserIsMouseInPointerEnabled(void)
{
    struct NtUserIsMouseInPointerEnabled_params _ = {qemu_host_win32u_vars->NtUserIsMouseInPointerEnabled};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserKillTimer(HWND hwnd, UINT_PTR id)
{
    struct NtUserKillTimer_params _ = {qemu_host_win32u_vars->NtUserKillTimer, hwnd, id};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserLockWindowUpdate(HWND hwnd)
{
    struct NtUserLockWindowUpdate_params _ = {qemu_host_win32u_vars->NtUserLockWindowUpdate, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserLogicalToPerMonitorDPIPhysicalPoint(HWND hwnd, POINT *pt)
{
    struct NtUserLogicalToPerMonitorDPIPhysicalPoint_params _ = {qemu_host_win32u_vars->NtUserLogicalToPerMonitorDPIPhysicalPoint, hwnd, pt};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserMapVirtualKeyEx(UINT code, UINT type, HKL layout)
{
    struct NtUserMapVirtualKeyEx_params _ = {qemu_host_win32u_vars->NtUserMapVirtualKeyEx, code, type, layout};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserMenuItemFromPoint(HWND hwnd, HMENU handle, int x, int y)
{
    struct NtUserMenuItemFromPoint_params _ = {qemu_host_win32u_vars->NtUserMenuItemFromPoint, hwnd, handle, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

LRESULT WINAPI NtUserMessageCall(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam, void *result_info, DWORD type, BOOL ansi)
{
    struct NtUserMessageCall_params _ = {qemu_host_win32u_vars->NtUserMessageCall, hwnd, msg, wparam, lparam, result_info, type, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserMoveWindow(HWND hwnd, INT x, INT y, INT cx, INT cy, BOOL repaint)
{
    struct NtUserMoveWindow_params _ = {qemu_host_win32u_vars->NtUserMoveWindow, hwnd, x, y, cx, cy, repaint};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserMsgWaitForMultipleObjectsEx(DWORD count, const HANDLE *handles, DWORD timeout, DWORD mask, DWORD flags)
{
    struct NtUserMsgWaitForMultipleObjectsEx_params _ = {qemu_host_win32u_vars->NtUserMsgWaitForMultipleObjectsEx, count, handles, timeout, mask, flags};
    qemu_host_call(&_);
    return _.$ret;
}

void WINAPI NtUserNotifyIMEStatus( HWND hwnd, UINT status )
{
    struct NtUserNotifyIMEStatus_params _ = {qemu_host_win32u_vars->NtUserNotifyIMEStatus, hwnd, status};
    qemu_host_call(&_);
}

void WINAPI NtUserNotifyWinEvent(DWORD event, HWND hwnd, LONG object_id, LONG child_id)
{
    struct NtUserNotifyWinEvent_params _ = {qemu_host_win32u_vars->NtUserNotifyWinEvent, event, hwnd, object_id, child_id};
    qemu_host_call(&_);
}

BOOL WINAPI NtUserOpenClipboard(HWND hwnd, ULONG unk)
{
    struct NtUserOpenClipboard_params _ = {qemu_host_win32u_vars->NtUserOpenClipboard, hwnd, unk};
    qemu_host_call(&_);
    return _.$ret;
}

HDESK WINAPI NtUserOpenDesktop(OBJECT_ATTRIBUTES *attr, DWORD flags, ACCESS_MASK access)
{
    struct NtUserOpenDesktop_params _ = {qemu_host_win32u_vars->NtUserOpenDesktop, attr, flags, access};
    qemu_host_call(&_);
    return _.$ret;
}

HDESK WINAPI NtUserOpenInputDesktop(DWORD flags, BOOL inherit, ACCESS_MASK access)
{
    struct NtUserOpenInputDesktop_params _ = {qemu_host_win32u_vars->NtUserOpenInputDesktop, flags, inherit, access};
    qemu_host_call(&_);
    return _.$ret;
}

HWINSTA WINAPI NtUserOpenWindowStation(OBJECT_ATTRIBUTES *attr, ACCESS_MASK access)
{
    struct NtUserOpenWindowStation_params _ = {qemu_host_win32u_vars->NtUserOpenWindowStation, attr, access};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserPeekMessage(MSG *msg_out, HWND hwnd, UINT first, UINT last, UINT flags)
{
    struct NtUserPeekMessage_params _ = {qemu_host_win32u_vars->NtUserPeekMessage, msg_out, hwnd, first, last, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserPerMonitorDPIPhysicalToLogicalPoint(HWND hwnd, POINT *pt)
{
    struct NtUserPerMonitorDPIPhysicalToLogicalPoint_params _ = {qemu_host_win32u_vars->NtUserPerMonitorDPIPhysicalToLogicalPoint, hwnd, pt};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserPostMessage(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    struct NtUserPostMessage_params _ = {qemu_host_win32u_vars->NtUserPostMessage, hwnd, msg, wparam, lparam};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserPostThreadMessage(DWORD thread, UINT msg, WPARAM wparam, LPARAM lparam)
{
    struct NtUserPostThreadMessage_params _ = {qemu_host_win32u_vars->NtUserPostThreadMessage, thread, msg, wparam, lparam};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserPrintWindow(HWND hwnd, HDC hdc, UINT flags)
{
    struct NtUserPrintWindow_params _ = {qemu_host_win32u_vars->NtUserPrintWindow, hwnd, hdc, flags};
    qemu_host_call(&_);
    return _.$ret;
}

LONG WINAPI NtUserQueryDisplayConfig(UINT32 flags, UINT32 *paths_count, DISPLAYCONFIG_PATH_INFO *paths, UINT32 *modes_count, DISPLAYCONFIG_MODE_INFO *modes, DISPLAYCONFIG_TOPOLOGY_ID *topology_id)
{
    struct NtUserQueryDisplayConfig_params _ = {qemu_host_win32u_vars->NtUserQueryDisplayConfig, flags, paths_count, paths, modes_count, modes, topology_id};
    qemu_host_call(&_);
    return _.$ret;
}

UINT_PTR WINAPI NtUserQueryInputContext(HIMC handle, UINT attr)
{
    struct NtUserQueryInputContext_params _ = {qemu_host_win32u_vars->NtUserQueryInputContext, handle, attr};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserRealChildWindowFromPoint(HWND parent, LONG x, LONG y)
{
    struct NtUserRealChildWindowFromPoint_params _ = {qemu_host_win32u_vars->NtUserRealChildWindowFromPoint, parent, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserRedrawWindow(HWND hwnd, const RECT *rect, HRGN hrgn, UINT flags)
{
    struct NtUserRedrawWindow_params _ = {qemu_host_win32u_vars->NtUserRedrawWindow, hwnd, rect, hrgn, flags};
    qemu_host_call(&_);
    return _.$ret;
}

ATOM WINAPI NtUserRegisterClassExWOW(const WNDCLASSEXW *wc, UNICODE_STRING *name, UNICODE_STRING *version, struct client_menu_name *client_menu_name, DWORD fnid, DWORD flags, DWORD *wow)
{
    struct NtUserRegisterClassExWOW_params _ = {qemu_host_win32u_vars->NtUserRegisterClassExWOW, wc, name, version, client_menu_name, fnid, flags, wow};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserRegisterHotKey(HWND hwnd, INT id, UINT modifiers, UINT vk)
{
    struct NtUserRegisterHotKey_params _ = {qemu_host_win32u_vars->NtUserRegisterHotKey, hwnd, id, modifiers, vk};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserRegisterRawInputDevices(const RAWINPUTDEVICE *devices, UINT device_count, UINT size)
{
    struct NtUserRegisterRawInputDevices_params _ = {qemu_host_win32u_vars->NtUserRegisterRawInputDevices, devices, device_count, size};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserReleaseDC(HWND hwnd, HDC hdc)
{
    struct NtUserReleaseDC_params _ = {qemu_host_win32u_vars->NtUserReleaseDC, hwnd, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserRemoveClipboardFormatListener(HWND hwnd)
{
    struct NtUserRemoveClipboardFormatListener_params _ = {qemu_host_win32u_vars->NtUserRemoveClipboardFormatListener, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserRemoveMenu(HMENU menu, UINT id, UINT flags)
{
    struct NtUserRemoveMenu_params _ = {qemu_host_win32u_vars->NtUserRemoveMenu, menu, id, flags};
    qemu_host_call(&_);
    return _.$ret;
}

HANDLE WINAPI NtUserRemoveProp(HWND hwnd, const WCHAR *str)
{
    struct NtUserRemoveProp_params _ = {qemu_host_win32u_vars->NtUserRemoveProp, hwnd, str};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserScrollDC(HDC hdc, INT dx, INT dy, const RECT *scroll, const RECT *clip, HRGN ret_update_rgn, RECT *update_rect)
{
    struct NtUserScrollDC_params _ = {qemu_host_win32u_vars->NtUserScrollDC, hdc, dx, dy, scroll, clip, ret_update_rgn, update_rect};
    qemu_host_call(&_);
    return _.$ret;
}

DECLSPEC_HIDDEN INT WINAPI NtUserScrollWindowEx(HWND hwnd, INT dx, INT dy, const RECT *rect, const RECT *clip_rect, HRGN update_rgn, RECT *update_rect, UINT flags)
{
    struct NtUserScrollWindowEx_params _ = {qemu_host_win32u_vars->NtUserScrollWindowEx, hwnd, dx, dy, rect, clip_rect, update_rgn, update_rect, flags};
    qemu_host_call(&_);
    return _.$ret;
}

HPALETTE WINAPI NtUserSelectPalette(HDC hdc, HPALETTE palette, WORD force_background)
{
    struct NtUserSelectPalette_params _ = {qemu_host_win32u_vars->NtUserSelectPalette, hdc, palette, force_background};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserSendInput(UINT count, INPUT *inputs, int size)
{
    struct NtUserSendInput_params _ = {qemu_host_win32u_vars->NtUserSendInput, count, inputs, size};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserSetActiveWindow(HWND hwnd)
{
    struct NtUserSetActiveWindow_params _ = {qemu_host_win32u_vars->NtUserSetActiveWindow, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserSetCapture(HWND hwnd)
{
    struct NtUserSetCapture_params _ = {qemu_host_win32u_vars->NtUserSetCapture, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserSetClassLong(HWND hwnd, INT offset, LONG newval, BOOL ansi)
{
    struct NtUserSetClassLong_params _ = {qemu_host_win32u_vars->NtUserSetClassLong, hwnd, offset, newval, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

ULONG_PTR WINAPI NtUserSetClassLongPtr(HWND hwnd, INT offset, LONG_PTR newval, BOOL ansi)
{
    struct NtUserSetClassLongPtr_params _ = {qemu_host_win32u_vars->NtUserSetClassLongPtr, hwnd, offset, newval, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

WORD WINAPI NtUserSetClassWord(HWND hwnd, INT offset, WORD newval)
{
    struct NtUserSetClassWord_params _ = {qemu_host_win32u_vars->NtUserSetClassWord, hwnd, offset, newval};
    qemu_host_call(&_);
    return _.$ret;
}

NTSTATUS WINAPI NtUserSetClipboardData(UINT format, HANDLE handle, struct set_clipboard_params *params)
{
    struct NtUserSetClipboardData_params _ = {qemu_host_win32u_vars->NtUserSetClipboardData, format, handle, params};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserSetClipboardViewer(HWND hwnd)
{
    struct NtUserSetClipboardViewer_params _ = {qemu_host_win32u_vars->NtUserSetClipboardViewer, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

HCURSOR WINAPI NtUserSetCursor(HCURSOR cursor)
{
    struct NtUserSetCursor_params _ = {qemu_host_win32u_vars->NtUserSetCursor, cursor};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetCursorIconData(HCURSOR cursor, UNICODE_STRING *module, UNICODE_STRING *res_name, struct cursoricon_desc *desc)
{
    struct NtUserSetCursorIconData_params _ = {qemu_host_win32u_vars->NtUserSetCursorIconData, cursor, module, res_name, desc};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetCursorPos(INT x, INT y)
{
    struct NtUserSetCursorPos_params _ = {qemu_host_win32u_vars->NtUserSetCursorPos, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserSetFocus(HWND hwnd)
{
    struct NtUserSetFocus_params _ = {qemu_host_win32u_vars->NtUserSetFocus, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

void WINAPI NtUserSetInternalWindowPos(HWND hwnd, UINT cmd, RECT *rect, POINT *pt)
{
    struct NtUserSetInternalWindowPos_params _ = {qemu_host_win32u_vars->NtUserSetInternalWindowPos, hwnd, cmd, rect, pt};
    qemu_host_call(&_);
}

BOOL WINAPI NtUserSetKeyboardState(BYTE *state)
{
    struct NtUserSetKeyboardState_params _ = {qemu_host_win32u_vars->NtUserSetKeyboardState, state};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetLayeredWindowAttributes(HWND hwnd, COLORREF key, BYTE alpha, DWORD flags)
{
    struct NtUserSetLayeredWindowAttributes_params _ = {qemu_host_win32u_vars->NtUserSetLayeredWindowAttributes, hwnd, key, alpha, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetMenu(HWND hwnd, HMENU menu)
{
    struct NtUserSetMenu_params _ = {qemu_host_win32u_vars->NtUserSetMenu, hwnd, menu};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetMenuContextHelpId(HMENU handle, DWORD id)
{
    struct NtUserSetMenuContextHelpId_params _ = {qemu_host_win32u_vars->NtUserSetMenuContextHelpId, handle, id};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetMenuDefaultItem(HMENU handle, UINT item, UINT bypos)
{
    struct NtUserSetMenuDefaultItem_params _ = {qemu_host_win32u_vars->NtUserSetMenuDefaultItem, handle, item, bypos};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetObjectInformation(HANDLE handle, INT index, void *info, DWORD len)
{
    struct NtUserSetObjectInformation_params _ = {qemu_host_win32u_vars->NtUserSetObjectInformation, handle, index, info, len};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserSetParent(HWND hwnd, HWND parent)
{
    struct NtUserSetParent_params _ = {qemu_host_win32u_vars->NtUserSetParent, hwnd, parent};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetProcessDpiAwarenessContext(ULONG awareness, ULONG unknown)
{
    struct NtUserSetProcessDpiAwarenessContext_params _ = {qemu_host_win32u_vars->NtUserSetProcessDpiAwarenessContext, awareness, unknown};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetProcessWindowStation(HWINSTA handle)
{
    struct NtUserSetProcessWindowStation_params _ = {qemu_host_win32u_vars->NtUserSetProcessWindowStation, handle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetProp(HWND hwnd, const WCHAR *str, HANDLE handle)
{
    struct NtUserSetProp_params _ = {qemu_host_win32u_vars->NtUserSetProp, hwnd, str, handle};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserSetScrollInfo(HWND hwnd, INT bar, const SCROLLINFO *info, BOOL redraw)
{
    struct NtUserSetScrollInfo_params _ = {qemu_host_win32u_vars->NtUserSetScrollInfo, hwnd, bar, info, redraw};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetShellWindowEx(HWND shell, HWND list_view)
{
    struct NtUserSetShellWindowEx_params _ = {qemu_host_win32u_vars->NtUserSetShellWindowEx, shell, list_view};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetSysColors(INT count, const INT *colors, const COLORREF *values)
{
    struct NtUserSetSysColors_params _ = {qemu_host_win32u_vars->NtUserSetSysColors, count, colors, values};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetSystemMenu(HWND hwnd, HMENU menu)
{
    struct NtUserSetSystemMenu_params _ = {qemu_host_win32u_vars->NtUserSetSystemMenu, hwnd, menu};
    qemu_host_call(&_);
    return _.$ret;
}

UINT_PTR WINAPI NtUserSetSystemTimer(HWND hwnd, UINT_PTR id, UINT timeout)
{
    struct NtUserSetSystemTimer_params _ = {qemu_host_win32u_vars->NtUserSetSystemTimer, hwnd, id, timeout};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetThreadDesktop(HDESK handle)
{
    struct NtUserSetThreadDesktop_params _ = {qemu_host_win32u_vars->NtUserSetThreadDesktop, handle};
    qemu_host_call(&_);
    return _.$ret;
}

UINT_PTR WINAPI NtUserSetTimer(HWND hwnd, UINT_PTR id, UINT timeout, TIMERPROC proc, ULONG tolerance)
{
    struct NtUserSetTimer_params _ = {qemu_host_win32u_vars->NtUserSetTimer, hwnd, id, timeout, proc, tolerance};
    qemu_host_call(&_);
    return _.$ret;
}

HWINEVENTHOOK WINAPI NtUserSetWinEventHook(DWORD event_min, DWORD event_max, HMODULE inst, UNICODE_STRING *module, WINEVENTPROC proc, DWORD pid, DWORD tid, DWORD flags)
{
    struct NtUserSetWinEventHook_params _ = {qemu_host_win32u_vars->NtUserSetWinEventHook, event_min, event_max, inst, module, proc, pid, tid, flags};
    qemu_host_call(&_);
    return _.$ret;
}

LONG WINAPI NtUserSetWindowLong(HWND hwnd, INT offset, LONG newval, BOOL ansi)
{
    struct NtUserSetWindowLong_params _ = {qemu_host_win32u_vars->NtUserSetWindowLong, hwnd, offset, newval, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

LONG_PTR WINAPI NtUserSetWindowLongPtr(HWND hwnd, INT offset, LONG_PTR newval, BOOL ansi)
{
    struct NtUserSetWindowLongPtr_params _ = {qemu_host_win32u_vars->NtUserSetWindowLongPtr, hwnd, offset, newval, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetWindowPlacement(HWND hwnd, const WINDOWPLACEMENT *wpl)
{
    struct NtUserSetWindowPlacement_params _ = {qemu_host_win32u_vars->NtUserSetWindowPlacement, hwnd, wpl};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSetWindowPos(HWND hwnd, HWND after, INT x, INT y, INT cx, INT cy, UINT flags)
{
    struct NtUserSetWindowPos_params _ = {qemu_host_win32u_vars->NtUserSetWindowPos, hwnd, after, x, y, cx, cy, flags};
    qemu_host_call(&_);
    return _.$ret;
}

int WINAPI NtUserSetWindowRgn(HWND hwnd, HRGN hrgn, BOOL redraw)
{
    struct NtUserSetWindowRgn_params _ = {qemu_host_win32u_vars->NtUserSetWindowRgn, hwnd, hrgn, redraw};
    qemu_host_call(&_);
    return _.$ret;
}

WORD WINAPI NtUserSetWindowWord(HWND hwnd, INT offset, WORD newval)
{
    struct NtUserSetWindowWord_params _ = {qemu_host_win32u_vars->NtUserSetWindowWord, hwnd, offset, newval};
    qemu_host_call(&_);
    return _.$ret;
}

HHOOK WINAPI NtUserSetWindowsHookEx(HINSTANCE inst, UNICODE_STRING *module, DWORD tid, INT id, HOOKPROC proc, BOOL ansi)
{
    struct NtUserSetWindowsHookEx_params _ = {qemu_host_win32u_vars->NtUserSetWindowsHookEx, inst, module, tid, id, proc, ansi};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserShowCaret(HWND hwnd)
{
    struct NtUserShowCaret_params _ = {qemu_host_win32u_vars->NtUserShowCaret, hwnd};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserShowCursor(BOOL show)
{
    struct NtUserShowCursor_params _ = {qemu_host_win32u_vars->NtUserShowCursor, show};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserShowScrollBar(HWND hwnd, INT bar, BOOL show)
{
    struct NtUserShowScrollBar_params _ = {qemu_host_win32u_vars->NtUserShowScrollBar, hwnd, bar, show};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserShowWindow(HWND hwnd, INT cmd)
{
    struct NtUserShowWindow_params _ = {qemu_host_win32u_vars->NtUserShowWindow, hwnd, cmd};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserShowWindowAsync(HWND hwnd, INT cmd)
{
    struct NtUserShowWindowAsync_params _ = {qemu_host_win32u_vars->NtUserShowWindowAsync, hwnd, cmd};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSystemParametersInfo(UINT action, UINT val, void *ptr, UINT winini)
{
    struct NtUserSystemParametersInfo_params _ = {qemu_host_win32u_vars->NtUserSystemParametersInfo, action, val, ptr, winini};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserSystemParametersInfoForDpi(UINT action, UINT val, PVOID ptr, UINT winini, UINT dpi)
{
    struct NtUserSystemParametersInfoForDpi_params _ = {qemu_host_win32u_vars->NtUserSystemParametersInfoForDpi, action, val, ptr, winini, dpi};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserThunkedMenuInfo(HMENU menu, const MENUINFO *info)
{
    struct NtUserThunkedMenuInfo_params _ = {qemu_host_win32u_vars->NtUserThunkedMenuInfo, menu, info};
    qemu_host_call(&_);
    return _.$ret;
}

UINT WINAPI NtUserThunkedMenuItemInfo(HMENU menu, UINT pos, UINT flags, UINT method, MENUITEMINFOW *info, UNICODE_STRING *str)
{
    struct NtUserThunkedMenuItemInfo_params _ = {qemu_host_win32u_vars->NtUserThunkedMenuItemInfo, menu, pos, flags, method, info, str};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserToUnicodeEx(UINT virt, UINT scan, const BYTE *state, WCHAR *str, int size, UINT flags, HKL layout)
{
    struct NtUserToUnicodeEx_params _ = {qemu_host_win32u_vars->NtUserToUnicodeEx, virt, scan, state, str, size, flags, layout};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserTrackMouseEvent(TRACKMOUSEEVENT *info)
{
    struct NtUserTrackMouseEvent_params _ = {qemu_host_win32u_vars->NtUserTrackMouseEvent, info};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserTrackPopupMenuEx(HMENU handle, UINT flags, INT x, INT y, HWND hwnd, TPMPARAMS *params)
{
    struct NtUserTrackPopupMenuEx_params _ = {qemu_host_win32u_vars->NtUserTrackPopupMenuEx, handle, flags, x, y, hwnd, params};
    qemu_host_call(&_);
    return _.$ret;
}

INT WINAPI NtUserTranslateAccelerator(HWND hwnd, HACCEL accel, MSG *msg)
{
    struct NtUserTranslateAccelerator_params _ = {qemu_host_win32u_vars->NtUserTranslateAccelerator, hwnd, accel, msg};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserTranslateMessage(const MSG *msg, UINT flags)
{
    struct NtUserTranslateMessage_params _ = {qemu_host_win32u_vars->NtUserTranslateMessage, msg, flags};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserUnhookWinEvent(HWINEVENTHOOK hEventHook)
{
    struct NtUserUnhookWinEvent_params _ = {qemu_host_win32u_vars->NtUserUnhookWinEvent, hEventHook};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserUnhookWindowsHookEx(HHOOK handle)
{
    struct NtUserUnhookWindowsHookEx_params _ = {qemu_host_win32u_vars->NtUserUnhookWindowsHookEx, handle};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserUnregisterClass(UNICODE_STRING *name, HINSTANCE instance, struct client_menu_name *client_menu_name)
{
    struct NtUserUnregisterClass_params _ = {qemu_host_win32u_vars->NtUserUnregisterClass, name, instance, client_menu_name};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserUnregisterHotKey(HWND hwnd, INT id)
{
    struct NtUserUnregisterHotKey_params _ = {qemu_host_win32u_vars->NtUserUnregisterHotKey, hwnd, id};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserUpdateInputContext(HIMC handle, UINT attr, UINT_PTR value)
{
    struct NtUserUpdateInputContext_params _ = {qemu_host_win32u_vars->NtUserUpdateInputContext, handle, attr, value};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserUpdateLayeredWindow(HWND hwnd, HDC hdc_dst, const POINT *pts_dst, const SIZE *size, HDC hdc_src, const POINT *pts_src, COLORREF key, const BLENDFUNCTION *blend, DWORD flags, const RECT *dirty)
{
    struct NtUserUpdateLayeredWindow_params _ = {qemu_host_win32u_vars->NtUserUpdateLayeredWindow, hwnd, hdc_dst, pts_dst, size, hdc_src, pts_src, key, blend, flags, dirty};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserValidateRect(HWND hwnd, const RECT *rect)
{
    struct NtUserValidateRect_params _ = {qemu_host_win32u_vars->NtUserValidateRect, hwnd, rect};
    qemu_host_call(&_);
    return _.$ret;
}

WORD WINAPI NtUserVkKeyScanEx(WCHAR chr, HKL layout)
{
    struct NtUserVkKeyScanEx_params _ = {qemu_host_win32u_vars->NtUserVkKeyScanEx, chr, layout};
    qemu_host_call(&_);
    return _.$ret;
}

DWORD WINAPI NtUserWaitForInputIdle(HANDLE process, DWORD timeout, BOOL wow)
{
    struct NtUserWaitForInputIdle_params _ = {qemu_host_win32u_vars->NtUserWaitForInputIdle, process, timeout, wow};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI NtUserWaitMessage(void)
{
    struct NtUserWaitMessage_params _ = {qemu_host_win32u_vars->NtUserWaitMessage};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserWindowFromDC(HDC hdc)
{
    struct NtUserWindowFromDC_params _ = {qemu_host_win32u_vars->NtUserWindowFromDC, hdc};
    qemu_host_call(&_);
    return _.$ret;
}

HWND WINAPI NtUserWindowFromPoint(LONG x, LONG y)
{
    struct NtUserWindowFromPoint_params _ = {qemu_host_win32u_vars->NtUserWindowFromPoint, x, y};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI __wine_get_file_outline_text_metric(const WCHAR *path, TEXTMETRICW *otm, UINT *em_square, WCHAR *face_name)
{
    struct __wine_get_file_outline_text_metric_params _ = {qemu_host_win32u_vars->__wine_get_file_outline_text_metric, path, otm, em_square, face_name};
    qemu_host_call(&_);
    return _.$ret;
}

BOOL WINAPI __wine_get_icm_profile(HDC hdc, BOOL allow_default, DWORD *size, WCHAR *filename)
{
    struct __wine_get_icm_profile_params _ = {qemu_host_win32u_vars->__wine_get_icm_profile, hdc, allow_default, size, filename};
    qemu_host_call(&_);
    return _.$ret;
}

WINUSERAPI BOOL WINAPI __wine_send_input( HWND hwnd, const INPUT *input, const RAWINPUT *rawinput )
{
    struct __wine_send_input_params _ = {qemu_host_win32u_vars->__wine_send_input, hwnd, input, rawinput};
    qemu_host_call(&_);
    return _.$ret;
}