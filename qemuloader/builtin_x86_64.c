#ifdef __x86_64__
#include "wine/asm.h"

#include "builtin.h"
#include "syscall_x86_64.h"

#define __STRING(x) #x

#define STRING(x) __STRING(x)

#define DEFINE_SYSCALL(name)                    \
__ASM_GLOBAL_FUNC(name,                         \
     "movl $" STRING(__NR_ ## name) ",%eax \n\t"\
     "movq %rcx, %r10              \n\t"        \
     "syscall                      \n\t"        \
     "ret")

__ASM_GLOBAL_FUNC(__restore,
	 "movl $" STRING(__NR_rt_sigreturn) ", %eax \n\t"
	 "syscall");

__ASM_GLOBAL_FUNC(__restore_rt,
	 "movl $" STRING(__NR_rt_sigreturn) ", %eax \n\t"
	 "syscall");

__ASM_GLOBAL_FUNC(munmap_and_exit,
     "movl $" STRING(__NR_munmap) ",%eax\n\t"
     "syscall                           \n\t"
     "movq %rdx, %rdi                   \n\t"
     "movl $" STRING(__NR_exit) ", %eax \n\t"
     "syscall");

__ASM_GLOBAL_FUNC(exit,
     "movl $" STRING(__NR_exit_group) ",%eax\n\t"
     "syscall");

__ASM_GLOBAL_FUNC(stat,
     "xorq %r10, %r10                        \n\t"
     "movq %rsi, %rdx                        \n\t"
     "movq %rdi, %rsi                        \n\t"
     "movl $-100, %edi                       \n\t"
     "movl $" STRING(__NR_newfstatat) ", %eax\n\t"
     "syscall                                \n\t"
     "ret");

__ASM_GLOBAL_FUNC(clone,
    "xor %eax,%eax      \n\t"
    "mov $56,%al        \n\t"
    "mov %rdi,%r11      \n\t"
    "mov %rdx,%rdi      \n\t"
    "mov %r8,%rdx       \n\t"
    "mov %r9,%r8        \n\t"
    "mov 8(%rsp),%r10   \n\t"
    "mov %r11,%r9       \n\t"
    "and $-16,%rsi      \n\t"
    "sub $8,%rsi        \n\t"
    "mov %rcx,(%rsi)    \n\t"
    "syscall            \n\t"
    "test %eax,%eax     \n\t"
    "jnz 1f             \n\t"
    "xor %ebp,%ebp      \n\t"
    "pop %rdi           \n\t"
    "call *%r9          \n\t"
    "mov %eax,%edi      \n\t"
    "xor %eax,%eax      \n\t"
    "mov $60,%al        \n\t"
    "syscall            \n\t"
    "hlt                \n\t"
    "1: ret");

/***********************************************************************
 *           qemu_host_direct_call
 */
__ASM_GLOBAL_FUNC( qemu_host_direct_call,
                   "movl $"STRING(__NR_CALLCODE)", %eax \n\t"
                   "syscall                             \n\t"
                   "ret                                 \n\t");

DEFINE_SYSCALL(mmap);

DEFINE_SYSCALL(mprotect);

DEFINE_SYSCALL(close);

DEFINE_SYSCALL(sigaltstack);

DEFINE_SYSCALL(arch_prctl);

DEFINE_SYSCALL(rt_sigprocmask);

DEFINE_SYSCALL(rt_sigaction);

#ifdef QEMULOADER_STATIC

__ASM_GLOBAL_FUNC(_start,
    "xor %rbp, %rbp                         \n\t"
    "leaq 8(%rsp), %rsi                     \n\t"
    "movq 0(%rsp), %rdi                     \n\t"
    "andq $-16, %rsp                        \n\t"
    "call main                              \n\t"
    "movq %rax,    %rdi                     \n\t"
    "movl $" STRING(__NR_exit_group)", %eax \n\t"
    "syscall");


__ASM_GLOBAL_FUNC(NtCurrentTeb,
     "leaq   -24(%rsp), %rdi                      \n\t"
     "movq  qemu_host_NtCurrentTeb(%rip), %rax    \n\t"
     "movq  %rax, (%rdi)                          \n\t"
     "movl  $"STRING(__NR_CALLCODE)", %eax        \n\t"
     "syscall                                     \n\t"
     "movq  -16(%rsp), %rax                        \n\t"
     "ret");

#endif

#endif