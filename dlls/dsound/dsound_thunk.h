/*  			DirectSound
 *
 * Copyright 1998 Marcus Meissner
 * Copyright 1998 Rob Riggs
 * Copyright 2000-2001 TransGaming Technologies, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "ntstatus.h"
#include "windef.h"
#include "winbase.h"
#include "winternl.h"
#include "wine/unixlib.h"

#ifndef __DSOUND_THUNK_H
#define __DSOUND_THUNK_H

enum unix_funcs
{
    unix_cp_fields_noresample,
    unix_cp_fields_resample,
};

struct cp_fields_resample_params
{
	UINT64 tmp_buffer, committedbuff, buffer_memory, cp_buffer;
	LONG64 freqAcc_start, freqAdjustNum, firstep, freqAdjustDen;
	float firgain;
	UINT istride, ostride;
	UINT required_input;
	UINT fir_cachesize;
	DWORD pwfx_channels;
	DWORD writelead;
	DWORD committed_mixpos;
	DWORD count;
	DWORD mix_channels;
	DWORD sec_mixpos;
	DWORD buflen;
	DWORD use_committed;
	DWORD is_loop;
	DWORD get, put, put_aux;
};

struct cp_fields_noresample_params
{
	UINT64 tmp_buffer, committedbuff, buffer_memory;
	UINT istride, ostride;
	DWORD pwfx_channels;
	DWORD writelead;
	DWORD committed_mixpos;
	DWORD count;
	DWORD mix_channels;
	DWORD sec_mixpos;
	DWORD buflen;
	DWORD use_committed;
	DWORD is_loop;
	DWORD get, put, put_aux;
};

#endif//__DSOUND_THUNK_H