/* DirectSound format conversion and mixing routines
 *
 * Copyright 2007 Maarten Lankhorst
 * Copyright 2011 Owen Rudge for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/* 8 bits is unsigned, the rest is signed.
 * First I tried to reuse existing stuff from alsa-lib, after that
 * didn't work, I gave up and just went for individual hacks.
 *
 * 24 bit is expensive to do, due to unaligned access.
 * In dlls/winex11.drv/dib_convert.c convert_888_to_0888_asis there is a way
 * around it, but I'm happy current code works, maybe something for later.
 *
 * The ^ 0x80 flips the signed bit, this is the conversion from
 * signed (-128.. 0.. 127) to unsigned (0...255)
 * This is only temporary: All 8 bit data should be converted to signed.
 * then when fed to the sound card, it should be converted to unsigned again.
 *
 * Sound is LITTLE endian
 */

#if 0
#pragma makedep unix
#endif

#include "config.h"
#include <stdarg.h>
#include <math.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windef.h"
#include "winbase.h"
#include "mmsystem.h"
#include "wine/debug.h"
#include "dsound_thunk.h"
#include "fir.h"
#include "wine/unixlib.h"

#ifdef WORDS_BIGENDIAN
#define le16(x) RtlUshortByteSwap((x))
#define le32(x) RtlUlongByteSwap((x))
#else
#define le16(x) (x)
#define le32(x) (x)
#endif

static float get8(BYTE *base, DWORD channel)
{
    const BYTE *buf = base + channel;
    return (buf[0] - 0x80) / (float)0x80;
}

static float get16(BYTE *base, DWORD channel)
{
    const BYTE *buf = base + 2 * channel;
    const SHORT *sbuf = (const SHORT*)(buf);
    SHORT sample = (SHORT)le16(*sbuf);
    return sample / (float)0x8000;
}

static float get24(BYTE *base, DWORD channel)
{
    LONG sample;
    const BYTE *buf = base + 3 * channel;

    /* The next expression deliberately has an overflow for buf[2] >= 0x80,
       this is how negative values are made.
     */
    sample = (buf[0] << 8) | (buf[1] << 16) | (buf[2] << 24);
    return sample / (float)0x80000000U;
}

static float get32(BYTE *base, DWORD channel)
{
    const BYTE *buf = base + 4 * channel;
    const LONG *sbuf = (const LONG*)(buf);
    LONG sample = le32(*sbuf);
    return sample / (float)0x80000000U;
}

static float getieee32(BYTE *base, DWORD channel)
{
    const BYTE *buf = base + 4 * channel;
    const float *sbuf = (const float*)(buf);
    /* The value will be clipped later, when put into some non-float buffer */
    return *sbuf;
}

typedef float(*bitsgetfunc)(BYTE*, DWORD);

static const bitsgetfunc getbpps[] =
{
    get8,
    get16,
    get24,
    get32,
    getieee32,
};

typedef void (*bitsputfunc)(void*, BYTE*, DWORD pos, DWORD channel, float value);

static void putieee32(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    float *fbuf = (float*)(buf + pos + sizeof(float) * channel);
    *fbuf = value;
}

static void putieee32_sum(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    float *fbuf = (float*)(buf + pos + sizeof(float) * channel);
    *fbuf += value;
}

static void put_mono2stereo(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
}

static void put_mono2quad(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 2, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 3, value);
}

static void put_stereo2quad(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    if (channel == 0) { /* Left */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value); /* Front left */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 2, value); /* Back left */
    } else if (channel == 1) { /* Right */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value); /* Front right */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 3, value); /* Back right */
    }
}

static void put_mono2surround51(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 2, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 3, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 4, value);
    (*(bitsputfunc)put_aux)(put_aux, buf, pos, 5, value);
}

static void put_stereo2surround51(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    if (channel == 0) { /* Left */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value); /* Front left */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 4, value); /* Back left */

        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 2, 0.0f); /* Mute front centre */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 3, 0.0f); /* Mute LFE */
    } else if (channel == 1) { /* Right */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value); /* Front right */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 5, value); /* Back right */
    }
}

static void put_surround512stereo(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    /* based on analyzing a recording of a dsound downmix */
    switch(channel){

    case 4: /* surround left */
        value *= 0.24f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        break;

    case 0: /* front left */
        value *= 1.0f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        break;

    case 5: /* surround right */
        value *= 0.24f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 1: /* front right */
        value *= 1.0f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 2: /* centre */
        value *= 0.7;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 3:
        /* LFE is totally ignored in dsound when downmixing to 2 channels */
        break;
    }
}

static void put_surround712stereo(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    /* based on analyzing a recording of a dsound downmix */
    switch(channel){

    case 6: /* back left */
        value *= 0.24f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        break;

    case 4: /* surround left */
        value *= 0.24f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        break;

    case 0: /* front left */
        value *= 1.0f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        break;

    case 7: /* back right */
        value *= 0.24f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 5: /* surround right */
        value *= 0.24f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 1: /* front right */
        value *= 1.0f;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 2: /* centre */
        value *= 0.7;
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 3:
        /* LFE is totally ignored in dsound when downmixing to 2 channels */
        break;
    }
}

static void put_quad2stereo(void* put_aux, BYTE* buf, DWORD pos, DWORD channel, float value)
{
    /* based on pulseaudio's downmix algorithm */
    switch(channel){

    case 2: /* back left */
        value *= 0.1f; /* (1/9) / (sum of left volumes) */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        break;

    case 0: /* front left */
        value *= 0.9f; /* 1 / (sum of left volumes) */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 0, value);
        break;

    case 3: /* back right */
        value *= 0.1f; /* (1/9) / (sum of right volumes) */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;

    case 1: /* front right */
        value *= 0.9f; /* 1 / (sum of right volumes) */
        (*(bitsputfunc)put_aux)(put_aux, buf, pos, 1, value);
        break;
    }
}

static bitsputfunc putbpps[] =
{
    putieee32,
    putieee32_sum,
    put_mono2stereo,
    put_mono2quad,
    put_stereo2quad,
    put_mono2surround51,
    put_stereo2surround51,
    put_surround512stereo,
    put_surround712stereo,
    put_quad2stereo,
};

static inline float get_current_sample(bitsgetfunc get, BOOL is_loop,
        BYTE *buffer, DWORD buflen, DWORD mixpos, DWORD channel)
{
    if (mixpos >= buflen && !is_loop)
        return 0.0f;
    return (*get)(buffer + (mixpos % buflen), channel);
}

static inline float get_current_sample_mono(bitsgetfunc get_aux, BOOL is_loop,
        BYTE *buffer, DWORD buflen, DWORD mixpos, DWORD channel)
{

    if (mixpos >= buflen && !is_loop)
        return 0.0f;
	else
	{
		float val = 0;
		BYTE* base = buffer + (mixpos % buflen);
		for (int c = 0; c < channel; c++)
			val += (*get_aux)(base, c);
		val /= channel;
		return val;
	}
}

static NTSTATUS cp_fields_noresample(struct cp_fields_noresample_params const* dsb)
{
    UINT istride = dsb->istride;
    UINT ostride = dsb->ostride;
    UINT committed_samples = 0;
    DWORD i, channel = dsb->pwfx_channels;
	BYTE* tmp_buffer = (BYTE*)(UINT_PTR)dsb->tmp_buffer;
	BYTE* committedbuff = (BYTE*)(UINT_PTR)dsb->committedbuff;
	BYTE* memory = (BYTE*)(UINT_PTR)dsb->buffer_memory;
    const bitsputfunc put = putbpps[dsb->put], put_aux = putbpps[dsb->put_aux];
    const bitsgetfunc get = getbpps[dsb->get];
    if(dsb->use_committed) {
        committed_samples = (dsb->writelead - dsb->committed_mixpos) / istride;
        committed_samples = committed_samples <= dsb->count ? committed_samples : dsb->count;
    }

	if (channel) {
		for (i = 0; i < committed_samples; i++) {
			const float sample = get_current_sample_mono(get, dsb->is_loop, committedbuff,
					dsb->writelead, dsb->committed_mixpos + i * istride, channel);
			for (int j = 0; j < dsb->mix_channels; j++)
				(*put)(put_aux, tmp_buffer, i * ostride, j, sample);
		}

		for (; i < dsb->count; i++) {
			const float sample = get_current_sample_mono(get, dsb->is_loop, committedbuff,
					dsb->writelead, dsb->committed_mixpos + i * istride, channel);
			for (int j = 0; j < dsb->mix_channels; j++)
				(*put)(put_aux, tmp_buffer, i * ostride, j, sample);
		}
	}
	else {
		for (i = 0; i < committed_samples; i++)
			for (channel = 0; channel < dsb->mix_channels; channel++)
				(*put)(put_aux, tmp_buffer, i * ostride, channel, get_current_sample(get, dsb->is_loop, committedbuff,
					dsb->writelead, dsb->committed_mixpos + i * istride, channel));

		for (; i < dsb->count; i++)
			for (channel = 0; channel < dsb->mix_channels; channel++)
				(*put)(put_aux, tmp_buffer, i * ostride, channel, get_current_sample(get, dsb->is_loop, memory,
					dsb->buflen, dsb->sec_mixpos + i * istride, channel));
	}
    return 0;
}

static NTSTATUS cp_fields_resample(struct cp_fields_resample_params const* dsb)
{
	UINT istride = dsb->istride;
    UINT ostride = dsb->ostride;
	UINT required_input = dsb->required_input;
    UINT committed_samples = 0;
	UINT dsbfirstep = dsb->firstep;
    DWORD i, channel = dsb->pwfx_channels;
	BYTE* tmp_buffer = (BYTE*)(UINT_PTR)dsb->tmp_buffer;
	BYTE* committedbuff = (BYTE*)(UINT_PTR)dsb->committedbuff;
	BYTE* memory = (BYTE*)(UINT_PTR)dsb->buffer_memory;
	float* fir_copy = (float*)(UINT_PTR)dsb->cp_buffer;
	float* intermediate = fir_copy + dsb->fir_cachesize;
	float* itmp = intermediate;
    const bitsputfunc put = putbpps[dsb->put], put_aux = putbpps[dsb->put_aux];
    const bitsgetfunc get = getbpps[dsb->get];
    if(dsb->use_committed) {
        committed_samples = (dsb->writelead - dsb->committed_mixpos) / istride;
        committed_samples = committed_samples <= required_input ? committed_samples : required_input;
    }

    /* Important: this buffer MUST be non-interleaved
     * if you want -msse3 to have any effect.
     * This is good for CPU cache effects, too.
     */
	if (channel) {
		for (i = 0; i < committed_samples; i++)
			*(itmp++) = get_current_sample_mono(get, dsb->is_loop, committedbuff,
				dsb->writelead, dsb->committed_mixpos + i * istride, channel);
		for (; i < required_input; i++)
			*(itmp++) = get_current_sample_mono(get, dsb->is_loop, memory,
					dsb->buflen, dsb->sec_mixpos + i * istride, channel);
	} else {
		for (channel = 0; channel < dsb->mix_channels; channel++) {
			for (i = 0; i < committed_samples; i++)
				*(itmp++) = get_current_sample(get, dsb->is_loop, committedbuff,
					dsb->writelead, dsb->committed_mixpos + i * istride, channel);
			for (; i < required_input; i++)
				*(itmp++) = get_current_sample(get, dsb->is_loop, memory,
						dsb->buflen, dsb->sec_mixpos + i * istride, channel);
		}
	}

    for(i = 0; i < dsb->count; ++i) {
        UINT int_fir_steps = (dsb->freqAcc_start + i * dsb->freqAdjustNum) * dsbfirstep / dsb->freqAdjustDen;
        float total_fir_steps = (dsb->freqAcc_start + i * dsb->freqAdjustNum) * dsbfirstep / (float)dsb->freqAdjustDen;
        UINT ipos = int_fir_steps / dsbfirstep;

        UINT idx = (ipos + 1) * dsbfirstep - int_fir_steps - 1;
        float rem = int_fir_steps + 1.0 - total_fir_steps;

        int fir_used = 0;
        while (idx < fir_len - 1) {
            fir_copy[fir_used++] = fir[idx] * (1.0 - rem) + fir[idx + 1] * rem;
            idx += dsb->firstep;
        }

        // assert(fir_used <= dsb->fir_cachesize);
        // assert(ipos + fir_used <= required_input);

        for (channel = 0; channel < dsb->mix_channels; channel++) {
            int j;
            float sum = 0.0;
            float* cache = &intermediate[channel * required_input + ipos];
            for (j = 0; j < fir_used; j++)
                sum += fir_copy[j] * cache[j];
            (*put)(put_aux, (BYTE *)tmp_buffer, i * ostride, channel, sum * dsb->firgain);
        }
    }
    return 0;
}

const unixlib_entry_t __wine_unix_call_wow64_funcs[] =
{
    (unixlib_entry_t)cp_fields_noresample,
    (unixlib_entry_t)cp_fields_resample,
};

const unixlib_entry_t __wine_unix_call_funcs[] =
{
    (unixlib_entry_t)cp_fields_noresample,
    (unixlib_entry_t)cp_fields_resample,
};