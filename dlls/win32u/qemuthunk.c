/*
 * Unix interface for qemuthunk
 *
 * Copyright 2022 Fan Wen Jie
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if 0
#pragma makedep unix
#endif

#include <stdarg.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windef.h"
#include "winnt.h"
#include "winbase.h"
#include "ntgdi.h"
#include "ntuser.h"
#include "wine/unixlib.h"
#include "wine/qemuthunk.h"

static void QEMU_HOST(NtGdiAbortDoc)(void *$context)
{
    struct NtGdiAbortDoc_params *_ = (typeof(_))$context;
    _->$ret = NtGdiAbortDoc(_->hdc);
}

static void QEMU_HOST(NtGdiAbortPath)(void *$context)
{
    struct NtGdiAbortPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiAbortPath(_->hdc);
}

static void QEMU_HOST(NtGdiAddFontMemResourceEx)(void *$context)
{
    struct NtGdiAddFontMemResourceEx_params *_ = (typeof(_))$context;
    _->$ret = NtGdiAddFontMemResourceEx(_->ptr, _->size, _->dv, _->dv_size, _->count);
}

static void QEMU_HOST(NtGdiAddFontResourceW)(void *$context)
{
    struct NtGdiAddFontResourceW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiAddFontResourceW(_->str, _->size, _->files, _->flags, _->tid, _->dv);
}

static void QEMU_HOST(NtGdiAlphaBlend)(void *$context)
{
    struct NtGdiAlphaBlend_params *_ = (typeof(_))$context;
    _->$ret = NtGdiAlphaBlend(_->hdc_dst, _->x_dst, _->y_dst, _->width_dst, _->height_dst, _->hdc_src, _->x_src, _->y_src, _->width_src, _->height_src, _->blend_function, _->xform);
}

static void QEMU_HOST(NtGdiAngleArc)(void *$context)
{
    struct NtGdiAngleArc_params *_ = (typeof(_))$context;
    _->$ret = NtGdiAngleArc(_->hdc, _->x, _->y, _->dwRadius, _->start_angle, _->sweep_angle);
}

static void QEMU_HOST(NtGdiArcInternal)(void *$context)
{
    struct NtGdiArcInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiArcInternal(_->type, _->hdc, _->left, _->top, _->right, _->bottom, _->xstart, _->ystart, _->xend, _->yend);
}

static void QEMU_HOST(NtGdiBeginPath)(void *$context)
{
    struct NtGdiBeginPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiBeginPath(_->hdc);
}

static void QEMU_HOST(NtGdiBitBlt)(void *$context)
{
    struct NtGdiBitBlt_params *_ = (typeof(_))$context;
    _->$ret = NtGdiBitBlt(_->hdc_dst, _->x_dst, _->y_dst, _->width, _->height, _->hdc_src, _->x_src, _->y_src, _->rop, _->bk_color, _->fl);
}

static void QEMU_HOST(NtGdiCloseFigure)(void *$context)
{
    struct NtGdiCloseFigure_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCloseFigure(_->hdc);
}

static void QEMU_HOST(NtGdiCombineRgn)(void *$context)
{
    struct NtGdiCombineRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCombineRgn(_->dest, _->src1, _->src2, _->mode);
}

static void QEMU_HOST(NtGdiComputeXformCoefficients)(void *$context)
{
    struct NtGdiComputeXformCoefficients_params *_ = (typeof(_))$context;
    _->$ret = NtGdiComputeXformCoefficients(_->hdc);
}

static void QEMU_HOST(NtGdiCreateBitmap)(void *$context)
{
    struct NtGdiCreateBitmap_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateBitmap(_->width, _->height, _->planes, _->bpp, _->bits);
}

static void QEMU_HOST(NtGdiCreateClientObj)(void *$context)
{
    struct NtGdiCreateClientObj_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateClientObj(_->type);
}

static void QEMU_HOST(NtGdiCreateCompatibleBitmap)(void *$context)
{
    struct NtGdiCreateCompatibleBitmap_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateCompatibleBitmap(_->hdc, _->width, _->height);
}

static void QEMU_HOST(NtGdiCreateCompatibleDC)(void *$context)
{
    struct NtGdiCreateCompatibleDC_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateCompatibleDC(_->hdc);
}

static void QEMU_HOST(NtGdiCreateDIBBrush)(void *$context)
{
    struct NtGdiCreateDIBBrush_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateDIBBrush(_->data, _->coloruse, _->size, _->is_8x8, _->pen, _->client);
}

static void QEMU_HOST(NtGdiCreateDIBSection)(void *$context)
{
    struct NtGdiCreateDIBSection_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateDIBSection(_->hdc, _->section, _->offset, _->bmi, _->usage, _->header_size, _->flags, _->color_space, _->bits);
}

static void QEMU_HOST(NtGdiCreateDIBitmapInternal)(void *$context)
{
    struct NtGdiCreateDIBitmapInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateDIBitmapInternal(_->hdc, _->width, _->height, _->init, _->bits, _->data, _->coloruse, _->max_info, _->max_bits, _->flags, _->xform);
}

static void QEMU_HOST(NtGdiCreateEllipticRgn)(void *$context)
{
    struct NtGdiCreateEllipticRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateEllipticRgn(_->left, _->top, _->right, _->bottom);
}

static void QEMU_HOST(NtGdiCreateHalftonePalette)(void *$context)
{
    struct NtGdiCreateHalftonePalette_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateHalftonePalette(_->hdc);
}

static void QEMU_HOST(NtGdiCreateHatchBrushInternal)(void *$context)
{
    struct NtGdiCreateHatchBrushInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateHatchBrushInternal(_->style, _->color, _->pen);
}

static void QEMU_HOST(NtGdiCreateMetafileDC)(void *$context)
{
    struct NtGdiCreateMetafileDC_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateMetafileDC(_->hdc);
}

static void QEMU_HOST(NtGdiCreatePaletteInternal)(void *$context)
{
    struct NtGdiCreatePaletteInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreatePaletteInternal(_->palette, _->count);
}

static void QEMU_HOST(NtGdiCreatePatternBrushInternal)(void *$context)
{
    struct NtGdiCreatePatternBrushInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreatePatternBrushInternal(_->hbitmap, _->pen, _->is_8x8);
}

static void QEMU_HOST(NtGdiCreatePen)(void *$context)
{
    struct NtGdiCreatePen_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreatePen(_->style, _->width, _->color, _->brush);
}

static void QEMU_HOST(NtGdiCreateRectRgn)(void *$context)
{
    struct NtGdiCreateRectRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateRectRgn(_->left, _->top, _->right, _->bottom);
}

static void QEMU_HOST(NtGdiCreateRoundRectRgn)(void *$context)
{
    struct NtGdiCreateRoundRectRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateRoundRectRgn(_->left, _->top, _->right, _->bottom, _->ellipse_width, _->ellipse_height);
}

static void QEMU_HOST(NtGdiCreateSolidBrush)(void *$context)
{
    struct NtGdiCreateSolidBrush_params *_ = (typeof(_))$context;
    _->$ret = NtGdiCreateSolidBrush(_->color, _->brush);
}

static void QEMU_HOST(NtGdiDdDDICheckVidPnExclusiveOwnership)(void *$context)
{
    struct NtGdiDdDDICheckVidPnExclusiveOwnership_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDICheckVidPnExclusiveOwnership(_->desc);
}

static void QEMU_HOST(NtGdiDdDDICloseAdapter)(void *$context)
{
    struct NtGdiDdDDICloseAdapter_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDICloseAdapter(_->desc);
}

static void QEMU_HOST(NtGdiDdDDICreateDCFromMemory)(void *$context)
{
    struct NtGdiDdDDICreateDCFromMemory_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDICreateDCFromMemory(_->desc);
}

static void QEMU_HOST(NtGdiDdDDICreateDevice)(void *$context)
{
    struct NtGdiDdDDICreateDevice_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDICreateDevice(_->desc);
}

static void QEMU_HOST(NtGdiDdDDIDestroyDCFromMemory)(void *$context)
{
    struct NtGdiDdDDIDestroyDCFromMemory_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIDestroyDCFromMemory(_->desc);
}

static void QEMU_HOST(NtGdiDdDDIDestroyDevice)(void *$context)
{
    struct NtGdiDdDDIDestroyDevice_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIDestroyDevice(_->desc);
}

static void QEMU_HOST(NtGdiDdDDIEscape)(void *$context)
{
    struct NtGdiDdDDIEscape_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIEscape(_->desc);
}

static void QEMU_HOST(NtGdiDdDDIOpenAdapterFromDeviceName)(void *$context)
{
    struct NtGdiDdDDIOpenAdapterFromDeviceName_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIOpenAdapterFromDeviceName(_->desc);
}

static void QEMU_HOST(NtGdiDdDDIOpenAdapterFromHdc)(void *$context)
{
    struct NtGdiDdDDIOpenAdapterFromHdc_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIOpenAdapterFromHdc(_->desc);
}

static void QEMU_HOST(NtGdiDdDDIOpenAdapterFromLuid)(void *$context)
{
    struct NtGdiDdDDIOpenAdapterFromLuid_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIOpenAdapterFromLuid(_->desc);
}

static void QEMU_HOST(NtGdiDdDDIQueryStatistics)(void *$context)
{
    struct NtGdiDdDDIQueryStatistics_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIQueryStatistics(_->stats);
}

static void QEMU_HOST(NtGdiDdDDIQueryVideoMemoryInfo)(void *$context)
{
    struct NtGdiDdDDIQueryVideoMemoryInfo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDIQueryVideoMemoryInfo(_->desc);
}

static void QEMU_HOST(NtGdiDdDDISetQueuedLimit)(void *$context)
{
    struct NtGdiDdDDISetQueuedLimit_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDISetQueuedLimit(_->desc);
}

static void QEMU_HOST(NtGdiDdDDISetVidPnSourceOwner)(void *$context)
{
    struct NtGdiDdDDISetVidPnSourceOwner_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDdDDISetVidPnSourceOwner(_->desc);
}

static void QEMU_HOST(NtGdiDeleteClientObj)(void *$context)
{
    struct NtGdiDeleteClientObj_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDeleteClientObj(_->obj);
}

static void QEMU_HOST(NtGdiDeleteObjectApp)(void *$context)
{
    struct NtGdiDeleteObjectApp_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDeleteObjectApp(_->obj);
}

static void QEMU_HOST(NtGdiDescribePixelFormat)(void *$context)
{
    struct NtGdiDescribePixelFormat_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDescribePixelFormat(_->hdc, _->format, _->size, _->descr);
}

static void QEMU_HOST(NtGdiDoPalette)(void *$context)
{
    struct NtGdiDoPalette_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDoPalette(_->handle, _->start, _->count, _->entries, _->func, _->inbound);
}

static void QEMU_HOST(NtGdiDrawStream)(void *$context)
{
    struct NtGdiDrawStream_params *_ = (typeof(_))$context;
    _->$ret = NtGdiDrawStream(_->hdc, _->in, _->pvin);
}

static void QEMU_HOST(NtGdiEllipse)(void *$context)
{
    struct NtGdiEllipse_params *_ = (typeof(_))$context;
    _->$ret = NtGdiEllipse(_->hdc, _->left, _->top, _->right, _->bottom);
}

static void QEMU_HOST(NtGdiEndDoc)(void *$context)
{
    struct NtGdiEndDoc_params *_ = (typeof(_))$context;
    _->$ret = NtGdiEndDoc(_->hdc);
}

static void QEMU_HOST(NtGdiEndPage)(void *$context)
{
    struct NtGdiEndPage_params *_ = (typeof(_))$context;
    _->$ret = NtGdiEndPage(_->hdc);
}

static void QEMU_HOST(NtGdiEndPath)(void *$context)
{
    struct NtGdiEndPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiEndPath(_->hdc);
}

static void QEMU_HOST(NtGdiEnumFonts)(void *$context)
{
    struct NtGdiEnumFonts_params *_ = (typeof(_))$context;
    _->$ret = NtGdiEnumFonts(_->hdc, _->type, _->win32_compat, _->face_name_len, _->face_name, _->charset, _->count, _->buf);
}

static void QEMU_HOST(NtGdiEqualRgn)(void *$context)
{
    struct NtGdiEqualRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiEqualRgn(_->hrgn1, _->hrgn2);
}

static void QEMU_HOST(NtGdiExcludeClipRect)(void *$context)
{
    struct NtGdiExcludeClipRect_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExcludeClipRect(_->hdc, _->left, _->top, _->right, _->bottom);
}

static void QEMU_HOST(NtGdiExtCreatePen)(void *$context)
{
    struct NtGdiExtCreatePen_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExtCreatePen(_->style, _->width, _->brush_style, _->color, _->client_hatch, _->hatch, _->style_count, _->style_bits, _->dib_size, _->old_style, _->brush);
}

static void QEMU_HOST(NtGdiExtCreateRegion)(void *$context)
{
    struct NtGdiExtCreateRegion_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExtCreateRegion(_->xform, _->count, _->data);
}

static void QEMU_HOST(NtGdiExtEscape)(void *$context)
{
    struct NtGdiExtEscape_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExtEscape(_->hdc, _->driver, _->driver_id, _->escape, _->input_size, _->input, _->output_size, _->output);
}

static void QEMU_HOST(NtGdiExtFloodFill)(void *$context)
{
    struct NtGdiExtFloodFill_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExtFloodFill(_->hdc, _->x, _->y, _->color, _->type);
}

static void QEMU_HOST(NtGdiExtGetObjectW)(void *$context)
{
    struct NtGdiExtGetObjectW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExtGetObjectW(_->handle, _->count, _->buffer);
}

static void QEMU_HOST(NtGdiExtSelectClipRgn)(void *$context)
{
    struct NtGdiExtSelectClipRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExtSelectClipRgn(_->hdc, _->region, _->mode);
}

static void QEMU_HOST(NtGdiExtTextOutW)(void *$context)
{
    struct NtGdiExtTextOutW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiExtTextOutW(_->hdc, _->x, _->y, _->flags, _->rect, _->str, _->count, _->dx, _->cp);
}

static void QEMU_HOST(NtGdiFillPath)(void *$context)
{
    struct NtGdiFillPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiFillPath(_->hdc);
}

static void QEMU_HOST(NtGdiFillRgn)(void *$context)
{
    struct NtGdiFillRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiFillRgn(_->hdc, _->hrgn, _->hbrush);
}

static void QEMU_HOST(NtGdiFlattenPath)(void *$context)
{
    struct NtGdiFlattenPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiFlattenPath(_->hdc);
}

static void QEMU_HOST(NtGdiFlush)(void *$context)
{
    struct NtGdiFlush_params *_ = (typeof(_))$context;
    _->$ret = NtGdiFlush();
}

static void QEMU_HOST(NtGdiFontIsLinked)(void *$context)
{
    struct NtGdiFontIsLinked_params *_ = (typeof(_))$context;
    _->$ret = NtGdiFontIsLinked(_->hdc);
}

static void QEMU_HOST(NtGdiFrameRgn)(void *$context)
{
    struct NtGdiFrameRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiFrameRgn(_->hdc, _->hrgn, _->brush, _->width, _->height);
}

static void QEMU_HOST(NtGdiGetAndSetDCDword)(void *$context)
{
    struct NtGdiGetAndSetDCDword_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetAndSetDCDword(_->hdc, _->method, _->value, _->result);
}

static void QEMU_HOST(NtGdiGetAppClipBox)(void *$context)
{
    struct NtGdiGetAppClipBox_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetAppClipBox(_->hdc, _->rect);
}

static void QEMU_HOST(NtGdiGetBitmapBits)(void *$context)
{
    struct NtGdiGetBitmapBits_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetBitmapBits(_->bitmap, _->count, _->bits);
}

static void QEMU_HOST(NtGdiGetBitmapDimension)(void *$context)
{
    struct NtGdiGetBitmapDimension_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetBitmapDimension(_->bitmap, _->size);
}

static void QEMU_HOST(NtGdiGetBoundsRect)(void *$context)
{
    struct NtGdiGetBoundsRect_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetBoundsRect(_->hdc, _->rect, _->flags);
}

static void QEMU_HOST(NtGdiGetCharABCWidthsW)(void *$context)
{
    struct NtGdiGetCharABCWidthsW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetCharABCWidthsW(_->hdc, _->first, _->last, _->chars, _->flags, _->buffer);
}

static void QEMU_HOST(NtGdiGetCharWidthInfo)(void *$context)
{
    struct NtGdiGetCharWidthInfo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetCharWidthInfo(_->hdc, _->info);
}

static void QEMU_HOST(NtGdiGetCharWidthW)(void *$context)
{
    struct NtGdiGetCharWidthW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetCharWidthW(_->hdc, _->first_char, _->last_char, _->chars, _->flags, _->buffer);
}

static void QEMU_HOST(NtGdiGetColorAdjustment)(void *$context)
{
    struct NtGdiGetColorAdjustment_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetColorAdjustment(_->hdc, _->ca);
}

static void QEMU_HOST(NtGdiGetDCDword)(void *$context)
{
    struct NtGdiGetDCDword_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetDCDword(_->hdc, _->method, _->result);
}

static void QEMU_HOST(NtGdiGetDCObject)(void *$context)
{
    struct NtGdiGetDCObject_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetDCObject(_->hdc, _->type);
}

static void QEMU_HOST(NtGdiGetDCPoint)(void *$context)
{
    struct NtGdiGetDCPoint_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetDCPoint(_->hdc, _->method, _->result);
}

static void QEMU_HOST(NtGdiGetDIBitsInternal)(void *$context)
{
    struct NtGdiGetDIBitsInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetDIBitsInternal(_->hdc, _->hbitmap, _->startscan, _->lines, _->bits, _->info, _->coloruse, _->max_bits, _->max_info);
}

static void QEMU_HOST(NtGdiGetDeviceCaps)(void *$context)
{
    struct NtGdiGetDeviceCaps_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetDeviceCaps(_->hdc, _->cap);
}

static void QEMU_HOST(NtGdiGetDeviceGammaRamp)(void *$context)
{
    struct NtGdiGetDeviceGammaRamp_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetDeviceGammaRamp(_->hdc, _->ptr);
}

static void QEMU_HOST(NtGdiGetFontData)(void *$context)
{
    struct NtGdiGetFontData_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetFontData(_->hdc, _->table, _->offset, _->buffer, _->length);
}

static void QEMU_HOST(NtGdiGetFontFileData)(void *$context)
{
    struct NtGdiGetFontFileData_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetFontFileData(_->instance_id, _->file_index, _->offset, _->buff, _->buff_size);
}

static void QEMU_HOST(NtGdiGetFontFileInfo)(void *$context)
{
    struct NtGdiGetFontFileInfo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetFontFileInfo(_->instance_id, _->file_index, _->info, _->size, _->needed);
}

static void QEMU_HOST(NtGdiGetFontUnicodeRanges)(void *$context)
{
    struct NtGdiGetFontUnicodeRanges_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetFontUnicodeRanges(_->hdc, _->lpgs);
}

static void QEMU_HOST(NtGdiGetGlyphIndicesW)(void *$context)
{
    struct NtGdiGetGlyphIndicesW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetGlyphIndicesW(_->hdc, _->str, _->count, _->indices, _->flags);
}

static void QEMU_HOST(NtGdiGetGlyphOutline)(void *$context)
{
    struct NtGdiGetGlyphOutline_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetGlyphOutline(_->hdc, _->ch, _->format, _->metrics, _->size, _->buffer, _->mat2, _->ignore_rotation);
}

static void QEMU_HOST(NtGdiGetKerningPairs)(void *$context)
{
    struct NtGdiGetKerningPairs_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetKerningPairs(_->hdc, _->count, _->kern_pair);
}

static void QEMU_HOST(NtGdiGetNearestColor)(void *$context)
{
    struct NtGdiGetNearestColor_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetNearestColor(_->hdc, _->color);
}

static void QEMU_HOST(NtGdiGetNearestPaletteIndex)(void *$context)
{
    struct NtGdiGetNearestPaletteIndex_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetNearestPaletteIndex(_->hpalette, _->color);
}

static void QEMU_HOST(NtGdiGetOutlineTextMetricsInternalW)(void *$context)
{
    struct NtGdiGetOutlineTextMetricsInternalW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetOutlineTextMetricsInternalW(_->hdc, _->cbData, _->otm, _->opts);
}

static void QEMU_HOST(NtGdiGetPath)(void *$context)
{
    struct NtGdiGetPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetPath(_->hdc, _->points, _->types, _->size);
}

static void QEMU_HOST(NtGdiGetPixel)(void *$context)
{
    struct NtGdiGetPixel_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetPixel(_->hdc, _->x, _->y);
}

static void QEMU_HOST(NtGdiGetRandomRgn)(void *$context)
{
    struct NtGdiGetRandomRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetRandomRgn(_->hdc, _->region, _->code);
}

static void QEMU_HOST(NtGdiGetRasterizerCaps)(void *$context)
{
    struct NtGdiGetRasterizerCaps_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetRasterizerCaps(_->status, _->size);
}

static void QEMU_HOST(NtGdiGetRealizationInfo)(void *$context)
{
    struct NtGdiGetRealizationInfo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetRealizationInfo(_->hdc, _->info);
}

static void QEMU_HOST(NtGdiGetRegionData)(void *$context)
{
    struct NtGdiGetRegionData_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetRegionData(_->hrgn, _->count, _->data);
}

static void QEMU_HOST(NtGdiGetRgnBox)(void *$context)
{
    struct NtGdiGetRgnBox_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetRgnBox(_->hrgn, _->rect);
}

static void QEMU_HOST(NtGdiGetSpoolMessage)(void *$context)
{
    struct NtGdiGetSpoolMessage_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetSpoolMessage(_->ptr1, _->data2, _->ptr3, _->data4);
}

static void QEMU_HOST(NtGdiGetSystemPaletteUse)(void *$context)
{
    struct NtGdiGetSystemPaletteUse_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetSystemPaletteUse(_->hdc);
}

static void QEMU_HOST(NtGdiGetTextCharsetInfo)(void *$context)
{
    struct NtGdiGetTextCharsetInfo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetTextCharsetInfo(_->hdc, _->fs, _->flags);
}

static void QEMU_HOST(NtGdiGetTextExtentExW)(void *$context)
{
    struct NtGdiGetTextExtentExW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetTextExtentExW(_->hdc, _->str, _->count, _->max_ext, _->nfit, _->dxs, _->size, _->flags);
}

static void QEMU_HOST(NtGdiGetTextFaceW)(void *$context)
{
    struct NtGdiGetTextFaceW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetTextFaceW(_->hdc, _->count, _->name, _->alias_name);
}

static void QEMU_HOST(NtGdiGetTextMetricsW)(void *$context)
{
    struct NtGdiGetTextMetricsW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetTextMetricsW(_->hdc, _->metrics, _->flags);
}

static void QEMU_HOST(NtGdiGetTransform)(void *$context)
{
    struct NtGdiGetTransform_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGetTransform(_->hdc, _->which, _->xform);
}

static void QEMU_HOST(NtGdiGradientFill)(void *$context)
{
    struct NtGdiGradientFill_params *_ = (typeof(_))$context;
    _->$ret = NtGdiGradientFill(_->hdc, _->vert_array, _->nvert, _->grad_array, _->ngrad, _->mode);
}

static void QEMU_HOST(NtGdiHfontCreate)(void *$context)
{
    struct NtGdiHfontCreate_params *_ = (typeof(_))$context;
    _->$ret = NtGdiHfontCreate(_->logfont, _->unk2, _->unk3, _->unk4, _->data);
}

static void QEMU_HOST(NtGdiIcmBrushInfo)(void *$context)
{
    struct NtGdiIcmBrushInfo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiIcmBrushInfo(_->hdc, _->handle, _->info, _->bits, _->bits_size, _->usage, _->unk, _->mode);
}

static void QEMU_HOST(NtGdiInitSpool)(void *$context)
{
    struct NtGdiInitSpool_params *_ = (typeof(_))$context;
    _->$ret = NtGdiInitSpool();
}

static void QEMU_HOST(NtGdiIntersectClipRect)(void *$context)
{
    struct NtGdiIntersectClipRect_params *_ = (typeof(_))$context;
    _->$ret = NtGdiIntersectClipRect(_->hdc, _->left, _->top, _->right, _->bottom);
}

static void QEMU_HOST(NtGdiInvertRgn)(void *$context)
{
    struct NtGdiInvertRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiInvertRgn(_->hdc, _->hrgn);
}

static void QEMU_HOST(NtGdiLineTo)(void *$context)
{
    struct NtGdiLineTo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiLineTo(_->hdc, _->x, _->y);
}

static void QEMU_HOST(NtGdiMaskBlt)(void *$context)
{
    struct NtGdiMaskBlt_params *_ = (typeof(_))$context;
    _->$ret = NtGdiMaskBlt(_->hdc, _->x_dst, _->y_dst, _->width_dst, _->height_dst, _->hdc_src, _->x_src, _->y_src, _->mask, _->x_mask, _->y_mask, _->rop, _->bk_color);
}

static void QEMU_HOST(NtGdiModifyWorldTransform)(void *$context)
{
    struct NtGdiModifyWorldTransform_params *_ = (typeof(_))$context;
    _->$ret = NtGdiModifyWorldTransform(_->hdc, _->xform, _->mode);
}

static void QEMU_HOST(NtGdiMoveTo)(void *$context)
{
    struct NtGdiMoveTo_params *_ = (typeof(_))$context;
    _->$ret = NtGdiMoveTo(_->hdc, _->x, _->y, _->pt);
}

static void QEMU_HOST(NtGdiOffsetClipRgn)(void *$context)
{
    struct NtGdiOffsetClipRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiOffsetClipRgn(_->hdc, _->x, _->y);
}

static void QEMU_HOST(NtGdiOffsetRgn)(void *$context)
{
    struct NtGdiOffsetRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiOffsetRgn(_->hrgn, _->x, _->y);
}

static void QEMU_HOST(NtGdiOpenDCW)(void *$context)
{
    struct NtGdiOpenDCW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiOpenDCW(_->device, _->devmode, _->output, _->type, _->is_display, _->hspool, _->driver_info, _->pdev);
}

static void QEMU_HOST(NtGdiPatBlt)(void *$context)
{
    struct NtGdiPatBlt_params *_ = (typeof(_))$context;
    _->$ret = NtGdiPatBlt(_->hdc, _->left, _->top, _->width, _->height, _->rop);
}

static void QEMU_HOST(NtGdiPathToRegion)(void *$context)
{
    struct NtGdiPathToRegion_params *_ = (typeof(_))$context;
    _->$ret = NtGdiPathToRegion(_->hdc);
}

static void QEMU_HOST(NtGdiPlgBlt)(void *$context)
{
    struct NtGdiPlgBlt_params *_ = (typeof(_))$context;
    _->$ret = NtGdiPlgBlt(_->hdc, _->point, _->hdc_src, _->x_src, _->y_src, _->width, _->height, _->mask, _->x_mask, _->y_mask, _->bk_color);
}

static void QEMU_HOST(NtGdiPolyDraw)(void *$context)
{
    struct NtGdiPolyDraw_params *_ = (typeof(_))$context;
    _->$ret = NtGdiPolyDraw(_->hdc, _->points, _->types, _->count);
}

static void QEMU_HOST(NtGdiPolyPolyDraw)(void *$context)
{
    struct NtGdiPolyPolyDraw_params *_ = (typeof(_))$context;
    _->$ret = NtGdiPolyPolyDraw(_->hdc, _->points, _->counts, _->count, _->function);
}

static void QEMU_HOST(NtGdiPtInRegion)(void *$context)
{
    struct NtGdiPtInRegion_params *_ = (typeof(_))$context;
    _->$ret = NtGdiPtInRegion(_->hrgn, _->x, _->y);
}

static void QEMU_HOST(NtGdiPtVisible)(void *$context)
{
    struct NtGdiPtVisible_params *_ = (typeof(_))$context;
    _->$ret = NtGdiPtVisible(_->hdc, _->x, _->y);
}

static void QEMU_HOST(NtGdiRectInRegion)(void *$context)
{
    struct NtGdiRectInRegion_params *_ = (typeof(_))$context;
    _->$ret = NtGdiRectInRegion(_->hrgn, _->rect);
}

static void QEMU_HOST(NtGdiRectVisible)(void *$context)
{
    struct NtGdiRectVisible_params *_ = (typeof(_))$context;
    _->$ret = NtGdiRectVisible(_->hdc, _->rect);
}

static void QEMU_HOST(NtGdiRectangle)(void *$context)
{
    struct NtGdiRectangle_params *_ = (typeof(_))$context;
    _->$ret = NtGdiRectangle(_->hdc, _->left, _->top, _->right, _->bottom);
}

static void QEMU_HOST(NtGdiRemoveFontMemResourceEx)(void *$context)
{
    struct NtGdiRemoveFontMemResourceEx_params *_ = (typeof(_))$context;
    _->$ret = NtGdiRemoveFontMemResourceEx(_->handle);
}

static void QEMU_HOST(NtGdiRemoveFontResourceW)(void *$context)
{
    struct NtGdiRemoveFontResourceW_params *_ = (typeof(_))$context;
    _->$ret = NtGdiRemoveFontResourceW(_->str, _->size, _->files, _->flags, _->tid, _->dv);
}

static void QEMU_HOST(NtGdiResetDC)(void *$context)
{
    struct NtGdiResetDC_params *_ = (typeof(_))$context;
    _->$ret = NtGdiResetDC(_->hdc, _->devmode, _->banding, _->driver_info, _->dev);
}

static void QEMU_HOST(NtGdiResizePalette)(void *$context)
{
    struct NtGdiResizePalette_params *_ = (typeof(_))$context;
    _->$ret = NtGdiResizePalette(_->palette, _->count);
}

static void QEMU_HOST(NtGdiRestoreDC)(void *$context)
{
    struct NtGdiRestoreDC_params *_ = (typeof(_))$context;
    _->$ret = NtGdiRestoreDC(_->hdc, _->level);
}

static void QEMU_HOST(NtGdiRoundRect)(void *$context)
{
    struct NtGdiRoundRect_params *_ = (typeof(_))$context;
    _->$ret = NtGdiRoundRect(_->hdc, _->left, _->top, _->right, _->bottom, _->ell_width, _->ell_height);
}

static void QEMU_HOST(NtGdiSaveDC)(void *$context)
{
    struct NtGdiSaveDC_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSaveDC(_->hdc);
}

static void QEMU_HOST(NtGdiScaleViewportExtEx)(void *$context)
{
    struct NtGdiScaleViewportExtEx_params *_ = (typeof(_))$context;
    _->$ret = NtGdiScaleViewportExtEx(_->hdc, _->x_num, _->x_denom, _->y_num, _->y_denom, _->size);
}

static void QEMU_HOST(NtGdiScaleWindowExtEx)(void *$context)
{
    struct NtGdiScaleWindowExtEx_params *_ = (typeof(_))$context;
    _->$ret = NtGdiScaleWindowExtEx(_->hdc, _->x_num, _->x_denom, _->y_num, _->y_denom, _->size);
}

static void QEMU_HOST(NtGdiSelectBitmap)(void *$context)
{
    struct NtGdiSelectBitmap_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSelectBitmap(_->hdc, _->handle);
}

static void QEMU_HOST(NtGdiSelectBrush)(void *$context)
{
    struct NtGdiSelectBrush_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSelectBrush(_->hdc, _->handle);
}

static void QEMU_HOST(NtGdiSelectClipPath)(void *$context)
{
    struct NtGdiSelectClipPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSelectClipPath(_->hdc, _->mode);
}

static void QEMU_HOST(NtGdiSelectFont)(void *$context)
{
    struct NtGdiSelectFont_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSelectFont(_->hdc, _->handle);
}

static void QEMU_HOST(NtGdiSelectPen)(void *$context)
{
    struct NtGdiSelectPen_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSelectPen(_->hdc, _->handle);
}

static void QEMU_HOST(NtGdiSetBitmapBits)(void *$context)
{
    struct NtGdiSetBitmapBits_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetBitmapBits(_->hbitmap, _->count, _->bits);
}

static void QEMU_HOST(NtGdiSetBitmapDimension)(void *$context)
{
    struct NtGdiSetBitmapDimension_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetBitmapDimension(_->hbitmap, _->x, _->y, _->prev_size);
}

static void QEMU_HOST(NtGdiSetBoundsRect)(void *$context)
{
    struct NtGdiSetBoundsRect_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetBoundsRect(_->hdc, _->rect, _->flags);
}

static void QEMU_HOST(NtGdiSetBrushOrg)(void *$context)
{
    struct NtGdiSetBrushOrg_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetBrushOrg(_->hdc, _->x, _->y, _->prev_org);
}

static void QEMU_HOST(NtGdiSetColorAdjustment)(void *$context)
{
    struct NtGdiSetColorAdjustment_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetColorAdjustment(_->hdc, _->ca);
}

static void QEMU_HOST(NtGdiSetDIBitsToDeviceInternal)(void *$context)
{
    struct NtGdiSetDIBitsToDeviceInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetDIBitsToDeviceInternal(_->hdc, _->x_dst, _->y_dst, _->cx, _->cy, _->x_src, _->y_src, _->startscan, _->lines, _->bits, _->bmi, _->coloruse, _->max_bits, _->max_info, _->xform_coords, _->xform);
}

static void QEMU_HOST(NtGdiSetDeviceGammaRamp)(void *$context)
{
    struct NtGdiSetDeviceGammaRamp_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetDeviceGammaRamp(_->hdc, _->ptr);
}

static void QEMU_HOST(NtGdiSetLayout)(void *$context)
{
    struct NtGdiSetLayout_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetLayout(_->hdc, _->wox, _->layout);
}

static void QEMU_HOST(NtGdiSetMagicColors)(void *$context)
{
    struct NtGdiSetMagicColors_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetMagicColors(_->hdc, _->magic, _->index);
}

static void QEMU_HOST(NtGdiSetMetaRgn)(void *$context)
{
    struct NtGdiSetMetaRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetMetaRgn(_->hdc);
}

static void QEMU_HOST(NtGdiSetPixel)(void *$context)
{
    struct NtGdiSetPixel_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetPixel(_->hdc, _->x, _->y, _->color);
}

static void QEMU_HOST(NtGdiSetPixelFormat)(void *$context)
{
    struct NtGdiSetPixelFormat_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetPixelFormat(_->hdc, _->format);
}

static void QEMU_HOST(NtGdiSetRectRgn)(void *$context)
{
    struct NtGdiSetRectRgn_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetRectRgn(_->hrgn, _->left, _->top, _->right, _->bottom);
}

static void QEMU_HOST(NtGdiSetSystemPaletteUse)(void *$context)
{
    struct NtGdiSetSystemPaletteUse_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetSystemPaletteUse(_->hdc, _->use);
}

static void QEMU_HOST(NtGdiSetTextJustification)(void *$context)
{
    struct NtGdiSetTextJustification_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetTextJustification(_->hdc, _->extra, _->breaks);
}

static void QEMU_HOST(NtGdiSetVirtualResolution)(void *$context)
{
    struct NtGdiSetVirtualResolution_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSetVirtualResolution(_->hdc, _->horz_res, _->vert_res, _->horz_size, _->vert_size);
}

static void QEMU_HOST(NtGdiStartDoc)(void *$context)
{
    struct NtGdiStartDoc_params *_ = (typeof(_))$context;
    _->$ret = NtGdiStartDoc(_->hdc, _->doc, _->banding, _->job);
}

static void QEMU_HOST(NtGdiStartPage)(void *$context)
{
    struct NtGdiStartPage_params *_ = (typeof(_))$context;
    _->$ret = NtGdiStartPage(_->hdc);
}

static void QEMU_HOST(NtGdiStretchBlt)(void *$context)
{
    struct NtGdiStretchBlt_params *_ = (typeof(_))$context;
    _->$ret = NtGdiStretchBlt(_->hdc, _->x_dst, _->y_dst, _->width_dst, _->height_dst, _->hdc_src, _->x_src, _->y_src, _->width_src, _->height_src, _->rop, _->bk_color);
}

static void QEMU_HOST(NtGdiStretchDIBitsInternal)(void *$context)
{
    struct NtGdiStretchDIBitsInternal_params *_ = (typeof(_))$context;
    _->$ret = NtGdiStretchDIBitsInternal(_->hdc, _->x_dst, _->y_dst, _->width_dst, _->height_dst, _->x_src, _->y_src, _->width_src, _->height_src, _->bits, _->bmi, _->coloruse, _->rop, _->max_info, _->max_bits, _->xform);
}

static void QEMU_HOST(NtGdiStrokeAndFillPath)(void *$context)
{
    struct NtGdiStrokeAndFillPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiStrokeAndFillPath(_->hdc);
}

static void QEMU_HOST(NtGdiStrokePath)(void *$context)
{
    struct NtGdiStrokePath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiStrokePath(_->hdc);
}

static void QEMU_HOST(NtGdiSwapBuffers)(void *$context)
{
    struct NtGdiSwapBuffers_params *_ = (typeof(_))$context;
    _->$ret = NtGdiSwapBuffers(_->hdc);
}

static void QEMU_HOST(NtGdiTransformPoints)(void *$context)
{
    struct NtGdiTransformPoints_params *_ = (typeof(_))$context;
    _->$ret = NtGdiTransformPoints(_->hdc, _->points_in, _->points_out, _->count, _->mode);
}

static void QEMU_HOST(NtGdiTransparentBlt)(void *$context)
{
    struct NtGdiTransparentBlt_params *_ = (typeof(_))$context;
    _->$ret = NtGdiTransparentBlt(_->hdc, _->x_dst, _->y_dst, _->width_dst, _->height_dst, _->hdc_src, _->x_src, _->y_src, _->width_src, _->height_src, _->color);
}

static void QEMU_HOST(NtGdiUnrealizeObject)(void *$context)
{
    struct NtGdiUnrealizeObject_params *_ = (typeof(_))$context;
    _->$ret = NtGdiUnrealizeObject(_->obj);
}

static void QEMU_HOST(NtGdiUpdateColors)(void *$context)
{
    struct NtGdiUpdateColors_params *_ = (typeof(_))$context;
    _->$ret = NtGdiUpdateColors(_->hdc);
}

static void QEMU_HOST(NtGdiWidenPath)(void *$context)
{
    struct NtGdiWidenPath_params *_ = (typeof(_))$context;
    _->$ret = NtGdiWidenPath(_->hdc);
}

static void QEMU_HOST(NtUserActivateKeyboardLayout)(void *$context)
{
    struct NtUserActivateKeyboardLayout_params *_ = (typeof(_))$context;
    _->$ret = NtUserActivateKeyboardLayout(_->layout, _->flags);
}

static void QEMU_HOST(NtUserAddClipboardFormatListener)(void *$context)
{
    struct NtUserAddClipboardFormatListener_params *_ = (typeof(_))$context;
    _->$ret = NtUserAddClipboardFormatListener(_->hwnd);
}

static void QEMU_HOST(NtUserAssociateInputContext)(void *$context)
{
    struct NtUserAssociateInputContext_params *_ = (typeof(_))$context;
    _->$ret = NtUserAssociateInputContext(_->hwnd, _->ctx, _->flags);
}

static void QEMU_HOST(NtUserAttachThreadInput)(void *$context)
{
    struct NtUserAttachThreadInput_params *_ = (typeof(_))$context;
    _->$ret = NtUserAttachThreadInput(_->from, _->to, _->attach);
}

static void QEMU_HOST(NtUserBeginPaint)(void *$context)
{
    struct NtUserBeginPaint_params *_ = (typeof(_))$context;
    _->$ret = NtUserBeginPaint(_->hwnd, _->ps);
}

static void QEMU_HOST(NtUserBuildHimcList)(void *$context)
{
    struct NtUserBuildHimcList_params *_ = (typeof(_))$context;
    _->$ret = NtUserBuildHimcList(_->thread_id, _->count, _->buffer, _->size);
}

static void QEMU_HOST(NtUserBuildHwndList)(void *$context)
{
    struct NtUserBuildHwndList_params *_ = (typeof(_))$context;
    _->$ret = NtUserBuildHwndList(_->desktop, _->unk2, _->unk3, _->unk4, _->thread_id, _->count, _->buffer, _->size);
}

static void QEMU_HOST(NtUserCallHwnd)(void *$context)
{
    struct NtUserCallHwnd_params *_ = (typeof(_))$context;
    _->$ret = NtUserCallHwnd(_->hwnd, _->code);
}

static void QEMU_HOST(NtUserCallHwndParam)(void *$context)
{
    struct NtUserCallHwndParam_params *_ = (typeof(_))$context;
    _->$ret = NtUserCallHwndParam(_->hwnd, _->param, _->code);
}

static void QEMU_HOST(NtUserCallMsgFilter)(void *$context)
{
    struct NtUserCallMsgFilter_params *_ = (typeof(_))$context;
    _->$ret = NtUserCallMsgFilter(_->msg, _->code);
}

static void QEMU_HOST(NtUserCallNextHookEx)(void *$context)
{
    struct NtUserCallNextHookEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserCallNextHookEx(_->hhook, _->code, _->wparam, _->lparam);
}

static void QEMU_HOST(NtUserCallNoParam)(void *$context)
{
    struct NtUserCallNoParam_params *_ = (typeof(_))$context;
    _->$ret = NtUserCallNoParam(_->code);
}

static void QEMU_HOST(NtUserCallOneParam)(void *$context)
{
    struct NtUserCallOneParam_params *_ = (typeof(_))$context;
    _->$ret = NtUserCallOneParam(_->arg, _->code);
}

static void QEMU_HOST(NtUserCallTwoParam)(void *$context)
{
    struct NtUserCallTwoParam_params *_ = (typeof(_))$context;
    _->$ret = NtUserCallTwoParam(_->arg1, _->arg2, _->code);
}

static void QEMU_HOST(NtUserChangeClipboardChain)(void *$context)
{
    struct NtUserChangeClipboardChain_params *_ = (typeof(_))$context;
    _->$ret = NtUserChangeClipboardChain(_->hwnd, _->next);
}

static void QEMU_HOST(NtUserChangeDisplaySettings)(void *$context)
{
    struct NtUserChangeDisplaySettings_params *_ = (typeof(_))$context;
    _->$ret = NtUserChangeDisplaySettings(_->devname, _->devmode, _->hwnd, _->flags, _->lparam);
}

static void QEMU_HOST(NtUserCheckMenuItem)(void *$context)
{
    struct NtUserCheckMenuItem_params *_ = (typeof(_))$context;
    _->$ret = NtUserCheckMenuItem(_->handle, _->id, _->flags);
}

static void QEMU_HOST(NtUserChildWindowFromPointEx)(void *$context)
{
    struct NtUserChildWindowFromPointEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserChildWindowFromPointEx(_->parent, _->x, _->y, _->flags);
}

static void QEMU_HOST(NtUserClipCursor)(void *$context)
{
    struct NtUserClipCursor_params *_ = (typeof(_))$context;
    _->$ret = NtUserClipCursor(_->rect);
}

static void QEMU_HOST(NtUserCloseClipboard)(void *$context)
{
    struct NtUserCloseClipboard_params *_ = (typeof(_))$context;
    _->$ret = NtUserCloseClipboard();
}

static void QEMU_HOST(NtUserCloseDesktop)(void *$context)
{
    struct NtUserCloseDesktop_params *_ = (typeof(_))$context;
    _->$ret = NtUserCloseDesktop(_->handle);
}

static void QEMU_HOST(NtUserCloseWindowStation)(void *$context)
{
    struct NtUserCloseWindowStation_params *_ = (typeof(_))$context;
    _->$ret = NtUserCloseWindowStation(_->handle);
}

static void QEMU_HOST(NtUserCopyAcceleratorTable)(void *$context)
{
    struct NtUserCopyAcceleratorTable_params *_ = (typeof(_))$context;
    _->$ret = NtUserCopyAcceleratorTable(_->src, _->dst, _->count);
}

static void QEMU_HOST(NtUserCountClipboardFormats)(void *$context)
{
    struct NtUserCountClipboardFormats_params *_ = (typeof(_))$context;
    _->$ret = NtUserCountClipboardFormats();
}

static void QEMU_HOST(NtUserCreateAcceleratorTable)(void *$context)
{
    struct NtUserCreateAcceleratorTable_params *_ = (typeof(_))$context;
    _->$ret = NtUserCreateAcceleratorTable(_->table, _->count);
}

static void QEMU_HOST(NtUserCreateCaret)(void *$context)
{
    struct NtUserCreateCaret_params *_ = (typeof(_))$context;
    _->$ret = NtUserCreateCaret(_->hwnd, _->bitmap, _->width, _->height);
}

static void QEMU_HOST(NtUserCreateDesktopEx)(void *$context)
{
    struct NtUserCreateDesktopEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserCreateDesktopEx(_->attr, _->device, _->devmode, _->flags, _->access, _->heap_size);
}

static void QEMU_HOST(NtUserCreateInputContext)(void *$context)
{
    struct NtUserCreateInputContext_params *_ = (typeof(_))$context;
    _->$ret = NtUserCreateInputContext(_->client_ptr);
}

static void QEMU_HOST(NtUserCreateWindowEx)(void *$context)
{
    struct NtUserCreateWindowEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserCreateWindowEx(_->ex_style, _->class_name, _->version, _->window_name, _->style, _->x, _->y, _->cx, _->cy, _->parent, _->menu, _->instance, _->params, _->flags, _->client_instance, _->unk, _->ansi);
}

static void QEMU_HOST(NtUserCreateWindowStation)(void *$context)
{
    struct NtUserCreateWindowStation_params *_ = (typeof(_))$context;
    _->$ret = NtUserCreateWindowStation(_->attr, _->mask, _->arg3, _->arg4, _->arg5, _->arg6, _->arg7);
}

static void QEMU_HOST(NtUserDeferWindowPosAndBand)(void *$context)
{
    struct NtUserDeferWindowPosAndBand_params *_ = (typeof(_))$context;
    _->$ret = NtUserDeferWindowPosAndBand(_->hdwp, _->hwnd, _->after, _->x, _->y, _->cx, _->cy, _->flags, _->unk1, _->unk2);
}

static void QEMU_HOST(NtUserDeleteMenu)(void *$context)
{
    struct NtUserDeleteMenu_params *_ = (typeof(_))$context;
    _->$ret = NtUserDeleteMenu(_->menu, _->id, _->flags);
}

static void QEMU_HOST(NtUserDestroyAcceleratorTable)(void *$context)
{
    struct NtUserDestroyAcceleratorTable_params *_ = (typeof(_))$context;
    _->$ret = NtUserDestroyAcceleratorTable(_->handle);
}

static void QEMU_HOST(NtUserDestroyCursor)(void *$context)
{
    struct NtUserDestroyCursor_params *_ = (typeof(_))$context;
    _->$ret = NtUserDestroyCursor(_->cursor, _->arg);
}

static void QEMU_HOST(NtUserDestroyInputContext)(void *$context)
{
    struct NtUserDestroyInputContext_params *_ = (typeof(_))$context;
    _->$ret = NtUserDestroyInputContext(_->handle);
}

static void QEMU_HOST(NtUserDestroyMenu)(void *$context)
{
    struct NtUserDestroyMenu_params *_ = (typeof(_))$context;
    _->$ret = NtUserDestroyMenu(_->menu);
}

static void QEMU_HOST(NtUserDestroyWindow)(void *$context)
{
    struct NtUserDestroyWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserDestroyWindow(_->hwnd);
}

static void QEMU_HOST(NtUserDisableThreadIme)(void *$context)
{
    struct NtUserDisableThreadIme_params *_ = (typeof(_))$context;
    _->$ret = NtUserDisableThreadIme(_->thread_id);
}

static void QEMU_HOST(NtUserDispatchMessage)(void *$context)
{
    struct NtUserDispatchMessage_params *_ = (typeof(_))$context;
    _->$ret = NtUserDispatchMessage(_->msg);
}

static void QEMU_HOST(NtUserDisplayConfigGetDeviceInfo)(void *$context)
{
    struct NtUserDisplayConfigGetDeviceInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserDisplayConfigGetDeviceInfo(_->packet);
}

static void QEMU_HOST(NtUserDragDetect)(void *$context)
{
    struct NtUserDragDetect_params *_ = (typeof(_))$context;
    _->$ret = NtUserDragDetect(_->hwnd, _->x, _->y);
}

static void QEMU_HOST(NtUserDragObject)(void *$context)
{
    struct NtUserDragObject_params *_ = (typeof(_))$context;
    _->$ret = NtUserDragObject(_->parent, _->hwnd, _->fmt, _->data, _->cursor);
}

static void QEMU_HOST(NtUserDrawCaptionTemp)(void *$context)
{
    struct NtUserDrawCaptionTemp_params *_ = (typeof(_))$context;
    _->$ret = NtUserDrawCaptionTemp(_->hwnd, _->hdc, _->rect, _->font, _->icon, _->str, _->flags);
}

static void QEMU_HOST(NtUserDrawIconEx)(void *$context)
{
    struct NtUserDrawIconEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserDrawIconEx(_->hdc, _->x0, _->y0, _->icon, _->width, _->height, _->istep, _->hbr, _->flags);
}

static void QEMU_HOST(NtUserDrawMenuBarTemp)(void *$context)
{
    struct NtUserDrawMenuBarTemp_params *_ = (typeof(_))$context;
    _->$ret = NtUserDrawMenuBarTemp(_->hwnd, _->hdc, _->rect, _->handle, _->font);
}

static void QEMU_HOST(NtUserEmptyClipboard)(void *$context)
{
    struct NtUserEmptyClipboard_params *_ = (typeof(_))$context;
    _->$ret = NtUserEmptyClipboard();
}

static void QEMU_HOST(NtUserEnableMenuItem)(void *$context)
{
    struct NtUserEnableMenuItem_params *_ = (typeof(_))$context;
    _->$ret = NtUserEnableMenuItem(_->handle, _->id, _->flags);
}

static void QEMU_HOST(NtUserEnableMouseInPointer)(void *$context)
{
    struct NtUserEnableMouseInPointer_params *_ = (typeof(_))$context;
    _->$ret = NtUserEnableMouseInPointer(_->$0);
}

static void QEMU_HOST(NtUserEnableScrollBar)(void *$context)
{
    struct NtUserEnableScrollBar_params *_ = (typeof(_))$context;
    _->$ret = NtUserEnableScrollBar(_->hwnd, _->bar, _->flags);
}

static void QEMU_HOST(NtUserEndDeferWindowPosEx)(void *$context)
{
    struct NtUserEndDeferWindowPosEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserEndDeferWindowPosEx(_->hdwp, _->async);
}

static void QEMU_HOST(NtUserEndMenu)(void *$context)
{
    struct NtUserEndMenu_params *_ = (typeof(_))$context;
    _->$ret = NtUserEndMenu();
}

static void QEMU_HOST(NtUserEndPaint)(void *$context)
{
    struct NtUserEndPaint_params *_ = (typeof(_))$context;
    _->$ret = NtUserEndPaint(_->hwnd, _->ps);
}

static void QEMU_HOST(NtUserEnumDisplayDevices)(void *$context)
{
    struct NtUserEnumDisplayDevices_params *_ = (typeof(_))$context;
    _->$ret = NtUserEnumDisplayDevices(_->device, _->index, _->info, _->flags);
}

static void QEMU_HOST(NtUserEnumDisplayMonitors)(void *$context)
{
    struct NtUserEnumDisplayMonitors_params *_ = (typeof(_))$context;
    _->$ret = NtUserEnumDisplayMonitors(_->hdc, _->rect, _->proc, _->lp);
}

static void QEMU_HOST(NtUserEnumDisplaySettings)(void *$context)
{
    struct NtUserEnumDisplaySettings_params *_ = (typeof(_))$context;
    _->$ret = NtUserEnumDisplaySettings(_->device, _->mode, _->dev_mode, _->flags);
}

static void QEMU_HOST(NtUserExcludeUpdateRgn)(void *$context)
{
    struct NtUserExcludeUpdateRgn_params *_ = (typeof(_))$context;
    _->$ret = NtUserExcludeUpdateRgn(_->hdc, _->hwnd);
}

static void QEMU_HOST(NtUserFindExistingCursorIcon)(void *$context)
{
    struct NtUserFindExistingCursorIcon_params *_ = (typeof(_))$context;
    _->$ret = NtUserFindExistingCursorIcon(_->module, _->res_name, _->desc);
}

static void QEMU_HOST(NtUserFindWindowEx)(void *$context)
{
    struct NtUserFindWindowEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserFindWindowEx(_->parent, _->child, _->class, _->title, _->unk);
}

static void QEMU_HOST(NtUserFlashWindowEx)(void *$context)
{
    struct NtUserFlashWindowEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserFlashWindowEx(_->info);
}

static void QEMU_HOST(NtUserGetAncestor)(void *$context)
{
    struct NtUserGetAncestor_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetAncestor(_->hwnd, _->type);
}

static void QEMU_HOST(NtUserGetAsyncKeyState)(void *$context)
{
    struct NtUserGetAsyncKeyState_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetAsyncKeyState(_->key);
}

static void QEMU_HOST(NtUserGetAtomName)(void *$context)
{
    struct NtUserGetAtomName_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetAtomName(_->atom, _->name);
}

static void QEMU_HOST(NtUserGetCaretBlinkTime)(void *$context)
{
    struct NtUserGetCaretBlinkTime_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetCaretBlinkTime();
}

static void QEMU_HOST(NtUserGetCaretPos)(void *$context)
{
    struct NtUserGetCaretPos_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetCaretPos(_->point);
}

static void QEMU_HOST(NtUserGetClassInfoEx)(void *$context)
{
    struct NtUserGetClassInfoEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetClassInfoEx(_->instance, _->name, _->wc, _->menu_name, _->ansi);
}

static void QEMU_HOST(NtUserGetClassName)(void *$context)
{
    struct NtUserGetClassName_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetClassName(_->hwnd, _->real, _->name);
}

static void QEMU_HOST(NtUserGetClipboardData)(void *$context)
{
    struct NtUserGetClipboardData_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetClipboardData(_->format, _->params);
}

static void QEMU_HOST(NtUserGetClipboardFormatName)(void *$context)
{
    struct NtUserGetClipboardFormatName_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetClipboardFormatName(_->format, _->buffer, _->maxlen);
}

static void QEMU_HOST(NtUserGetClipboardOwner)(void *$context)
{
    struct NtUserGetClipboardOwner_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetClipboardOwner();
}

static void QEMU_HOST(NtUserGetClipboardSequenceNumber)(void *$context)
{
    struct NtUserGetClipboardSequenceNumber_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetClipboardSequenceNumber();
}

static void QEMU_HOST(NtUserGetClipboardViewer)(void *$context)
{
    struct NtUserGetClipboardViewer_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetClipboardViewer();
}

static void QEMU_HOST(NtUserGetCursor)(void *$context)
{
    struct NtUserGetCursor_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetCursor();
}

static void QEMU_HOST(NtUserGetCursorFrameInfo)(void *$context)
{
    struct NtUserGetCursorFrameInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetCursorFrameInfo(_->hCursor, _->istep, _->rate_jiffies, _->num_steps);
}

static void QEMU_HOST(NtUserGetCursorInfo)(void *$context)
{
    struct NtUserGetCursorInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetCursorInfo(_->info);
}

static void QEMU_HOST(NtUserGetDC)(void *$context)
{
    struct NtUserGetDC_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetDC(_->hwnd);
}

static void QEMU_HOST(NtUserGetDCEx)(void *$context)
{
    struct NtUserGetDCEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetDCEx(_->hwnd, _->clip_rgn, _->flags);
}

static void QEMU_HOST(NtUserGetDisplayConfigBufferSizes)(void *$context)
{
    struct NtUserGetDisplayConfigBufferSizes_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetDisplayConfigBufferSizes(_->flags, _->num_path_info, _->num_mode_info);
}

static void QEMU_HOST(NtUserGetDoubleClickTime)(void *$context)
{
    struct NtUserGetDoubleClickTime_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetDoubleClickTime();
}

static void QEMU_HOST(NtUserGetDpiForMonitor)(void *$context)
{
    struct NtUserGetDpiForMonitor_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetDpiForMonitor(_->monitor, _->type, _->x, _->y);
}

static void QEMU_HOST(NtUserGetForegroundWindow)(void *$context)
{
    struct NtUserGetForegroundWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetForegroundWindow();
}

static void QEMU_HOST(NtUserGetGUIThreadInfo)(void *$context)
{
    struct NtUserGetGUIThreadInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetGUIThreadInfo(_->id, _->info);
}

static void QEMU_HOST(NtUserGetIconInfo)(void *$context)
{
    struct NtUserGetIconInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetIconInfo(_->icon, _->info, _->module, _->res_name, _->bpp, _->unk);
}

static void QEMU_HOST(NtUserGetIconSize)(void *$context)
{
    struct NtUserGetIconSize_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetIconSize(_->handle, _->step, _->width, _->height);
}

static void QEMU_HOST(NtUserGetInternalWindowPos)(void *$context)
{
    struct NtUserGetInternalWindowPos_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetInternalWindowPos(_->hwnd, _->rect, _->pt);
}

static void QEMU_HOST(NtUserGetKeyNameText)(void *$context)
{
    struct NtUserGetKeyNameText_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetKeyNameText(_->lparam, _->buffer, _->size);
}

static void QEMU_HOST(NtUserGetKeyState)(void *$context)
{
    struct NtUserGetKeyState_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetKeyState(_->vkey);
}

static void QEMU_HOST(NtUserGetKeyboardLayout)(void *$context)
{
    struct NtUserGetKeyboardLayout_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetKeyboardLayout(_->thread_id);
}

static void QEMU_HOST(NtUserGetKeyboardLayoutList)(void *$context)
{
    struct NtUserGetKeyboardLayoutList_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetKeyboardLayoutList(_->size, _->layouts);
}

static void QEMU_HOST(NtUserGetKeyboardLayoutName)(void *$context)
{
    struct NtUserGetKeyboardLayoutName_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetKeyboardLayoutName(_->name);
}

static void QEMU_HOST(NtUserGetKeyboardState)(void *$context)
{
    struct NtUserGetKeyboardState_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetKeyboardState(_->state);
}

static void QEMU_HOST(NtUserGetLayeredWindowAttributes)(void *$context)
{
    struct NtUserGetLayeredWindowAttributes_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetLayeredWindowAttributes(_->hwnd, _->key, _->alpha, _->flags);
}

static void QEMU_HOST(NtUserGetMenuBarInfo)(void *$context)
{
    struct NtUserGetMenuBarInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetMenuBarInfo(_->hwnd, _->id, _->item, _->info);
}

static void QEMU_HOST(NtUserGetMenuItemRect)(void *$context)
{
    struct NtUserGetMenuItemRect_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetMenuItemRect(_->hwnd, _->menu, _->item, _->rect);
}

static void QEMU_HOST(NtUserGetMessage)(void *$context)
{
    struct NtUserGetMessage_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetMessage(_->msg, _->hwnd, _->first, _->last);
}

static void QEMU_HOST(NtUserGetMouseMovePointsEx)(void *$context)
{
    struct NtUserGetMouseMovePointsEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetMouseMovePointsEx(_->size, _->ptin, _->ptout, _->count, _->resolution);
}

static void QEMU_HOST(NtUserGetObjectInformation)(void *$context)
{
    struct NtUserGetObjectInformation_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetObjectInformation(_->handle, _->index, _->info, _->len, _->needed);
}

static void QEMU_HOST(NtUserGetOpenClipboardWindow)(void *$context)
{
    struct NtUserGetOpenClipboardWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetOpenClipboardWindow();
}

static void QEMU_HOST(NtUserGetPointerInfoList)(void *$context)
{
    struct NtUserGetPointerInfoList_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetPointerInfoList(_->id, _->type, _->$2, _->$3, _->size, _->entry_count, _->pointer_count, _->pointer_info);
}

static void QEMU_HOST(NtUserGetPriorityClipboardFormat)(void *$context)
{
    struct NtUserGetPriorityClipboardFormat_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetPriorityClipboardFormat(_->list, _->count);
}

static void QEMU_HOST(NtUserGetProcessDpiAwarenessContext)(void *$context)
{
    struct NtUserGetProcessDpiAwarenessContext_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetProcessDpiAwarenessContext(_->process);
}

static void QEMU_HOST(NtUserGetProcessWindowStation)(void *$context)
{
    struct NtUserGetProcessWindowStation_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetProcessWindowStation();
}

static void QEMU_HOST(NtUserGetProp)(void *$context)
{
    struct NtUserGetProp_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetProp(_->hwnd, _->str);
}

static void QEMU_HOST(NtUserGetQueueStatus)(void *$context)
{
    struct NtUserGetQueueStatus_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetQueueStatus(_->flags);
}

static void QEMU_HOST(NtUserGetRawInputBuffer)(void *$context)
{
    struct NtUserGetRawInputBuffer_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetRawInputBuffer(_->data, _->data_size, _->header_size);
}

static void QEMU_HOST(NtUserGetRawInputData)(void *$context)
{
    struct NtUserGetRawInputData_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetRawInputData(_->rawinput, _->command, _->data, _->data_size, _->header_size);
}

static void QEMU_HOST(NtUserGetRawInputDeviceInfo)(void *$context)
{
    struct NtUserGetRawInputDeviceInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetRawInputDeviceInfo(_->handle, _->command, _->data, _->data_size);
}

static void QEMU_HOST(NtUserGetRawInputDeviceList)(void *$context)
{
    struct NtUserGetRawInputDeviceList_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetRawInputDeviceList(_->devices, _->device_count, _->size);
}

static void QEMU_HOST(NtUserGetRegisteredRawInputDevices)(void *$context)
{
    struct NtUserGetRegisteredRawInputDevices_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetRegisteredRawInputDevices(_->devices, _->device_count, _->size);
}

static void QEMU_HOST(NtUserGetScrollBarInfo)(void *$context)
{
    struct NtUserGetScrollBarInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetScrollBarInfo(_->hwnd, _->id, _->info);
}

static void QEMU_HOST(NtUserGetSystemDpiForProcess)(void *$context)
{
    struct NtUserGetSystemDpiForProcess_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetSystemDpiForProcess(_->process);
}

static void QEMU_HOST(NtUserGetSystemMenu)(void *$context)
{
    struct NtUserGetSystemMenu_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetSystemMenu(_->hwnd, _->revert);
}

static void QEMU_HOST(NtUserGetThreadDesktop)(void *$context)
{
    struct NtUserGetThreadDesktop_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetThreadDesktop(_->thread);
}

static void QEMU_HOST(NtUserGetTitleBarInfo)(void *$context)
{
    struct NtUserGetTitleBarInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetTitleBarInfo(_->hwnd, _->info);
}

static void QEMU_HOST(NtUserGetUpdateRect)(void *$context)
{
    struct NtUserGetUpdateRect_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetUpdateRect(_->hwnd, _->rect, _->erase);
}

static void QEMU_HOST(NtUserGetUpdateRgn)(void *$context)
{
    struct NtUserGetUpdateRgn_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetUpdateRgn(_->hwnd, _->hrgn, _->erase);
}

static void QEMU_HOST(NtUserGetUpdatedClipboardFormats)(void *$context)
{
    struct NtUserGetUpdatedClipboardFormats_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetUpdatedClipboardFormats(_->formats, _->size, _->out_size);
}

static void QEMU_HOST(NtUserGetWindowDC)(void *$context)
{
    struct NtUserGetWindowDC_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetWindowDC(_->hwnd);
}

static void QEMU_HOST(NtUserGetWindowPlacement)(void *$context)
{
    struct NtUserGetWindowPlacement_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetWindowPlacement(_->hwnd, _->placement);
}

static void QEMU_HOST(NtUserGetWindowRgnEx)(void *$context)
{
    struct NtUserGetWindowRgnEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserGetWindowRgnEx(_->hwnd, _->hrgn, _->unk);
}

static void QEMU_HOST(NtUserHideCaret)(void *$context)
{
    struct NtUserHideCaret_params *_ = (typeof(_))$context;
    _->$ret = NtUserHideCaret(_->hwnd);
}

static void QEMU_HOST(NtUserHiliteMenuItem)(void *$context)
{
    struct NtUserHiliteMenuItem_params *_ = (typeof(_))$context;
    _->$ret = NtUserHiliteMenuItem(_->hwnd, _->handle, _->item, _->hilite);
}

static void QEMU_HOST(NtUserInitializeClientPfnArrays)(void *$context)
{
    struct NtUserInitializeClientPfnArrays_params *_ = (typeof(_))$context;
    _->$ret = NtUserInitializeClientPfnArrays(_->client_procsA, _->client_procsW, _->client_workers, _->user_module);
}

static void QEMU_HOST(NtUserInternalGetWindowIcon)(void *$context)
{
    struct NtUserInternalGetWindowIcon_params *_ = (typeof(_))$context;
    _->$ret = NtUserInternalGetWindowIcon(_->hwnd, _->type);
}

static void QEMU_HOST(NtUserInternalGetWindowText)(void *$context)
{
    struct NtUserInternalGetWindowText_params *_ = (typeof(_))$context;
    _->$ret = NtUserInternalGetWindowText(_->hwnd, _->text, _->count);
}

static void QEMU_HOST(NtUserInvalidateRect)(void *$context)
{
    struct NtUserInvalidateRect_params *_ = (typeof(_))$context;
    _->$ret = NtUserInvalidateRect(_->hwnd, _->rect, _->erase);
}

static void QEMU_HOST(NtUserInvalidateRgn)(void *$context)
{
    struct NtUserInvalidateRgn_params *_ = (typeof(_))$context;
    _->$ret = NtUserInvalidateRgn(_->hwnd, _->hrgn, _->erase);
}

static void QEMU_HOST(NtUserIsClipboardFormatAvailable)(void *$context)
{
    struct NtUserIsClipboardFormatAvailable_params *_ = (typeof(_))$context;
    _->$ret = NtUserIsClipboardFormatAvailable(_->format);
}

static void QEMU_HOST(NtUserIsMouseInPointerEnabled)(void *$context)
{
    struct NtUserIsMouseInPointerEnabled_params *_ = (typeof(_))$context;
    _->$ret = NtUserIsMouseInPointerEnabled();
}

static void QEMU_HOST(NtUserKillTimer)(void *$context)
{
    struct NtUserKillTimer_params *_ = (typeof(_))$context;
    _->$ret = NtUserKillTimer(_->hwnd, _->id);
}

static void QEMU_HOST(NtUserLockWindowUpdate)(void *$context)
{
    struct NtUserLockWindowUpdate_params *_ = (typeof(_))$context;
    _->$ret = NtUserLockWindowUpdate(_->hwnd);
}

static void QEMU_HOST(NtUserLogicalToPerMonitorDPIPhysicalPoint)(void *$context)
{
    struct NtUserLogicalToPerMonitorDPIPhysicalPoint_params *_ = (typeof(_))$context;
    _->$ret = NtUserLogicalToPerMonitorDPIPhysicalPoint(_->hwnd, _->pt);
}

static void QEMU_HOST(NtUserMapVirtualKeyEx)(void *$context)
{
    struct NtUserMapVirtualKeyEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserMapVirtualKeyEx(_->code, _->type, _->layout);
}

static void QEMU_HOST(NtUserMenuItemFromPoint)(void *$context)
{
    struct NtUserMenuItemFromPoint_params *_ = (typeof(_))$context;
    _->$ret = NtUserMenuItemFromPoint(_->hwnd, _->handle, _->x, _->y);
}

static void QEMU_HOST(NtUserMessageCall)(void *$context)
{
    struct NtUserMessageCall_params *_ = (typeof(_))$context;
    _->$ret = NtUserMessageCall(_->hwnd, _->msg, _->wparam, _->lparam, _->result_info, _->type, _->ansi);
}

static void QEMU_HOST(NtUserMoveWindow)(void *$context)
{
    struct NtUserMoveWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserMoveWindow(_->hwnd, _->x, _->y, _->cx, _->cy, _->repaint);
}

static void QEMU_HOST(NtUserMsgWaitForMultipleObjectsEx)(void *$context)
{
    struct NtUserMsgWaitForMultipleObjectsEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserMsgWaitForMultipleObjectsEx(_->count, _->handles, _->timeout, _->mask, _->flags);
}

static void QEMU_HOST(NtUserNotifyIMEStatus)(void *$context)
{
    struct NtUserNotifyIMEStatus_params *_ = (typeof(_))$context;
    NtUserNotifyIMEStatus(_->hwnd, _->status);
}

static void QEMU_HOST(NtUserNotifyWinEvent)(void *$context)
{
    struct NtUserNotifyWinEvent_params *_ = (typeof(_))$context;
    NtUserNotifyWinEvent(_->event, _->hwnd, _->object_id, _->child_id);
}

static void QEMU_HOST(NtUserOpenClipboard)(void *$context)
{
    struct NtUserOpenClipboard_params *_ = (typeof(_))$context;
    _->$ret = NtUserOpenClipboard(_->hwnd, _->unk);
}

static void QEMU_HOST(NtUserOpenDesktop)(void *$context)
{
    struct NtUserOpenDesktop_params *_ = (typeof(_))$context;
    _->$ret = NtUserOpenDesktop(_->attr, _->flags, _->access);
}

static void QEMU_HOST(NtUserOpenInputDesktop)(void *$context)
{
    struct NtUserOpenInputDesktop_params *_ = (typeof(_))$context;
    _->$ret = NtUserOpenInputDesktop(_->flags, _->inherit, _->access);
}

static void QEMU_HOST(NtUserOpenWindowStation)(void *$context)
{
    struct NtUserOpenWindowStation_params *_ = (typeof(_))$context;
    _->$ret = NtUserOpenWindowStation(_->attr, _->access);
}

static void QEMU_HOST(NtUserPeekMessage)(void *$context)
{
    struct NtUserPeekMessage_params *_ = (typeof(_))$context;
    _->$ret = NtUserPeekMessage(_->msg_out, _->hwnd, _->first, _->last, _->flags);
}

static void QEMU_HOST(NtUserPerMonitorDPIPhysicalToLogicalPoint)(void *$context)
{
    struct NtUserPerMonitorDPIPhysicalToLogicalPoint_params *_ = (typeof(_))$context;
    _->$ret = NtUserPerMonitorDPIPhysicalToLogicalPoint(_->hwnd, _->pt);
}

static void QEMU_HOST(NtUserPostMessage)(void *$context)
{
    struct NtUserPostMessage_params *_ = (typeof(_))$context;
    _->$ret = NtUserPostMessage(_->hwnd, _->msg, _->wparam, _->lparam);
}

static void QEMU_HOST(NtUserPostThreadMessage)(void *$context)
{
    struct NtUserPostThreadMessage_params *_ = (typeof(_))$context;
    _->$ret = NtUserPostThreadMessage(_->thread, _->msg, _->wparam, _->lparam);
}

static void QEMU_HOST(NtUserPrintWindow)(void *$context)
{
    struct NtUserPrintWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserPrintWindow(_->hwnd, _->hdc, _->flags);
}

static void QEMU_HOST(NtUserQueryDisplayConfig)(void *$context)
{
    struct NtUserQueryDisplayConfig_params *_ = (typeof(_))$context;
    _->$ret = NtUserQueryDisplayConfig(_->flags, _->paths_count, _->paths, _->modes_count, _->modes, _->topology_id);
}

static void QEMU_HOST(NtUserQueryInputContext)(void *$context)
{
    struct NtUserQueryInputContext_params *_ = (typeof(_))$context;
    _->$ret = NtUserQueryInputContext(_->handle, _->attr);
}

static void QEMU_HOST(NtUserRealChildWindowFromPoint)(void *$context)
{
    struct NtUserRealChildWindowFromPoint_params *_ = (typeof(_))$context;
    _->$ret = NtUserRealChildWindowFromPoint(_->parent, _->x, _->y);
}

static void QEMU_HOST(NtUserRedrawWindow)(void *$context)
{
    struct NtUserRedrawWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserRedrawWindow(_->hwnd, _->rect, _->hrgn, _->flags);
}

static void QEMU_HOST(NtUserRegisterClassExWOW)(void *$context)
{
    struct NtUserRegisterClassExWOW_params *_ = (typeof(_))$context;
    _->$ret = NtUserRegisterClassExWOW(_->wc, _->name, _->version, _->client_menu_name, _->fnid, _->flags, _->wow);
}

static void QEMU_HOST(NtUserRegisterHotKey)(void *$context)
{
    struct NtUserRegisterHotKey_params *_ = (typeof(_))$context;
    _->$ret = NtUserRegisterHotKey(_->hwnd, _->id, _->modifiers, _->vk);
}

static void QEMU_HOST(NtUserRegisterRawInputDevices)(void *$context)
{
    struct NtUserRegisterRawInputDevices_params *_ = (typeof(_))$context;
    _->$ret = NtUserRegisterRawInputDevices(_->devices, _->device_count, _->size);
}

static void QEMU_HOST(NtUserReleaseDC)(void *$context)
{
    struct NtUserReleaseDC_params *_ = (typeof(_))$context;
    _->$ret = NtUserReleaseDC(_->hwnd, _->hdc);
}

static void QEMU_HOST(NtUserRemoveClipboardFormatListener)(void *$context)
{
    struct NtUserRemoveClipboardFormatListener_params *_ = (typeof(_))$context;
    _->$ret = NtUserRemoveClipboardFormatListener(_->hwnd);
}

static void QEMU_HOST(NtUserRemoveMenu)(void *$context)
{
    struct NtUserRemoveMenu_params *_ = (typeof(_))$context;
    _->$ret = NtUserRemoveMenu(_->menu, _->id, _->flags);
}

static void QEMU_HOST(NtUserRemoveProp)(void *$context)
{
    struct NtUserRemoveProp_params *_ = (typeof(_))$context;
    _->$ret = NtUserRemoveProp(_->hwnd, _->str);
}

static void QEMU_HOST(NtUserScrollDC)(void *$context)
{
    struct NtUserScrollDC_params *_ = (typeof(_))$context;
    _->$ret = NtUserScrollDC(_->hdc, _->dx, _->dy, _->scroll, _->clip, _->ret_update_rgn, _->update_rect);
}

static void QEMU_HOST(NtUserScrollWindowEx)(void *$context)
{
    struct NtUserScrollWindowEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserScrollWindowEx(_->hwnd, _->dx, _->dy, _->rect, _->clip_rect, _->update_rgn, _->update_rect, _->flags);
}

static void QEMU_HOST(NtUserSelectPalette)(void *$context)
{
    struct NtUserSelectPalette_params *_ = (typeof(_))$context;
    _->$ret = NtUserSelectPalette(_->hdc, _->palette, _->force_background);
}

static void QEMU_HOST(NtUserSendInput)(void *$context)
{
    struct NtUserSendInput_params *_ = (typeof(_))$context;
    _->$ret = NtUserSendInput(_->count, _->inputs, _->size);
}

static void QEMU_HOST(NtUserSetActiveWindow)(void *$context)
{
    struct NtUserSetActiveWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetActiveWindow(_->hwnd);
}

static void QEMU_HOST(NtUserSetCapture)(void *$context)
{
    struct NtUserSetCapture_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetCapture(_->hwnd);
}

static void QEMU_HOST(NtUserSetClassLong)(void *$context)
{
    struct NtUserSetClassLong_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetClassLong(_->hwnd, _->offset, _->newval, _->ansi);
}

static void QEMU_HOST(NtUserSetClassLongPtr)(void *$context)
{
    struct NtUserSetClassLongPtr_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetClassLongPtr(_->hwnd, _->offset, _->newval, _->ansi);
}

static void QEMU_HOST(NtUserSetClassWord)(void *$context)
{
    struct NtUserSetClassWord_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetClassWord(_->hwnd, _->offset, _->newval);
}

static void QEMU_HOST(NtUserSetClipboardData)(void *$context)
{
    struct NtUserSetClipboardData_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetClipboardData(_->format, _->handle, _->params);
}

static void QEMU_HOST(NtUserSetClipboardViewer)(void *$context)
{
    struct NtUserSetClipboardViewer_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetClipboardViewer(_->hwnd);
}

static void QEMU_HOST(NtUserSetCursor)(void *$context)
{
    struct NtUserSetCursor_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetCursor(_->cursor);
}

static void QEMU_HOST(NtUserSetCursorIconData)(void *$context)
{
    struct NtUserSetCursorIconData_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetCursorIconData(_->cursor, _->module, _->res_name, _->desc);
}

static void QEMU_HOST(NtUserSetCursorPos)(void *$context)
{
    struct NtUserSetCursorPos_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetCursorPos(_->x, _->y);
}

static void QEMU_HOST(NtUserSetFocus)(void *$context)
{
    struct NtUserSetFocus_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetFocus(_->hwnd);
}

static void QEMU_HOST(NtUserSetInternalWindowPos)(void *$context)
{
    struct NtUserSetInternalWindowPos_params *_ = (typeof(_))$context;
    NtUserSetInternalWindowPos(_->hwnd, _->cmd, _->rect, _->pt);
}

static void QEMU_HOST(NtUserSetKeyboardState)(void *$context)
{
    struct NtUserSetKeyboardState_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetKeyboardState(_->state);
}

static void QEMU_HOST(NtUserSetLayeredWindowAttributes)(void *$context)
{
    struct NtUserSetLayeredWindowAttributes_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetLayeredWindowAttributes(_->hwnd, _->key, _->alpha, _->flags);
}

static void QEMU_HOST(NtUserSetMenu)(void *$context)
{
    struct NtUserSetMenu_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetMenu(_->hwnd, _->menu);
}

static void QEMU_HOST(NtUserSetMenuContextHelpId)(void *$context)
{
    struct NtUserSetMenuContextHelpId_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetMenuContextHelpId(_->handle, _->id);
}

static void QEMU_HOST(NtUserSetMenuDefaultItem)(void *$context)
{
    struct NtUserSetMenuDefaultItem_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetMenuDefaultItem(_->handle, _->item, _->bypos);
}

static void QEMU_HOST(NtUserSetObjectInformation)(void *$context)
{
    struct NtUserSetObjectInformation_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetObjectInformation(_->handle, _->index, _->info, _->len);
}

static void QEMU_HOST(NtUserSetParent)(void *$context)
{
    struct NtUserSetParent_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetParent(_->hwnd, _->parent);
}

static void QEMU_HOST(NtUserSetProcessDpiAwarenessContext)(void *$context)
{
    struct NtUserSetProcessDpiAwarenessContext_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetProcessDpiAwarenessContext(_->awareness, _->unknown);
}

static void QEMU_HOST(NtUserSetProcessWindowStation)(void *$context)
{
    struct NtUserSetProcessWindowStation_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetProcessWindowStation(_->handle);
}

static void QEMU_HOST(NtUserSetProp)(void *$context)
{
    struct NtUserSetProp_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetProp(_->hwnd, _->str, _->handle);
}

static void QEMU_HOST(NtUserSetScrollInfo)(void *$context)
{
    struct NtUserSetScrollInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetScrollInfo(_->hwnd, _->bar, _->info, _->redraw);
}

static void QEMU_HOST(NtUserSetShellWindowEx)(void *$context)
{
    struct NtUserSetShellWindowEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetShellWindowEx(_->shell, _->list_view);
}

static void QEMU_HOST(NtUserSetSysColors)(void *$context)
{
    struct NtUserSetSysColors_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetSysColors(_->count, _->colors, _->values);
}

static void QEMU_HOST(NtUserSetSystemMenu)(void *$context)
{
    struct NtUserSetSystemMenu_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetSystemMenu(_->hwnd, _->menu);
}

static void QEMU_HOST(NtUserSetSystemTimer)(void *$context)
{
    struct NtUserSetSystemTimer_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetSystemTimer(_->hwnd, _->id, _->timeout);
}

static void QEMU_HOST(NtUserSetThreadDesktop)(void *$context)
{
    struct NtUserSetThreadDesktop_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetThreadDesktop(_->handle);
}

static void QEMU_HOST(NtUserSetTimer)(void *$context)
{
    struct NtUserSetTimer_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetTimer(_->hwnd, _->id, _->timeout, _->proc, _->tolerance);
}

static void QEMU_HOST(NtUserSetWinEventHook)(void *$context)
{
    struct NtUserSetWinEventHook_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWinEventHook(_->event_min, _->event_max, _->inst, _->module, _->proc, _->pid, _->tid, _->flags);
}

static void QEMU_HOST(NtUserSetWindowLong)(void *$context)
{
    struct NtUserSetWindowLong_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWindowLong(_->hwnd, _->offset, _->newval, _->ansi);
}

static void QEMU_HOST(NtUserSetWindowLongPtr)(void *$context)
{
    struct NtUserSetWindowLongPtr_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWindowLongPtr(_->hwnd, _->offset, _->newval, _->ansi);
}

static void QEMU_HOST(NtUserSetWindowPlacement)(void *$context)
{
    struct NtUserSetWindowPlacement_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWindowPlacement(_->hwnd, _->wpl);
}

static void QEMU_HOST(NtUserSetWindowPos)(void *$context)
{
    struct NtUserSetWindowPos_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWindowPos(_->hwnd, _->after, _->x, _->y, _->cx, _->cy, _->flags);
}

static void QEMU_HOST(NtUserSetWindowRgn)(void *$context)
{
    struct NtUserSetWindowRgn_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWindowRgn(_->hwnd, _->hrgn, _->redraw);
}

static void QEMU_HOST(NtUserSetWindowWord)(void *$context)
{
    struct NtUserSetWindowWord_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWindowWord(_->hwnd, _->offset, _->newval);
}

static void QEMU_HOST(NtUserSetWindowsHookEx)(void *$context)
{
    struct NtUserSetWindowsHookEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserSetWindowsHookEx(_->inst, _->module, _->tid, _->id, _->proc, _->ansi);
}

static void QEMU_HOST(NtUserShowCaret)(void *$context)
{
    struct NtUserShowCaret_params *_ = (typeof(_))$context;
    _->$ret = NtUserShowCaret(_->hwnd);
}

static void QEMU_HOST(NtUserShowCursor)(void *$context)
{
    struct NtUserShowCursor_params *_ = (typeof(_))$context;
    _->$ret = NtUserShowCursor(_->show);
}

static void QEMU_HOST(NtUserShowScrollBar)(void *$context)
{
    struct NtUserShowScrollBar_params *_ = (typeof(_))$context;
    _->$ret = NtUserShowScrollBar(_->hwnd, _->bar, _->show);
}

static void QEMU_HOST(NtUserShowWindow)(void *$context)
{
    struct NtUserShowWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserShowWindow(_->hwnd, _->cmd);
}

static void QEMU_HOST(NtUserShowWindowAsync)(void *$context)
{
    struct NtUserShowWindowAsync_params *_ = (typeof(_))$context;
    _->$ret = NtUserShowWindowAsync(_->hwnd, _->cmd);
}

static void QEMU_HOST(NtUserSystemParametersInfo)(void *$context)
{
    struct NtUserSystemParametersInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserSystemParametersInfo(_->action, _->val, _->ptr, _->winini);
}

static void QEMU_HOST(NtUserSystemParametersInfoForDpi)(void *$context)
{
    struct NtUserSystemParametersInfoForDpi_params *_ = (typeof(_))$context;
    _->$ret = NtUserSystemParametersInfoForDpi(_->action, _->val, _->ptr, _->winini, _->dpi);
}

static void QEMU_HOST(NtUserThunkedMenuInfo)(void *$context)
{
    struct NtUserThunkedMenuInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserThunkedMenuInfo(_->menu, _->info);
}

static void QEMU_HOST(NtUserThunkedMenuItemInfo)(void *$context)
{
    struct NtUserThunkedMenuItemInfo_params *_ = (typeof(_))$context;
    _->$ret = NtUserThunkedMenuItemInfo(_->menu, _->pos, _->flags, _->method, _->info, _->str);
}

static void QEMU_HOST(NtUserToUnicodeEx)(void *$context)
{
    struct NtUserToUnicodeEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserToUnicodeEx(_->virt, _->scan, _->state, _->str, _->size, _->flags, _->layout);
}

static void QEMU_HOST(NtUserTrackMouseEvent)(void *$context)
{
    struct NtUserTrackMouseEvent_params *_ = (typeof(_))$context;
    _->$ret = NtUserTrackMouseEvent(_->info);
}

static void QEMU_HOST(NtUserTrackPopupMenuEx)(void *$context)
{
    struct NtUserTrackPopupMenuEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserTrackPopupMenuEx(_->handle, _->flags, _->x, _->y, _->hwnd, _->params);
}

static void QEMU_HOST(NtUserTranslateAccelerator)(void *$context)
{
    struct NtUserTranslateAccelerator_params *_ = (typeof(_))$context;
    _->$ret = NtUserTranslateAccelerator(_->hwnd, _->accel, _->msg);
}

static void QEMU_HOST(NtUserTranslateMessage)(void *$context)
{
    struct NtUserTranslateMessage_params *_ = (typeof(_))$context;
    _->$ret = NtUserTranslateMessage(_->msg, _->flags);
}

static void QEMU_HOST(NtUserUnhookWinEvent)(void *$context)
{
    struct NtUserUnhookWinEvent_params *_ = (typeof(_))$context;
    _->$ret = NtUserUnhookWinEvent(_->hEventHook);
}

static void QEMU_HOST(NtUserUnhookWindowsHookEx)(void *$context)
{
    struct NtUserUnhookWindowsHookEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserUnhookWindowsHookEx(_->handle);
}

static void QEMU_HOST(NtUserUnregisterClass)(void *$context)
{
    struct NtUserUnregisterClass_params *_ = (typeof(_))$context;
    _->$ret = NtUserUnregisterClass(_->name, _->instance, _->client_menu_name);
}

static void QEMU_HOST(NtUserUnregisterHotKey)(void *$context)
{
    struct NtUserUnregisterHotKey_params *_ = (typeof(_))$context;
    _->$ret = NtUserUnregisterHotKey(_->hwnd, _->id);
}

static void QEMU_HOST(NtUserUpdateInputContext)(void *$context)
{
    struct NtUserUpdateInputContext_params *_ = (typeof(_))$context;
    _->$ret = NtUserUpdateInputContext(_->handle, _->attr, _->value);
}

static void QEMU_HOST(NtUserUpdateLayeredWindow)(void *$context)
{
    struct NtUserUpdateLayeredWindow_params *_ = (typeof(_))$context;
    _->$ret = NtUserUpdateLayeredWindow(_->hwnd, _->hdc_dst, _->pts_dst, _->size, _->hdc_src, _->pts_src, _->key, _->blend, _->flags, _->dirty);
}

static void QEMU_HOST(NtUserValidateRect)(void *$context)
{
    struct NtUserValidateRect_params *_ = (typeof(_))$context;
    _->$ret = NtUserValidateRect(_->hwnd, _->rect);
}

static void QEMU_HOST(NtUserVkKeyScanEx)(void *$context)
{
    struct NtUserVkKeyScanEx_params *_ = (typeof(_))$context;
    _->$ret = NtUserVkKeyScanEx(_->chr, _->layout);
}

static void QEMU_HOST(NtUserWaitForInputIdle)(void *$context)
{
    struct NtUserWaitForInputIdle_params *_ = (typeof(_))$context;
    _->$ret = NtUserWaitForInputIdle(_->process, _->timeout, _->wow);
}

static void QEMU_HOST(NtUserWaitMessage)(void *$context)
{
    struct NtUserWaitMessage_params *_ = (typeof(_))$context;
    _->$ret = NtUserWaitMessage();
}

static void QEMU_HOST(NtUserWindowFromDC)(void *$context)
{
    struct NtUserWindowFromDC_params *_ = (typeof(_))$context;
    _->$ret = NtUserWindowFromDC(_->hdc);
}

static void QEMU_HOST(NtUserWindowFromPoint)(void *$context)
{
    struct NtUserWindowFromPoint_params *_ = (typeof(_))$context;
    _->$ret = NtUserWindowFromPoint(_->x, _->y);
}

static void QEMU_HOST(__wine_get_file_outline_text_metric)(void *$context)
{
    struct __wine_get_file_outline_text_metric_params *_ = (typeof(_))$context;
    _->$ret = __wine_get_file_outline_text_metric(_->path, _->otm, _->em_square, _->face_name);
}

static void QEMU_HOST(__wine_get_icm_profile)(void *$context)
{
    struct __wine_get_icm_profile_params *_ = (typeof(_))$context;
    _->$ret = __wine_get_icm_profile(_->hdc, _->allow_default, _->size, _->filename);
}

static void QEMU_HOST(__wine_send_input)(void *$context)
{
    struct __wine_send_input_params *_ = (typeof(_))$context;
    _->$ret = __wine_send_input(_->hwnd, _->input, _->rawinput);
}

qemu_host_win32u_vars_t qemu_host_win32u_vars =
{
    QEMU_HOST_REGISTER_METHOD(NtGdiAbortDoc),
    QEMU_HOST_REGISTER_METHOD(NtGdiAbortPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiAddFontMemResourceEx),
    QEMU_HOST_REGISTER_METHOD(NtGdiAddFontResourceW),
    QEMU_HOST_REGISTER_METHOD(NtGdiAlphaBlend),
    QEMU_HOST_REGISTER_METHOD(NtGdiAngleArc),
    QEMU_HOST_REGISTER_METHOD(NtGdiArcInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiBeginPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiBitBlt),
    QEMU_HOST_REGISTER_METHOD(NtGdiCloseFigure),
    QEMU_HOST_REGISTER_METHOD(NtGdiCombineRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiComputeXformCoefficients),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateBitmap),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateClientObj),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateCompatibleBitmap),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateCompatibleDC),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateDIBBrush),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateDIBSection),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateDIBitmapInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateEllipticRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateHalftonePalette),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateHatchBrushInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateMetafileDC),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreatePaletteInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreatePatternBrushInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreatePen),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateRectRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateRoundRectRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiCreateSolidBrush),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDICheckVidPnExclusiveOwnership),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDICloseAdapter),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDICreateDCFromMemory),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDICreateDevice),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIDestroyDCFromMemory),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIDestroyDevice),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIEscape),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIOpenAdapterFromDeviceName),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIOpenAdapterFromHdc),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIOpenAdapterFromLuid),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIQueryStatistics),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDIQueryVideoMemoryInfo),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDISetQueuedLimit),
    QEMU_HOST_REGISTER_METHOD(NtGdiDdDDISetVidPnSourceOwner),
    QEMU_HOST_REGISTER_METHOD(NtGdiDeleteClientObj),
    QEMU_HOST_REGISTER_METHOD(NtGdiDeleteObjectApp),
    QEMU_HOST_REGISTER_METHOD(NtGdiDescribePixelFormat),
    QEMU_HOST_REGISTER_METHOD(NtGdiDoPalette),
    QEMU_HOST_REGISTER_METHOD(NtGdiDrawStream),
    QEMU_HOST_REGISTER_METHOD(NtGdiEllipse),
    QEMU_HOST_REGISTER_METHOD(NtGdiEndDoc),
    QEMU_HOST_REGISTER_METHOD(NtGdiEndPage),
    QEMU_HOST_REGISTER_METHOD(NtGdiEndPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiEnumFonts),
    QEMU_HOST_REGISTER_METHOD(NtGdiEqualRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiExcludeClipRect),
    QEMU_HOST_REGISTER_METHOD(NtGdiExtCreatePen),
    QEMU_HOST_REGISTER_METHOD(NtGdiExtCreateRegion),
    QEMU_HOST_REGISTER_METHOD(NtGdiExtEscape),
    QEMU_HOST_REGISTER_METHOD(NtGdiExtFloodFill),
    QEMU_HOST_REGISTER_METHOD(NtGdiExtGetObjectW),
    QEMU_HOST_REGISTER_METHOD(NtGdiExtSelectClipRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiExtTextOutW),
    QEMU_HOST_REGISTER_METHOD(NtGdiFillPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiFillRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiFlattenPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiFlush),
    QEMU_HOST_REGISTER_METHOD(NtGdiFontIsLinked),
    QEMU_HOST_REGISTER_METHOD(NtGdiFrameRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetAndSetDCDword),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetAppClipBox),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetBitmapBits),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetBitmapDimension),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetBoundsRect),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetCharABCWidthsW),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetCharWidthInfo),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetCharWidthW),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetColorAdjustment),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetDCDword),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetDCObject),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetDCPoint),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetDIBitsInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetDeviceCaps),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetDeviceGammaRamp),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetFontData),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetFontFileData),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetFontFileInfo),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetFontUnicodeRanges),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetGlyphIndicesW),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetGlyphOutline),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetKerningPairs),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetNearestColor),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetNearestPaletteIndex),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetOutlineTextMetricsInternalW),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetPixel),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetRandomRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetRasterizerCaps),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetRealizationInfo),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetRegionData),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetRgnBox),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetSpoolMessage),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetSystemPaletteUse),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetTextCharsetInfo),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetTextExtentExW),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetTextFaceW),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetTextMetricsW),
    QEMU_HOST_REGISTER_METHOD(NtGdiGetTransform),
    QEMU_HOST_REGISTER_METHOD(NtGdiGradientFill),
    QEMU_HOST_REGISTER_METHOD(NtGdiHfontCreate),
    QEMU_HOST_REGISTER_METHOD(NtGdiIcmBrushInfo),
    QEMU_HOST_REGISTER_METHOD(NtGdiInitSpool),
    QEMU_HOST_REGISTER_METHOD(NtGdiIntersectClipRect),
    QEMU_HOST_REGISTER_METHOD(NtGdiInvertRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiLineTo),
    QEMU_HOST_REGISTER_METHOD(NtGdiMaskBlt),
    QEMU_HOST_REGISTER_METHOD(NtGdiModifyWorldTransform),
    QEMU_HOST_REGISTER_METHOD(NtGdiMoveTo),
    QEMU_HOST_REGISTER_METHOD(NtGdiOffsetClipRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiOffsetRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiOpenDCW),
    QEMU_HOST_REGISTER_METHOD(NtGdiPatBlt),
    QEMU_HOST_REGISTER_METHOD(NtGdiPathToRegion),
    QEMU_HOST_REGISTER_METHOD(NtGdiPlgBlt),
    QEMU_HOST_REGISTER_METHOD(NtGdiPolyDraw),
    QEMU_HOST_REGISTER_METHOD(NtGdiPolyPolyDraw),
    QEMU_HOST_REGISTER_METHOD(NtGdiPtInRegion),
    QEMU_HOST_REGISTER_METHOD(NtGdiPtVisible),
    QEMU_HOST_REGISTER_METHOD(NtGdiRectInRegion),
    QEMU_HOST_REGISTER_METHOD(NtGdiRectVisible),
    QEMU_HOST_REGISTER_METHOD(NtGdiRectangle),
    QEMU_HOST_REGISTER_METHOD(NtGdiRemoveFontMemResourceEx),
    QEMU_HOST_REGISTER_METHOD(NtGdiRemoveFontResourceW),
    QEMU_HOST_REGISTER_METHOD(NtGdiResetDC),
    QEMU_HOST_REGISTER_METHOD(NtGdiResizePalette),
    QEMU_HOST_REGISTER_METHOD(NtGdiRestoreDC),
    QEMU_HOST_REGISTER_METHOD(NtGdiRoundRect),
    QEMU_HOST_REGISTER_METHOD(NtGdiSaveDC),
    QEMU_HOST_REGISTER_METHOD(NtGdiScaleViewportExtEx),
    QEMU_HOST_REGISTER_METHOD(NtGdiScaleWindowExtEx),
    QEMU_HOST_REGISTER_METHOD(NtGdiSelectBitmap),
    QEMU_HOST_REGISTER_METHOD(NtGdiSelectBrush),
    QEMU_HOST_REGISTER_METHOD(NtGdiSelectClipPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiSelectFont),
    QEMU_HOST_REGISTER_METHOD(NtGdiSelectPen),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetBitmapBits),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetBitmapDimension),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetBoundsRect),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetBrushOrg),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetColorAdjustment),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetDIBitsToDeviceInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetDeviceGammaRamp),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetLayout),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetMagicColors),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetMetaRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetPixel),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetPixelFormat),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetRectRgn),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetSystemPaletteUse),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetTextJustification),
    QEMU_HOST_REGISTER_METHOD(NtGdiSetVirtualResolution),
    QEMU_HOST_REGISTER_METHOD(NtGdiStartDoc),
    QEMU_HOST_REGISTER_METHOD(NtGdiStartPage),
    QEMU_HOST_REGISTER_METHOD(NtGdiStretchBlt),
    QEMU_HOST_REGISTER_METHOD(NtGdiStretchDIBitsInternal),
    QEMU_HOST_REGISTER_METHOD(NtGdiStrokeAndFillPath),
    QEMU_HOST_REGISTER_METHOD(NtGdiStrokePath),
    QEMU_HOST_REGISTER_METHOD(NtGdiSwapBuffers),
    QEMU_HOST_REGISTER_METHOD(NtGdiTransformPoints),
    QEMU_HOST_REGISTER_METHOD(NtGdiTransparentBlt),
    QEMU_HOST_REGISTER_METHOD(NtGdiUnrealizeObject),
    QEMU_HOST_REGISTER_METHOD(NtGdiUpdateColors),
    QEMU_HOST_REGISTER_METHOD(NtGdiWidenPath),
    QEMU_HOST_REGISTER_METHOD(NtUserActivateKeyboardLayout),
    QEMU_HOST_REGISTER_METHOD(NtUserAddClipboardFormatListener),
    QEMU_HOST_REGISTER_METHOD(NtUserAssociateInputContext),
    QEMU_HOST_REGISTER_METHOD(NtUserAttachThreadInput),
    QEMU_HOST_REGISTER_METHOD(NtUserBeginPaint),
    QEMU_HOST_REGISTER_METHOD(NtUserBuildHimcList),
    QEMU_HOST_REGISTER_METHOD(NtUserBuildHwndList),
    QEMU_HOST_REGISTER_METHOD(NtUserCallHwnd),
    QEMU_HOST_REGISTER_METHOD(NtUserCallHwndParam),
    QEMU_HOST_REGISTER_METHOD(NtUserCallMsgFilter),
    QEMU_HOST_REGISTER_METHOD(NtUserCallNextHookEx),
    QEMU_HOST_REGISTER_METHOD(NtUserCallNoParam),
    QEMU_HOST_REGISTER_METHOD(NtUserCallOneParam),
    QEMU_HOST_REGISTER_METHOD(NtUserCallTwoParam),
    QEMU_HOST_REGISTER_METHOD(NtUserChangeClipboardChain),
    QEMU_HOST_REGISTER_METHOD(NtUserChangeDisplaySettings),
    QEMU_HOST_REGISTER_METHOD(NtUserCheckMenuItem),
    QEMU_HOST_REGISTER_METHOD(NtUserChildWindowFromPointEx),
    QEMU_HOST_REGISTER_METHOD(NtUserClipCursor),
    QEMU_HOST_REGISTER_METHOD(NtUserCloseClipboard),
    QEMU_HOST_REGISTER_METHOD(NtUserCloseDesktop),
    QEMU_HOST_REGISTER_METHOD(NtUserCloseWindowStation),
    QEMU_HOST_REGISTER_METHOD(NtUserCopyAcceleratorTable),
    QEMU_HOST_REGISTER_METHOD(NtUserCountClipboardFormats),
    QEMU_HOST_REGISTER_METHOD(NtUserCreateAcceleratorTable),
    QEMU_HOST_REGISTER_METHOD(NtUserCreateCaret),
    QEMU_HOST_REGISTER_METHOD(NtUserCreateDesktopEx),
    QEMU_HOST_REGISTER_METHOD(NtUserCreateInputContext),
    QEMU_HOST_REGISTER_METHOD(NtUserCreateWindowEx),
    QEMU_HOST_REGISTER_METHOD(NtUserCreateWindowStation),
    QEMU_HOST_REGISTER_METHOD(NtUserDeferWindowPosAndBand),
    QEMU_HOST_REGISTER_METHOD(NtUserDeleteMenu),
    QEMU_HOST_REGISTER_METHOD(NtUserDestroyAcceleratorTable),
    QEMU_HOST_REGISTER_METHOD(NtUserDestroyCursor),
    QEMU_HOST_REGISTER_METHOD(NtUserDestroyInputContext),
    QEMU_HOST_REGISTER_METHOD(NtUserDestroyMenu),
    QEMU_HOST_REGISTER_METHOD(NtUserDestroyWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserDisableThreadIme),
    QEMU_HOST_REGISTER_METHOD(NtUserDispatchMessage),
    QEMU_HOST_REGISTER_METHOD(NtUserDisplayConfigGetDeviceInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserDragDetect),
    QEMU_HOST_REGISTER_METHOD(NtUserDragObject),
    QEMU_HOST_REGISTER_METHOD(NtUserDrawCaptionTemp),
    QEMU_HOST_REGISTER_METHOD(NtUserDrawIconEx),
    QEMU_HOST_REGISTER_METHOD(NtUserDrawMenuBarTemp),
    QEMU_HOST_REGISTER_METHOD(NtUserEmptyClipboard),
    QEMU_HOST_REGISTER_METHOD(NtUserEnableMenuItem),
    QEMU_HOST_REGISTER_METHOD(NtUserEnableMouseInPointer),
    QEMU_HOST_REGISTER_METHOD(NtUserEnableScrollBar),
    QEMU_HOST_REGISTER_METHOD(NtUserEndDeferWindowPosEx),
    QEMU_HOST_REGISTER_METHOD(NtUserEndMenu),
    QEMU_HOST_REGISTER_METHOD(NtUserEndPaint),
    QEMU_HOST_REGISTER_METHOD(NtUserEnumDisplayDevices),
    QEMU_HOST_REGISTER_METHOD(NtUserEnumDisplayMonitors),
    QEMU_HOST_REGISTER_METHOD(NtUserEnumDisplaySettings),
    QEMU_HOST_REGISTER_METHOD(NtUserExcludeUpdateRgn),
    QEMU_HOST_REGISTER_METHOD(NtUserFindExistingCursorIcon),
    QEMU_HOST_REGISTER_METHOD(NtUserFindWindowEx),
    QEMU_HOST_REGISTER_METHOD(NtUserFlashWindowEx),
    QEMU_HOST_REGISTER_METHOD(NtUserGetAncestor),
    QEMU_HOST_REGISTER_METHOD(NtUserGetAsyncKeyState),
    QEMU_HOST_REGISTER_METHOD(NtUserGetAtomName),
    QEMU_HOST_REGISTER_METHOD(NtUserGetCaretBlinkTime),
    QEMU_HOST_REGISTER_METHOD(NtUserGetCaretPos),
    QEMU_HOST_REGISTER_METHOD(NtUserGetClassInfoEx),
    QEMU_HOST_REGISTER_METHOD(NtUserGetClassName),
    QEMU_HOST_REGISTER_METHOD(NtUserGetClipboardData),
    QEMU_HOST_REGISTER_METHOD(NtUserGetClipboardFormatName),
    QEMU_HOST_REGISTER_METHOD(NtUserGetClipboardOwner),
    QEMU_HOST_REGISTER_METHOD(NtUserGetClipboardSequenceNumber),
    QEMU_HOST_REGISTER_METHOD(NtUserGetClipboardViewer),
    QEMU_HOST_REGISTER_METHOD(NtUserGetCursor),
    QEMU_HOST_REGISTER_METHOD(NtUserGetCursorFrameInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetCursorInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetDC),
    QEMU_HOST_REGISTER_METHOD(NtUserGetDCEx),
    QEMU_HOST_REGISTER_METHOD(NtUserGetDisplayConfigBufferSizes),
    QEMU_HOST_REGISTER_METHOD(NtUserGetDoubleClickTime),
    QEMU_HOST_REGISTER_METHOD(NtUserGetDpiForMonitor),
    QEMU_HOST_REGISTER_METHOD(NtUserGetForegroundWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserGetGUIThreadInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetIconInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetIconSize),
    QEMU_HOST_REGISTER_METHOD(NtUserGetInternalWindowPos),
    QEMU_HOST_REGISTER_METHOD(NtUserGetKeyNameText),
    QEMU_HOST_REGISTER_METHOD(NtUserGetKeyState),
    QEMU_HOST_REGISTER_METHOD(NtUserGetKeyboardLayout),
    QEMU_HOST_REGISTER_METHOD(NtUserGetKeyboardLayoutList),
    QEMU_HOST_REGISTER_METHOD(NtUserGetKeyboardLayoutName),
    QEMU_HOST_REGISTER_METHOD(NtUserGetKeyboardState),
    QEMU_HOST_REGISTER_METHOD(NtUserGetLayeredWindowAttributes),
    QEMU_HOST_REGISTER_METHOD(NtUserGetMenuBarInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetMenuItemRect),
    QEMU_HOST_REGISTER_METHOD(NtUserGetMessage),
    QEMU_HOST_REGISTER_METHOD(NtUserGetMouseMovePointsEx),
    QEMU_HOST_REGISTER_METHOD(NtUserGetObjectInformation),
    QEMU_HOST_REGISTER_METHOD(NtUserGetOpenClipboardWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserGetPointerInfoList),
    QEMU_HOST_REGISTER_METHOD(NtUserGetPriorityClipboardFormat),
    QEMU_HOST_REGISTER_METHOD(NtUserGetProcessDpiAwarenessContext),
    QEMU_HOST_REGISTER_METHOD(NtUserGetProcessWindowStation),
    QEMU_HOST_REGISTER_METHOD(NtUserGetProp),
    QEMU_HOST_REGISTER_METHOD(NtUserGetQueueStatus),
    QEMU_HOST_REGISTER_METHOD(NtUserGetRawInputBuffer),
    QEMU_HOST_REGISTER_METHOD(NtUserGetRawInputData),
    QEMU_HOST_REGISTER_METHOD(NtUserGetRawInputDeviceInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetRawInputDeviceList),
    QEMU_HOST_REGISTER_METHOD(NtUserGetRegisteredRawInputDevices),
    QEMU_HOST_REGISTER_METHOD(NtUserGetScrollBarInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetSystemDpiForProcess),
    QEMU_HOST_REGISTER_METHOD(NtUserGetSystemMenu),
    QEMU_HOST_REGISTER_METHOD(NtUserGetThreadDesktop),
    QEMU_HOST_REGISTER_METHOD(NtUserGetTitleBarInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserGetUpdateRect),
    QEMU_HOST_REGISTER_METHOD(NtUserGetUpdateRgn),
    QEMU_HOST_REGISTER_METHOD(NtUserGetUpdatedClipboardFormats),
    QEMU_HOST_REGISTER_METHOD(NtUserGetWindowDC),
    QEMU_HOST_REGISTER_METHOD(NtUserGetWindowPlacement),
    QEMU_HOST_REGISTER_METHOD(NtUserGetWindowRgnEx),
    QEMU_HOST_REGISTER_METHOD(NtUserHideCaret),
    QEMU_HOST_REGISTER_METHOD(NtUserHiliteMenuItem),
    QEMU_HOST_REGISTER_METHOD(NtUserInitializeClientPfnArrays),
    QEMU_HOST_REGISTER_METHOD(NtUserInternalGetWindowIcon),
    QEMU_HOST_REGISTER_METHOD(NtUserInternalGetWindowText),
    QEMU_HOST_REGISTER_METHOD(NtUserInvalidateRect),
    QEMU_HOST_REGISTER_METHOD(NtUserInvalidateRgn),
    QEMU_HOST_REGISTER_METHOD(NtUserIsClipboardFormatAvailable),
    QEMU_HOST_REGISTER_METHOD(NtUserIsMouseInPointerEnabled),
    QEMU_HOST_REGISTER_METHOD(NtUserKillTimer),
    QEMU_HOST_REGISTER_METHOD(NtUserLockWindowUpdate),
    QEMU_HOST_REGISTER_METHOD(NtUserLogicalToPerMonitorDPIPhysicalPoint),
    QEMU_HOST_REGISTER_METHOD(NtUserMapVirtualKeyEx),
    QEMU_HOST_REGISTER_METHOD(NtUserMenuItemFromPoint),
    QEMU_HOST_REGISTER_METHOD(NtUserMessageCall),
    QEMU_HOST_REGISTER_METHOD(NtUserMoveWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserMsgWaitForMultipleObjectsEx),
    QEMU_HOST_REGISTER_METHOD(NtUserNotifyIMEStatus),
    QEMU_HOST_REGISTER_METHOD(NtUserNotifyWinEvent),
    QEMU_HOST_REGISTER_METHOD(NtUserOpenClipboard),
    QEMU_HOST_REGISTER_METHOD(NtUserOpenDesktop),
    QEMU_HOST_REGISTER_METHOD(NtUserOpenInputDesktop),
    QEMU_HOST_REGISTER_METHOD(NtUserOpenWindowStation),
    QEMU_HOST_REGISTER_METHOD(NtUserPeekMessage),
    QEMU_HOST_REGISTER_METHOD(NtUserPerMonitorDPIPhysicalToLogicalPoint),
    QEMU_HOST_REGISTER_METHOD(NtUserPostMessage),
    QEMU_HOST_REGISTER_METHOD(NtUserPostThreadMessage),
    QEMU_HOST_REGISTER_METHOD(NtUserPrintWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserQueryDisplayConfig),
    QEMU_HOST_REGISTER_METHOD(NtUserQueryInputContext),
    QEMU_HOST_REGISTER_METHOD(NtUserRealChildWindowFromPoint),
    QEMU_HOST_REGISTER_METHOD(NtUserRedrawWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserRegisterClassExWOW),
    QEMU_HOST_REGISTER_METHOD(NtUserRegisterHotKey),
    QEMU_HOST_REGISTER_METHOD(NtUserRegisterRawInputDevices),
    QEMU_HOST_REGISTER_METHOD(NtUserReleaseDC),
    QEMU_HOST_REGISTER_METHOD(NtUserRemoveClipboardFormatListener),
    QEMU_HOST_REGISTER_METHOD(NtUserRemoveMenu),
    QEMU_HOST_REGISTER_METHOD(NtUserRemoveProp),
    QEMU_HOST_REGISTER_METHOD(NtUserScrollDC),
    QEMU_HOST_REGISTER_METHOD(NtUserScrollWindowEx),
    QEMU_HOST_REGISTER_METHOD(NtUserSelectPalette),
    QEMU_HOST_REGISTER_METHOD(NtUserSendInput),
    QEMU_HOST_REGISTER_METHOD(NtUserSetActiveWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserSetCapture),
    QEMU_HOST_REGISTER_METHOD(NtUserSetClassLong),
    QEMU_HOST_REGISTER_METHOD(NtUserSetClassLongPtr),
    QEMU_HOST_REGISTER_METHOD(NtUserSetClassWord),
    QEMU_HOST_REGISTER_METHOD(NtUserSetClipboardData),
    QEMU_HOST_REGISTER_METHOD(NtUserSetClipboardViewer),
    QEMU_HOST_REGISTER_METHOD(NtUserSetCursor),
    QEMU_HOST_REGISTER_METHOD(NtUserSetCursorIconData),
    QEMU_HOST_REGISTER_METHOD(NtUserSetCursorPos),
    QEMU_HOST_REGISTER_METHOD(NtUserSetFocus),
    QEMU_HOST_REGISTER_METHOD(NtUserSetInternalWindowPos),
    QEMU_HOST_REGISTER_METHOD(NtUserSetKeyboardState),
    QEMU_HOST_REGISTER_METHOD(NtUserSetLayeredWindowAttributes),
    QEMU_HOST_REGISTER_METHOD(NtUserSetMenu),
    QEMU_HOST_REGISTER_METHOD(NtUserSetMenuContextHelpId),
    QEMU_HOST_REGISTER_METHOD(NtUserSetMenuDefaultItem),
    QEMU_HOST_REGISTER_METHOD(NtUserSetObjectInformation),
    QEMU_HOST_REGISTER_METHOD(NtUserSetParent),
    QEMU_HOST_REGISTER_METHOD(NtUserSetProcessDpiAwarenessContext),
    QEMU_HOST_REGISTER_METHOD(NtUserSetProcessWindowStation),
    QEMU_HOST_REGISTER_METHOD(NtUserSetProp),
    QEMU_HOST_REGISTER_METHOD(NtUserSetScrollInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserSetShellWindowEx),
    QEMU_HOST_REGISTER_METHOD(NtUserSetSysColors),
    QEMU_HOST_REGISTER_METHOD(NtUserSetSystemMenu),
    QEMU_HOST_REGISTER_METHOD(NtUserSetSystemTimer),
    QEMU_HOST_REGISTER_METHOD(NtUserSetThreadDesktop),
    QEMU_HOST_REGISTER_METHOD(NtUserSetTimer),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWinEventHook),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWindowLong),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWindowLongPtr),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWindowPlacement),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWindowPos),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWindowRgn),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWindowWord),
    QEMU_HOST_REGISTER_METHOD(NtUserSetWindowsHookEx),
    QEMU_HOST_REGISTER_METHOD(NtUserShowCaret),
    QEMU_HOST_REGISTER_METHOD(NtUserShowCursor),
    QEMU_HOST_REGISTER_METHOD(NtUserShowScrollBar),
    QEMU_HOST_REGISTER_METHOD(NtUserShowWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserShowWindowAsync),
    QEMU_HOST_REGISTER_METHOD(NtUserSystemParametersInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserSystemParametersInfoForDpi),
    QEMU_HOST_REGISTER_METHOD(NtUserThunkedMenuInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserThunkedMenuItemInfo),
    QEMU_HOST_REGISTER_METHOD(NtUserToUnicodeEx),
    QEMU_HOST_REGISTER_METHOD(NtUserTrackMouseEvent),
    QEMU_HOST_REGISTER_METHOD(NtUserTrackPopupMenuEx),
    QEMU_HOST_REGISTER_METHOD(NtUserTranslateAccelerator),
    QEMU_HOST_REGISTER_METHOD(NtUserTranslateMessage),
    QEMU_HOST_REGISTER_METHOD(NtUserUnhookWinEvent),
    QEMU_HOST_REGISTER_METHOD(NtUserUnhookWindowsHookEx),
    QEMU_HOST_REGISTER_METHOD(NtUserUnregisterClass),
    QEMU_HOST_REGISTER_METHOD(NtUserUnregisterHotKey),
    QEMU_HOST_REGISTER_METHOD(NtUserUpdateInputContext),
    QEMU_HOST_REGISTER_METHOD(NtUserUpdateLayeredWindow),
    QEMU_HOST_REGISTER_METHOD(NtUserValidateRect),
    QEMU_HOST_REGISTER_METHOD(NtUserVkKeyScanEx),
    QEMU_HOST_REGISTER_METHOD(NtUserWaitForInputIdle),
    QEMU_HOST_REGISTER_METHOD(NtUserWaitMessage),
    QEMU_HOST_REGISTER_METHOD(NtUserWindowFromDC),
    QEMU_HOST_REGISTER_METHOD(NtUserWindowFromPoint),
    QEMU_HOST_REGISTER_METHOD(__wine_get_file_outline_text_metric),
    QEMU_HOST_REGISTER_METHOD(__wine_get_icm_profile),
    QEMU_HOST_REGISTER_METHOD(__wine_send_input),
};