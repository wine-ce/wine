/*
 * RISCV64 signal handling routines
 *
 * Copyright 2023 Fan WenJie
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if 0
#pragma makedep unix
#endif

#if (__riscv_xlen == 64)

#include "config.h"

#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "ntstatus.h"
#include "windef.h"
#include "winnt.h"
#include "winternl.h"
#include "wine/asm.h"
#include "unix_private.h"
#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(seh);

/***********************************************************************
 * signal context platform-specific definitions
 */
#ifdef linux


/***********************************************************************
 *           signal_set_full_context
 */
NTSTATUS signal_set_full_context( CONTEXT *context )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/***********************************************************************
 *              NtSetContextThread  (NTDLL.@)
 *              ZwSetContextThread  (NTDLL.@)
 */
NTSTATUS WINAPI NtSetContextThread( HANDLE handle, const CONTEXT *context )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/***********************************************************************
 *              NtGetContextThread  (NTDLL.@)
 *              ZwGetContextThread  (NTDLL.@)
 */
NTSTATUS WINAPI NtGetContextThread( HANDLE handle, CONTEXT *context )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/***********************************************************************
 *              set_thread_wow64_context
 */
NTSTATUS set_thread_wow64_context( HANDLE handle, const void *ctx, ULONG size )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/***********************************************************************
 *              get_thread_wow64_context
 */
NTSTATUS get_thread_wow64_context( HANDLE handle, void *ctx, ULONG size )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}

/***********************************************************************
 *           call_user_apc_dispatcher_native
 */
NTSTATUS call_user_apc_dispatcher_native( CONTEXT *context, ULONG_PTR arg1, ULONG_PTR arg2, ULONG_PTR arg3,
                                   PNTAPCFUNC func, NTSTATUS status )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/***********************************************************************
 *           call_raise_user_exception_dispatcher
 */
void call_raise_user_exception_dispatcher(void)
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}


/***********************************************************************
 *           call_user_exception_dispatcher
 */
NTSTATUS call_user_exception_dispatcher( EXCEPTION_RECORD *rec, CONTEXT *context )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/***********************************************************************
 *           KeUserModeCallbackNative
 */
NTSTATUS WINAPI KeUserModeCallbackNative( ULONG id, const void *args, ULONG len, void **ret_ptr, ULONG *ret_len )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/***********************************************************************
 *           NtCallbackReturn  (NTDLL.@)
 */
NTSTATUS WINAPI NtCallbackReturn( void *ret_ptr, ULONG ret_len, NTSTATUS status )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/**********************************************************************
 *           get_thread_ldt_entry
 */
NTSTATUS get_thread_ldt_entry( HANDLE handle, void *data, ULONG len, ULONG *ret_len )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/******************************************************************************
 *           NtSetLdtEntries   (NTDLL.@)
 *           ZwSetLdtEntries   (NTDLL.@)
 */
NTSTATUS WINAPI NtSetLdtEntries( ULONG sel1, LDT_ENTRY entry1, ULONG sel2, LDT_ENTRY entry2 )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/**********************************************************************
 *             signal_init_threading
 */
void signal_init_threading(void)
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}


/**********************************************************************
 *             signal_alloc_thread
 */
NTSTATUS signal_alloc_thread( TEB *teb )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}


/**********************************************************************
 *             signal_free_thread
 */
void signal_free_thread( TEB *teb )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}


/**********************************************************************
 *		signal_init_process
 */
void signal_init_process(void)
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}


/***********************************************************************
 *           call_init_thunk
 */
void DECLSPEC_HIDDEN call_init_thunk( LPTHREAD_START_ROUTINE entry, void *arg, BOOL suspend, TEB *teb )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}

NTSTATUS unwind_builtin_dll( void *args )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
    return STATUS_NOT_IMPLEMENTED;
}

void DECLSPEC_NORETURN signal_start_thread( PRTL_THREAD_START_ROUTINE entry, void *arg,
                                                   BOOL suspend, TEB *teb )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}

void DECLSPEC_NORETURN signal_exit_thread( int status, void (*func)(int), TEB *teb )
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}

/***********************************************************************
 *           __wine_syscall_dispatcher
 */
void __wine_syscall_dispatcher(void)
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}

/***********************************************************************
 *           __wine_unix_call_dispatcher
 */
void __wine_unix_call_dispatcher(void)
{
    ERR("%s: NOT Implemented On RISCV64\n", __FUNCTION__); exit(1);
}

/***********************************************************************
 *           __wine_setjmpex
 */
__ASM_GLOBAL_FUNC( __wine_setjmpex,
                "sd a1,    (a0)    \n\t"
                "sd s0,    8(a0)   \n\t"
                "sd s1,    16(a0)  \n\t"
                "sd s2,    24(a0)  \n\t"
                "sd s3,    32(a0)  \n\t"
                "sd s4,    40(a0)  \n\t"
                "sd s5,    48(a0)  \n\t"
                "sd s6,    56(a0)  \n\t"
                "sd s7,    64(a0)  \n\t"
                "sd s8,    72(a0)  \n\t"
                "sd s9,    80(a0)  \n\t"
                "sd s10,   88(a0)  \n\t"
                "sd s11,   96(a0)  \n\t"
                "sd sp,    104(a0) \n\t"
                "sd ra,    112(a0) \n\t"
                "fsd fs0,  120(a0) \n\t"
                "fsd fs1,  128(a0) \n\t"
                "fsd fs2,  136(a0) \n\t"
                "fsd fs3,  144(a0) \n\t"
                "fsd fs4,  152(a0) \n\t"
                "fsd fs5,  160(a0) \n\t"
                "fsd fs6,  168(a0) \n\t"
                "fsd fs7,  176(a0) \n\t"
                "fsd fs8,  184(a0) \n\t"
                "fsd fs9,  192(a0) \n\t"
                "fsd fs10, 200(a0) \n\t"
                "fsd fs11, 208(a0) \n\t"
                "li a0, 0          \n\t"
                "ret               \n\t")

/***********************************************************************
 *           __wine_longjmp
 */
__ASM_GLOBAL_FUNC( __wine_longjmp,
                "ld s0,     8(a0)   \n\t"
                "ld s1,     16(a0)  \n\t"
                "ld s2,     24(a0)  \n\t"
                "ld s3,     32(a0)  \n\t"
                "ld s4,     40(a0)  \n\t"
                "ld s5,     48(a0)  \n\t"
                "ld s6,     56(a0)  \n\t"
                "ld s7,     64(a0)  \n\t"
                "ld s8,     72(a0)  \n\t"
                "ld s9,     80(a0)  \n\t"
                "ld s10,    88(a0)  \n\t"
                "ld s11,    96(a0)  \n\t"
                "ld sp,     104(a0) \n\t"
                "ld ra,     112(a0) \n\t"
                "fld fs0,   120(a0) \n\t"
                "fld fs1,   128(a0) \n\t"
                "fld fs2,   136(a0) \n\t"
                "fld fs3,   144(a0) \n\t"
                "fld fs4,   152(a0) \n\t"
                "fld fs5,   160(a0) \n\t"
                "fld fs6,   168(a0) \n\t"
                "fld fs7,   176(a0) \n\t"
                "fld fs8,   184(a0) \n\t"
                "fld fs9,   192(a0) \n\t"
                "fld fs10,  200(a0) \n\t"
                "fld fs11,  208(a0) \n\t"
                "seqz a0, a1        \n\t"
                "add a0, a0, a1     \n\t"
                "ret                \n\t")

/***********************************************************************
 *           coroutine_create
 */
__ASM_GLOBAL_FUNC( coroutine_create,
                   "mv ra, zero\n\t"
                   "mv sp, a1  \n\t"
                   "jr a2      \n\t" )

#endif  /* linux */
#endif  /* __riscv_xlen == 64 */
