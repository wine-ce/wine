/*
 * Unix interface for qemuthunk
 *
 * Copyright 2022 Fan Wen Jie
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#if 0
#pragma makedep unix
#endif

#include "config.h"

#include <stdarg.h>
#include <sys/types.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>

#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "windef.h"
#include "winternl.h"
#include "winbase.h"
#include "rpc.h"
#include "sspi.h"
#include "ntsecapi.h"
#include "ntsecpkg.h"

#include "wine/debug.h"
#include "wine/rbtree.h"
#include "wine/list.h"
#include "wine/server.h"
#include "wine/unixlib.h"
#include "wine/qemuthunk.h"

#include "unixlib.h"
#include "unix_private.h"

static void QEMU_HOST(NtAcceptConnectPort)(void *$context)
{
    struct NtAcceptConnectPort_params *_ = (typeof(_))$context;
    _->$ret = NtAcceptConnectPort(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtAccessCheck)(void *$context)
{
    struct NtAccessCheck_params *_ = (typeof(_))$context;
    _->$ret = NtAccessCheck(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7);
}

static void QEMU_HOST(NtAccessCheckAndAuditAlarm)(void *$context)
{
    struct NtAccessCheckAndAuditAlarm_params *_ = (typeof(_))$context;
    _->$ret = NtAccessCheckAndAuditAlarm(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9, _->$10);
}

static void QEMU_HOST(NtAddAtom)(void *$context)
{
    struct NtAddAtom_params *_ = (typeof(_))$context;
    _->$ret = NtAddAtom(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtAdjustGroupsToken)(void *$context)
{
    struct NtAdjustGroupsToken_params *_ = (typeof(_))$context;
    _->$ret = NtAdjustGroupsToken(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtAdjustPrivilegesToken)(void *$context)
{
    struct NtAdjustPrivilegesToken_params *_ = (typeof(_))$context;
    _->$ret = NtAdjustPrivilegesToken(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtAlertResumeThread)(void *$context)
{
    struct NtAlertResumeThread_params *_ = (typeof(_))$context;
    _->$ret = NtAlertResumeThread(_->$0, _->$1);
}

static void QEMU_HOST(NtAlertThread)(void *$context)
{
    struct NtAlertThread_params *_ = (typeof(_))$context;
    _->$ret = NtAlertThread(_->ThreadHandle);
}

static void QEMU_HOST(NtAlertThreadByThreadId)(void *$context)
{
    struct NtAlertThreadByThreadId_params *_ = (typeof(_))$context;
    _->$ret = NtAlertThreadByThreadId(_->$0);
}

static void QEMU_HOST(NtAllocateLocallyUniqueId)(void *$context)
{
    struct NtAllocateLocallyUniqueId_params *_ = (typeof(_))$context;
    _->$ret = NtAllocateLocallyUniqueId(_->lpLuid);
}

static void QEMU_HOST(NtAllocateUuids)(void *$context)
{
    struct NtAllocateUuids_params *_ = (typeof(_))$context;
    _->$ret = NtAllocateUuids(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtAllocateVirtualMemory)(void *$context)
{
    struct NtAllocateVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtAllocateVirtualMemory(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtAllocateVirtualMemoryEx)(void *$context)
{
    struct NtAllocateVirtualMemoryEx_params *_ = (typeof(_))$context;
    _->$ret = NtAllocateVirtualMemoryEx(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6);
}

static void QEMU_HOST(NtAreMappedFilesTheSame)(void *$context)
{
    struct NtAreMappedFilesTheSame_params *_ = (typeof(_))$context;
    _->$ret = NtAreMappedFilesTheSame(_->$0, _->$1);
}

static void QEMU_HOST(NtAssignProcessToJobObject)(void *$context)
{
    struct NtAssignProcessToJobObject_params *_ = (typeof(_))$context;
    _->$ret = NtAssignProcessToJobObject(_->$0, _->$1);
}

static void QEMU_HOST(NtCancelIoFile)(void *$context)
{
    struct NtCancelIoFile_params *_ = (typeof(_))$context;
    _->$ret = NtCancelIoFile(_->$0, _->$1);
}

static void QEMU_HOST(NtCancelIoFileEx)(void *$context)
{
    struct NtCancelIoFileEx_params *_ = (typeof(_))$context;
    _->$ret = NtCancelIoFileEx(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtCancelSynchronousIoFile)(void *$context)
{
    struct NtCancelSynchronousIoFile_params *_ = (typeof(_))$context;
    _->$ret = NtCancelSynchronousIoFile(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtCancelTimer)(void *$context)
{
    struct NtCancelTimer_params *_ = (typeof(_))$context;
    _->$ret = NtCancelTimer(_->$0, _->$1);
}

static void QEMU_HOST(NtClearEvent)(void *$context)
{
    struct NtClearEvent_params *_ = (typeof(_))$context;
    _->$ret = NtClearEvent(_->$0);
}

static void QEMU_HOST(NtClose)(void *$context)
{
    struct NtClose_params *_ = (typeof(_))$context;
    _->$ret = NtClose(_->$0);
}

static void QEMU_HOST(NtCommitTransaction)(void *$context)
{
    struct NtCommitTransaction_params *_ = (typeof(_))$context;
    _->$ret = NtCommitTransaction(_->$0, _->$1);
}

static void QEMU_HOST(NtCompareObjects)(void *$context)
{
    struct NtCompareObjects_params *_ = (typeof(_))$context;
    _->$ret = NtCompareObjects(_->$0, _->$1);
}

static void QEMU_HOST(NtCompleteConnectPort)(void *$context)
{
    struct NtCompleteConnectPort_params *_ = (typeof(_))$context;
    _->$ret = NtCompleteConnectPort(_->$0);
}

static void QEMU_HOST(NtConnectPort)(void *$context)
{
    struct NtConnectPort_params *_ = (typeof(_))$context;
    _->$ret = NtConnectPort(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7);
}

static void QEMU_HOST(NtCreateDebugObject)(void *$context)
{
    struct NtCreateDebugObject_params *_ = (typeof(_))$context;
    _->$ret = NtCreateDebugObject(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtCreateDirectoryObject)(void *$context)
{
    struct NtCreateDirectoryObject_params *_ = (typeof(_))$context;
    _->$ret = NtCreateDirectoryObject(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtCreateEvent)(void *$context)
{
    struct NtCreateEvent_params *_ = (typeof(_))$context;
    _->$ret = NtCreateEvent(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtCreateFile)(void *$context)
{
    struct NtCreateFile_params *_ = (typeof(_))$context;
    _->$ret = NtCreateFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9, _->$10);
}

static void QEMU_HOST(NtCreateIoCompletion)(void *$context)
{
    struct NtCreateIoCompletion_params *_ = (typeof(_))$context;
    _->$ret = NtCreateIoCompletion(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtCreateJobObject)(void *$context)
{
    struct NtCreateJobObject_params *_ = (typeof(_))$context;
    _->$ret = NtCreateJobObject(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtCreateKey)(void *$context)
{
    struct NtCreateKey_params *_ = (typeof(_))$context;
    _->$ret = NtCreateKey(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6);
}

static void QEMU_HOST(NtCreateKeyTransacted)(void *$context)
{
    struct NtCreateKeyTransacted_params *_ = (typeof(_))$context;
    _->$ret = NtCreateKeyTransacted(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7);
}

static void QEMU_HOST(NtCreateKeyedEvent)(void *$context)
{
    struct NtCreateKeyedEvent_params *_ = (typeof(_))$context;
    _->$ret = NtCreateKeyedEvent(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtCreateLowBoxToken)(void *$context)
{
    struct NtCreateLowBoxToken_params *_ = (typeof(_))$context;
    _->$ret = NtCreateLowBoxToken(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtCreateMailslotFile)(void *$context)
{
    struct NtCreateMailslotFile_params *_ = (typeof(_))$context;
    _->$ret = NtCreateMailslotFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7);
}

static void QEMU_HOST(NtCreateMutant)(void *$context)
{
    struct NtCreateMutant_params *_ = (typeof(_))$context;
    _->$ret = NtCreateMutant(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtCreateNamedPipeFile)(void *$context)
{
    struct NtCreateNamedPipeFile_params *_ = (typeof(_))$context;
    _->$ret = NtCreateNamedPipeFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9, _->$10, _->$11, _->$12, _->$13);
}

static void QEMU_HOST(NtCreatePagingFile)(void *$context)
{
    struct NtCreatePagingFile_params *_ = (typeof(_))$context;
    _->$ret = NtCreatePagingFile(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtCreatePort)(void *$context)
{
    struct NtCreatePort_params *_ = (typeof(_))$context;
    _->$ret = NtCreatePort(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtCreateSection)(void *$context)
{
    struct NtCreateSection_params *_ = (typeof(_))$context;
    _->$ret = NtCreateSection(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6);
}

static void QEMU_HOST(NtCreateSemaphore)(void *$context)
{
    struct NtCreateSemaphore_params *_ = (typeof(_))$context;
    _->$ret = NtCreateSemaphore(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtCreateSymbolicLinkObject)(void *$context)
{
    struct NtCreateSymbolicLinkObject_params *_ = (typeof(_))$context;
    _->$ret = NtCreateSymbolicLinkObject(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtCreateThreadEx)(void *$context)
{
    struct NtCreateThreadEx_params *_ = (typeof(_))$context;
    _->$ret = NtCreateThreadEx(_->handle, _->access, _->attr, _->process, _->start, _->param, _->flags, _->zero_bits, _->stack_commit, _->stack_reserve, _->attr_list);
}

static void QEMU_HOST(NtCreateTimer)(void *$context)
{
    struct NtCreateTimer_params *_ = (typeof(_))$context;
    _->$ret = NtCreateTimer(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtCreateTransaction)(void *$context)
{
    struct NtCreateTransaction_params *_ = (typeof(_))$context;
    _->$ret = NtCreateTransaction(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9);
}

static void QEMU_HOST(NtCreateUserProcess)(void *$context)
{
    struct NtCreateUserProcess_params *_ = (typeof(_))$context;
    _->$ret = NtCreateUserProcess(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9, _->$10);
}

static void QEMU_HOST(NtCurrentTeb)(void *$context)
{
    struct NtCurrentTeb_params *_ = (typeof(_))$context;
    _->$ret = NtCurrentTeb();
}

static void QEMU_HOST(NtDebugActiveProcess)(void *$context)
{
    struct NtDebugActiveProcess_params *_ = (typeof(_))$context;
    _->$ret = NtDebugActiveProcess(_->$0, _->$1);
}

static void QEMU_HOST(NtDebugContinue)(void *$context)
{
    struct NtDebugContinue_params *_ = (typeof(_))$context;
    _->$ret = NtDebugContinue(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtDelayExecution)(void *$context)
{
    struct NtDelayExecution_params *_ = (typeof(_))$context;
    _->$ret = NtDelayExecution(_->$0, _->$1);
}

static void QEMU_HOST(NtDeleteAtom)(void *$context)
{
    struct NtDeleteAtom_params *_ = (typeof(_))$context;
    _->$ret = NtDeleteAtom(_->$0);
}

static void QEMU_HOST(NtDeleteFile)(void *$context)
{
    struct NtDeleteFile_params *_ = (typeof(_))$context;
    _->$ret = NtDeleteFile(_->$0);
}

static void QEMU_HOST(NtDeleteKey)(void *$context)
{
    struct NtDeleteKey_params *_ = (typeof(_))$context;
    _->$ret = NtDeleteKey(_->$0);
}

static void QEMU_HOST(NtDeleteValueKey)(void *$context)
{
    struct NtDeleteValueKey_params *_ = (typeof(_))$context;
    _->$ret = NtDeleteValueKey(_->$0, _->$1);
}

static void QEMU_HOST(NtDeviceIoControlFile)(void *$context)
{
    struct NtDeviceIoControlFile_params *_ = (typeof(_))$context;
    _->$ret = NtDeviceIoControlFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9);
}

static void QEMU_HOST(NtDisplayString)(void *$context)
{
    struct NtDisplayString_params *_ = (typeof(_))$context;
    _->$ret = NtDisplayString(_->$0);
}

static void QEMU_HOST(NtDuplicateObject)(void *$context)
{
    struct NtDuplicateObject_params *_ = (typeof(_))$context;
    _->$ret = NtDuplicateObject(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6);
}

static void QEMU_HOST(NtDuplicateToken)(void *$context)
{
    struct NtDuplicateToken_params *_ = (typeof(_))$context;
    _->$ret = NtDuplicateToken(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtEnumerateKey)(void *$context)
{
    struct NtEnumerateKey_params *_ = (typeof(_))$context;
    _->$ret = NtEnumerateKey(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtEnumerateValueKey)(void *$context)
{
    struct NtEnumerateValueKey_params *_ = (typeof(_))$context;
    _->$ret = NtEnumerateValueKey(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtFilterToken)(void *$context)
{
    struct NtFilterToken_params *_ = (typeof(_))$context;
    _->$ret = NtFilterToken(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtFindAtom)(void *$context)
{
    struct NtFindAtom_params *_ = (typeof(_))$context;
    _->$ret = NtFindAtom(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtFlushBuffersFile)(void *$context)
{
    struct NtFlushBuffersFile_params *_ = (typeof(_))$context;
    _->$ret = NtFlushBuffersFile(_->$0, _->$1);
}

static void QEMU_HOST(NtFlushInstructionCache)(void *$context)
{
    struct NtFlushInstructionCache_params *_ = (typeof(_))$context;
    _->$ret = NtFlushInstructionCache(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtFlushKey)(void *$context)
{
    struct NtFlushKey_params *_ = (typeof(_))$context;
    _->$ret = NtFlushKey(_->$0);
}

static void QEMU_HOST(NtFlushProcessWriteBuffers)(void *$context)
{
    struct NtFlushProcessWriteBuffers_params *_ = (typeof(_))$context;
    NtFlushProcessWriteBuffers();
}

static void QEMU_HOST(NtFlushVirtualMemory)(void *$context)
{
    struct NtFlushVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtFlushVirtualMemory(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtFreeVirtualMemory)(void *$context)
{
    struct NtFreeVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtFreeVirtualMemory(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtFsControlFile)(void *$context)
{
    struct NtFsControlFile_params *_ = (typeof(_))$context;
    _->$ret = NtFsControlFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9);
}

static void QEMU_HOST(NtGetCurrentProcessorNumber)(void *$context)
{
    struct NtGetCurrentProcessorNumber_params *_ = (typeof(_))$context;
    _->$ret = NtGetCurrentProcessorNumber();
}

static void QEMU_HOST(NtGetNextThread)(void *$context)
{
    struct NtGetNextThread_params *_ = (typeof(_))$context;
    _->$ret = NtGetNextThread(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtGetNlsSectionPtr)(void *$context)
{
    struct NtGetNlsSectionPtr_params *_ = (typeof(_))$context;
    _->$ret = NtGetNlsSectionPtr(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtGetWriteWatch)(void *$context)
{
    struct NtGetWriteWatch_params *_ = (typeof(_))$context;
    _->$ret = NtGetWriteWatch(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6);
}

static void QEMU_HOST(NtImpersonateAnonymousToken)(void *$context)
{
    struct NtImpersonateAnonymousToken_params *_ = (typeof(_))$context;
    _->$ret = NtImpersonateAnonymousToken(_->$0);
}

static void QEMU_HOST(NtInitializeNlsFiles)(void *$context)
{
    struct NtInitializeNlsFiles_params *_ = (typeof(_))$context;
    _->$ret = NtInitializeNlsFiles(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtInitiatePowerAction)(void *$context)
{
    struct NtInitiatePowerAction_params *_ = (typeof(_))$context;
    _->$ret = NtInitiatePowerAction(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtIsProcessInJob)(void *$context)
{
    struct NtIsProcessInJob_params *_ = (typeof(_))$context;
    _->$ret = NtIsProcessInJob(_->$0, _->$1);
}

static void QEMU_HOST(NtListenPort)(void *$context)
{
    struct NtListenPort_params *_ = (typeof(_))$context;
    _->$ret = NtListenPort(_->$0, _->$1);
}

static void QEMU_HOST(NtLoadDriver)(void *$context)
{
    struct NtLoadDriver_params *_ = (typeof(_))$context;
    _->$ret = NtLoadDriver(_->$0);
}

static void QEMU_HOST(NtLoadKey)(void *$context)
{
    struct NtLoadKey_params *_ = (typeof(_))$context;
    _->$ret = NtLoadKey(_->$0, _->$1);
}

static void QEMU_HOST(NtLoadKey2)(void *$context)
{
    struct NtLoadKey2_params *_ = (typeof(_))$context;
    _->$ret = NtLoadKey2(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtLoadKeyEx)(void *$context)
{
    struct NtLoadKeyEx_params *_ = (typeof(_))$context;
    _->$ret = NtLoadKeyEx(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7);
}

static void QEMU_HOST(NtLockFile)(void *$context)
{
    struct NtLockFile_params *_ = (typeof(_))$context;
    _->$ret = NtLockFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9);
}

static void QEMU_HOST(NtLockVirtualMemory)(void *$context)
{
    struct NtLockVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtLockVirtualMemory(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtMakeTemporaryObject)(void *$context)
{
    struct NtMakeTemporaryObject_params *_ = (typeof(_))$context;
    _->$ret = NtMakeTemporaryObject(_->$0);
}

static void QEMU_HOST(NtMapViewOfSection)(void *$context)
{
    struct NtMapViewOfSection_params *_ = (typeof(_))$context;
    _->$ret = NtMapViewOfSection(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9);
}

static void QEMU_HOST(NtMapViewOfSectionEx)(void *$context)
{
    struct NtMapViewOfSectionEx_params *_ = (typeof(_))$context;
    _->$ret = NtMapViewOfSectionEx(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtNotifyChangeDirectoryFile)(void *$context)
{
    struct NtNotifyChangeDirectoryFile_params *_ = (typeof(_))$context;
    _->$ret = NtNotifyChangeDirectoryFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtNotifyChangeKey)(void *$context)
{
    struct NtNotifyChangeKey_params *_ = (typeof(_))$context;
    _->$ret = NtNotifyChangeKey(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9);
}

static void QEMU_HOST(NtNotifyChangeMultipleKeys)(void *$context)
{
    struct NtNotifyChangeMultipleKeys_params *_ = (typeof(_))$context;
    _->$ret = NtNotifyChangeMultipleKeys(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9, _->$10, _->$11);
}

static void QEMU_HOST(NtOpenDirectoryObject)(void *$context)
{
    struct NtOpenDirectoryObject_params *_ = (typeof(_))$context;
    _->$ret = NtOpenDirectoryObject(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenEvent)(void *$context)
{
    struct NtOpenEvent_params *_ = (typeof(_))$context;
    _->$ret = NtOpenEvent(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenFile)(void *$context)
{
    struct NtOpenFile_params *_ = (typeof(_))$context;
    _->$ret = NtOpenFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtOpenIoCompletion)(void *$context)
{
    struct NtOpenIoCompletion_params *_ = (typeof(_))$context;
    _->$ret = NtOpenIoCompletion(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenJobObject)(void *$context)
{
    struct NtOpenJobObject_params *_ = (typeof(_))$context;
    _->$ret = NtOpenJobObject(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenKey)(void *$context)
{
    struct NtOpenKey_params *_ = (typeof(_))$context;
    _->$ret = NtOpenKey(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenKeyEx)(void *$context)
{
    struct NtOpenKeyEx_params *_ = (typeof(_))$context;
    _->$ret = NtOpenKeyEx(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtOpenKeyTransacted)(void *$context)
{
    struct NtOpenKeyTransacted_params *_ = (typeof(_))$context;
    _->$ret = NtOpenKeyTransacted(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtOpenKeyTransactedEx)(void *$context)
{
    struct NtOpenKeyTransactedEx_params *_ = (typeof(_))$context;
    _->$ret = NtOpenKeyTransactedEx(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtOpenKeyedEvent)(void *$context)
{
    struct NtOpenKeyedEvent_params *_ = (typeof(_))$context;
    _->$ret = NtOpenKeyedEvent(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenMutant)(void *$context)
{
    struct NtOpenMutant_params *_ = (typeof(_))$context;
    _->$ret = NtOpenMutant(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenProcess)(void *$context)
{
    struct NtOpenProcess_params *_ = (typeof(_))$context;
    _->$ret = NtOpenProcess(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtOpenProcessToken)(void *$context)
{
    struct NtOpenProcessToken_params *_ = (typeof(_))$context;
    _->$ret = NtOpenProcessToken(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenProcessTokenEx)(void *$context)
{
    struct NtOpenProcessTokenEx_params *_ = (typeof(_))$context;
    _->$ret = NtOpenProcessTokenEx(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtOpenSection)(void *$context)
{
    struct NtOpenSection_params *_ = (typeof(_))$context;
    _->$ret = NtOpenSection(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenSemaphore)(void *$context)
{
    struct NtOpenSemaphore_params *_ = (typeof(_))$context;
    _->$ret = NtOpenSemaphore(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenSymbolicLinkObject)(void *$context)
{
    struct NtOpenSymbolicLinkObject_params *_ = (typeof(_))$context;
    _->$ret = NtOpenSymbolicLinkObject(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtOpenThread)(void *$context)
{
    struct NtOpenThread_params *_ = (typeof(_))$context;
    _->$ret = NtOpenThread(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtOpenThreadToken)(void *$context)
{
    struct NtOpenThreadToken_params *_ = (typeof(_))$context;
    _->$ret = NtOpenThreadToken(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtOpenThreadTokenEx)(void *$context)
{
    struct NtOpenThreadTokenEx_params *_ = (typeof(_))$context;
    _->$ret = NtOpenThreadTokenEx(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtOpenTimer)(void *$context)
{
    struct NtOpenTimer_params *_ = (typeof(_))$context;
    _->$ret = NtOpenTimer(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtPowerInformation)(void *$context)
{
    struct NtPowerInformation_params *_ = (typeof(_))$context;
    _->$ret = NtPowerInformation(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtPrivilegeCheck)(void *$context)
{
    struct NtPrivilegeCheck_params *_ = (typeof(_))$context;
    _->$ret = NtPrivilegeCheck(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtProtectVirtualMemory)(void *$context)
{
    struct NtProtectVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtProtectVirtualMemory(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtPulseEvent)(void *$context)
{
    struct NtPulseEvent_params *_ = (typeof(_))$context;
    _->$ret = NtPulseEvent(_->$0, _->$1);
}

static void QEMU_HOST(NtQueryAttributesFile)(void *$context)
{
    struct NtQueryAttributesFile_params *_ = (typeof(_))$context;
    _->$ret = NtQueryAttributesFile(_->$0, _->$1);
}

static void QEMU_HOST(NtQueryDefaultLocale)(void *$context)
{
    struct NtQueryDefaultLocale_params *_ = (typeof(_))$context;
    _->$ret = NtQueryDefaultLocale(_->$0, _->$1);
}

static void QEMU_HOST(NtQueryDefaultUILanguage)(void *$context)
{
    struct NtQueryDefaultUILanguage_params *_ = (typeof(_))$context;
    _->$ret = NtQueryDefaultUILanguage(_->$0);
}

static void QEMU_HOST(NtQueryDirectoryFile)(void *$context)
{
    struct NtQueryDirectoryFile_params *_ = (typeof(_))$context;
    _->$ret = NtQueryDirectoryFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8, _->$9, _->$10);
}

static void QEMU_HOST(NtQueryDirectoryObject)(void *$context)
{
    struct NtQueryDirectoryObject_params *_ = (typeof(_))$context;
    _->$ret = NtQueryDirectoryObject(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6);
}

static void QEMU_HOST(NtQueryEaFile)(void *$context)
{
    struct NtQueryEaFile_params *_ = (typeof(_))$context;
    _->$ret = NtQueryEaFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtQueryEvent)(void *$context)
{
    struct NtQueryEvent_params *_ = (typeof(_))$context;
    _->$ret = NtQueryEvent(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryFullAttributesFile)(void *$context)
{
    struct NtQueryFullAttributesFile_params *_ = (typeof(_))$context;
    _->$ret = NtQueryFullAttributesFile(_->$0, _->$1);
}

static void QEMU_HOST(NtQueryInformationAtom)(void *$context)
{
    struct NtQueryInformationAtom_params *_ = (typeof(_))$context;
    _->$ret = NtQueryInformationAtom(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryInformationFile)(void *$context)
{
    struct NtQueryInformationFile_params *_ = (typeof(_))$context;
    _->$ret = NtQueryInformationFile(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryInformationJobObject)(void *$context)
{
    struct NtQueryInformationJobObject_params *_ = (typeof(_))$context;
    _->$ret = NtQueryInformationJobObject(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryInformationProcess)(void *$context)
{
    struct NtQueryInformationProcess_params *_ = (typeof(_))$context;
    _->$ret = NtQueryInformationProcess(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryInformationThread)(void *$context)
{
    struct NtQueryInformationThread_params *_ = (typeof(_))$context;
    _->$ret = NtQueryInformationThread(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryInformationToken)(void *$context)
{
    struct NtQueryInformationToken_params *_ = (typeof(_))$context;
    _->$ret = NtQueryInformationToken(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryInstallUILanguage)(void *$context)
{
    struct NtQueryInstallUILanguage_params *_ = (typeof(_))$context;
    _->$ret = NtQueryInstallUILanguage(_->$0);
}

static void QEMU_HOST(NtQueryIoCompletion)(void *$context)
{
    struct NtQueryIoCompletion_params *_ = (typeof(_))$context;
    _->$ret = NtQueryIoCompletion(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryKey)(void *$context)
{
    struct NtQueryKey_params *_ = (typeof(_))$context;
    _->$ret = NtQueryKey(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryLicenseValue)(void *$context)
{
    struct NtQueryLicenseValue_params *_ = (typeof(_))$context;
    _->$ret = NtQueryLicenseValue(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryMultipleValueKey)(void *$context)
{
    struct NtQueryMultipleValueKey_params *_ = (typeof(_))$context;
    _->$ret = NtQueryMultipleValueKey(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtQueryMutant)(void *$context)
{
    struct NtQueryMutant_params *_ = (typeof(_))$context;
    _->$ret = NtQueryMutant(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryObject)(void *$context)
{
    struct NtQueryObject_params *_ = (typeof(_))$context;
    _->$ret = NtQueryObject(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryPerformanceCounter)(void *$context)
{
    struct NtQueryPerformanceCounter_params *_ = (typeof(_))$context;
    _->$ret = NtQueryPerformanceCounter(_->$0, _->$1);
}

static void QEMU_HOST(NtQuerySection)(void *$context)
{
    struct NtQuerySection_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySection(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQuerySecurityObject)(void *$context)
{
    struct NtQuerySecurityObject_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySecurityObject(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQuerySemaphore)(void *$context)
{
    struct NtQuerySemaphore_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySemaphore(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQuerySymbolicLinkObject)(void *$context)
{
    struct NtQuerySymbolicLinkObject_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySymbolicLinkObject(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtQuerySystemEnvironmentValue)(void *$context)
{
    struct NtQuerySystemEnvironmentValue_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySystemEnvironmentValue(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtQuerySystemEnvironmentValueEx)(void *$context)
{
    struct NtQuerySystemEnvironmentValueEx_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySystemEnvironmentValueEx(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQuerySystemInformation)(void *$context)
{
    struct NtQuerySystemInformation_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySystemInformation(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtQuerySystemInformationEx)(void *$context)
{
    struct NtQuerySystemInformationEx_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySystemInformationEx(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtQuerySystemTime)(void *$context)
{
    struct NtQuerySystemTime_params *_ = (typeof(_))$context;
    _->$ret = NtQuerySystemTime(_->$0);
}

static void QEMU_HOST(NtQueryTimer)(void *$context)
{
    struct NtQueryTimer_params *_ = (typeof(_))$context;
    _->$ret = NtQueryTimer(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueryTimerResolution)(void *$context)
{
    struct NtQueryTimerResolution_params *_ = (typeof(_))$context;
    _->$ret = NtQueryTimerResolution(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtQueryValueKey)(void *$context)
{
    struct NtQueryValueKey_params *_ = (typeof(_))$context;
    _->$ret = NtQueryValueKey(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtQueryVirtualMemory)(void *$context)
{
    struct NtQueryVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtQueryVirtualMemory(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtQueryVolumeInformationFile)(void *$context)
{
    struct NtQueryVolumeInformationFile_params *_ = (typeof(_))$context;
    _->$ret = NtQueryVolumeInformationFile(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtQueueApcThread)(void *$context)
{
    struct NtQueueApcThread_params *_ = (typeof(_))$context;
    _->$ret = NtQueueApcThread(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtRaiseHardError)(void *$context)
{
    struct NtRaiseHardError_params *_ = (typeof(_))$context;
    _->$ret = NtRaiseHardError(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtReadFile)(void *$context)
{
    struct NtReadFile_params *_ = (typeof(_))$context;
    _->$ret = NtReadFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtReadFileScatter)(void *$context)
{
    struct NtReadFileScatter_params *_ = (typeof(_))$context;
    _->$ret = NtReadFileScatter(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtReadVirtualMemory)(void *$context)
{
    struct NtReadVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtReadVirtualMemory(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtRegisterThreadTerminatePort)(void *$context)
{
    struct NtRegisterThreadTerminatePort_params *_ = (typeof(_))$context;
    _->$ret = NtRegisterThreadTerminatePort(_->$0);
}

static void QEMU_HOST(NtReleaseKeyedEvent)(void *$context)
{
    struct NtReleaseKeyedEvent_params *_ = (typeof(_))$context;
    _->$ret = NtReleaseKeyedEvent(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtReleaseMutant)(void *$context)
{
    struct NtReleaseMutant_params *_ = (typeof(_))$context;
    _->$ret = NtReleaseMutant(_->$0, _->$1);
}

static void QEMU_HOST(NtReleaseSemaphore)(void *$context)
{
    struct NtReleaseSemaphore_params *_ = (typeof(_))$context;
    _->$ret = NtReleaseSemaphore(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtRemoveIoCompletion)(void *$context)
{
    struct NtRemoveIoCompletion_params *_ = (typeof(_))$context;
    _->$ret = NtRemoveIoCompletion(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtRemoveIoCompletionEx)(void *$context)
{
    struct NtRemoveIoCompletionEx_params *_ = (typeof(_))$context;
    _->$ret = NtRemoveIoCompletionEx(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtRemoveProcessDebug)(void *$context)
{
    struct NtRemoveProcessDebug_params *_ = (typeof(_))$context;
    _->$ret = NtRemoveProcessDebug(_->$0, _->$1);
}

static void QEMU_HOST(NtRenameKey)(void *$context)
{
    struct NtRenameKey_params *_ = (typeof(_))$context;
    _->$ret = NtRenameKey(_->$0, _->$1);
}

static void QEMU_HOST(NtReplaceKey)(void *$context)
{
    struct NtReplaceKey_params *_ = (typeof(_))$context;
    _->$ret = NtReplaceKey(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtReplyWaitReceivePort)(void *$context)
{
    struct NtReplyWaitReceivePort_params *_ = (typeof(_))$context;
    _->$ret = NtReplyWaitReceivePort(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtRequestWaitReplyPort)(void *$context)
{
    struct NtRequestWaitReplyPort_params *_ = (typeof(_))$context;
    _->$ret = NtRequestWaitReplyPort(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtResetEvent)(void *$context)
{
    struct NtResetEvent_params *_ = (typeof(_))$context;
    _->$ret = NtResetEvent(_->$0, _->$1);
}

static void QEMU_HOST(NtResetWriteWatch)(void *$context)
{
    struct NtResetWriteWatch_params *_ = (typeof(_))$context;
    _->$ret = NtResetWriteWatch(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtRestoreKey)(void *$context)
{
    struct NtRestoreKey_params *_ = (typeof(_))$context;
    _->$ret = NtRestoreKey(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtResumeProcess)(void *$context)
{
    struct NtResumeProcess_params *_ = (typeof(_))$context;
    _->$ret = NtResumeProcess(_->$0);
}

static void QEMU_HOST(NtResumeThread)(void *$context)
{
    struct NtResumeThread_params *_ = (typeof(_))$context;
    _->$ret = NtResumeThread(_->$0, _->$1);
}

static void QEMU_HOST(NtRollbackTransaction)(void *$context)
{
    struct NtRollbackTransaction_params *_ = (typeof(_))$context;
    _->$ret = NtRollbackTransaction(_->$0, _->$1);
}

static void QEMU_HOST(NtSaveKey)(void *$context)
{
    struct NtSaveKey_params *_ = (typeof(_))$context;
    _->$ret = NtSaveKey(_->$0, _->$1);
}

static void QEMU_HOST(NtSecureConnectPort)(void *$context)
{
    struct NtSecureConnectPort_params *_ = (typeof(_))$context;
    _->$ret = NtSecureConnectPort(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtSetDebugFilterState)(void *$context)
{
    struct NtSetDebugFilterState_params *_ = (typeof(_))$context;
    _->$ret = NtSetDebugFilterState(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtSetDefaultLocale)(void *$context)
{
    struct NtSetDefaultLocale_params *_ = (typeof(_))$context;
    _->$ret = NtSetDefaultLocale(_->$0, _->$1);
}

static void QEMU_HOST(NtSetDefaultUILanguage)(void *$context)
{
    struct NtSetDefaultUILanguage_params *_ = (typeof(_))$context;
    _->$ret = NtSetDefaultUILanguage(_->$0);
}

static void QEMU_HOST(NtSetEaFile)(void *$context)
{
    struct NtSetEaFile_params *_ = (typeof(_))$context;
    _->$ret = NtSetEaFile(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSetEvent)(void *$context)
{
    struct NtSetEvent_params *_ = (typeof(_))$context;
    _->$ret = NtSetEvent(_->$0, _->$1);
}

static void QEMU_HOST(NtSetInformationDebugObject)(void *$context)
{
    struct NtSetInformationDebugObject_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationDebugObject(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtSetInformationFile)(void *$context)
{
    struct NtSetInformationFile_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationFile(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtSetInformationJobObject)(void *$context)
{
    struct NtSetInformationJobObject_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationJobObject(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSetInformationKey)(void *$context)
{
    struct NtSetInformationKey_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationKey(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSetInformationObject)(void *$context)
{
    struct NtSetInformationObject_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationObject(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSetInformationProcess)(void *$context)
{
    struct NtSetInformationProcess_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationProcess(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSetInformationThread)(void *$context)
{
    struct NtSetInformationThread_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationThread(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSetInformationToken)(void *$context)
{
    struct NtSetInformationToken_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationToken(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSetInformationVirtualMemory)(void *$context)
{
    struct NtSetInformationVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtSetInformationVirtualMemory(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtSetIntervalProfile)(void *$context)
{
    struct NtSetIntervalProfile_params *_ = (typeof(_))$context;
    _->$ret = NtSetIntervalProfile(_->$0, _->$1);
}

static void QEMU_HOST(NtSetIoCompletion)(void *$context)
{
    struct NtSetIoCompletion_params *_ = (typeof(_))$context;
    _->$ret = NtSetIoCompletion(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtSetSecurityObject)(void *$context)
{
    struct NtSetSecurityObject_params *_ = (typeof(_))$context;
    _->$ret = NtSetSecurityObject(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtSetSystemInformation)(void *$context)
{
    struct NtSetSystemInformation_params *_ = (typeof(_))$context;
    _->$ret = NtSetSystemInformation(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtSetSystemTime)(void *$context)
{
    struct NtSetSystemTime_params *_ = (typeof(_))$context;
    _->$ret = NtSetSystemTime(_->$0, _->$1);
}

static void QEMU_HOST(NtSetTimer)(void *$context)
{
    struct NtSetTimer_params *_ = (typeof(_))$context;
    _->$ret = NtSetTimer(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6);
}

static void QEMU_HOST(NtSetTimerResolution)(void *$context)
{
    struct NtSetTimerResolution_params *_ = (typeof(_))$context;
    _->$ret = NtSetTimerResolution(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtSetValueKey)(void *$context)
{
    struct NtSetValueKey_params *_ = (typeof(_))$context;
    _->$ret = NtSetValueKey(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtSetVolumeInformationFile)(void *$context)
{
    struct NtSetVolumeInformationFile_params *_ = (typeof(_))$context;
    _->$ret = NtSetVolumeInformationFile(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtShutdownSystem)(void *$context)
{
    struct NtShutdownSystem_params *_ = (typeof(_))$context;
    _->$ret = NtShutdownSystem(_->$0);
}

static void QEMU_HOST(NtSignalAndWaitForSingleObject)(void *$context)
{
    struct NtSignalAndWaitForSingleObject_params *_ = (typeof(_))$context;
    _->$ret = NtSignalAndWaitForSingleObject(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtSuspendProcess)(void *$context)
{
    struct NtSuspendProcess_params *_ = (typeof(_))$context;
    _->$ret = NtSuspendProcess(_->$0);
}

static void QEMU_HOST(NtSuspendThread)(void *$context)
{
    struct NtSuspendThread_params *_ = (typeof(_))$context;
    _->$ret = NtSuspendThread(_->$0, _->$1);
}

static void QEMU_HOST(NtSystemDebugControl)(void *$context)
{
    struct NtSystemDebugControl_params *_ = (typeof(_))$context;
    _->$ret = NtSystemDebugControl(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtTerminateJobObject)(void *$context)
{
    struct NtTerminateJobObject_params *_ = (typeof(_))$context;
    _->$ret = NtTerminateJobObject(_->$0, _->$1);
}

static void QEMU_HOST(NtTestAlert)(void *$context)
{
    struct NtTestAlert_params *_ = (typeof(_))$context;
    _->$ret = NtTestAlert();
}

static void QEMU_HOST(NtTraceControl)(void *$context)
{
    struct NtTraceControl_params *_ = (typeof(_))$context;
    _->$ret = NtTraceControl(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtUnloadDriver)(void *$context)
{
    struct NtUnloadDriver_params *_ = (typeof(_))$context;
    _->$ret = NtUnloadDriver(_->$0);
}

static void QEMU_HOST(NtUnloadKey)(void *$context)
{
    struct NtUnloadKey_params *_ = (typeof(_))$context;
    _->$ret = NtUnloadKey(_->$0);
}

static void QEMU_HOST(NtUnlockFile)(void *$context)
{
    struct NtUnlockFile_params *_ = (typeof(_))$context;
    _->$ret = NtUnlockFile(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtUnlockVirtualMemory)(void *$context)
{
    struct NtUnlockVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtUnlockVirtualMemory(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtUnmapViewOfSection)(void *$context)
{
    struct NtUnmapViewOfSection_params *_ = (typeof(_))$context;
    _->$ret = NtUnmapViewOfSection(_->$0, _->$1);
}

static void QEMU_HOST(NtUnmapViewOfSectionEx)(void *$context)
{
    struct NtUnmapViewOfSectionEx_params *_ = (typeof(_))$context;
    _->$ret = NtUnmapViewOfSectionEx(_->$0, _->$1, _->$2);
}

static void QEMU_HOST(NtWaitForAlertByThreadId)(void *$context)
{
    struct NtWaitForAlertByThreadId_params *_ = (typeof(_))$context;
    _->$ret = NtWaitForAlertByThreadId(_->$0, _->$1);
}

static void QEMU_HOST(NtWaitForDebugEvent)(void *$context)
{
    struct NtWaitForDebugEvent_params *_ = (typeof(_))$context;
    _->$ret = NtWaitForDebugEvent(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtWaitForKeyedEvent)(void *$context)
{
    struct NtWaitForKeyedEvent_params *_ = (typeof(_))$context;
    _->$ret = NtWaitForKeyedEvent(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtWaitForMultipleObjects)(void *$context)
{
    struct NtWaitForMultipleObjects_params *_ = (typeof(_))$context;
    _->$ret = NtWaitForMultipleObjects(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtWaitForSingleObject)(void *$context)
{
    struct NtWaitForSingleObject_params *_ = (typeof(_))$context;
    _->$ret = NtWaitForSingleObject(_->$0, _->$1, _->$2);
}

#ifndef _WIN64

static void QEMU_HOST(NtWow64AllocateVirtualMemory64)(void *$context)
{
    struct NtWow64AllocateVirtualMemory64_params *_ = (typeof(_))$context;
    _->$ret = NtWow64AllocateVirtualMemory64(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5);
}

static void QEMU_HOST(NtWow64GetNativeSystemInformation)(void *$context)
{
    struct NtWow64GetNativeSystemInformation_params *_ = (typeof(_))$context;
    _->$ret = NtWow64GetNativeSystemInformation(_->$0, _->$1, _->$2, _->$3);
}

static void QEMU_HOST(NtWow64ReadVirtualMemory64)(void *$context)
{
    struct NtWow64ReadVirtualMemory64_params *_ = (typeof(_))$context;
    _->$ret = NtWow64ReadVirtualMemory64(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtWow64WriteVirtualMemory64)(void *$context)
{
    struct NtWow64WriteVirtualMemory64_params *_ = (typeof(_))$context;
    _->$ret = NtWow64WriteVirtualMemory64(_->$0, _->$1, _->$2, _->$3, _->$4);
}

#endif

static void QEMU_HOST(NtWriteFile)(void *$context)
{
    struct NtWriteFile_params *_ = (typeof(_))$context;
    _->$ret = NtWriteFile(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtWriteFileGather)(void *$context)
{
    struct NtWriteFileGather_params *_ = (typeof(_))$context;
    _->$ret = NtWriteFileGather(_->$0, _->$1, _->$2, _->$3, _->$4, _->$5, _->$6, _->$7, _->$8);
}

static void QEMU_HOST(NtWriteVirtualMemory)(void *$context)
{
    struct NtWriteVirtualMemory_params *_ = (typeof(_))$context;
    _->$ret = NtWriteVirtualMemory(_->$0, _->$1, _->$2, _->$3, _->$4);
}

static void QEMU_HOST(NtYieldExecution)(void *$context)
{
    struct NtYieldExecution_params *_ = (typeof(_))$context;
    _->$ret = NtYieldExecution();
}

static void QEMU_HOST(__qemu_host_dispatcher)(void *$context)
{
    struct __qemu_host_dispatcher_params *_ = (typeof(_))$context;
    _->$ret = __qemu_host_dispatcher(_->state, _->data);
}

static void QEMU_HOST(__qemu_host_init)(void *$context)
{
    struct __qemu_host_init_params *_ = (typeof(_))$context;
    __qemu_host_init(_->sp);
}

static void QEMU_HOST(__qemu_switch_host_state)(void *$context)
{
    struct __qemu_switch_host_state_params *_ = (typeof(_))$context;
    __qemu_switch_host_state(_->user_data);
}

static void QEMU_HOST(__wine_dbg_header)(void *$context)
{
    struct __wine_dbg_header_params *_ = (typeof(_))$context;
    _->$ret = __wine_dbg_header(_->cls, _->channel, _->function);
}

static void QEMU_HOST(__wine_dbg_output)(void *$context)
{
    struct __wine_dbg_output_params *_ = (typeof(_))$context;
    _->$ret = __wine_dbg_output(_->str);
}

static void QEMU_HOST(__wine_dbg_strdup)(void *$context)
{
    struct __wine_dbg_strdup_params *_ = (typeof(_))$context;
    _->$ret = __wine_dbg_strdup(_->str);
}

static void QEMU_HOST(__wine_unix_call)(void *$context)
{
    struct __wine_unix_call_params *_ = (typeof(_))$context;
    _->$ret = __wine_unix_call(_->args, _->func);
}

static void QEMU_HOST(get_thread_context)(void *$context)
{
    struct get_thread_context_params *_ = (typeof(_))$context;
    _->$ret = get_thread_context(_->handle, _->context, _->self, _->machine);
}

static void QEMU_HOST(process_close_server_socket)(void *$context)
{
    struct process_close_server_socket_params *_ = (typeof(_))$context;
    process_close_server_socket();
}

static void QEMU_HOST(virtual_free_teb)(void *$context)
{
    struct virtual_free_teb_params *_ = (typeof(_))$context;
    virtual_free_teb(_->teb);
}

static void QEMU_HOST(send_debug_event)(void *$context)
{
    struct send_debug_event_params *_ = (typeof(_))$context;
    _->$ret = send_debug_event(_->rec, _->context, _->first_chance);
}

static void QEMU_HOST(server_init_thread)(void *$context)
{
    struct server_init_thread_params *_ = (typeof(_))$context;
    server_init_thread(_->entry_point, _->suspend);
}

static void QEMU_HOST(server_pipe)(void *$context)
{
    struct server_pipe_params *_ = (typeof(_))$context;
    _->$ret = server_pipe(_->fd);
}

static void QEMU_HOST(server_select)(void *$context)
{
    struct server_select_params *_ = (typeof(_))$context;
    _->$ret = server_select(_->select_op, _->size, _->flags, _->abs_timeout, _->context, _->user_apc);
}

static void QEMU_HOST(set_thread_context)(void *$context)
{
    struct set_thread_context_params *_ = (typeof(_))$context;
    _->$ret = set_thread_context(_->handle, _->context, _->self, _->machine);
}

static void QEMU_HOST(virtual_handle_fault)(void *$context)
{
    struct virtual_handle_fault_params *_ = (typeof(_))$context;
    _->$ret = virtual_handle_fault(_->addr, _->err, _->stack);
}

static void QEMU_HOST(virtual_is_valid_code_address)(void *$context)
{
    struct virtual_is_valid_code_address_params *_ = (typeof(_))$context;
    _->$ret = virtual_is_valid_code_address(_->addr, _->size);
}

static void QEMU_HOST(virtual_setup_exception)(void *$context)
{
    struct virtual_setup_exception_params *_ = (typeof(_))$context;
    _->$ret = virtual_setup_exception(_->stack_ptr, _->size, _->rec);
}

static void QEMU_HOST(virtual_uninterrupted_read_memory)(void *$context)
{
    struct virtual_uninterrupted_read_memory_params *_ = (typeof(_))$context;
    _->$ret = virtual_uninterrupted_read_memory(_->addr, _->buffer, _->size);
}

static void QEMU_HOST(virtual_uninterrupted_write_memory)(void *$context)
{
    struct virtual_uninterrupted_write_memory_params *_ = (typeof(_))$context;
    _->$ret = virtual_uninterrupted_write_memory(_->addr, _->buffer, _->size);
}

static void QEMU_HOST(wait_suspend)(void *$context)
{
    struct wait_suspend_params *_ = (typeof(_))$context;
    wait_suspend(_->context);
}

static void QEMU_HOST(wine_nt_to_unix_file_name)(void *$context)
{
    struct wine_nt_to_unix_file_name_params *_ = (typeof(_))$context;
    _->$ret = wine_nt_to_unix_file_name(_->attr, _->nameA, _->size, _->disposition);
}

static void QEMU_HOST(wine_server_call)(void *$context)
{
    struct wine_server_call_params *_ = (typeof(_))$context;
    _->$ret = wine_server_call(_->req_ptr);
}

static void QEMU_HOST(wine_server_send_fd)(void *$context)
{
    struct wine_server_send_fd_params *_ = (typeof(_))$context;
    wine_server_send_fd(_->fd);
}

static void QEMU_HOST(wine_unix_to_nt_file_name)(void *$context)
{
    struct wine_unix_to_nt_file_name_params *_ = (typeof(_))$context;
    _->$ret = wine_unix_to_nt_file_name(_->name, _->buffer, _->size);
}

qemu_host_vars_t qemu_host_vars =
{
    QEMU_HOST_REGISTER_METHOD(NtAcceptConnectPort),
    QEMU_HOST_REGISTER_METHOD(NtAccessCheck),
    QEMU_HOST_REGISTER_METHOD(NtAccessCheckAndAuditAlarm),
    QEMU_HOST_REGISTER_METHOD(NtAddAtom),
    QEMU_HOST_REGISTER_METHOD(NtAdjustGroupsToken),
    QEMU_HOST_REGISTER_METHOD(NtAdjustPrivilegesToken),
    QEMU_HOST_REGISTER_METHOD(NtAlertResumeThread),
    QEMU_HOST_REGISTER_METHOD(NtAlertThread),
    QEMU_HOST_REGISTER_METHOD(NtAlertThreadByThreadId),
    QEMU_HOST_REGISTER_METHOD(NtAllocateLocallyUniqueId),
    QEMU_HOST_REGISTER_METHOD(NtAllocateUuids),
    QEMU_HOST_REGISTER_METHOD(NtAllocateVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtAllocateVirtualMemoryEx),
    QEMU_HOST_REGISTER_METHOD(NtAreMappedFilesTheSame),
    QEMU_HOST_REGISTER_METHOD(NtAssignProcessToJobObject),
    QEMU_HOST_REGISTER_METHOD(NtCancelIoFile),
    QEMU_HOST_REGISTER_METHOD(NtCancelIoFileEx),
    QEMU_HOST_REGISTER_METHOD(NtCancelSynchronousIoFile),
    QEMU_HOST_REGISTER_METHOD(NtCancelTimer),
    QEMU_HOST_REGISTER_METHOD(NtClearEvent),
    QEMU_HOST_REGISTER_METHOD(NtClose),
    QEMU_HOST_REGISTER_METHOD(NtCommitTransaction),
    QEMU_HOST_REGISTER_METHOD(NtCompareObjects),
    QEMU_HOST_REGISTER_METHOD(NtCompleteConnectPort),
    QEMU_HOST_REGISTER_METHOD(NtConnectPort),
    QEMU_HOST_REGISTER_METHOD(NtCreateDebugObject),
    QEMU_HOST_REGISTER_METHOD(NtCreateDirectoryObject),
    QEMU_HOST_REGISTER_METHOD(NtCreateEvent),
    QEMU_HOST_REGISTER_METHOD(NtCreateFile),
    QEMU_HOST_REGISTER_METHOD(NtCreateIoCompletion),
    QEMU_HOST_REGISTER_METHOD(NtCreateJobObject),
    QEMU_HOST_REGISTER_METHOD(NtCreateKey),
    QEMU_HOST_REGISTER_METHOD(NtCreateKeyTransacted),
    QEMU_HOST_REGISTER_METHOD(NtCreateKeyedEvent),
    QEMU_HOST_REGISTER_METHOD(NtCreateLowBoxToken),
    QEMU_HOST_REGISTER_METHOD(NtCreateMailslotFile),
    QEMU_HOST_REGISTER_METHOD(NtCreateMutant),
    QEMU_HOST_REGISTER_METHOD(NtCreateNamedPipeFile),
    QEMU_HOST_REGISTER_METHOD(NtCreatePagingFile),
    QEMU_HOST_REGISTER_METHOD(NtCreatePort),
    QEMU_HOST_REGISTER_METHOD(NtCreateSection),
    QEMU_HOST_REGISTER_METHOD(NtCreateSemaphore),
    QEMU_HOST_REGISTER_METHOD(NtCreateSymbolicLinkObject),
    QEMU_HOST_REGISTER_METHOD(NtCreateThreadEx),
    QEMU_HOST_REGISTER_METHOD(NtCreateTimer),
    QEMU_HOST_REGISTER_METHOD(NtCreateTransaction),
    QEMU_HOST_REGISTER_METHOD(NtCreateUserProcess),
    QEMU_HOST_REGISTER_METHOD(NtCurrentTeb),
    QEMU_HOST_REGISTER_METHOD(NtDebugActiveProcess),
    QEMU_HOST_REGISTER_METHOD(NtDebugContinue),
    QEMU_HOST_REGISTER_METHOD(NtDelayExecution),
    QEMU_HOST_REGISTER_METHOD(NtDeleteAtom),
    QEMU_HOST_REGISTER_METHOD(NtDeleteFile),
    QEMU_HOST_REGISTER_METHOD(NtDeleteKey),
    QEMU_HOST_REGISTER_METHOD(NtDeleteValueKey),
    QEMU_HOST_REGISTER_METHOD(NtDeviceIoControlFile),
    QEMU_HOST_REGISTER_METHOD(NtDisplayString),
    QEMU_HOST_REGISTER_METHOD(NtDuplicateObject),
    QEMU_HOST_REGISTER_METHOD(NtDuplicateToken),
    QEMU_HOST_REGISTER_METHOD(NtEnumerateKey),
    QEMU_HOST_REGISTER_METHOD(NtEnumerateValueKey),
    QEMU_HOST_REGISTER_METHOD(NtFilterToken),
    QEMU_HOST_REGISTER_METHOD(NtFindAtom),
    QEMU_HOST_REGISTER_METHOD(NtFlushBuffersFile),
    QEMU_HOST_REGISTER_METHOD(NtFlushInstructionCache),
    QEMU_HOST_REGISTER_METHOD(NtFlushKey),
    QEMU_HOST_REGISTER_METHOD(NtFlushProcessWriteBuffers),
    QEMU_HOST_REGISTER_METHOD(NtFlushVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtFreeVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtFsControlFile),
    QEMU_HOST_REGISTER_METHOD(NtGetCurrentProcessorNumber),
    QEMU_HOST_REGISTER_METHOD(NtGetNextThread),
    QEMU_HOST_REGISTER_METHOD(NtGetNlsSectionPtr),
    QEMU_HOST_REGISTER_METHOD(NtGetWriteWatch),
    QEMU_HOST_REGISTER_METHOD(NtImpersonateAnonymousToken),
    QEMU_HOST_REGISTER_METHOD(NtInitializeNlsFiles),
    QEMU_HOST_REGISTER_METHOD(NtInitiatePowerAction),
    QEMU_HOST_REGISTER_METHOD(NtIsProcessInJob),
    QEMU_HOST_REGISTER_METHOD(NtListenPort),
    QEMU_HOST_REGISTER_METHOD(NtLoadDriver),
    QEMU_HOST_REGISTER_METHOD(NtLoadKey),
    QEMU_HOST_REGISTER_METHOD(NtLoadKey2),
    QEMU_HOST_REGISTER_METHOD(NtLoadKeyEx),
    QEMU_HOST_REGISTER_METHOD(NtLockFile),
    QEMU_HOST_REGISTER_METHOD(NtLockVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtMakeTemporaryObject),
    QEMU_HOST_REGISTER_METHOD(NtMapViewOfSection),
    QEMU_HOST_REGISTER_METHOD(NtMapViewOfSectionEx),
    QEMU_HOST_REGISTER_METHOD(NtNotifyChangeDirectoryFile),
    QEMU_HOST_REGISTER_METHOD(NtNotifyChangeKey),
    QEMU_HOST_REGISTER_METHOD(NtNotifyChangeMultipleKeys),
    QEMU_HOST_REGISTER_METHOD(NtOpenDirectoryObject),
    QEMU_HOST_REGISTER_METHOD(NtOpenEvent),
    QEMU_HOST_REGISTER_METHOD(NtOpenFile),
    QEMU_HOST_REGISTER_METHOD(NtOpenIoCompletion),
    QEMU_HOST_REGISTER_METHOD(NtOpenJobObject),
    QEMU_HOST_REGISTER_METHOD(NtOpenKey),
    QEMU_HOST_REGISTER_METHOD(NtOpenKeyEx),
    QEMU_HOST_REGISTER_METHOD(NtOpenKeyTransacted),
    QEMU_HOST_REGISTER_METHOD(NtOpenKeyTransactedEx),
    QEMU_HOST_REGISTER_METHOD(NtOpenKeyedEvent),
    QEMU_HOST_REGISTER_METHOD(NtOpenMutant),
    QEMU_HOST_REGISTER_METHOD(NtOpenProcess),
    QEMU_HOST_REGISTER_METHOD(NtOpenProcessToken),
    QEMU_HOST_REGISTER_METHOD(NtOpenProcessTokenEx),
    QEMU_HOST_REGISTER_METHOD(NtOpenSection),
    QEMU_HOST_REGISTER_METHOD(NtOpenSemaphore),
    QEMU_HOST_REGISTER_METHOD(NtOpenSymbolicLinkObject),
    QEMU_HOST_REGISTER_METHOD(NtOpenThread),
    QEMU_HOST_REGISTER_METHOD(NtOpenThreadToken),
    QEMU_HOST_REGISTER_METHOD(NtOpenThreadTokenEx),
    QEMU_HOST_REGISTER_METHOD(NtOpenTimer),
    QEMU_HOST_REGISTER_METHOD(NtPowerInformation),
    QEMU_HOST_REGISTER_METHOD(NtPrivilegeCheck),
    QEMU_HOST_REGISTER_METHOD(NtProtectVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtPulseEvent),
    QEMU_HOST_REGISTER_METHOD(NtQueryAttributesFile),
    QEMU_HOST_REGISTER_METHOD(NtQueryDefaultLocale),
    QEMU_HOST_REGISTER_METHOD(NtQueryDefaultUILanguage),
    QEMU_HOST_REGISTER_METHOD(NtQueryDirectoryFile),
    QEMU_HOST_REGISTER_METHOD(NtQueryDirectoryObject),
    QEMU_HOST_REGISTER_METHOD(NtQueryEaFile),
    QEMU_HOST_REGISTER_METHOD(NtQueryEvent),
    QEMU_HOST_REGISTER_METHOD(NtQueryFullAttributesFile),
    QEMU_HOST_REGISTER_METHOD(NtQueryInformationAtom),
    QEMU_HOST_REGISTER_METHOD(NtQueryInformationFile),
    QEMU_HOST_REGISTER_METHOD(NtQueryInformationJobObject),
    QEMU_HOST_REGISTER_METHOD(NtQueryInformationProcess),
    QEMU_HOST_REGISTER_METHOD(NtQueryInformationThread),
    QEMU_HOST_REGISTER_METHOD(NtQueryInformationToken),
    QEMU_HOST_REGISTER_METHOD(NtQueryInstallUILanguage),
    QEMU_HOST_REGISTER_METHOD(NtQueryIoCompletion),
    QEMU_HOST_REGISTER_METHOD(NtQueryKey),
    QEMU_HOST_REGISTER_METHOD(NtQueryLicenseValue),
    QEMU_HOST_REGISTER_METHOD(NtQueryMultipleValueKey),
    QEMU_HOST_REGISTER_METHOD(NtQueryMutant),
    QEMU_HOST_REGISTER_METHOD(NtQueryObject),
    QEMU_HOST_REGISTER_METHOD(NtQueryPerformanceCounter),
    QEMU_HOST_REGISTER_METHOD(NtQuerySection),
    QEMU_HOST_REGISTER_METHOD(NtQuerySecurityObject),
    QEMU_HOST_REGISTER_METHOD(NtQuerySemaphore),
    QEMU_HOST_REGISTER_METHOD(NtQuerySymbolicLinkObject),
    QEMU_HOST_REGISTER_METHOD(NtQuerySystemEnvironmentValue),
    QEMU_HOST_REGISTER_METHOD(NtQuerySystemEnvironmentValueEx),
    QEMU_HOST_REGISTER_METHOD(NtQuerySystemInformation),
    QEMU_HOST_REGISTER_METHOD(NtQuerySystemInformationEx),
    QEMU_HOST_REGISTER_METHOD(NtQuerySystemTime),
    QEMU_HOST_REGISTER_METHOD(NtQueryTimer),
    QEMU_HOST_REGISTER_METHOD(NtQueryTimerResolution),
    QEMU_HOST_REGISTER_METHOD(NtQueryValueKey),
    QEMU_HOST_REGISTER_METHOD(NtQueryVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtQueryVolumeInformationFile),
    QEMU_HOST_REGISTER_METHOD(NtQueueApcThread),
    QEMU_HOST_REGISTER_METHOD(NtRaiseHardError),
    QEMU_HOST_REGISTER_METHOD(NtReadFile),
    QEMU_HOST_REGISTER_METHOD(NtReadFileScatter),
    QEMU_HOST_REGISTER_METHOD(NtReadVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtRegisterThreadTerminatePort),
    QEMU_HOST_REGISTER_METHOD(NtReleaseKeyedEvent),
    QEMU_HOST_REGISTER_METHOD(NtReleaseMutant),
    QEMU_HOST_REGISTER_METHOD(NtReleaseSemaphore),
    QEMU_HOST_REGISTER_METHOD(NtRemoveIoCompletion),
    QEMU_HOST_REGISTER_METHOD(NtRemoveIoCompletionEx),
    QEMU_HOST_REGISTER_METHOD(NtRemoveProcessDebug),
    QEMU_HOST_REGISTER_METHOD(NtRenameKey),
    QEMU_HOST_REGISTER_METHOD(NtReplaceKey),
    QEMU_HOST_REGISTER_METHOD(NtReplyWaitReceivePort),
    QEMU_HOST_REGISTER_METHOD(NtRequestWaitReplyPort),
    QEMU_HOST_REGISTER_METHOD(NtResetEvent),
    QEMU_HOST_REGISTER_METHOD(NtResetWriteWatch),
    QEMU_HOST_REGISTER_METHOD(NtRestoreKey),
    QEMU_HOST_REGISTER_METHOD(NtResumeProcess),
    QEMU_HOST_REGISTER_METHOD(NtResumeThread),
    QEMU_HOST_REGISTER_METHOD(NtRollbackTransaction),
    QEMU_HOST_REGISTER_METHOD(NtSaveKey),
    QEMU_HOST_REGISTER_METHOD(NtSecureConnectPort),
    QEMU_HOST_REGISTER_METHOD(NtSetDebugFilterState),
    QEMU_HOST_REGISTER_METHOD(NtSetDefaultLocale),
    QEMU_HOST_REGISTER_METHOD(NtSetDefaultUILanguage),
    QEMU_HOST_REGISTER_METHOD(NtSetEaFile),
    QEMU_HOST_REGISTER_METHOD(NtSetEvent),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationDebugObject),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationFile),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationJobObject),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationKey),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationObject),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationProcess),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationThread),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationToken),
    QEMU_HOST_REGISTER_METHOD(NtSetInformationVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtSetIntervalProfile),
    QEMU_HOST_REGISTER_METHOD(NtSetIoCompletion),
    QEMU_HOST_REGISTER_METHOD(NtSetSecurityObject),
    QEMU_HOST_REGISTER_METHOD(NtSetSystemInformation),
    QEMU_HOST_REGISTER_METHOD(NtSetSystemTime),
    QEMU_HOST_REGISTER_METHOD(NtSetTimer),
    QEMU_HOST_REGISTER_METHOD(NtSetTimerResolution),
    QEMU_HOST_REGISTER_METHOD(NtSetValueKey),
    QEMU_HOST_REGISTER_METHOD(NtSetVolumeInformationFile),
    QEMU_HOST_REGISTER_METHOD(NtShutdownSystem),
    QEMU_HOST_REGISTER_METHOD(NtSignalAndWaitForSingleObject),
    QEMU_HOST_REGISTER_METHOD(NtSuspendProcess),
    QEMU_HOST_REGISTER_METHOD(NtSuspendThread),
    QEMU_HOST_REGISTER_METHOD(NtSystemDebugControl),
    QEMU_HOST_REGISTER_METHOD(NtTerminateJobObject),
    QEMU_HOST_REGISTER_METHOD(NtTestAlert),
    QEMU_HOST_REGISTER_METHOD(NtTraceControl),
    QEMU_HOST_REGISTER_METHOD(NtUnloadDriver),
    QEMU_HOST_REGISTER_METHOD(NtUnloadKey),
    QEMU_HOST_REGISTER_METHOD(NtUnlockFile),
    QEMU_HOST_REGISTER_METHOD(NtUnlockVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtUnmapViewOfSection),
    QEMU_HOST_REGISTER_METHOD(NtUnmapViewOfSectionEx),
    QEMU_HOST_REGISTER_METHOD(NtWaitForAlertByThreadId),
    QEMU_HOST_REGISTER_METHOD(NtWaitForDebugEvent),
    QEMU_HOST_REGISTER_METHOD(NtWaitForKeyedEvent),
    QEMU_HOST_REGISTER_METHOD(NtWaitForMultipleObjects),
    QEMU_HOST_REGISTER_METHOD(NtWaitForSingleObject),
#ifndef _WIN64
    QEMU_HOST_REGISTER_METHOD(NtWow64AllocateVirtualMemory64),
    QEMU_HOST_REGISTER_METHOD(NtWow64GetNativeSystemInformation),
    QEMU_HOST_REGISTER_METHOD(NtWow64ReadVirtualMemory64),
    QEMU_HOST_REGISTER_METHOD(NtWow64WriteVirtualMemory64),
#endif
    QEMU_HOST_REGISTER_METHOD(NtWriteFile),
    QEMU_HOST_REGISTER_METHOD(NtWriteFileGather),
    QEMU_HOST_REGISTER_METHOD(NtWriteVirtualMemory),
    QEMU_HOST_REGISTER_METHOD(NtYieldExecution),
    QEMU_HOST_REGISTER_METHOD(__qemu_host_dispatcher),
    QEMU_HOST_REGISTER_METHOD(__qemu_host_init),
    QEMU_HOST_REGISTER_METHOD(__qemu_switch_host_state),
    QEMU_HOST_REGISTER_METHOD(__wine_dbg_header),
    QEMU_HOST_REGISTER_METHOD(__wine_dbg_output),
    QEMU_HOST_REGISTER_METHOD(__wine_dbg_strdup),
    QEMU_HOST_REGISTER_METHOD(__wine_unix_call),
    QEMU_HOST_REGISTER_METHOD(get_thread_context),
    QEMU_HOST_REGISTER_METHOD(process_close_server_socket),
    QEMU_HOST_REGISTER_METHOD(send_debug_event),
    QEMU_HOST_REGISTER_METHOD(server_init_thread),
    QEMU_HOST_REGISTER_METHOD(server_pipe),
    QEMU_HOST_REGISTER_METHOD(server_select),
    QEMU_HOST_REGISTER_METHOD(set_thread_context),
    QEMU_HOST_REGISTER_METHOD(virtual_free_teb),
    QEMU_HOST_REGISTER_METHOD(virtual_handle_fault),
    QEMU_HOST_REGISTER_METHOD(virtual_is_valid_code_address),
    QEMU_HOST_REGISTER_METHOD(virtual_setup_exception),
    QEMU_HOST_REGISTER_METHOD(virtual_uninterrupted_read_memory),
    QEMU_HOST_REGISTER_METHOD(virtual_uninterrupted_write_memory),
    QEMU_HOST_REGISTER_METHOD(wait_suspend),
    QEMU_HOST_REGISTER_METHOD(wine_nt_to_unix_file_name),
    QEMU_HOST_REGISTER_METHOD(wine_server_call),
    QEMU_HOST_REGISTER_METHOD(wine_server_send_fd),
    QEMU_HOST_REGISTER_METHOD(wine_unix_to_nt_file_name),

    QEMU_HOST_REGISTER_VARIABLE(cpu_info),
    QEMU_HOST_REGISTER_VARIABLE(main_image_info),
    QEMU_HOST_REGISTER_VARIABLE(nb_threads),
    QEMU_HOST_REGISTER_VARIABLE(process_exiting),
    QEMU_HOST_REGISTER_VARIABLE(server_block_set),
    QEMU_HOST_REGISTER_VARIABLE(server_start_time),
    QEMU_HOST_REGISTER_VARIABLE(wow_peb),
};
