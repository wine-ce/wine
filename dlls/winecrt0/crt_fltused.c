/*
 * Copyright 2020 Jacek Caban for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifdef __WINE_PE_BUILD

/* referenced by MSVC to pull crt support for floating points. we don't use it. */
int _fltused = 0;

/*
 * builtin function for arm
 *
 * Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
 * See https://llvm.org/LICENSE.txt for license information.
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
 *
 */

#if defined(__arm__)

#include <float.h>
#include <limits.h>

typedef unsigned long long du_int;
typedef long long di_int;
typedef unsigned int su_int;
typedef int si_int;

#define __fixdfdi       __dtoi64
#define __fixunsdfdi    __dtou64
#define __floatdidf     __i64tod
#define __floatundidf   __u64tod
#define __floatdisf     __i64tos
#define __fixsfdi       __stoi64
#define __fixunssfdi    __stou64
#define __floatundisf   __u64tos


typedef union {
    du_int all;
    struct {
        su_int low;
        su_int high;
    } s;
} udwords;

typedef union {
  su_int u;
  float f;
} float_bits;

static __attribute__((used)) du_int __udivmoddi4(du_int b, du_int a, du_int* rem) {
    const unsigned n_uword_bits = sizeof(su_int) * CHAR_BIT;
    const unsigned n_udword_bits = sizeof(du_int) * CHAR_BIT;
    udwords n;
    n.all = a;
    udwords d;
    d.all = b;
    udwords q;
    udwords r;
    unsigned sr;
    // special cases, X is unknown, K != 0
    if (n.s.high == 0) {
        if (d.s.high == 0) {
            // 0 X
            // ---
            // 0 X
            if (rem)
                *rem = n.s.low % d.s.low;
            return n.s.low / d.s.low;
        }
        // 0 X
        // ---
        // K X
        if (rem)
            *rem = n.s.low;
        return 0;
    }
    // n.s.high != 0
    if (d.s.low == 0) {
        if (d.s.high == 0) {
            // K X
            // ---
            // 0 0
            if (rem)
                *rem = n.s.high % d.s.low;
            return n.s.high / d.s.low;
        }
        // d.s.high != 0
        if (n.s.low == 0) {
            // K 0
            // ---
            // K 0
            if (rem) {
                r.s.high = n.s.high % d.s.high;
                r.s.low = 0;
                *rem = r.all;
            }
            return n.s.high / d.s.high;
        }
        // K K
        // ---
        // K 0
        if ((d.s.high & (d.s.high - 1)) == 0) /* if d is a power of 2 */ {
            if (rem) {
                r.s.low = n.s.low;
                r.s.high = n.s.high & (d.s.high - 1);
                *rem = r.all;
            }
            return n.s.high >> __builtin_ctz(d.s.high);
        }
        // K K
        // ---
        // K 0
        sr = __builtin_clz(d.s.high) - __builtin_clz(n.s.high);
        // 0 <= sr <= n_uword_bits - 2 or sr large
        if (sr > n_uword_bits - 2) {
            if (rem)
                *rem = n.all;
            return 0;
        }
        ++sr;
        // 1 <= sr <= n_uword_bits - 1
        // q.all = n.all << (n_udword_bits - sr);
        q.s.low = 0;
        q.s.high = n.s.low << (n_uword_bits - sr);
        // r.all = n.all >> sr;
        r.s.high = n.s.high >> sr;
        r.s.low = (n.s.high << (n_uword_bits - sr)) | (n.s.low >> sr);
    }
    else /* d.s.low != 0 */ {
        if (d.s.high == 0) {
            // K X
            // ---
            // 0 K
            if ((d.s.low & (d.s.low - 1)) == 0) /* if d is a power of 2 */ {
                if (rem)
                    *rem = n.s.low & (d.s.low - 1);
                if (d.s.low == 1)
                    return n.all;
                sr = __builtin_ctz(d.s.low);
                q.s.high = n.s.high >> sr;
                q.s.low = (n.s.high << (n_uword_bits - sr)) | (n.s.low >> sr);
                return q.all;
            }
            // K X
            // ---
            // 0 K
            sr = 1 + n_uword_bits + __builtin_clz(d.s.low) - __builtin_clz(n.s.high);
            // 2 <= sr <= n_udword_bits - 1
            // q.all = n.all << (n_udword_bits - sr);
            // r.all = n.all >> sr;
            if (sr == n_uword_bits) {
                q.s.low = 0;
                q.s.high = n.s.low;
                r.s.high = 0;
                r.s.low = n.s.high;
            }
            else if (sr < n_uword_bits) /* 2 <= sr <= n_uword_bits - 1 */ {
                q.s.low = 0;
                q.s.high = n.s.low << (n_uword_bits - sr);
                r.s.high = n.s.high >> sr;
                r.s.low = (n.s.high << (n_uword_bits - sr)) | (n.s.low >> sr);
            }
            else /* n_uword_bits + 1 <= sr <= n_udword_bits - 1 */ {
                q.s.low = n.s.low << (n_udword_bits - sr);
                q.s.high = (n.s.high << (n_udword_bits - sr)) |
                    (n.s.low >> (sr - n_uword_bits));
                r.s.high = 0;
                r.s.low = n.s.high >> (sr - n_uword_bits);
            }
        }
        else {
            // K X
            // ---
            // K K
            sr = __builtin_clz(d.s.high) - __builtin_clz(n.s.high);
            // 0 <= sr <= n_uword_bits - 1 or sr large
            if (sr > n_uword_bits - 1) {
                if (rem)
                    *rem = n.all;
                return 0;
            }
            ++sr;
            // 1 <= sr <= n_uword_bits
            // q.all = n.all << (n_udword_bits - sr);
            q.s.low = 0;
            if (sr == n_uword_bits) {
                q.s.high = n.s.low;
                r.s.high = 0;
                r.s.low = n.s.high;
            }
            else {
                q.s.high = n.s.low << (n_uword_bits - sr);
                r.s.high = n.s.high >> sr;
                r.s.low = (n.s.high << (n_uword_bits - sr)) | (n.s.low >> sr);
            }
        }
    }
    // Not a special case
    // q and r are initialized with:
    // q.all = n.all << (n_udword_bits - sr);
    // r.all = n.all >> sr;
    // 1 <= sr <= n_udword_bits - 1
    su_int carry = 0;
    for (; sr > 0; --sr) {
        // r:q = ((r:q)  << 1) | carry
        r.s.high = (r.s.high << 1) | (r.s.low >> (n_uword_bits - 1));
        r.s.low = (r.s.low << 1) | (q.s.high >> (n_uword_bits - 1));
        q.s.high = (q.s.high << 1) | (q.s.low >> (n_uword_bits - 1));
        q.s.low = (q.s.low << 1) | carry;
        // carry = 0;
        // if (r.all >= d.all)
        // {
        //      r.all -= d.all;
        //      carry = 1;
        // }
        const di_int s = (di_int)(d.all - r.all - 1) >> (n_udword_bits - 1);
        carry = s & 1;
        r.all -= d.all & s;
    }
    q.all = (q.all << 1) | carry;
    if (rem)
        *rem = r.all;
    return q.all;
}

static di_int __divdi3(di_int b, di_int a)
{
    const int bits_in_dword_m1 = (int)(sizeof(di_int) * CHAR_BIT) - 1;
    di_int s_a = a >> bits_in_dword_m1;                   // s_a = a < 0 ? -1 : 0
    di_int s_b = b >> bits_in_dword_m1;                   // s_b = b < 0 ? -1 : 0
    a = (a ^ s_a) - s_a;                                  // negate if s_a == -1
    b = (b ^ s_b) - s_b;                                  // negate if s_b == -1
    s_a ^= s_b;                                           // sign of quotient
    return (__udivmoddi4(b, a, (du_int*)0) ^ s_a) - s_a; // negate if s_a == -1
}

static __attribute__((used)) di_int __divmoddi4(di_int b, di_int a, di_int* rem)
{
    di_int d = __divdi3(b, a);
    *rem = a - (d * b);
    return d;
}

du_int __fixunsdfdi(double a)
{
    if (a <= 0.0)
        return 0;
    su_int high = a / 4294967296.f;               // a / 0x1p32f;
    su_int low = a - (double)high * 4294967296.f; // high * 0x1p32f;
    return ((du_int)high << 32) | low;
}

di_int __fixdfdi(double a) {
    if (a < 0.0) {
        return -__fixunsdfdi(-a);
    }
    return __fixunsdfdi(a);
}

double __floatdidf(di_int a) {
    static const double twop52 = 4503599627370496.0; // 0x1.0p52
    static const double twop32 = 4294967296.0;       // 0x1.0p32

    union {
        di_int x;
        double d;
    } low = { .d = twop52 };

    const double high = (si_int)(a >> 32) * twop32;
    low.x |= a & 0x00000000ffffffffLL;

    const double result = (high - twop52) + low.d;
    return result;
}

double __floatundidf(du_int a) {
    static const double twop52 = 4503599627370496.0;           // 0x1.0p52
    static const double twop84 = 19342813113834066795298816.0; // 0x1.0p84
    static const double twop84_plus_twop52 =
        19342813118337666422669312.0; // 0x1.00000001p84

    union {
        du_int x;
        double d;
    } high = { .d = twop84 };
    union {
        du_int x;
        double d;
    } low = { .d = twop52 };

    high.x |= a >> 32;

    low.x |= a & 0x00000000ffffffffULL;

    const double result = (high.d - twop84_plus_twop52) + low.d;
    return result;
}

float __floatdisf(di_int a) {
  if (a == 0)
    return 0.0F;
  const unsigned N = sizeof(di_int) * CHAR_BIT;
  const di_int s = a >> (N - 1);
  a = (a ^ s) - s;
  int sd = N - __builtin_clzll(a); // number of significant digits
  int e = sd - 1;                  // exponent
  if (sd > FLT_MANT_DIG) {
    //  start:  0000000000000000000001xxxxxxxxxxxxxxxxxxxxxxPQxxxxxxxxxxxxxxxxxx
    //  finish: 000000000000000000000000000000000000001xxxxxxxxxxxxxxxxxxxxxxPQR
    //                                                12345678901234567890123456
    //  1 = msb 1 bit
    //  P = bit FLT_MANT_DIG-1 bits to the right of 1
    //  Q = bit FLT_MANT_DIG bits to the right of 1
    //  R = "or" of all bits to the right of Q
    switch (sd) {
    case FLT_MANT_DIG + 1:
      a <<= 1;
      break;
    case FLT_MANT_DIG + 2:
      break;
    default:
      a = ((du_int)a >> (sd - (FLT_MANT_DIG + 2))) |
          ((a & ((du_int)(-1) >> ((N + FLT_MANT_DIG + 2) - sd))) != 0);
    };
    // finish:
    a |= (a & 4) != 0; // Or P into R
    ++a;               // round - this step may add a significant bit
    a >>= 2;           // dump Q and R
    // a is now rounded to FLT_MANT_DIG or FLT_MANT_DIG+1 bits
    if (a & ((du_int)1 << FLT_MANT_DIG)) {
      a >>= 1;
      ++e;
    }
    // a is now rounded to FLT_MANT_DIG bits
  } else {
    a <<= (FLT_MANT_DIG - sd);
    // a is now rounded to FLT_MANT_DIG bits
  }
  float_bits fb;
  fb.u = ((su_int)s & 0x80000000) | // sign
         ((e + 127) << 23) |        // exponent
         ((su_int)a & 0x007FFFFF);  // mantissa
  return fb.f;
}

float __floatundisf(du_int a) {
  if (a == 0)
    return 0.0F;
  const unsigned N = sizeof(du_int) * CHAR_BIT;
  int sd = N - __builtin_clzll(a); // number of significant digits
  int e = sd - 1;                  // 8 exponent
  if (sd > FLT_MANT_DIG) {
    //  start:  0000000000000000000001xxxxxxxxxxxxxxxxxxxxxxPQxxxxxxxxxxxxxxxxxx
    //  finish: 000000000000000000000000000000000000001xxxxxxxxxxxxxxxxxxxxxxPQR
    //                                                12345678901234567890123456
    //  1 = msb 1 bit
    //  P = bit FLT_MANT_DIG-1 bits to the right of 1
    //  Q = bit FLT_MANT_DIG bits to the right of 1
    //  R = "or" of all bits to the right of Q
    switch (sd) {
    case FLT_MANT_DIG + 1:
      a <<= 1;
      break;
    case FLT_MANT_DIG + 2:
      break;
    default:
      a = (a >> (sd - (FLT_MANT_DIG + 2))) |
          ((a & ((du_int)(-1) >> ((N + FLT_MANT_DIG + 2) - sd))) != 0);
    };
    // finish:
    a |= (a & 4) != 0; // Or P into R
    ++a;               // round - this step may add a significant bit
    a >>= 2;           // dump Q and R
    // a is now rounded to FLT_MANT_DIG or FLT_MANT_DIG+1 bits
    if (a & ((du_int)1 << FLT_MANT_DIG)) {
      a >>= 1;
      ++e;
    }
    // a is now rounded to FLT_MANT_DIG bits
  } else {
    a <<= (FLT_MANT_DIG - sd);
    // a is now rounded to FLT_MANT_DIG bits
  }
  float_bits fb;
  fb.u = ((e + 127) << 23) |       // exponent
         ((su_int)a & 0x007FFFFF); // mantissa
  return fb.f;
}

du_int __fixunssfdi(float a) {
  if (a <= 0.0f)
    return 0;
  double da = a;
  su_int high = da / 4294967296.f;               // da / 0x1p32f;
  su_int low = da - (double)high * 4294967296.f; // high * 0x1p32f;
  return ((du_int)high << 32) | low;
}

di_int __fixsfdi(float a) {
  if (a < 0.0f) {
    return -__fixunssfdi(-a);
  }
  return __fixunssfdi(a);
}

asm(".globl	__rt_udiv64         \n\t"
    "__rt_udiv64:               \n\t"
    "push.w	{ lr }              \n\t"
    "sub    sp, #12             \n\t"
    "add.w  r12, sp, #4         \n\t"
    "str.w  r12, [sp]           \n\t"
	"bl	__udivmoddi4            \n\t"
	"ldrd	r2, r3, [sp, #4]    \n\t"
	"add	sp, #12             \n\t"
	"pop.w	{ pc }              \n\t");

asm(".globl	__rt_sdiv64         \n\t"
    "__rt_sdiv64:               \n\t"
    "push.w	{ lr }              \n\t"
    "sub    sp, #12             \n\t"
    "add.w  r12, sp, #4         \n\t"
    "str.w  r12, [sp]           \n\t"
	"bl	__divmoddi4             \n\t"
	"ldrd	r2, r3, [sp, #4]    \n\t"
	"add	sp, #12             \n\t"
	"pop.w	{ pc }              \n\t");

#endif

#endif
